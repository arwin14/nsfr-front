<?php

// class Model extends CI_Model {
	
// 	public function __construct() {
// 		parent::__construct();
// 		$this->load->model('UserAccount');
// 	}
	
// 	public function UserAccount() {
// 		return $this->UserAccount;
// 	}
	
// }

class Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('UserAccount');
		$this->load->model('ModuleRels');
	}
	
	/*
	*
	* Reflects the UserAccount class and its values
	*
	*/
	public function UserAccount() {
		return $this->UserAccount;
	}
	
	/*
	*
	* Reflects the ModuleRels class and its values
	*
	*/
	public function ModuleRels() {
		return $this->ModuleRels;
	}
}

?>