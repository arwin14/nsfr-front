<?php
session_name($GLOBALS['project']);
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
date_default_timezone_set('UTC');
class Helper extends ModelResponse {
	
	private $title = null;
	private $view = null;
	private $viewjs = null;
	private $template = null;
	
	const SESSION_STARTED = TRUE;
	const SESSION_ENDED = FALSE;
	public static function instance() {
		return $CI =& get_instance();
	}
	public function setTitle($title) {
		$this->title = $title;
	}
	
	public function getTitle() {
		return $this->title;
	}
	
	public function setView($view, $data = "", $bool = FALSE) {
		ob_start();
		$this->load->view($view.'.php', $data);		
		$this->view = ob_get_contents();
		ob_end_clean();
		
		if($bool) {
			ob_start();
			$viewjs = $view.'.js.php';
			$this->load->view($viewjs);
			$this->viewjs = ob_get_contents();
			ob_end_clean();
		}		
	}
	
	public function getVIew() {
		return $this->view . $this->viewjs;
	}
	public function setMenu($menu) {
		$m = Helper::instance()->load->view($menu,'',TRUE);
		Helper::$menu = $m;
	}
	
	public function setTemplate($template) {
		if(self::getView() != null) {
			$content['title'] = self::getTitle();
			$content['content'] = self::getVIew();
			$this->content = $content;
			$this->load->view($template, $content);
		} else {
			show_error("Please set the content first before setting the template");
		}		
	}
	
	private static function setSessionInstance($bool) {
		if($bool == TRUE) {
			$_SESSION["sessionState"] = self::SESSION_STARTED;
		} else {
			$_SESSION["sessionState"] = self::SESSION_ENDED;
		}
	}	
	
	private static function getSessionInstance() {
		return isset($_SESSION["sessionState"]) &&
							$_SESSION["sessionState"] == TRUE ?
								self::SESSION_STARTED : 
								self::SESSION_ENDED;
	}
	
	public function sessionStart() {
		self::setSessionInstance(TRUE);
		redirect();
	}

	public static function sessionStartSetTrue() {
		self::setSessionInstance(TRUE);
	}
	
	public function sessionTerminate() {
		self::setSessionInstance(FALSE);
		session_destroy();
		redirect();
	}
	
	public static function sessionStartedHook($indexpage) {
		if(self::getSessionInstance()) redirect($indexpage);
	}
	
	public static function sessionEndedHook($loginpage) {
		if(!self::getSessionInstance()) {
			redirect($loginpage);
		}
	}
	
	private function setSessionLock($bool) {
		if($bool == TRUE) {
			$_SESSION["sessionLock"] = TRUE;
		} else {
			$_SESSION["sessionLock"] = FALSE;
		}
	}
	
	private function getSessionLock() {
		return isset($_SESSION["sessionLock"]) &&
		$_SESSION["sessionLock"] == TRUE ?
	   TRUE : FALSE;
	}
	
	public function sessionLockHook($lockpage) {
		if(self::getSessionLock()) redirect($lockpage);
	}
	
	public function sessionUnlockHook($homepage) {
		if(!self::getSessionLock()) redirect($homepage);
	}
	
	public function sessionLock() {
		self::setSessionLock(TRUE);
		redirect();
	}
	
	public function sessionUnlock() {
		self::setSessionLock(FALSE);
	}
	
	public function put($key, $val) {
		$_SESSION[$key] = $val;
	}
	
	public static function get($key = "") {
		if(isset($_SESSION[$key])) {
			return $_SESSION[$key];
		} else {
			return "test";
		}
	}
	
	public static function Model() {
		return Helper::instance()->Model;
	}
	
	protected function getToken() {
		$salt = $GLOBALS['project'];
		$tokenstr = $salt .":". date('mdY') .":". $_SERVER['HTTP_HOST'];
		$token = md5($tokenstr);
		return $token;
	}
	
	// public function role($userrole) {
	// 	if(in_array($userrole,Helper::Model()->UserAccount()->getModules())) return true;
	// 	return false;
	// }

	public static function role($userrole) {
		if(in_array($userrole,Helper::Model()->UserAccount()->getModules())) return true;
		return false;
	}
	
	public function rolehook($userrole) {
		if(!self::role($userrole)) {
			redirect();
			return true;
		}
		return false;
	}
	
	public function serviceCallBudget($data, $url) {


       $ch = curl_init();  
            //$data = array("data"=>$data);  
            curl_setopt($ch, CURLOPT_URL, $url);  
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));  
            curl_setopt($ch, CURLOPT_POST, count($data));    
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
            curl_setopt($ch, CURLOPT_TIMEOUT, 3000);  
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
            $ret = json_decode(curl_exec($ch));  
            if(!curl_error($ch)) return $ret;
            //var_dump($ret);die();
            //else return 'failed';  
            curl_close($ch);
        }


        public function serviceCall($data, $url) {


          $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);  
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));  
        curl_setopt($ch, CURLOPT_POST, count($data));    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt($ch, CURLOPT_TIMEOUT, 3000);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));  
        $ret = json_decode(curl_exec($ch));  
        if(!curl_error($ch)) return $ret;
        curl_close($ch);
	}

	
public static function serviceGet($url) { 
//$urls = "http://localhost:48082/ACPCAccountingAPI/BUDGETService/ORS/API/v1/getDataBAF306B/date/2018-04-01";         
  $ch = curl_init();      
  curl_setopt($ch, CURLOPT_URL, $url);    
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET'); 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
  $ret  = curl_exec($ch);
  $result  = json_encode($ret);
  //var_dump($ret); die();       
   return $ret;
		curl_close($ch);
	
}
	
}

class ModelResponse extends CI_Model {
	
	private $code = null;
	private $message = null;
	private $DATA = array();
	private $ret = null;
	
	public function __construct($code = "", $message = "", $data = array()) {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Model');
		
		$this->code = $code;
		$this->message = $message;
		$this->DATA = $data;
		if((!empty($data)) || (sizeof($data) > 0)) {
			$this->ret = json_encode(array("Code"=>$this->code, "Message"=>$this->message, "Data"=>$this->DATA));
		} else {
			$this->ret = json_encode(array("Code"=>$this->code, "Message"=>$this->message));
		}
		return $this->ret;
	}
	
	public function busy() {
		$this->code = 99;
		$this->message = "System is busy. Please try again later.";
		$this->ret = json_encode(array("Code"=>$this->code,"Message"=>$this->message));
	}
	
	public function ModelResponse($code = "", $message = "", $data = array()) {
		$this->code = $code;
		$this->message = $message;
		$this->DATA = $data;
		if((!empty($data)) || (sizeof($data) > 0)) {
			$this->ret = json_encode(array("Code"=>$this->code, "Message"=>$this->message, "Data"=>$this->DATA));
		} else {
			$this->ret = json_encode(array("Code"=>$this->code, "Message"=>$this->message));
		}
	}
	
	public function setCode($val) {
		$this->code = $val;
	}
	
	public function getCode() {
		return $this->code;
	}
	
	public function setMessage($val) {
		$this->message = $val;
	}
	
	public function getMessage() {
		return $this->message;
	}
	
	public function setData($val) {
		$this->DATA = $val;
	}
	
	public function getData() {
		return $this->DATA;
	}
	
	public function __toString() {
		return $this->ret;
	}
	
}
?>