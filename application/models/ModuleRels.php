<?php
/**
*
* WARNING: This is only intended for developers.
* Changing the values will result to many errors.
* Author: Telcom Live Content Inc.
* 
*/
class ModuleRels {
	
	// /* USER MANAGEMENT */
	// const USER_MANAGEMENT_MAIN_MENU			=			100;
	// const USER_CONFIGURATION_SUB_MENU		=			110;
	// const ADD_NEW_USER						=			111;
	// const VIEW_SPECIFIC_USER				=			112;
	// const UPDATE_USER						=			113;
	// const ACTIVATE_DEACTIVATE_USER			=			114;
	// const RESTART_SESSION					=			115;
	// const UNLOCK_LOCK_USER					=			116;
	// const USER_LEVEL_CONFIGURATION_SUB_MENU	=			120;
	// const ADD_NEW_USER_LEVEL				=			121;
	// const VIEW_SPECIFIC_USER_LEVEL			=			122;
	// const UPDATE_USER_LEVEL					=			123;
	// const ACTIVATE_DEACTIVATE_USER_LEVEL	=			124;
	// const RESTART_USER_SESSION				=			125;
	// /* REPORTS */
	// const NSFR_MAIN_MENU					=			200;



	//DASHBOARD
	const DASHBOARD         							=      100;
	const DASHBOARD_SOLO         						=      110;
	const DASHBOARD_CONSILODATE        				    =      120;
	//NSFR
	const NSFR_MAIN_MENU       							=      200;

	const NSFR_PART1        							=      230;
	const NSFR_PART1_EXCEL  							=      231;

	const NSFR_PART2        							=      260;
	const NSFR_PART2_EXCEL  							=      261;

	const NSFR_PART3        							=      290;
	const NSFR_PART3_EXCEL  							=      291;

	//SIMULATION
	const SIMULATION        							=      300;
	const SIMULATION_IMPORT 							=      310;
	const SIMULATION_ADD								=	   311;
	const SIMULATION_DELETE								=	   312;
	const SIMULATION_UPDATE								=      313;
	const SIMULATION_STATUS								=      314;
	const SIMULATION_ADDREASON							=      315;

	//UTILITIES
	const UTILITIES_MAIN_MENU							=	   400;
	const UTILITIES_FORMMODIFICATION					=	   430; //============================================
	const UTILITIES_FORMMODIFICATION_AVAIL_FORM  		=      431;
	const UTILITIES_FORMMODIFICATION_AVAIL_UPDATE       =      432;
	const UTILITIES_FORMMODIFICATION_AVAIL_DELETE       =      433;

	const UTILITIES_CURENCY	     						=      460;
	const UTILITIES_CURENCY_ADDFORM						=      461;
	const UTILITIES_CURENCY_UPDATE						=      462;
	const UTILITIES_CURENCY_DELETE						=      463;

    
    const APPLICATION_MODIFICATION						=	   490;
	//const APPLICATION_ATTACH1_PDF						=	   490;
	const APPLICATION_ATTACH1_UPDATE					=	   491;
	const APPLICATION_ATTACH2_PDF						=	   492;
	const APPLICATION_ATTACH2_UPDATE					=	   493;

	//USERMANAGEMENT_MANAGEMENT
	const USERMANAGEMENT								=      500;

	const USERCONFIG									=      530;
	const USERCONFIG_ADD								=      531;
	const USERCONFIG_UPDATE								=	   532;
	const USERCONFIG_REMOVE								=      533;

	const USERLEVELCONFIG								=	   560;
	const USERLEVELCONFIG_ADD							=	   561;
	const USERLEVELCONFIG_UPDATE						= 	   562;
	const USERLEVELCONFIG_DELETE						=      563;


// ============================ new ModuleRels =====================================
	const ARCHIVE										=      600;
	const ARCHIVE_APPROVED								=      610;
	const ARCHIVE_PENDING								=      620;
	const ARCHIVE_RJECTED								=      630;
	const ARCHIVE_SHORTFALL								=      640;
	const ARCHIVE_SHORTFALL_UPDATE						=      641;



	const SETTINGS										=      700;
	const SETTINGS_EXTRACTIONPATH						=      710;
	const SETTINGS_EMAILCONFIGURATION					=      720;







}
?>