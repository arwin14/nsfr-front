<?php
/**
*
* WARNING: This is only intended for developers.
* Changing the values will result to many errors.
* Author: Telcom Live Content Inc.
* 
*/
class ModuleRels {
	
	/* USER MANAGEMENT */
	const USER_MANAGEMENT_MAIN_MENU			=			100;
	const USER_CONFIGURATION_SUB_MENU		=			110;
	const ADD_NEW_USER						=			111;
	const VIEW_SPECIFIC_USER				=			112;
	const UPDATE_USER						=			113;
	const ACTIVATE_DEACTIVATE_USER			=			114;
	const RESTART_SESSION					=			115;
	const UNLOCK_LOCK_USER					=			116;
	const USER_LEVEL_CONFIGURATION_SUB_MENU	=			120;
	const ADD_NEW_USER_LEVEL				=			121;
	const VIEW_SPECIFIC_USER_LEVEL			=			122;
	const UPDATE_USER_LEVEL					=			123;
	const ACTIVATE_DEACTIVATE_USER_LEVEL	=			124;
	const RESTART_USER_SESSION				=			125;
	/* REPORTS */
	const REPORTS_MAIN_MENU					=			200;
}
?>