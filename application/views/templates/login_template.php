<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/thirdparty/img/mufg-logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>MUFG | <?php echo $title; ?></title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/light-bootstrap-dashboard.css?v=1.4.1" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- Loader Css -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/loader.css" rel="stylesheet">

    <!-- Global Css -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/Global.css" rel="stylesheet">

    <!--   Core JS Files  -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>

</head>
<body>

<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->
            <a class="navbar-brand" href="../dashboard.html">Mitsubishi UFJ Financial Group</a>
        </div>
        <!-- <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li>
                   <a href="register.html">
                        Register
                    </a>
                </li>
            </ul>
        </div> -->
    </div>
</nav>


<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" data-color="orange" data-image="<?php echo base_url(); ?>assets/thirdparty/img/mufgBG2.jpg">

    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
        <div class="content">
            <?php echo $content; ?>
        </div>

        <footer class="footer footer-transparent">
            <div class="container">
                <!-- <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                               Blog
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <!-- <p class="copyright pull-left">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://mobilemoney.ph/">Telcom Live Content Inc.</a>, Innovation Beyond Expectation
                </p> -->
            </div>
        </footer>

    </div>

</div>


</body>

    <!--  Forms Validations Plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.validate.min.js"></script>

    <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/moment.min.js"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-datetimepicker.min.js"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-selectpicker.js"></script>

    <!--  Checkbox, Radio, Switch and Tags Input Plugins -->
        <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-switch-tags.min.js"></script>

    <!--  Charts Plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-notify.js"></script>

    <!-- Sweet Alert 2 plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/sweetalert2.js"></script>

    <!-- Vector Map plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery-jvectormap.js"></script>

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->

    <!-- Wizard Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.bootstrap.wizard.min.js"></script>

    <!--  Datatable Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-table.js"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/fullcalendar.min.js"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/light-bootstrap-dashboard.js?v=1.4.1"></script>

    <!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
    <!-- <script src="<?php echo base_url(); ?>assets/thirdparty/js/demo.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/dvajs.js"></script>

    <script type="text/javascript">
        $().ready(function(){
            lbd.checkFullPageBackgroundImage();

            setTimeout(function(){
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 700)
        });
    </script>

</html>
