<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/thirdparty/img/mufg-logo.png">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <title>MUFG | <?php echo $title; ?></title>
      <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
      <meta name="viewport" content="width=device-width" />
      <!-- Bootstrap core CSS     -->
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/bootstrap.min.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/myanimate.css" rel="stylesheet" />
      <!--  Light Bootstrap Dashboard core CSS    -->
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/light-bootstrap-dashboard.css?v=1.4.1" rel="stylesheet"/>
      <!--  CSS for Demo Purpose, don't include it in your project     -->
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
      <!-- <link href="<?php echo base_url(); ?>assets/thirdparty/css/font-awesomes.min.css" rel='stylesheet'> -->
      <!-- <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'> -->
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/fonts.css"  rel='stylesheet' type='text/css'> 
      
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/select2.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/pe-icon-7-stroke.css" rel="stylesheet" />
      <!-- Loader Css -->
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/loader.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/jquery-confirm.css" rel="stylesheet">

      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/chartist-plugin-legend-master/chartist-plugin-legend.css" />
      <!-- Global Css -->
      <link href="<?php echo base_url(); ?>assets/thirdparty/css/Global.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/css/myloader.css" />
      <!--   Core JS Files  -->
      <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery-confirm.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/thirdparty/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/local/module/js/addFunction/budget.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/local/module/js/FormFunction/budget.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/thirdparty/navbar/budget.js" type="text/javascript"></script>

      <!-- <link rel="stylesheet" href="http://www.orangehilldev.com/jstree-bootstrap-theme/demo/assets/dist/themes/proton/style.css" /> -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/treeview/css/style1.css" />

      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.9/themes/default/style.min.css" />
     <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/treeview/css/style2.css" /> -->

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /> -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/thirdparty/datepicker/css/datepicker.css" />

     <!--  <script src="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.9/jstree.min.js"></script> -->
     <script src="<?php echo base_url(); ?>assets/thirdparty/treeview/js/jstree.js"></script>

      <!--  Google Maps Plugin   
      <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyA-aQnT8sfwa810Nd9sL0P9hx8k0uQOsMQ&libraries=places"></script> -->
   </head>
   <body>
      <div class="page-loader-wrapper">
         <div class="loader">
            <div class="preloader pl-size-xl">
               <div class="spinner-layer pl-indigo">
                  <div class="circle-clipper left">
                     <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                     <div class="circle"></div>
                  </div>
               </div>
            </div>
            <p>Please wait...</p>
         </div>
      </div>
      <div class="wrapper">
         <div class="sidebar" data-color="orange" data-image="<?php echo base_url(); ?>assets/thirdparty/img/full-screen-image-3.jpg">
            <!--
               Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
               Tip 2: you can also add an image using data-image tag
               -->
            <div class="logo">
               <!--    <a href="http://www.nwrb.gov.ph/" target="_blank" class="simple-text logo-mini">
                  N
                  </a>
                  
                  <a href="http://www.nwrb.gov.ph/" target="_blank" class="simple-text logo-normal">
                  MUFG
                  
                  </a> -->
               <center>
                  <img src="<?php echo base_url(); ?>assets/thirdparty/img/mufg-logo.png" / style="width:20%;">
               </center>
            </div>
            <div class="sidebar-wrapper">
               <div class="user">
                  <div class="info">
                     <div class="photo">
                        <img src="<?php echo base_url(); ?>assets/thirdparty/img/faces/face-9.jpg" />
                     </div>
                     <a data-toggle="collapse" href="#MyCollapse" class="collapsed">
                     <span style="text-transform: uppercase;">
                     <?php echo Helper::get("username"); ?>
                     <b class="caret"></b>
                     </span>
                     </a>
                     <div class="collapse" id="MyCollapse">
                        <ul class="nav">
                           <li>
                              <a href="<?php echo base_url(); ?>session/logout">
                              <span class="sidebar-mini">S</span>
                              <span class="sidebar-normal">Sign Off</span>
                              </a>
                           </li>
                           <li>
                              <a href="<?php echo base_url(); ?>usermanagement/UserAccountConfig">
                              <span class="sidebar-mini">MF</span>
                              <span class="sidebar-normal">My Profile</span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <ul class="nav">
                <?php
                if(Helper::role(ModuleRels::DASHBOARD) && (Helper::role(ModuleRels::DASHBOARD_SOLO) || Helper::role(ModuleRels::DASHBOARD_SOLO))):
                ?>
                  <li>
                     <a data-toggle="collapse" href="#dashboardmenu">
                        <i class="pe-7s-display1"></i>
                        <p>Dashboard
                           <b class="caret"></b>
                        </p>
                     </a>
                     <div class="collapse" id="dashboardmenu">
                        <ul class="nav">
                        <?php if(Helper::role(ModuleRels::DASHBOARD_SOLO)): ?>
                           <li>
                              <a href="<?php echo base_url(); ?>Dashboard/Dashboard/Solo">
                              <span class="sidebar-mini">SD</span>
                              <span class="sidebar-normal">Solo</span>
                              </a>
                           </li>
                           <?php  endif;
                            if(Helper::role(ModuleRels::DASHBOARD_CONSILODATE)):
                           ?>
                           <li>
                              <a href="<?php echo base_url(); ?>Dashboard/Dashboard/Consolidated">
                              <span class="sidebar-mini">CD</span>
                              <span class="sidebar-normal">Consolidated</span>
                              </a>
                           </li>
                           <?php  endif; ?>
                        </ul>
                     </div>
                  </li>
                 <?php  endif; 
                 if(Helper::role(ModuleRels::NSFR_MAIN_MENU) && (Helper::role(ModuleRels::NSFR_PART1) || Helper::role(ModuleRels::NSFR_PART2) || Helper::role(ModuleRels::NSFR_PART3))):
                 ?>
                  <li>
                     <a data-toggle="collapse" href="#nsfrmenu">
                        <i class="pe-7s-news-paper"></i>
                        <p>NSFR
                           <b class="caret"></b>
                        </p>
                     </a>
                     <div class="collapse" id="nsfrmenu">
                        <ul class="nav">
                            <?php if(Helper::role(ModuleRels::NSFR_PART1)): ?>
                           <li>
                              <a href="<?php echo base_url(); ?>genFundBAF/UserConfig">
                              <span class="sidebar-mini">P1</span>
                              <span class="sidebar-normal">Part I</span>
                              </a>
                           </li>
                           <?php 
                           endif;
                           if(Helper::role(ModuleRels::NSFR_PART2)): 
                            ?>
                           <li>
                              <a href="<?php echo base_url(); ?>genFundAvail/AvailConfig">
                              <span class="sidebar-mini">P2</span>
                              <span class="sidebar-normal">Part II</span>
                              </a>
                           </li>
                           <?php 
                           endif;
                           if(Helper::role(ModuleRels::NSFR_PART3)): 
                            ?>
                           <li>
                              <a href="<?php echo base_url(); ?>genFundRequired/UserConfig">
                              <span class="sidebar-mini">P3</span>
                              <span class="sidebar-normal">Part III</span>
                              </a>
                           </li>
                           <?php 
                           endif; 
                            ?>
                        </ul>
                     </div>
                  </li>
                   <?php  
                    endif;
                    if(Helper::role(ModuleRels::SIMULATION)):  
                   ?>
                  <li>
                     <a href="<?php echo base_url(); ?>budgetAllotmentSB/AllotmentConfig">
                        <i class="pe-7s-box2"></i>
                        <p>Simulation</p>
                     </a>
                  </li>
                   <?php  
                    endif;
                    //if(Helper::role(ModuleRels::SIMULATION)):  
                   ?>
                 
                 
             

                   <?php 
                           //endif;
                           //if(Helper::role(ModuleRels::ARCHIVE)):
                    ?>
                     <li>
                     <a data-toggle="collapse" href="#forms">
                        <i class="pe-7s-add-user"></i>
                        <p>WorkSheet Management
                           <b class="caret"></b>
                        </p>
                     </a>

                     <div class="collapse" id="forms">
                        <ul class="nav">
                          <?php if(Helper::role(ModuleRels::UTILITIES_FORMMODIFICATION)): ?>
                            <li>
                              <a href="<?php echo base_url(); ?>appMode/AppmodConfig">
                              <span class="sidebar-mini">RD</span>
                              <span class="sidebar-normal">Reference Data</span>
                              </a>
                           </li>
                        <?php 
                           endif;
                           if(Helper::role(ModuleRels::UTILITIES_CURENCY)): 
                            ?>
                          <li>
                              <a href="<?php echo base_url(); ?>appForm/AppformConfig">
                              <span class="sidebar-mini">AP</span>
                              <span class="sidebar-normal">Application Forms</span>
                              </a>
                           </li>
                           <?php 
                            endif;      
                           ?>
                           <li>
                              <a href="<?php echo base_url(); ?>genFundUtilities/UserConfig">
                              <span class="sidebar-mini">FM</span>
                              <span class="sidebar-normal">Form Modification</span>
                              </a>
                           </li>

                        </ul>
                     </div>
                  </li>



                     <?php  //endif; 
                 // if(Helper::role(ModuleRels::UTILITIES_MAIN_MENU) && (Helper::role(ModuleRels::UTILITIES_FORMMODIFICATION) || Helper::role(ModuleRels::UTILITIES_CURENCY_ADDFORM) || Helper::role(ModuleRels::UTILITIES_CURENCY))):
                  ?>
                  <li>
                     <a data-toggle="collapse" href="#utilitiesmenu">
                        <i class="pe-7s-settings"></i>
                        <p>Currency Management
                           <b class="caret"></b>
                        </p>
                     </a>
                     <div class="collapse" id="utilitiesmenu">
                        <ul class="nav">
                      
                             <li>
                              <a href="<?php echo base_url(); ?>currency/CurrencylistConfig">
                              <span class="sidebar-mini">CM</span>
                              <span class="sidebar-normal">Currency List</span>
                              </a>
                           </li>
                        
                        <?php 
                             if(Helper::role(ModuleRels::UTILITIES_CURENCY)): 
                            ?>
                           <li>
                              <a href="<?php echo base_url(); ?>currency/CurrencyConfig">
                              <span class="sidebar-mini">CM</span>
                              <span class="sidebar-normal">Currency Modification</span>
                              </a>
                           </li>
                           <?php 
                           endif;
                            //if(Helper::role(ModuleRels::UTILITIES_FORMMODIFICATION)): 
                            ?>
            
                        </ul>
                     </div>
                  </li>
                        <?php  //endif; 
                 if(Helper::role(ModuleRels::USERMANAGEMENT) && (Helper::role(ModuleRels::USERCONFIG) || Helper::role(ModuleRels::USERLEVELCONFIG))):
                  ?>
                  <li>
                     <a data-toggle="collapse" href="#usermenu">
                        <i class="pe-7s-add-user"></i>
                        <p>User Management
                           <b class="caret"></b>
                        </p>
                     </a>
                     <div class="collapse" id="usermenu">
                        <ul class="nav">
                            <?php 
                           if(Helper::role(ModuleRels::USERCONFIG)): 
                            ?>
                           <li>
                              <a href="<?php echo base_url(); ?>usermanagement/UserConfig">
                              <span class="sidebar-mini">UC</span>
                              <span class="sidebar-normal">User Configuration</span>
                              </a>
                           </li>
                           <?php 
                           endif;
                           if(Helper::role(ModuleRels::USERLEVELCONFIG)): 
                            ?>
                             <li>
                              <a href="<?php echo base_url(); ?>usermanagement/UserLevelConfig">
                              <span class="sidebar-mini">LC</span>
                              <span class="sidebar-normal">User Level Configuration</span>
                              </a>
                           </li>
            
                           <?php 
                           endif;
                            ?>

                        </ul>
                     </div>
                  </li>

                    <?php 
                           endif;
                          if(Helper::role(ModuleRels::ARCHIVE_APPROVED)  || Helper::role(ModuleRels::ARCHIVE_PENDING) || Helper::role(ModuleRels::ARCHIVE_RJECTED) ||  Helper::role(ModuleRels::ARCHIVE_SHORTFALL)):
                    ?>


                  <li>
                     <a data-toggle="collapse" href="#archive">
                        <i class="pe-7s-add-user"></i>
                        <p>Archive
                           <b class="caret"></b>
                        </p>
                     </a>

                     <div class="collapse" id="archive">
                        <ul class="nav">
                          <?php
                            if(Helper::role(ModuleRels::ARCHIVE_APPROVED)):
                          ?>
                          <li>
                            <a href="<?php echo base_url(); ?>archive/ApproveConfig">
                              <span class="sidebar-mini">A</span>
                              <p>Approved</p>
                            </a>
                          </li>
                          <?php
                            endif;
                            if(Helper::role(ModuleRels::ARCHIVE_PENDING)):
                          ?>
                          <li>
                            <a href="<?php echo base_url(); ?>archive/PendingConfig">
                              <span class="sidebar-mini">P</span>
                              <p>Pending</p>
                            </a>
                          </li>
                           <?php
                            endif;
                            if(Helper::role(ModuleRels::ARCHIVE_RJECTED)):
                          ?>
                          <li>
                            <a href="<?php echo base_url(); ?>archive/RejectConfig">
                               <span class="sidebar-mini">R</span>
                              <p>Rejected</p>
                            </a>
                          </li>
                           <?php
                            endif;
                            if(Helper::role(ModuleRels::ARCHIVE_SHORTFALL)):
                          ?>
                          <li>
                            <a href="<?php echo base_url(); ?>archive/ShortfallConfig">
                              <span class="sidebar-mini">S</span>
                              <p>Shortfall</p>
                            </a>
                          </li>
                           <?php
                            endif;
                            //if(Helper::role(ModuleRels::ARCHIVE_SHORTFALL)):
                          ?>
                          <li>
                            <a href="<?php echo base_url(); ?>archive/OriginalConfig">
                              <span class="sidebar-mini">S</span>
                              <p>Original</p>
                            </a>
                          </li>
                          <?php
                        //endif;
                          ?>
                        </ul>
                     </div>
                  </li>
                   <?php 
                          endif;
                          if(Helper::role(ModuleRels::SETTINGS_EXTRACTIONPATH)  || Helper::role(ModuleRels::SETTINGS_EMAILCONFIGURATION)):
                    ?>

                  <li>
                     <a data-toggle="collapse" href="#settings">
                        <i class="pe-7s-tools"></i>
                        <p>Settings
                           <b class="caret"></b>
                        </p>
                     </a>
                    <div class="collapse" id="settings">
                        <ul class="nav">
                    <?php
                          if(Helper::role(ModuleRels::SETTINGS_EXTRACTIONPATH)):
                      ?>      
                        <li>
                         <a href="<?php echo base_url(); ?>settings/SettingsConfig">
                             <span class="sidebar-mini">EC</span>
                            <p>EXTRACTION CONFIGURATION</p>
                         </a>
                      </li>
                     <?php
                            endif;
                            if(Helper::role(ModuleRels::SETTINGS_EMAILCONFIGURATION)):
                      ?>   
                       <li>
                         <a href="<?php echo base_url(); ?>settings/EmailConfig">
                            <i class="pe-7s-tools"></i>
                            <p>EMAIL CONFIGURATION</p>
                         </a>
                      </li>
                      <?php
                    endif;
                      ?>
                        </ul>
                     </div>
                  </li>
                   <?php
                      endif;
                   ?>
























                  

                
          
               </ul>
            </div>
         </div>
         <div class="main-panel">
            <nav class="navbar navbar-default" >
               <div class="container-fluid">
                  <div class="navbar-minimize">
                     <button id="minimizeSidebar" class="btn btn-info btn-fill btn-round btn-icon">
                     <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                     <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                     </button>
                  </div>
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle" data-toggle="collapse">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                  </div>
               </div>
            </nav>
            <div class="main-content">
               <div class="container-fluid">
                  <div class="row" style="display: none">
                     <div class="col-md-12">
                        <div class="form-group">
                           <input type="text" value="<?php echo base_url(); ?>" class="form-control BASE_URL" readonly>
                        </div>
                     </div>
                  </div>
                  <!-- 
                     <style type="text/css">
                     / CHANGE COLOR HERE / 
                     ol.etapier li.done {
                     border-color: yellowgreen ;
                     }
                     / CHANGE COLOR HERE / 
                     ol.etapier li.done:before {
                     background-color: yellowgreen;
                     border-color: yellowgreen;
                     }
                     ol.etapier {
                     display: table;
                     list-style-type: none;
                     margin: 0 auto 20px auto;
                     padding: 0;
                     table-layout: fixed;
                     width: 100%;
                     }
                     ol.etapier a {
                     display: table-cell;
                     text-align: center;
                     white-space: nowrap;
                     position: relative;
                     }
                     ol.etapier a li {
                     display: block;
                     text-align: center;
                     white-space: nowrap;
                     position: relative;
                     }
                     ol.etapier li {
                     display: table-cell;
                     text-align: center;
                     padding-bottom: 10px;
                     white-space: nowrap;
                     position: relative;
                     }
                     
                     ol.etapier li a {
                     color: inherit;
                     }
                     
                     ol.etapier li {
                     color: silver; 
                     border-bottom: 4px solid silver;
                     }
                     ol.etapier li.done {
                     color: black;
                     }
                     
                     ol.etapier li:before {
                     position: absolute;
                     bottom: -11px;
                     left: 50%;
                     margin-left: -7.5px;
                     
                     color: white;
                     height: 15px;
                     width: 15px;
                     line-height: 15px;
                     border: 2px solid silver;
                     border-radius: 15px;
                     
                     }
                     ol.etapier li.done:before {
                     content: "\2713";
                     color: white;
                     }
                     ol.etapier li.todo:before {
                     content: " " ;
                     background-color: white;
                     }
                     </style>
                     <div class="content">
                     <div class="row">
                     <div class="col-md-12">
                     <ol class="etapier">
                     <li class="done">
                     <button  class="btn btn-social btn-round btn-fill btn-success">
                         <i class="pe-7s-print"></i>
                         checker
                     </button>
                     </li>
                     <li class="todo">
                     <button  class="btn btn-social btn-round btn-fill">
                         <i class="fa fa-fa-user"></i>
                     </button>
                     </li>
                     <li class="todo">
                     <button  class="btn btn-social btn-round btn-fill">
                         <i class="fa fa-fa-user"></i>
                     </button>
                     </li>
                     
                     </ol>
                     </div>
                     </div>
                     </div>
                     
                     -->
                  <?php echo $content; ?>
               </div>
            </div>
            <footer class="footer">
               <div class="container-fluid">
                  <p class="copyright pull-right">
                     &copy; <script>document.write(new Date().getFullYear())</script> <a target="_blank" href="http://mobilemoney.ph/">Telcom Live Content Inc.</a>
                  </p>
               </div>
            </footer>
         </div>
      </div>
      <div class="modal fade" id="displayModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button class="btn btn-social btn-round btn-danger pull-right" data-dismiss="modal" aria-label="Close">
                  <i class="fa fa-close"> </i>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Modal title</h4>
               </div>
               <div class="modal-body">
                  ...
               </div>
            </div>
         </div>
      </div>



       <div class="modal fade" id="secdisplayModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content"><!-- 
               <div class="modal-header">
                  <button class="btn btn-social btn-round btn-danger pull-right" data-dismiss="modal" aria-label="Close">
                  <i class="fa fa-close"> </i>
                  </button>
                  <h4 class="modal-title" id="mysecModalLabel">Modal title</h4>
               </div> -->
               <div class="modal-body">
                  ...
               </div>
            </div>
         </div>
      </div>
      <!-- 
         <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <button class="btn btn-social btn-round btn-danger pull-right" data-dismiss="modal" aria-label="Close">
                             <i class="fa fa-close"> </i>
                         </button>
                         <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                     </div>
                     <div class="modal-body">
                         ...
                     </div>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-fill btn-md" data-dismiss="modal">Close</button>
                     </div>
                 </div>
             </div>
         </div> -->
   </body>
   <!--  Forms Validations Plugin -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.validate.min.js"></script>
   <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/moment.min.js"></script>
   <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script> -->
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/datepicker/js/datepicker.js"></script>
   <!--  Date Time Picker Plugin is included in this js file -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-datetimepicker.min.js"></script>
   <!--  Select Picker Plugin -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-selectpicker.js"></script>
   <!--  Checkbox, Radio, Switch and Tags Input Plugins -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-switch-tags.min.js"></script>
   <!--  Charts Plugin -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/chartist.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/thirdparty/chartist-plugin-axistitle-master/dist/chartist-plugin-axistitle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/thirdparty/chartist-plugin-legend-master/chartist-plugin-legend.js"></script>
    <script src="<?php echo base_url(); ?>assets/thirdparty/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>
   <!--  Notifications Plugin    -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-notify.js"></script>
   <!-- Sweet Alert 2 plugin -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/sweetalert2.js"></script>
   <!-- Vector Map plugin -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery-jvectormap.js"></script>
   <!--  Google Maps Plugin    -->
   <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
   <!-- Wizard Plugin    -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.bootstrap.wizard.min.js"></script>
   <!--  Bootstrap Table Plugin    -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-table.js"></script>
   <!--  Plugin for DataTables.net  -->
   <!--      <script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script> -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.datatables.js"></script>
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/dataTables.cellEdit.js"></script>
   <!--  Full Calendar Plugin    -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/fullcalendar.min.js"></script>
   <!-- Light Bootstrap Dashboard Core javascript and methods -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/light-bootstrap-dashboard.js?v=1.4.1"></script>
   <!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/demo.js"></script>
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/dvajs.js"></script>
   <!-- global js -->
   <script src="<?php echo base_url(); ?>assets/thirdparty/js/Global.js"></script>
   <script type="text/javascript">
      var url = window.location;
          var pathname = window.location.href;
          $('.nav > li > a[href="'+pathname+'"]').parent().addClass('active').parent().parent().addClass('in').parent().addClass('active');
   </script>
</html>