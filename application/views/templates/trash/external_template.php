<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/thirdparty/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>NWRB | <?php echo $title; ?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/light-bootstrap-dashboard.css?v=1.4.1" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- Loader Css -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/loader.css" rel="stylesheet">

    <!-- Global Css -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/Global.css" rel="stylesheet">

    <!--   Core JS Files  -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>

    <!-- Camera -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/camera/webcam.min.js" type="text/javascript"></script>

    <!-- Signature -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/signature/signature_pad.umd.js" type="text/javascript"></script>
</head>
<body>	
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader pl-size-xl">
            <div class="spinner-layer pl-indigo">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p class="text_loader">Please wait...</p>
    </div>
</div>
<div class="wrapper">
    <div class="sidebar" data-color="nwrb" data-image="<?php echo base_url(); ?>assets/thirdparty/img/full-screen-image-3.jpg">
        <!--
            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag
        -->
        <div class="logo">
            <a href="http://www.nwrb.gov.ph/" target="_blank" class="simple-text logo-mini">
                N
            </a>

            <a href="http://www.nwrb.gov.ph/" target="_blank" class="simple-text logo-normal">
				NWRB
			</a>
        </div>
    	<div class="sidebar-wrapper">
			<ul class="nav">
				<li class="active">
					<a href="<?php echo base_url(); ?>onlineapplication">
                        <i class="pe-7s-note2"></i>
						<p>Application</p>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>">
						<i class="pe-7s-user"></i>
						<p>Login</p>
					</a>
				</li>
			</ul>
    	</div>
    </div>
    <div class="main-panel">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-info btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
				</div>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="http://www.nwrb.gov.ph/"  target="_blank">National Water Resources Board</a>
				</div>
			</div>
		</nav>
        <div class="main-content">
            <div class="container-fluid">
   			 	<?php echo $content; ?>
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a target="_blank" href="http://mobilemoney.ph/">Telcom Live Content Inc.</a>
                </p>
            </div>
        </footer>
    </div>
</div>
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-social btn-round btn-danger pull-right" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"> </i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-fill btn-md" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="signatureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-social btn-round btn-danger pull-right" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"> </i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-fill btn-md" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-social btn-round btn-danger pull-right" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"> </i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-fill btn-md" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>

	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.validate.min.js"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/moment.min.js"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-datetimepicker.min.js"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-selectpicker.js"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-switch-tags.min.js"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-notify.js"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/sweetalert2.js"></script>

    <!-- Vector Map plugin -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery-jvectormap.js"></script>

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
    
	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.bootstrap.wizard.min.js"></script>

    <!--  Bootstrap Table Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-table.js"></script>

	<!--  Plugin for DataTables.net  -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.datatables.js"></script>


    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/fullcalendar.min.js"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/light-bootstrap-dashboard.js?v=1.4.1"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<!-- <script src="<?php echo base_url(); ?>assets/thirdparty/js/demo.js"></script> -->
	<script src="<?php echo base_url(); ?>assets/thirdparty/js/dvajs.js"></script>

    <!-- plupload -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/plupload/js/plupload/plupload.full.min.js"></script>

    <!-- global js -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/Global.js"></script>
</html>