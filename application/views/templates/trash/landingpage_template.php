<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/thirdparty/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>NWRB | <?php echo $title; ?></title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/light-bootstrap-dashboard.css?v=1.4.1" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- Loader Css -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/loader.css" rel="stylesheet">

    <!-- Global Css -->
    <link href="<?php echo base_url(); ?>assets/thirdparty/css/Global.css" rel="stylesheet">

    <!--   Core JS Files  -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
</head>
<body>  
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader pl-size-xl">
            <div class="spinner-layer pl-indigo">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                   <a href="<?php echo base_url();?>">
                        Looking to login?
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="wrapper wrapper-full-page">
    <div class="full-page register-page" data-color="nwrb" data-image="<?php echo base_url(); ?>assets/thirdparty/img/phil.jpg">
    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
        <div class="content">
            <div class="container">
                <?php echo $content; ?>
            </div>
        </div>
        <footer class="footer footer-transparent">
            <div class="container">
                <p class="copyright text-center" style="color: white">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a target="_blank" href="http://mobilemoney.ph/">Telcom Live Content Inc.</a>
                </p>
            </div>
        </footer>
    </div>
</div>
</body>

    <!--  Forms Validations Plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.validate.min.js"></script>

    <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/moment.min.js"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-datetimepicker.min.js"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-selectpicker.js"></script>

    <!--  Checkbox, Radio, Switch and Tags Input Plugins -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-switch-tags.min.js"></script>

    <!--  Charts Plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-notify.js"></script>

    <!-- Sweet Alert 2 plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/sweetalert2.js"></script>

    <!-- Vector Map plugin -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery-jvectormap.js"></script>

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
    
    <!-- Wizard Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.bootstrap.wizard.min.js"></script>

    <!--  Bootstrap Table Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap-table.js"></script>

    <!--  Plugin for DataTables.net  -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.datatables.js"></script>


    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/fullcalendar.min.js"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/light-bootstrap-dashboard.js?v=1.4.1"></script>

    <!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
    <!-- <script src="<?php echo base_url(); ?>assets/thirdparty/js/demo.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/dvajs.js"></script>

    <!-- global js -->
    <script src="<?php echo base_url(); ?>assets/thirdparty/js/Global.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){      
            lbd.checkFullPageBackgroundImage();
        });
    </script>
</html>