  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';
//$baseURL =  'http://localhost:48082/ACPCAccountingAPI/BUDGETService/';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


//===========================================================

   public function getDataUpdate(){
       $refId = $_POST['refId'];
       //var_dump($refid); die();
      $url = $GLOBALS['backend_URL']."ORS/API/v1/getSpecData/refid/".$refId;

       $ret = Helper::serviceGet($url);
     //var_dump($content); die();
       
        return $ret;

    }
    //===========================================================




   public function getOptionsData(){
       $url = $GLOBALS['backend_URL']."ORS/API/v1/getAllExpenditures/expenditures/";
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
       //var_dump($result); //die();
        //var_dump($ret); die();
        return $ret;



    }
    //===========================================================

   public function getOptionsDataAlters(){
       $date = '2018-04-01';
       $url = $GLOBALS['backend_URL']."ORS/API/v1/getAllExpenditures/expenditures/";
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
    
        return $ret;



    }
    //===========================================================

   public function getDailyDataColl(){
      $theDate = $_POST['date'];
      //var_dump($theDate); die();
        //http://127.0.0.1:8080/NSFRAccountingAPI/BUDGETService/NSFRA/API/v1/getDataCAPtype/type/2/date/2018-09-01
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataCAPtype/type/2/date/".$theDate;
        $ret = Helper::serviceGet($url);
        // var_dump($ret); die();
        return $ret;



    }

// //========================================================


     public function removeBudgets(){
      $refId = $_POST['refId'];
       $url = $GLOBALS['backend_URL']."ORS/API/v1/deactivateORS/refid/".$refId;     
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);       
      return $ret;


    }

  


//========================================================

    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>