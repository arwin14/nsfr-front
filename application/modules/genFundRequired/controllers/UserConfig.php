	<?php

class UserConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');
		$this->Helper->setTitle('User Configuration'); 
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}

	


	public function getDailyData(){
		$ret = $this->UserConfigCollection->getDailyDataColl($_POST);


		$arrsSTD = json_decode($ret);
		$arrs = json_decode( json_encode($arrsSTD), true);
		$result = $arrs['ResponseResult'];
		$return = array();
		//var_dump($result);
		if($result == "NO DATA FOUND"){
			$return = "";
		}else{	

		for ($key=44; $key < sizeof($result); $key++) { 
			$return[] = array(
			 "code" => $result[$key]['code'],
			 "name" => $result[$key]['name'],
			 "amount" => $result[$key]['amount'],
			 "btn" => "<a href='#' class='btn btn-simple btn-info btn-icon like'><i class='fa fa-eye'></i></a>".
			          "<a href='#' class='btn btn-simple btn-warning btn-icon edit'><i class='fa fa-edit'></i></a>".
			          "<a href='#' class='btn btn-simple btn-danger btn-icon remove'><i class='fa fa-times'></i></a>"
			);
		}
		// foreach ($result as $key => $value) {
		// $return[] = array(
		// 	 "code" => $result[$key]['code'],
		// 	 "name" => $result[$key]['name'],
		// 	 "amount" => $result[$key]['amount'],
		// 	 "btn" => "<a href='#' class='btn btn-simple btn-info btn-icon like'><i class='fa fa-eye'></i></a>".
		// 	          "<a href='#' class='btn btn-simple btn-warning btn-icon edit'><i class='fa fa-edit'></i></a>".
		// 	          "<a href='#' class='btn btn-simple btn-danger btn-icon remove'><i class='fa fa-times'></i></a>"
		// 	);
		// }
		}
		// foreach ($result as $key => $value) {
		// $return[] = array(
		// 	 "code" => $result[$key]['code'],
		// 	 "name" => $result[$key]['name'],
		// 	 "amount" => $result[$key]['amount'],
		// 	 "btn" => "<a href='#' class='btn btn-simple btn-info btn-icon like'><i class='fa fa-heart'></i></a>".
		// 	          "<a href='#' class='btn btn-simple btn-warning btn-icon edit'><i class='fa fa-edit'></i></a>".
		// 	          "<a href='#' class='btn btn-simple btn-danger btn-icon remove'><i class='fa fa-times'></i></a>"
		// 	);
		// }

		 $result = json_encode($return);
		//var_dump($return); die();
		echo $result;
		
			}


	

//==========================================================================================================

	public function exportExcel(){
	    ob_start();
	    $mydate = $_POST['excelDate'];
		$out = $this->TRExportExcel($mydate);
		ob_end_clean();
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $out;
		die();
		exit;
	}



	// public function TRExportExcel($mydate){
	// 	$thedate = $mydate;
	// 	//var_dump($thedate); die();

		

	// 			$title = '<table style="font-weight:bold; font-size: 12px;">' 
	// 						. '<tr style="text-align:center;">' 
	// 						. '<td colspan="3" align="center">MUFG</td>'
	// 						. '</tr>'  
	// 						. '<tr style="text-align:center;">'
	// 						. '<td colspan="3" align="center">Name of Bank</td>'
	// 						.	'</tr>'
	// 						. '<tr style="text-align:center;">'
	// 						. '<td colspan="3" align="center"><b>BASEL III NSFR REPORT (Solo)</b></td>'
	// 						.	'</tr>'
	// 						. '<tr style="text-align:center;">' 
	// 						. '<td colspan="3" align="center">As of (Monthly-end)</td>' 
	// 						. '</tr>' 
	// 					. '</table>';


	// 			$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
	// 						 <thead>
 //                     <tr>
 //                        <th class="no-sort">Item</th>
 //                        <th>Nature of Item</th>
 //                        <th>Amount</th>
 //                     </tr>
 //                  </thead>
	// 						<tbody>';

	// 				$tabledata = '';
	// 				$data = '';
					
							
	// 						$url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataCAP/type/3/date/".$thedate;
 //      						$ret = Helper::serviceGet($url);
 //      						$arrsSTD = json_decode($ret);
 //      						$arrs = json_decode( json_encode($arrsSTD), true);
	// 						$result = $arrs['ResponseResult']; 
	// 					foreach ($result as $key => $value) {
	// 						//var_dump($arrs['ResponseResult'][$key]['code']);
	// 						if($key > 44){
	// 										$data = $data . '<tr>'.
	// 										'<td > '. $result[$key]['code'] .'</td>' .
	// 										'<td > '. $result[$key]['name'] .'</td>' .
	// 										'<td > '. $result[$key]['amount'] .'</td>' .
	// 										'</tr>';
	// 						}else{

	// 							}

	// 				}
						
	// 				$tableend = '</tbody></table>';

	// 				//var_dump($title . $tablehead . $data . $tableend); die();	
	// 			//die();
	// 				return  $title . $tablehead . $data . $tableend;




		
	// }
	
}
?>