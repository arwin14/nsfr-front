<?php

class UserConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}

	
	public function getOptionAdd(){
		$ret = $this->UserConfigCollection->getOptionDataAdd($_POST);
		echo $ret;
		
			}

	public function getUpdate(){
		$ret = $this->UserConfigCollection->getDataUpdate($_POST);
		echo $ret;
			}


	public function getSpecific(){
		
		$ret = $this->UserConfigCollection->getSpecifics($_POST);
		echo $ret;
			}



	public function removeBudget(){

		$ret = $this->UserConfigCollection->removeBudgets($_POST);
		echo $ret;
			}
	

	public function addAllotment(){
		$ret = $this->UserConfigCollection->addAllotments($_POST);
		echo $ret;
	}





	public function getExpAllot(){
		$ret = $this->UserConfigCollection->getExpAllots($_POST);


		$arrsSTD = json_decode($ret);
		$arrs = json_decode( json_encode($arrsSTD), true);
		$result = $arrs['ResponseResult'];
		$return = array();
		/*var_dump($result); die();*/
		if($result == "NO DATA FOUND"){
			$return[] = array(
			 "refference" => '',
			 "Eexpendtype" => 'No Data Found',
			 "date" => '',
			 "btn" => ''
			);
		}else{	
		foreach ($result as $key => $value) {
		$return[] = array(
			 // "refid" => $result[$key]['refid'],
			 "refference" => $result[$key]['refid'],
			 "Eexpendtype" => $result[$key]['title'],
			 "date" => $result[$key]['dateCreated'],
			 "btn" => "<a href='#' class='btn btn-simple btn-info btn-icon like'><i class='fa fa-eye'></i></a>".
			 		  "<a class='btn btn-simple btn-warning btn-icon editbtn' data-refid='".$result[$key]['refid']."' data-type='".$result[$key]['title']."'><i class='fa fa-edit'></i></a>".
			          "<a  class='btn btn-simple btn-danger btn-icon remove'><i class='fa fa-times'></i></a>"
			);
		}
		}

		$result = json_encode($return);
		//var_dump($result); die();
		echo $result;
		
	}





public function addModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/addAllotment.php', "addrecord", TRUE);
		echo json_encode($result);
	//var_dump($result);
}

public function UpdateModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/updateAllotment.php', "updaterecord", TRUE);
		echo json_encode($result);
	//var_dump($result);
}


//added by jomer
public function addImportCSVModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/importcsvmodal.php', "addcsvrecord", TRUE);
		echo json_encode($result);
}

public function readCSVfile(){
	$columns_selected = [2,3];
	$row_starts = 1;
	$row = 0;
	$file_handle = fopen($_FILES["CSVfile"]["tmp_name"], 'r');
	
    while (!feof($file_handle) ) {
    	$selecteddetails = array();
    	$readline = fgetcsv($file_handle, 1024);
    	if (empty($readline) || $row < $row_starts) {

    	} else {
    		$j = 0;
    		foreach($readline as $key => $val) {
	    		for ($i=0; $i < sizeof($columns_selected); $i++) { 
	    			if ($key == $columns_selected[$i]) {
	    				$selecteddetails[$j] = $val;
	    				$j++;
	    			}
	    		}
			}
    		$line_of_text[] = $selecteddetails;
    	}
    	$row++;
    }

    fclose($file_handle);

    if (empty($line_of_text)) {
     $returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '1'
    				);
	} else {
		$returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '0'
    				);
	}
    
    $jsonlineoftext = json_encode($returndata);
    echo $jsonlineoftext;
}

public function submitCSVData(){

	$ret = new UserConfigCollection();


	$nsfraContentList = array();
	foreach ($_POST['codefield'] as $k3 => $v3) {
		$nsfraContentList[] = array(
									"expendCode" => $_POST['codefield'][$k3]
									,"amount" => $_POST['amountfield'][$k3]
								);
	}

	$requestdata = array(
						'fundDate'=>$_POST['funddate'],
						'createdBy'=>$_SESSION['userid'],
						'nsfraContentList'=>$nsfraContentList,
					);

	$datajson = json_encode($requestdata);

	$ret = $this->UserConfigCollection->addAllotmentsbyCSVModel($datajson);
	echo $ret;
}


//end of added by jomer








	public function updateAllotment(){	
		$ret = $this->UserConfigCollection->updateAllotments($_POST);
		echo $ret;
	}



	public function getExpend(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getExpendData($_POST);
		echo $ret;
		
			}

	// public function getExpAllot(){

	// 	// var_dump($_POST['date']); die();
	// 	$ret = $this->UserConfigCollection->getExpAllots($_POST);
	// 	echo $ret;
		
	// 		}
//==========================================================================================================
	
	
}
?>