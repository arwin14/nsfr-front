 <form id="addrecordform" class="uppercase" accept-charset="utf-8">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="body">
                           <div class="container-fluid">
                              <div class="col-md-6">
                                 <label class="form-label" style="font-size: 1.25rem">Specify Budget Allotment Form Type:</label><br>
                          
                               <div class="form-group form-float">
                                       <div class="form-line" id="budget_listModalhandle">
                                          <select class="form-control" id="expendtypeAddOption" name="expendtypeAddOption" required>
                                              <option value="--"  selected>Expend Type</option>
                                           <!--   <option value="PERSONNEL SERVICES">PS</option>
                                             <option value="MAINTENANCE AND OTHER OPERATING EXPENSES">MOOE</option>
                                             <option value="CAPITAL OUTLAY">CO</option>
                                             <option value="FINANCIAL EXPENSE">FE</option> --> -->
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                  <div class="col-md-6">
                                 <label class="form-label" style="font-size: 1.25rem">Date Created:</label><br>
                          
                             <div class="form-group form-float">
                  <div class="form-line">
                     <!-- <input type="text" id="dateSearch" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>"> -->
                     <input type="text" id="dateCreated" name="dateCreated" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                  </div>
               </div>
                                 </div>
                              </div>

                           </div><br>
                           <div style="width:90%; margin:auto;">
                             <!--  <div class="alert alert-danger fade in" id="searchMssgModal">
                                 <a href="#" class="close" data-dismiss="alert">&times;</a>
                                 <strong>Error!</strong> Should filled this form, that will allow to generate form.
                              </div>
                            -->
                           </div>
                              <button type="button" class="btn btn-block" id="generate_form" name="generate_form">
                                 <strong>GENERATE FORM</strong>
                            </button>
                            <!--   <div class="col-md-12">
                              
                              </div> -->
                           <div id="more_information" class="">
                              <div>
                                 <div class="row"  id="formloader">
                                    <div class="col-md-12">
                                       <div class="loader">
                                          <p>Loading...</p>
                                          <div class="loader-inner"></div>
                                          <div class="loader-inner"></div>
                                          <div class="loader-inner"></div>
                                       </div>
                                    </div>
                                   
                                 </div>
                              </div>

                              <div class="alert alert-danger fade in" id="AllotmentMssgModal">
                                 <a href="#" class="close" data-dismiss="alert">&times;</a>
                                 <strong>Error!</strong> Should filled this form, that will allow to generate form.
                              </div>
                              <div class="row clearfix" id="formGen" style="width:95%; margin:auto;">
                              </div><br>
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer" style="padding: 0; margin: 0">
                        <input type="hidden" name="AllotmentDate" id="AllotmentDate" value="<?php echo date('Y-m-d'); ?>">
                        <div class="row clearfix">
                           <div class="col-md-6">
                              <button type="button" class="btn btn-lg btn-success btn-block waves-effect" id="btnsubmitadd" name="btnsubmitadd" disabled>
                              <i class="fa fa-paper-plane fa-fw"></i>
                              <strong>SUBMIT</strong>
                              </button>
                           </div>
                           <div class="col-md-6">
                              <button id="btnclear" name="btnclear" type="button" class="btn btn-lg btn-danger btn-block waves-effect" disabled>
                              <i class="fa fa-trash fa-fw"></i>
                              <strong>CLEAR</strong>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </form>