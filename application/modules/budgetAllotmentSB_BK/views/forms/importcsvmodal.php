<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="content">
                    <div class="">
                        <div class="row">
                            <div class="col-md-6">
                                <form id="csvformcatcher">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Upload CSV
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-file-excel-o"></i></span>
                                            <input type="text" class="form-control errorClass1" disabled placeholder="Upload File">
                                            <span class="input-group-btn">
                                              <button class="btn btn-fill btn-warning btn-wd btn-fill browse" type="button"><i class="fa fa-upload"></i> Browse</button>
                                            </span>
                                        </div>
                                        <input type="file" id="inputcsvholder" name="CSVfile" class="file errorClass" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style="visibility: hidden">
                                    </div>  
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date Created</label>
                                    <input type="text" id="dateCreatedCSV" name="dateCreated" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row" id="errorpane" hidden>
                            <center>
                                <h5 class="text-danger">Failed to render data from CSV. Please re-upload</h5>
                            </center>
                        </div>
                        <div class="row" id="showdatapane" hidden>
                            <div class="col-md-12">
                                <h5>
                                    Rendered CSV data
                                </h5>
                                <form id="csvdataform">
                                    <div id="csvdatafield" style="margin: 15px;">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Code</label>
                                                    <input type="text" id="codefieldid-0" class="form-control codefield" name="codefield[]">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Amount</label>
                                                    <input type="text" id="amountfieldid-0" class="form-control amountfield" name="amountfield[]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
                    
                <br>
            </div>
        </div>
        <div class="modal-footer" style="padding: 0; margin: 0">
            <input type="hidden" name="AllotmentDate" id="AllotmentDate" value="<?php echo date('Y-m-d'); ?>">
            <div class="row clearfix">
               <div class="col-md-12">
                    <button type="button" class="btn btn-lg btn-success btn-block waves-effect" id="btnsubmitaddcsv" name="btnsubmitadd" disabled>
                        <i class="fa fa-paper-plane fa-fw"></i>
                        <strong>SUBMIT</strong>
                    </button>
               </div>
               <!-- <div class="col-md-6">
                    <button id="btnclear" name="btnclear" type="button" class="btn btn-lg btn-danger btn-block waves-effect" disabled>
                        <i class="fa fa-trash fa-fw"></i>
                        <strong>CLEAR</strong>
                    </button>
               </div> -->
            </div>
        </div>
    </div>
</div>
         