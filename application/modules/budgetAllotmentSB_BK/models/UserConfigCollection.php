  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }





       public function getOptionDataAdd(){
       // http://127.0.0.1:8080/NSFRAccountingAPI/BUDGETService/NSFRA/API/v1/getAllExpenditureType/type
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllExpenditureType/type';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;
      }  
// //========================================================
   public function getExpendData(){
         $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getExpenditureType/type/'.$_POST['id'];
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;



    }

// //========================================================
   public function getExpAllots(){
       // $url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataBAF306B/date/2018-04-10";
     $date = $_POST['date'];

     //var_dump($date);
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataByMonthAndYearNSFRA/date/".$date;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
       // var_dump($ret); die();
        return $ret;



    }
// //========================================================
   public function getSpecifics(){
       // $url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataBAF306B/date/2018-04-10";
     $refid = $_POST['refId'];

     //var_dump($refid);
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getSpecDataNSFRA/refid/".$refid;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
     //   var_dump($ret); die();
        return $ret;



    }
        

// //========================================================

    //===========ADDED BY JOMER=========
    public function addAllotmentsbyCSVModel($datajson){
     
      

      /*var_dump($datajson);die();*/

      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['backend_URL']."NSFRA/API/v1/insertNSFRA");
      echo json_encode($ret);


      /*var_dump("DIE");
      var_dump($datajson);die();*/

    }

  //===========END ADD BY JOMER========


    //  public function getDataUpdate(){
    //   $refId = $_POST['refId'];
    //   var_dump($refId); die();
    //   $url = "http://180.150.134.136:48082/ACPCAccountingAPI/BUDGETService/ORS/API/v1/deactivateORS/refid/".$refId;
    //    $json_string =  file_get_contents( $GLOBALS['backend_URL']."ORS/API/v1/deactivateORS/refid/".$refId);
    //     // $ret = Helper::serviceCall("http://180.150.134.136:48082/ACPCAccountingAPI/BUDGETService/ORS/API/v1/getSpecData/refid/".$refId);
    //    //return json_decode($json_string);

    // }

// //========================================================


     public function removeBudgets(){
      $refId = $_POST['refId'];
       $url =  $GLOBALS['backend_URL']."NSFRA/API/v1/deactivateNSFRA/refid/".$refId;     
     var_dump($url);
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   
      var_dump($ret); die();
      return $ret;


    }

  
// //========================================================
  public function addAllotments(){
   $post = $this->input->post();
      $post_data = array();

      foreach ($post as $k => $v) {
        $post_data[$k] = $this->input->post($k,true);

      }


      foreach($post_data['allotmentTxt'] as $k3 => $v3){

       
        $formdata[] = 
          array(
          'expendCode'=>$_POST['expendCode'][$k3],
          'amount'=>($_POST['allotmentTxt'][$k3] == '' || $_POST['allotmentTxt'][$k3] == '0') ? '0': $_POST['allotmentTxt'][$k3]
            );                       
      }



      $arr =
        array(

            "fundDate" => date('Y-m-d'),
            "createdBy" =>  $_SESSION['userid'],
            "nsfraContentList" => $formdata
            );
      $result = json_encode($arr);
      //var_dump("-------"+$result);die();
      $ret = Helper::serviceCallBudget($result,  $GLOBALS['backend_URL']."NSFRA/API/v1/insertNSFRA");
      echo json_encode($ret);

     }
//========================================================

     public function updateAllotments(){
    //var_dump('hello to allotment'); die();
   $post = $this->input->post();
      $post_data = array();

      foreach ($post as $k => $v) {
        $post_data[$k] = $this->input->post($k,true);

      }


     foreach($post_data['allotmentupdateTxt'] as $k3 => $v3){
        $formdata[] = 
          array(
          'expendId' => $_POST['fundid'][$k3],
          'expendCode'=>$_POST['expendcode'][$k3],
          'amount'=>($_POST['allotmentupdateTxt'][$k3] == '' || $_POST['allotmentupdateTxt'][$k3] == '0') ? '0': $_POST['allotmentupdateTxt'][$k3],
          'nsfrStatus'=>'ACTIVE'
            );                  
      
          }



      $arr =
        array(
             "refid" => $_POST['refid'],
            "fundDate" => date('Y-m-d'),
            "updatedBy" => $_SESSION['userid'],
            "createdBy" =>  $_SESSION['userid'],
            "nsfraStatus" => 'ACTIVE',
            "nsfraContentList" => $formdata
            );




      $result = json_encode($arr);
      //var_dump($result); die();
      $ret = Helper::serviceCallBudget($result,  $GLOBALS['backend_URL']."NSFRA/API/v1/updateNSFRA");
      echo json_encode($ret);

     }
//========================================================

    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>