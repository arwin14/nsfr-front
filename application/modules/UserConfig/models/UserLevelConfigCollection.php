<?php
class UserLevelConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }

    // public function $roles($id,$module){

    // }

    public function insertuserlevel($postdata){
        // var_dump(json_encode($_POST));die();
        $this->db->select("*")->from("webuserslevel")->where("userlevelname",$postdata["userlevelname"]);
        $query = $this->db->get();
        $num = $query->num_rows();
        if($num==1){
            return "Create Web User Level Failed.";
        }else{
            $data = array(
                "userlevelname" => $postdata["userlevelname"],
                "description" => $postdata["description"],
                "status" => $postdata["status"]
            );
            if($this->db->insert("webuserslevel", $data)){
                $this->db->select("userlevelid as ID")->from("webuserslevel")->order_by("userlevelid","DESC")->limit("1");
                $query = $this->db->get();
                $id = $query->row("ID");
                $roles = array();
                foreach ($postdata["roles"] as $key => $value) {
                    $roles[] = array("userlevelid"=>$id, "module"=>$value, "status"=>"ACTIVE");
                }
                if($this->db->insert_batch('webuserslevelconfig',$roles)){
                    return "Create Web User Level Successfully.";
                }else{
                    return "Create Web User Level Failed.";
                }
            }
        }
    }

    public function sql($postdata){
            $data = array(
                "userlevelname" => $postdata["userlevelname"],
                "description" => $postdata["description"],
                "status" => $postdata["status"]
            );
            $this->db->where('userlevelid', $postdata["userlevelid"]);
            if($this->db->update("webuserslevel", $data)){
                $this->db->where('userlevelid', $postdata["userlevelid"]);
                print_r("id: " . $postdata["userlevelid"]);
                if($this->db->delete("webuserslevelconfig")){
                    $roles = array();
                    foreach ($postdata["roles"] as $key => $value) {
                        $roles[] = array("userlevelid"=>$postdata["userlevelid"], "module"=>$value, "status"=>"ACTIVE");
                    }
                    if($this->db->insert_batch('webuserslevelconfig',$roles)){
                        return "Update Web User Level Successfully.";
                    }else{
                        return "Update Web User Level Failed. Batch Insert";
                    }
                }else{
                    return "Update Web User Level Failed. delete current roles";
                }
            }else{
                return "Update Web User Level Failed. update user level info";
            }
    }

    public function updateuserlevel($postdata){
        $this->db->select("*")->from("webuserslevel")->where("userlevelid",$postdata["userlevelid"]);
        $query = $this->db->get();
        $levelname = $query->row("LEVELNAME");
        if($levelname == $postdata["userlevelname"]){
            return $this->sql($postdata);
        }else{
            $this->db->select("*")->from("webuserslevel")->where("userlevelname",$postdata["userlevelname"])->where("userlevelid !=", $postdata["userlevelid"]);
            $query = $this->db->get();
            $num = $query->num_rows();
            if($num==1){
                return "Update Web User Level Failed. User level name exist.";
            }else{
                return $this->sql($postdata);  
            }
        }
    }

    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userlevelid",$id);
        return $this->db->update("webuserslevel",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userlevelid",$id);
        return $this->db->update("webuserslevel",$data);
    }

    public function userlevels(){
    	$this->db->select("*")->from("webuserslevel")->order_by("userlevelname", "ASC");
        $query = $this->db->get();
        if($query -> num_rows() > 0){
          return $query->result_array();
        }else {
          return false;
        }
    }

    public function userlevelroles(){
    	$this->db->select("*")->from("webuserslevelmodule")->order_by("module","ASC");
        $query = $this->db->get();
        if($query -> num_rows() > 0){
          return $query->result_array();
        }else {
          return false;
        }	
    }

    public function userroles($id){
    	$this->db->select("*")->from("webuserslevelconfig")->where("userlevelid",$id);
                  
        $query = $this->db->get();
        if($query -> num_rows() > 0){
          return $query->result_array();
        }else {
          return false;
        }	
    }
    
}

?>