<style type="text/css">

div.form-group{
	margin-bottom: -.5em;
}

.displaynone{
	display: none;
}

</style>
		<div class="container-fluid">
            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    	<div class="header">
                    		<h2>User Management - User Config</h2>
                    	</div>
                    	<div class="body">
                    		<div class="row clearfix">
                    			<div class="col-md-5 col-xs-12 col-sm-12">
		                    		<form id="frmuser" enctype="multipart/form-data" accept-charset="utf-8">
		                    			<!-- <div id="form-user" role="form" data-toggle="validator"> -->
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Agent ID</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="agentid" id="agentid" class="form-control">
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">User Level</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<select name="userlevel" id="userlevel" class="form-control"><option value="">Select User Level</option></select>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Username</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="username" id="username" class='form-control'>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">First Name</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="fname" id="fname" class='form-control'>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Middle Name</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="mname" id="mname" class='form-control'>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Last Name</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="lname" id="lname" class='form-control'>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Gender</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<select name="gender" id="gender" class='form-control'><option value="">Select Gender</option><option value="Male">Male</option><option value="Female">Female</option></select>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">E-mail Address</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="email" name="email" id="email" class='form-control'>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Status</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<select name="status" id="status" class='form-control'><option value="">Select Status</option><option value="ACTIVE" selected>ACTIVE</option><option value="INACTIVE">INACTIVE</option></select>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
			                    					<button type="submit" id="btnsubmit" class="btn btn-primary waves-effect pull-right" style="margin-right:.5em">Submit</button>
					            					<button type="button" id="btnupdate" class="btn btn-warning waves-effect displaynone pull-right" style="margin-right:.5em">Update</button>
					            					<button type="button" id="btndeactivate" class="btn btn-danger waves-effect displaynone pull-right" style="margin-right:.5em">Deactivate</button>
					            					<button type="button" id="btnactivate" class="btn btn-info waves-effect displaynone pull-right" style="margin-right:.5em">Activate</button>
					            					<button type="button" id="btnreset" class="btn btn-default waves-effect pull-right" style="margin-right:.5em">Reset</button>
			                    				</div>
			                    			</div>
		                    			<!-- </div> -->
		                    		</form>
                    			</div>
                    			<div class="col-md-7 col-sm-12 col-xs-12">
			            			<table id="tblroles" class="table table-bordered table-striped table-hover dataTable">
			            				<thead>
			            					<tr>
				            					<th style="word-wrap: break-word;width:10%">ID</th>
				            					<th style="word-wrap: break-word;width:20%">CODE</th>
				            					<th style="word-wrap: break-word;width:40%">Role Description</th>
				            					<th style="word-wrap: break-word;width:20%">Status</th>
				            					<th style="word-wrap: break-word;width:auto">&nbsp;</th>
				            				</tr>
			            				</thead>
			            			</table>
			            		</div>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>

            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    	<div class="header">
                    		<h2>Web Users</h2>
                    	</div>
                    	<div class="body">
                    		<table id="tblusers" class="table table-bordered table-striped table-hover dataTable">
			        			<thead>
			        				<tr>
				        				<th style="word-wrap: break-word;width:12%">ID</th>
				        				<th style="word-wrap: break-word;width:18%">Agent</th>
				        				<th style="word-wrap: break-word;width:15%">Username</th>
				        				<th style="word-wrap: break-word;width:auto">User Level</th>
				        				<th style="word-wrap: break-word;width:18%">Email</th>
				        				<th style="word-wrap: break-word;width:10%">Gender</th>
				        				<th style="word-wrap: break-word;width:10%">Status</th>
				        				<th style="word-wrap: break-word;width:auto">Action</th>
				        			</tr>
			        			</thead>
			        		</table>
                    	</div>
                    </div>
                </div>
            </div>

        </div>
      

	<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/UserManagement/userconfig.js"></script> 

