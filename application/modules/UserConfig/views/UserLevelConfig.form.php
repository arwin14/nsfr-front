<style type="text/css">

div.form-group{
	margin-bottom: -.5em;
}

.displaynone{
	display: none;
}

</style>
		<div class="container-fluid">
            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    	<div class="header">
                    		<h2>User Management - User Level Config</h2>
                    	</div>
                    	<div class="body">
                    		<div class="row clearfix">
                    			<div class="col-md-5 col-xs-12 col-sm-12">
		                    		<form id="frmuser" enctype="multipart/form-data" accept-charset="utf-8">
		                    			<!-- <div id="form-user" role="form" data-toggle="validator"> -->
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">User Level ID</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="userlevelid" id="userlevelid" class='form-control' style="pointer-events:none" readonly>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">User Level Name</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="userlevelname" id="userlevelname" class='form-control'>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Description</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<input type="text" name="description" id="description" class='form-control' >
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					                                <label class="form-label">Status</label>
				                                    <div class="form-group"><div class="form-line">
							                    		<select name="status" id="status" class='form-control'><option value="">Select Status</option><option value="ACTIVE" selected>ACTIVE</option><option value="INACTIVE">INACTIVE</option></select>
						            					<!-- <div class="help-block with-errors"></div> -->
							                    	</div></div>
				                                </div>
			                    			</div>
			                    			<div class="row clearfix">
			                    				<div class="col-md-12 col-xs-12 col-sm-12">
					            					<button type="submit" id="btnsubmit" class="btn btn-primary waves-effect pull-right" style="margin-right:.5em">Submit</button>
					            					<button type="button" id="btnupdate" class="btn btn-warning waves-effect displaynone pull-right" style="margin-right:.5em">Update</button>
					            					<button type="button" id="btndeactivate" class="btn btn-danger waves-effect displaynone pull-right" style="margin-right:.5em">Deactivate</button>
					            					<button type="button" id="btnactivate" class="btn btn-info waves-effect displaynone pull-right" style="margin-right:.5em">Activate</button>
					            					<button type="button" id="btnreset" class="btn btn-default waves-effect pull-right" style="margin-right:.5em">Reset </button>
					            				</div>
					            			</div>		
		                    			<!-- </div> -->
		                    		</form>
                    			</div>

                    			<div class="col-md-7 col-sm-12 col-xs-12">
			            			<table id="tblroles" class="table table-bordered table-striped table-hover dataTable">
			            				<thead>
			            					<tr>
				            					<th style="word-wrap: break-word;width:10%">ID</th>
				            					<th style="word-wrap: break-word;width:20%">CODE</th>
				            					<th style="word-wrap: break-word;width:40%">Role Description</th>
				            					<th style="word-wrap: break-word;width:20%">Status</th>
				            					<th style="word-wrap: break-word;width:auto">&nbsp;</th>
				            				</tr>
			            				</thead>
			            			</table>
			            		</div>
			            	</div>
                    	</div>
                    </div>
                </div>
            </div>

            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    	<div class="header">
                    		<h2>Web User Levels</h2>
                    	</div>
                    	<div class="body">
                    		<table id="tbluserlevels" class="table table-bordered table-striped table-hover dataTable">
			        			<thead>
			        				<tr>
				        				<th style="word-wrap: break-word;width:auto">User Level ID</th>
					        			<th style="word-wrap: break-word;width:auto">User level Name</th>
					        			<th style="word-wrap: break-word;width:auto">Description</th>
					        			<th style="word-wrap: break-word;width:auto">Status</th>
					        			<th style="word-wrap: break-word;width:auto">Action</th>
				        			</tr>
			        			</thead>
			        		</table>
                    	</div>
                    </div>
                </div>
            </div>

        </div>

	<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/UserManagement/userlevelconfig.js"></script> 
