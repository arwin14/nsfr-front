<?php

class UserConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
		$this->load->model('UserConfig');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}
	public function submitUser(){

		$this->LetterList->insertdata($_REQUEST);
		
	}

}

?>