<?php

class UserLevelConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserLevelConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Level Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserLevelConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}

	public function submitUserLevel(){

		// var_dump(json_encode($_POST));die();
		$ret = $this->UserLevelConfigCollection->insertuserlevel($_POST);
		echo $ret;
	}

	public function updateUserLevel(){
		$ret = $this->UserLevelConfigCollection->updateuserlevel($_POST);
		echo $ret;
	}

	public function getUserLevels(){
		$ret = $this->UserLevelConfigCollection->userlevels();
		echo json_encode(array("data"=>$ret));	
	}

	public function getuserlevelRoles(){
		$ret = $this->UserLevelConfigCollection->userlevelroles();
		echo json_encode(array("data"=>$ret));		
	}

	public function getUserRoles(){
		$modules = $this->UserLevelConfigCollection->userlevelroles();
		$usermodules = $this->UserLevelConfigCollection->userroles($_POST["id"]);
		echo json_encode(array("data"=>array("modules" => $modules, "usermodules" => $usermodules)));
	}

	public function activateUserLevel(){
		$ret = $this->UserLevelConfigCollection->activateuser($_POST["id"]);
		echo $ret;
	}
	
	public function deactivateUserLevel(){
		$ret = $this->UserLevelConfigCollection->deactivateuser($_POST["id"]);
		echo $ret;
	}
}

?>