<?php
//var_dump($reasons[0]); 
?>
<div style="background:rgb(244,234,159);">
   <br>
   <table style="width:90%; margin:auto;">
      <thead>
         <th>Net Stable Funding Ratio</th>
         <th>Available Stable Funding</th>
         <th>Required Stable Funding</th>
      </thead>
      <tbody>
         <td id="nsfr" style="font-size: 20pt;"><?php echo round($reasons[0]->nsfr, 2).'%'; ?></td>
            <td id="asf" style="font-size: 20pt;"><?php echo $reasons[0]->asf; ?></td>
            <td id="rsf" style="font-size: 20pt;"><?php echo $reasons[0]->rsf; ?></td>
      </tbody>
   </table>
   <br>
</div>
<br>
<div class="container-fluid">
   <form id="reasonForm" data-id="<?php //echo $form['refid']; ?>">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="content">
                  <div class="">
                     <div class="row">
                        <div class="col-md-6">
                           <input type="hidden" name="id" value="<?php echo $reasons[0]->id; ?>">
                           <input type="hidden" name="refid" value="<?php echo $reasons[0]->refid; ?>">
                           <input type="hidden" name="nsfr" value="<?php echo $reasons[0]->nsfr; ?>">
                           <input type="hidden" name="asf" value="<?php echo $reasons[0]->asf; ?>">
                           <input type="hidden" name="rsf" value="<?php echo $reasons[0]->rsf; ?>">
                           <input type="hidden" name="measurement_date" value="<?php echo $reasons[0]->measurement_date; ?>">
                           <h4 class="title">Reason Provider</h4>
                           <p class="category">(OPTIONAL</p>
                        </div>
                        <div class="col-md-6">
                           <label class="form-label" style="font-size: 1.25rem">Duration:</label><br>
                           <div class="form-group form-float">
                              <div class="form-line">
                                 <input type="text" id="duration" name="duration" value="<?php echo $reasons[0]->expected_duration; ?>" class="form-control ">
                              </div>
                           </div>
                           <br>
                        </div>
                     </div>
                     <div id="content">

                        <div class="form-group">
                           <label for="comment">Comment:</label>
                           <textarea class="form-control" rows="5" name="reason" id="reason"><?php echo $reasons[0]->reason; ?>
                           </textarea>
                        </div>
                        <div class="form-group">
                           <label for="comment">Action:</label>
                           <textarea class="form-control" rows="5" name="action" id="action"><?php echo $reasons[0]->action; ?>
                           </textarea>
                        </div>
                     </div>
                  </div>
                  <br>
               </div>
            </div>
            <div class="modal-footer" style="padding: 0; margin: 0">
               <div class="row clearfix">
                  <div class="col-md-12">
                     <button type="button" class="btn btn-lg btn-success btn-block waves-effect" id="updatebtn" name="importRawDTR" >
                     <i class="fa fa-paper-plane fa-fw"></i>
                     <strong>SUBMIT</strong>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>