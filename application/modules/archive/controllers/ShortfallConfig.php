<?php

class ShortfallConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('ShortfallConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User configuration'); //--> Set title to the Template
		$this->Helper->setView('ShortfallConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}





		public function getArchive(){
		$return[] =  array();
		$ret = new ShortfallConfigCollection();
		if(!empty($_POST)) {
			$date = $_POST['date'];

			if($ret->getArchicves($date)) {
					$rets= new ModelResponse($ret->getCode(), $ret->getMessage(), $ret->getData());
					$return = json_decode($rets);
					//$button = ($return->Data[$key]->nsfr > 100)? "": "<button class='btn btn-warning btn-fill btn-wd btnshortfall' data-id='".$return->Data[$key]->refid."' data-functionname = 'tblAllocate' data-direct='ShortfallConfig/reasonProvider'>Update Shortfall</button>";
					//<a  class='btn btn-simple btn-danger btn-icon'><i class='fa fa-times' style='color:black;'></i></a>
					foreach ($return->Data as $key => $value) {
						$button = ($return->Data[$key]->nsfr > 100)? "": "<a class='btn btn-simple btn-warning btn-icon btnshortfall' data-refid='".$return->Data[$key]->refid."' data-id='".$return->Data[$key]->id."' data-functionname = 'tblAllocate' data-direct='ShortfallConfig/reasonProvider' data-date='' ><i class='fa fa-edit'></i></a>";
						$return->Data[$key]->roundedNSFR = number_format($return->Data[$key]->nsfr, 2);
						$return->Data[$key]->roundedASF = number_format($return->Data[$key]->asf, 2);
						$return->Data[$key]->roundedRSF = number_format($return->Data[$key]->rsf, 2);
						$return->Data[$key]->btn = $button;

					}

			} else {
				$return = new ModelResponse($ret->getCode(), $ret->getMessage());
			}
			$result = json_encode($return);
		        
		        echo $result;

		} else {
					show_404();
		}
		
	}




function reasonProvider(){
	 //var_dump($_POST); die();
	$data = array();
	$ret = $this->ShortfallConfigCollection->getReasonSpecific($_POST);
	$data['reasons'] = $ret->ResponseResult;
	//var_dump($data['reasons']);


	//$dataTittle = $this->GroupbyTitle($data);

	// $requestData = array();
	// foreach ($dataTittle as $key => $value) {
	// 		$populatedetail = array();
	// 		foreach ($data as $datakey => $datavalue) {
	// 			if($data[$datakey]->title ==  $dataTittle[$key]){
	// 				$populatedetail[] = array(
	// 					"name" => $data[$datakey]->name,
	// 					"id"   => $data[$datakey]->code
	// 				);
	// 			}else{
	// 			}
	// 		}
	// 		$requestData[] = array(
	// 			'title' => $dataTittle[$key],
	// 			'content' => $populatedetail
	// 		);
	// }

	// $requestedData = array();
	// $requestedData['form'] = $requestData;


	$result = array();
	$result['form'] = $this->load->view('forms/updateshortfall.php',$data, TRUE);
	echo json_encode($result);
	}






public function getSpecific(){

	 $ret = new ShortfallConfigCollection();
	 $ret = $ret->getOrigData($_POST);
	$data = $ret->ResponseResult;
	   $dataReturn[] = array();
	   $ASF =  $this->Populate($data, 1); 
	   $RSF =  $this->Populate($data, 2);         
	
	   $data['asf'] =  $this->GroupbyTitle($ASF);
	   $data['rsf'] =  $this->GroupbyTitle($RSF);
	   $data['data'] = array(
	   							"Available Stable Funding" => $data['asf'],
	   							"Required Stable Funding" => $data['rsf'],
	   						);

	   $result['form'] = $this->load->view('forms/specificForm.php', $data, TRUE);
		echo json_encode($result);
}





private function GroupbyTitle($data){
		
		$title = array();
		$titlecontent = array();
		foreach ($data as $key => $value) {
				array_push($title, $data[$key]->title);
		}
		$title = array_unique($title);

		foreach ($title as $a => $val) {
					foreach ($data as $aa => $vals) {
						if($title[$a] == $data[$aa]->title){
							$titlecontent[$title[$a]][] = $data[$aa];
						}else{
						}
					}
		}
		return $titlecontent;
}




private function Populate($data, $type){
	$returndata = array();
	foreach ($data as $key => $value) {
		if($key == 0 || $data[$key]->fund_type != $type){

		}else{
		$returndata[] = $data[$key];
		}
	}		
	return $returndata;
}











public function updateArchive(){
	if(!empty($_POST)) {
		
		$requestData = array();
		$ret = new ShortfallConfigCollection();
			foreach ($_POST as $k3 => $v3) {
				 $requestData += [ $k3 => $_POST[$k3] ];
			}
			$requesteddata = json_encode($requestData);
			//var_dump($requesteddata); die();
			if($ret->updateArchicves($requesteddata)) {
				$ret= new ModelResponse($ret->getCode(), $ret->getMessage());
			} else {
				$ret = new ModelResponse($ret->getCode(), $ret->getMessage());
			}
			    echo $ret;
		}else {
			show_404();
		 	 }
	}
}
?>