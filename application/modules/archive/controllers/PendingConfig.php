<?php

class PendingConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('PendingConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User configuration'); //--> Set title to the Template
		$this->Helper->setView('PendingConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}



	// public function getExpAllot(){
	// 	$ret = $this->AllotmentConfigCollection->getExpAllots($_POST);


	// 	$arrsSTD = json_decode($ret);
	// 	$arrs = json_decode( json_encode($arrsSTD), true);
	// 	$result = $arrs['ResponseResult'];
	// 	$return = array();
	// 	//var_dump($result); die();
	// 	if($result == "NO DATA FOUND"){
	// 		$return = "";
	// 	}else{	
	// 	foreach ($result as $key => $value) {
	// 	$return[] = array(
	// 		 "refference" => $result[$key]['refid'],
	// 		 "Status" => $result[$key]['status_approval'],
	// 		 "Eexpendtype" => $result[$key]['title'],
	// 		 "nsfraStatus" => $result[$key]['nsfraStatus'],
	// 		 "date" => $result[$key]['dateCreated'],
	// 		 "btn" => "<a  class='btn btn-simple btn-danger btn-icon remove' data-id='".$result[$key]['refid']."' data-functionname = 'tblAllocate' data-direct='AllotmentConfig/removeBudget' data-date='".substr($result[$key]['dateCreated'],0,-10)."'><i class='fa fa-times'></i></a>"
	// 		);
	// 	}
	// 	}

	// 	$result = json_encode($return);
	// 	//var_dump($result); die();
	// 	echo $result;
		
	// }








		public function getArchive(){
		//var_dump($_POST);die();
		$return[] =  array();
		$ret = new PendingConfigCollection();
		if(!empty($_POST)) {
			$date = $_POST['date'];

			if($ret->getArchicves($date)) {
					$rets= new ModelResponse($ret->getCode(), $ret->getMessage(), $ret->getData());
					$return = json_decode($rets);
					//var_dump($return);

					foreach ($return->Data as $key => $value) {
						$return->Data[$key]->roundedNSFR = number_format($return->Data[$key]->nsfr, 2);
						$return->Data[$key]->roundedASF = number_format($return->Data[$key]->asf, 2);
						$return->Data[$key]->roundedRSF = number_format($return->Data[$key]->rsf, 2);
					}

			} else {
				$return = new ModelResponse($ret->getCode(), $ret->getMessage());
			}
			$result = json_encode($return);
		        
		     echo $result;

		} else {
					show_404();
		}
		
	}












public function getSpecific(){

	 $ret = new PendingConfigCollection();
	 $ret = $ret->getOrigData($_POST);
	$data = $ret->ResponseResult;

	   $dataReturn[] = array();
	   $ASF =  $this->Populate($data, 1); 
	   $RSF =  $this->Populate($data, 2);         
	
	   $data['asf'] =  $this->GroupbyTitle($ASF);
	   $data['rsf'] =  $this->GroupbyTitle($RSF);

	   $data['data'] = array(
	   							"Available Stable Funding" => $data['asf'],
	   							"Required Stable Funding" => $data['rsf'],
	   						);

	   $result['form'] = $this->load->view('forms/specificForm.php', $data, TRUE);
		echo json_encode($result);
}





private function GroupbyTitle($data){
		
		$title = array();
		$titlecontent = array();
		foreach ($data as $key => $value) {
				array_push($title, $data[$key]->title);
		}
		$title = array_unique($title);

		foreach ($title as $a => $val) {
					foreach ($data as $aa => $vals) {
						if($title[$a] == $data[$aa]->title){
							$titlecontent[$title[$a]][] = $data[$aa];
						}else{
						}
					}
		}
		return $titlecontent;
}




private function Populate($data, $type){
	$returndata = array();
	foreach ($data as $key => $value) {
		if($key == 0 || $data[$key]->fund_type != $type){

		}else{
		$returndata[] = $data[$key];
		}
	}		
	return $returndata;
}











public function updateArchive(){
	if(!empty($_POST)) {
		
		$requestData = array();
		$ret = new PendingConfigCollection();
			foreach ($_POST as $k3 => $v3) {
				 $requestData += [ $k3 => $_POST[$k3] ];
			}
			$requesteddata = json_encode($requestData);
			if($ret->updateArchicves($requesteddata)) {
				$ret= new ModelResponse($ret->getCode(), $ret->getMessage());
			} else {
				$ret = new ModelResponse($ret->getCode(), $ret->getMessage());
			}
			    echo $ret;
		}else {
			show_404();
		 	 }
	}


























// 	public function upload()
// 	{
// 	//	 var_dump($_POST); die();
// 	$columns_selected = [2,3];
// 	$row_starts = 1;
// 	$row = 0;
// 	$file_handle = fopen($_FILES['file']['tmp_name'], 'r');
//     while (!feof($file_handle) ) {
//     	$selecteddetails = array();
//     	$readline = fgetcsv($file_handle, 1024);
//     	if (empty($readline) || $row < $row_starts) {

//     	} else {
//     		$j = 0;
//     		foreach($readline as $key => $val) {
// 	    		for ($i=0; $i < sizeof($columns_selected); $i++) { 
// 	    			if ($key == $columns_selected[$i]) {
// 	    				$selecteddetails[$j] = $val;
// 	    				$j++;
// 	    			}
// 	    		}
// 			}
//     		$line_of_text[] = $selecteddetails;
//     	}
//     	$row++;
//     }





//     fclose($file_handle);

//     if (empty($line_of_text)) {
//      $returndata = array(
//     					'CSVData'=>$line_of_text
//     					,'Code' => '1'
//     				);
// 	} else {
// 		$returndata = array(
//     					'CSVData'=>$line_of_text
//     					,'Code' => '0'
//     				);
// 	}
    

// $populate = $returndata['CSVData'];
// //var_dump($populate); die();
// $nsfraContentList = array();
// 	foreach ($populate as $k3 => $v3) {
// 		$nsfraContentList[] = array(
// 									"expendCode" => $populate[$k3][0]
// 									,"amount" => $populate[$k3][1]
// 								);
// 	}


// 	$requestdata = array(
// 						'fundDate'=>$_POST['date'],
// 						'createdBy'=>$_SESSION['userid'],
// 						'nsfraContentList'=>$nsfraContentList,
// 					    );	
// 	$jsonlineoftext = json_encode($requestdata);

// 	$ret = $this->AllotmentConfigCollection->addAllotmentsbyCSVModel($jsonlineoftext);
// 	echo $ret;


// 	}




// // 	public function readCSVfile(){
// // 	$columns_selected = [2,3];
// // 	$row_starts = 1;
// // 	$row = 0;
// // 	$file_handle = fopen($_FILES["CSVfile"]["tmp_name"], 'r');
// //     while (!feof($file_handle) ) {
// //     	$selecteddetails = array();
// //     	$readline = fgetcsv($file_handle, 1024);
// //     	if (empty($readline) || $row < $row_starts) {

// //     	} else {
// //     		$j = 0;
// //     		foreach($readline as $key => $val) {
// // 	    		for ($i=0; $i < sizeof($columns_selected); $i++) { 
// // 	    			if ($key == $columns_selected[$i]) {
// // 	    				$selecteddetails[$j] = $val;
// // 	    				$j++;
// // 	    			}
// // 	    		}
// // 			}
// //     		$line_of_text[] = $selecteddetails;
// //     	}
// //     	$row++;
// //     }

// //     fclose($file_handle);

// //     if (empty($line_of_text)) {
// //      $returndata = array(
// //     					'CSVData'=>$line_of_text
// //     					,'Code' => '1'
// //     				);
// // 	} else {
// // 		$returndata = array(
// //     					'CSVData'=>$line_of_text
// //     					,'Code' => '0'
// //     				);
// // 	}
    
// //     $jsonlineoftext = json_encode($returndata);
// //     echo $jsonlineoftext;
// // }


// 	// public function remove()
// 	// {
// 	// 	$file = $this->input->post("file");
// 	// 	if ($file && file_exists($this->upload_path . "/" . $file)) {
// 	// 		unlink($this->upload_path . "/" . $file);
// 	// 	}
// 	// }

// 	// public function list_files()
// 	// {
// 	// 	// $this->load->helper("file");
// 	// 	// $files = get_filenames($this->upload_path);
// 	// 	// // we need name and size for dropzone mockfile
// 	// 	// foreach ($files as &$file) {
// 	// 	// 	$file = array(
// 	// 	// 		'name' => $file,
// 	// 	// 		'size' => filesize($this->upload_path . "/" . $file)
// 	// 	// 	);
// 	// 	// }
// 	// }














// 	public function addAllotment(){
// 		//$ret = $this->AllotmentConfigCollection->addAllotments($_POST);

// 		//$nsfraContentList = array();
// 			foreach ($_POST['amount'] as $k3 => $v3) {
// 				$amount = ($_POST['amount'][$k3] == "" || $_POST['amount'][$k3] < 1)? 0 : $_POST['amount'][$k3];
// 				$nsfraContentList[] = array(
// 									"expendCode" => $_POST['code'][$k3]
// 									,"amount" => $amount
// 								);
// 			}


// 	$requestdata = array(
// 						'fundDate'=>$_POST['dateCreated'][0],
// 						'createdBy'=>$_SESSION['userid'],
// 						'nsfraContentList'=>$nsfraContentList,
// 					    );	
// 			// 			var_dump($requestdata);

// 			// die();
// 				$jsonlineoftext = json_encode($requestdata);

// 			$ret = $this->AllotmentConfigCollection->addAllotments($jsonlineoftext);
// 			echo $ret;
// 	}

// 	public function dummy()
// 	{
// 		$data = array_map(function($x,$y){
// 			return [
// 				'dummy'=>$x,
// 				'text'=>$y
// 					];
// 		}, $_POST['dummy'], $_POST['simulatedtxt']);
	
// 		print_r($data);
// 	}




// 	public function getUpdate(){
// 		$ret = $this->AllotmentConfigCollection->getDataUpdate($_POST);
// 		echo $ret;
// 			}


// 	public function getSpecific(){
		
// 		$ret = $this->AllotmentConfigCollection->getSpecifics($_POST);
// 		echo $ret;
// 			}
// 	public function getSpecificforTotal(){
		
// 	$ret = $this->AllotmentConfigCollection->getSpecificsforTotal($_POST);
// 	echo $ret;
// 		}


// 	public function getSpecificValidate(){
		
// 		$ret = $this->AllotmentConfigCollection->getSpecificsValidate($_POST);
// 		echo $ret;
// 			}

//    public function getValidateSpecific(){
		
// 		$ret = $this->AllotmentConfigCollection->getValidateSpecifics($_POST);
// 		echo $ret;
// 			}


// 	public function getModule(){
		
// 		if(Helper::role($_POST['module'])){
// 		echo "1";
// 		}else{
// 		echo "0"; 
// 		}
// 		//echo $jsonlineoftext;
// 	}


// 	public function getOptionAdd(){
// 		$ret = $this->AllotmentConfigCollection->getOptionDataAdd($_POST);
// 		echo $ret;
		
// 			}

// 	public function removeBudget(){
// 		$ret = $this->AllotmentConfigCollection->removeBudgets($_POST);
// 		echo $ret;
// 			}

// 	// public function ChangeStatus(){
// 	// 	//urlvar_dump($_POST); die();
// 	// 	$ret = $this->AllotmentConfigCollection->ChangesStatus($_POST);
// 	// 	echo $ret;
// 	// 		}
	
// 	public function submitApproval(){
// 	//$ret = $this->AllotmentConfigCollection->ChangesStatus($_POST);
// 	//echo $ret;
// 		$requestedData = array();
// 		$requestedData['form'] = $_POST;
// 		$result = array();
// 		$result['form'] = $this->load->view('forms/addReason.php', $requestedData, TRUE);
// 		echo json_encode($result);
// 	}



// 	public function addApproval(){
// 		$arr = array(
// 			"refid" => $_POST['refid'],
// 			"reason" => $_POST['reason'],
// 			"action" => $_POST['action'],
// 			"duration" => $_POST['duration'],
// 			"nsfr" => round($_POST['NSFR'] * 100, 2),
// 			"asf" => $_POST['ASF'],
// 			"rsf" => $_POST['RSF'],
// 			"date" => $_POST['fund_date']
// 		);

// 		var_dump($arr); die();
// 		$ret = new AllotmentConfigCollection();
// 		$Request = json_encode($arr);
// 		$ret = $this->AllotmentConfigCollection->addApprovals($Request);
// 		echo $ret;


// 	}





// 	public function getExpAllot(){
// 		$ret = $this->AllotmentConfigCollection->getExpAllots($_POST);


// 		$arrsSTD = json_decode($ret);
// 		$arrs = json_decode( json_encode($arrsSTD), true);
// 		$result = $arrs['ResponseResult'];
// 		$return = array();
// 		//var_dump($result); die();
// 		if($result == "NO DATA FOUND"){
// 			$return = "";
// 		}else{	
// 		foreach ($result as $key => $value) {
// 		$return[] = array(
// 			 "refference" => $result[$key]['refid'],
// 			 "Status" => $result[$key]['status_approval'],
// 			 "Eexpendtype" => $result[$key]['title'],
// 			 "nsfraStatus" => $result[$key]['nsfraStatus'],
// 			 "date" => $result[$key]['dateCreated'],
// 			 "btn" => "<a  class='btn btn-simple btn-danger btn-icon remove' data-id='".$result[$key]['refid']."' data-functionname = 'tblAllocate' data-direct='AllotmentConfig/removeBudget' data-date='".substr($result[$key]['dateCreated'],0,-10)."'><i class='fa fa-times'></i></a>"
// 			);
// 		}
// 		}

// 		$result = json_encode($return);
// 		//var_dump($result); die();
// 		echo $result;
		
// 	}









// //====================================================================
// public function addModal(){
// 	//$myfirstFunction = $this->gg();

// 	$ret = $this->AllotmentConfigCollection->getNatureofItem($_POST);
// 	$data = $ret->ResponseResult;
// 	$dataTittle = $this->GroupbyTitle($data);

// 	$requestData = array();
// 	foreach ($dataTittle as $key => $value) {
// 			$populatedetail = array();
// 			foreach ($data as $datakey => $datavalue) {
// 				if($data[$datakey]->title ==  $dataTittle[$key]){
// 					$populatedetail[] = array(
// 						"name" => $data[$datakey]->name,
// 						"id"   => $data[$datakey]->code
// 					);
// 				}else{
// 				}
// 			}
// 			$requestData[] = array(
// 				'title' => $dataTittle[$key],
// 				'content' => $populatedetail
// 			);
// 	}

// 	$requestedData = array();
// 	$requestedData['form'] = $requestData;

// 	//var_dump($requestedData); die();
// 	$result = array();
// 	$result['form'] = $this->load->view('forms/addAllotment.php', $requestedData, TRUE);
// 		echo json_encode($result);
// 	}

// 	private function GroupbyTitle($data){
// 		$title = [];
// 		foreach ($data as $key => $value) {
// 				array_push($title,$data[$key]->title);
// 		}
// 		$title = array_unique($title);
// 		return $title;
// 	}
// //====================================================================















// public function UpdateModal(){
// 	$result = array();
// 	$result['form'] = $this->load->view('forms/updateAllotment.php', "updaterecord", TRUE);
// 		echo json_encode($result);
// 	//var_dump($result);
// }


// //added by jomer
// public function addImportCSVModal(){
// 	$result = array();
// 	$result['form'] = $this->load->view('forms/importcsvmodal.php', "addcsvrecord", TRUE);
// 		echo json_encode($result);
// }

// public function tablecollapseview(){
// 	$result = array();
// 	$result['form'] = $this->load->view('forms/tablecollapseview.php', "tablecollapseview", TRUE);
// 		echo json_encode($result);
// }



// // public function submitCSVData(){

// // 	//$ret = new AllotmentConfigCollection();


// // 	$nsfraContentList = array();
// // 	foreach ($_POST['codefield'] as $k3 => $v3) {
// // 		$nsfraContentList[] = array(
// // 									"expendCode" => $_POST['codefield'][$k3]
// // 									,"amount" => $_POST['amountfield'][$k3]
// // 								);
// // 	}

// // 	$requestdata = array(
// // 						'fundDate'=>$_POST['funddate'],
// // 						'createdBy'=>$_SESSION['userid'],
// // 						'nsfraContentList'=>$nsfraContentList,
// // 					    );

// // 	$datajson = json_encode($requestdata);
// // 	$ret = $this->AllotmentConfigCollection->addAllotmentsbyCSVModel($datajson);
	
// // 	echo $ret;


// // 	// $ret = $this->AllotmentConfigCollection->addAllotments($_POST);
// // 	// echo $ret;
// // }

// public function submitchangesdata(){
// 	// / var_dump($_POST); die();
// 	$fulldate = (explode (" ", $_POST['funddate']));
// 	$date = $fulldate[0];
//      foreach($_POST['simulatedtxt'] as $k3 => $v3){
//          $formdata[] = 

//            array(
//           'expendId' => $_POST['expendid'][$k3],
//           'expendCode'=>$_POST['expendCode'][$k3],
//           'amount'=>($_POST['simulatedtxt'][$k3] == '' || $_POST['simulatedtxt'][$k3] == '0') ? '0': $_POST['simulatedtxt'][$k3],
//           'nsfrStatus'=>'ACTIVE'
//              );                  
      
//           }



//       $arr =
//         array(
//              "refid" => $_POST['refid'],
//             "fundDate" => $date,
//             "updatedBy" => $_SESSION['userid'],
//             "createdBy" =>  $_SESSION['userid'],
//             "nsfraStatus" => 'ACTIVE',
//             "nsfraContentList" => $formdata
//             );







// 	$ret = new AllotmentConfigCollection();
// 	$Request = json_encode($arr);
// 		// var_dump($Request);
// 		// die();
// 	$ret = $this->AllotmentConfigCollection->updateAllotments($Request);
// 	echo $ret;
// 	//var_dump($Request); die();

// }



// 	// public function updateAllotment(){	
// 	// 	$ret = $this->AllotmentConfigCollection->updateAllotments($_POST);
// 	// 	echo $ret;
// 	// }



// 	public function getExpend(){

// 		// var_dump($_POST['date']); die();
// 		$ret = $this->AllotmentConfigCollection->getExpendData($_POST);
// 		echo $ret;
		
// 			}

// 	// public function getExpAllot(){

// 	// 	// var_dump($_POST['date']); die();
// 	// 	$ret = $this->AllotmentConfigCollection->getExpAllots($_POST);
// 	// 	echo $ret;
		
// 	// 		}
// //==========================================================================================================
	
	
}
?>