<div class="login-box">
        <div class="logo">
			<a href="<?php echo base_url(); ?>">
				<!-- <img class="p-b-20" src="<?php echo base_url(); ?>assets/custom/images/singlelgalogo.png" width=50 alt="" /> -->
				<span class="font-35"><b>Easy Parking System</b></span>
			</a>
			<small>Cloud-based Parking Ticketing System</small>
        </div>
		<?php if(isset($serverMessage)): ?>
		<div class="alert <?php echo $serverCode == 0 ? 'alert-info' : 'alert-danger';?>">
			<a href="#" aria-hidden="true" data-dismiss="alert" class="close" style=""><i class="font-15 material-icons">close</i></a>
			<strong><?php echo $serverCode == 0 ? 'Great!' : 'Oh snap!'; ?></strong> <?php echo $serverMessage; ?>
		</div>
		<?php endif; ?>
        <div class="card">
            <div class="body">
                <form id="sign_in" action="<?php echo base_url(); ?>session/validate" method="POST">
                    <div class="msg">Sign in to your EPS account</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" style="text-transform: uppercase;" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
							<a href="<?php echo base_url(); ?>session/ForgotPassword">Forgot Password?</a>
                        </div>
                        <div class="col-xs-4 pull-right">
                            <button class="btn btn-block bg-yellow waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
					<input type="hidden" name="ip" id="ip" />
                </form>
            </div>
        </div>
		<center style="margin-top:-20px;">
			<small>EPS &copy; 2018 <a style="color:#FFFFFF;" href="http://www.mobilemoney.ph" target="_blank">Telcom Live Content, Inc.</a></small>
		</center>
    </div>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/modules/js/session/session.js"></script>