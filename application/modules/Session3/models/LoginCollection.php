<?php
class LoginCollection extends Helper {
	
	public function __construct() {
		ModelResponse::busy();
		
		$this->load->model("UserAccount");
	}
			
	public function login($user, $passw, $token) {
		$data = json_encode(array("Username"=>$user,"Password"=>$passw,"Token"=>$token));

		$ret = parent::serviceCall("LOGN",$data);
		//var_dump($ret);die();
		if($ret != null) {
			if($ret->Code == 0) {
				if(isset($ret->Data)) {
					$useracct = $ret->Data->details;
					$sessUser = new UserAccount();
					$sessUser->put($useracct);
					$sessUser->modules($ret->Data->roles);
					$sessUser->sessionId($ret->Data->sessionid);
					$this->ModelResponse($ret->Code, $ret->Message, $useracct);
				} else {
					$this->ModelResponse($ret->Code, $ret->Message);
				}				
				return true;
			} else {
				$this->ModelResponse($ret->Code, $ret->Message);
			}			
		}
		return false;
	}
	
	public function sessionLogout() {
		$data = json_encode(array("SessionId"=>UserAccount::getSessionId()));
		$ret = parent::serviceCall("LOGT",$data);		
		if($ret != null) {
			if($ret->Code == 0) {
				$this->ModelResponse($ret->Code, $ret->Message);
				return true;
			} else {
				$this->ModelResponse($ret->Code, $ret->Message);
			}			
		}
		return false;
	}
	
	public function checkSessionState() {
		$data = json_encode(array("SessionId"=>UserAccount::getSessionId()));
		$ret = parent::serviceCall("CHKSESS",$data);
		if($ret != null) {
			if($ret->Code == 0) {
				$this->ModelResponse($ret->Code, $ret->Message);
				return true;
			} else {
				$this->ModelResponse($ret->Code, $ret->Message);
			}			
		}
		return false;
	}
	
	public function cPass($oldpass,$newpass,$newpass2) {
		$data = json_encode(array("SessionId"=>UserAccount::getSessionId(),
								 "UserId"=>Helper::get('userid'),
								 "OldPassword"=>$oldpass,
								 "NewPassword"=>$newpass,
								 "ConfirmNewPassword"=>$newpass2));
		$ret = parent::serviceCall("CPASS",$data);
		if($ret != null) {
			if($ret->Code == 0) {
				$this->ModelResponse($ret->Code, $ret->Message);
				return true;
			} else {
				$this->ModelResponse($ret->Code, $ret->Message);
			}			
		}
		return false;
	}
	
	public function fPass($email, $baseurl) {
		$data = json_encode(array("Email"=>$email, "BaseUrl"=>$baseurl));
		$ret = parent::serviceCall("FPASS",$data);
		if($ret != null) {
			if($ret->Code == 0) {
				$this->ModelResponse($ret->Code, $ret->Message);
				return true;
			} else {
				$this->ModelResponse($ret->Code, $ret->Message);
			}			
		}
		return false;
	}
	
	public function fCPass($linkid, $newpass, $newpass2) {
		$data = json_encode(array("LinkId"=>$linkid,"NewPassword"=>$newpass,"NewPassword2"=>$newpass2));
		$ret = parent::serviceCall("FCPASS",$data);
		if($ret != null) {
			if($ret->Code == 0) {
				$this->ModelResponse($ret->Code, $ret->Message);
				return true;
			} else {
				$this->ModelResponse($ret->Code, $ret->Message);
			}			
		}
		return false;
	}

}
?>