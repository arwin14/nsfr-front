<?php

class Session extends MX_Controller {
	
	public function __construct() {
		parent::__construct();		
		$this->load->model('Helper');
		$this->load->model('LoginCollection');
	}	
	
	public function index() {
		if(Helper::get('isfirstlogin') == 0) {
			Helper::sessionStartedHook('home');
		} else {
			Helper::sessionStartedHook('session/ChangePass');
		}
		Helper::setTitle('Sign In | EPS');
		Helper::setView('login','',FALSE);
		Helper::setTemplate('templates/login_template');
	}	
	
	public function validate() {
		Helper::sessionStartedHook('home');

		$ret = new LoginCollection();
		$user = isset($_POST['username']) ? strtoupper($_POST['username']) : "";
		$passw = isset($_POST['password']) ? $_POST['password'] : "";
		$token =  isset($_POST['ip']) ? base64_encode($user . ':' . $_POST['ip']) : "";
		
		if($ret->login($user,$passw,$token)) {		
			Helper::sessionStart();
		} else {
			$response['serverCode'] = $ret->getCode();
			$response['serverMessage'] = $ret->getMessage();
			Helper::setView('login',$response,FALSE);
			Helper::setTemplate('templates/login_template');
		}
	}
	
	public function logout() {
		$ret = new LoginCollection();
		
		if($ret->sessionLogout()) {
			Helper::sessionTerminate();
		}
		
		self::checksession();
	}

	public static function checksession() {
		$ret = new LoginCollection();		
		
		if(!$ret->checkSessionState()) {
			Helper::sessionTerminate();
		}		
	}
	
}
?>
