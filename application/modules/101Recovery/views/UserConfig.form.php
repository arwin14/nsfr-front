<style type="text/css">
   .displaynone{
   display: none;
   }
   .no-sort::after { display: none!important; }
   .no-sort { 
   pointer-events: none!important; cursor: default!important; 
   }
   .loaderme {
   border: 6px solid #f3f3f3;
   border-radius: 50%;
   border-top: 6px solid #3498db;
   width: 50px;
   height: 50px;
   -webkit-animation: spin 2s linear infinite; /* Safari */
   animation: spin 2s linear infinite;
   }
   /* Safari */
   @-webkit-keyframes spin {
   0% { -webkit-transform: rotate(0deg); }
   100% { -webkit-transform: rotate(360deg); }
   }
   @keyframes spin {
   0% { transform: rotate(0deg); }
   100% { transform: rotate(360deg); }
   }
   .loader{
   width: 70px;
   height: 70px;
   margin: 40px auto;
   }
   .loader p{
   font-size: 16px;
   color: #777;
   }
   .loader .loader-inner{
   display: inline-block;
   width: 15px;
   border-radius: 15px;
   background: #74d2ba;
   }
   .loader .loader-inner:nth-last-child(1){
   -webkit-animation: loading 1.5s 1s infinite;
   animation: loading 1.5s 1s infinite;
   }
   .loader .loader-inner:nth-last-child(2){
   -webkit-animation: loading 1.5s .5s infinite;
   animation: loading 1.5s .5s infinite;
   }
   .loader .loader-inner:nth-last-child(3){
   -webkit-animation: loading 1.5s 0s infinite;
   animation: loading 1.5s 0s infinite;
   }
   @-webkit-keyframes loading{
   0%{
   height: 15px;
   }
   50%{
   height: 35px;
   }
   100%{
   height: 15px;
   }
   }
   @keyframes loading{
   0%{
   height: 15px;
   }
   50%{
   height: 35px;
   }
   100%{
   height: 15px;
   }
   .dataTables_filter {
   display: none; 
   }
   #tblOrs tbody td {
   white-space: nowrap;
   }
</style>
<div class="container-fluid" style="font-size:8pt;">
   <div class="block-header">
   </div>
   <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="card">
            <div class="header bg-green">
               <h2 style="font-size: 2.2rem">
                  DATA Recovery - PS
                  <small>Agricultural Credit Policy Council</small> 
               </h2>
               <ul class="header-dropdown m-r-0">
                  <li>
                     <button type="button" id="btnDateSearch" class="btn bg-light-green btn-circle-lg waves-effect waves-circle waves-float" data-toggle="collapse" data-target=".dateSearch">
                     <i class="material-icons">date_range</i>
                     </button>
                  </li>
               </ul>
            </div>
            <div class="body">
               <div class="row clearfix">
               </div>
               <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#home">ORS</a></li>
                  <li><a data-toggle="tab" href="#menu1">Budget Allotment</a></li>
                  <li><a data-toggle="tab" href="#menu2">NCA Program</a></li>
               </ul>
               <br>
               <div class="dateSearch collapse">
                  <div class="row clearfix">
                     <div class="col-md-6">
                     </div>
                     <div class="col-md-4">
                        <div class="form-group form-float">
                           <div class="form-line">
                              <input type="text" class="form-control" id="searchDate" value="<?php echo date('Y-m-d'); ?>" name="search_table_textbox">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-2">
                        <button type="button" class="btn btn-sm btn-block bg-green waves-effect" id="submitDate" name=""
                           data-toggle="collapse" data-target="#tableOptionsCollapse">
                        <i class="material-icons">filter_list</i>
                        <span>
                        <strong>Search</strong>
                        </span>
                        </button>
                     </div>
                  </div>
               </div>
               <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                     <div class="row"  id="formloaderOrs">
                        <div class="col-md-12">
                           <div class="loader">
                              <p>Loading...</p>
                              <div class="loader-inner"></div>
                              <div class="loader-inner"></div>
                              <div class="loader-inner"></div>
                           </div>
                        </div>
                     </div>
                     <div id="ORSHandler">
                        <div class="table-responsive" id="GASStblhandledr" style="display:; background:;">
                           <h3>ORS DATA RECOVERY</h3>
                           <table id="tblOrs" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                              <thead>
                                 <tr>
                                    <th colspan="8" class="no-sort"></th>
                                    <th colspan="4" style="text-align: center;">1.A.1</th>
                                    <th colspan="3" style="text-align: center;">11.A.1</th>
                                    <th colspan="3" style="text-align: center;">11.A.2</th>
                                    <th colspan="4" style="text-align: center;">Total</th>
                                 </tr>
                                 <tr>
                                    <th class="no-sort"></th>
                                    <th></th>
                                    <th></th>
                                    <th class="no-sort">REMARKS ON Ca</th>
                                    <th class="no-sort">CODE</th>
                                    <th class="no-sort">OBLIGATIONS</th>
                                    <th class="no-sort">TAX</th>
                                    <th class="no-sort">DISBURSEMENTS</th>
                                    <th class="no-sort">PS</th>
                                    <th class="no-sort">MOOE</th>
                                    <th class="no-sort">CO</th>
                                    <th class="no-sort">FE</th>
                                    <th class="no-sort">PS</th>
                                    <th class="no-sort">MOOE</th>
                                    <th class="no-sort">CO</th>
                                    <th class="no-sort">PS</th>
                                    <th class="no-sort">MOOE</th>
                                    <th class="no-sort">CO</th>
                                    <th class="no-sort">PS</th>
                                    <th class="no-sort">CO</th>
                                    <th class="no-sort">FE</th>
                                    <th class="no-sort">MOOE</th>
                                 </tr>
                              </thead>
                              <tbody> 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div id="menu1" class="tab-pane fade">
                     <div class="row"  id="formloaderAllot">
                        <div class="col-md-12">
                           <div class="loader">
                              <p>Loading2...</p>
                              <div class="loader-inner"></div>
                              <div class="loader-inner"></div>
                              <div class="loader-inner"></div>
                           </div>
                        </div>
                     </div>
                     <div id="ALLOTMENTHandler">
                        <div class="table-responsive" id="GASStblhandledr" style="display:; background:;">
                           <h3>Budget Allotment DATA RECOVERY</h3>
                           <table id="tblallotment" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                              <thead>
                                 <tr>
                                    <th style="text-align: center; width:9%;"></th>
                                    <th style="text-align: center;">EXPEND TYPE</th>
                                    <th style="text-align: center;">BUDGET CATEGORY</th>
                                    <th style="text-align: center;">DATE CREATED</th>
                                 </tr>
                              </thead>
                              <tbody> 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div id="menu2" class="tab-pane fade">
                     <div class="row"  id="formloaderNCA">
                        <div class="col-md-12">
                           <div class="loader">
                              <p>Loading3...</p>
                              <div class="loader-inner"></div>
                              <div class="loader-inner"></div>
                              <div class="loader-inner"></div>
                           </div>
                        </div>
                     </div>
                     <div id="NCAHandler">
                        <div class="table-responsive" id="NCAtblhandledr" style="display:; background:;">
                           <h3>Budget Allotment DATA RECOVERY</h3>
                           <table id="NCAtbl" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                              <thead>
                                 <tr>
                                    <th style="color:rgba(0,0,0,0);">aa0000000000000</th>
                                    <th>January</th>
                                    <th>February</th>
                                    <th>March</th>
                                    <th>April</th>
                                    <th>May</th>
                                    <th>June</th>
                                    <th>July</th>
                                    <th>August</th>
                                    <th>September</th>
                                    <th>October</th>
                                    <th>November</th>
                                    <th>December</th>
                                    <th>Total</th>
                                 </tr>
                              </thead>
                              <tbody> 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/101Recovery/budget.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>