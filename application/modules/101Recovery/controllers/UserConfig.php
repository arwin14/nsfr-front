<?php

class UserConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}

	
//=========================================================================================================
		public function getNCA(){
		
		$ret = $this->UserConfigCollection->getNCAData($_POST);
		echo $ret;
		
			}
//=========================================================================================================
  		public function getAllot(){
		
		$ret = $this->UserConfigCollection->getAllots($_POST);
		echo $ret;
		
			}
//=========================================================================================================
  		public function getOrs(){
		
		$ret = $this->UserConfigCollection->getOrsdata($_POST);
		echo $ret;
		
			}
//=========================================================================================================
  		public function restoreOrs(){
		//var_dump($_POST['refId']); die();
		$ret = $this->UserConfigCollection->restoreOrsData($_POST);
		echo $ret;
		
			}
//=========================================================================================================
  		public function restoreNCA(){
		//var_dump($_POST['refId']); die();
		$ret = $this->UserConfigCollection->restoreNCAData($_POST);
		echo $ret;
		
			}
//=========================================================================================================
  		public function restoreAllot(){
		//var_dump($_POST['refId']); die();
		$ret = $this->UserConfigCollection->restoreAllotData($_POST);
		echo $ret;
		
			}
// //==========================================================================================================

// 	public function exportExcelGASS(){
// 	    ob_start();
// 	    $mydate = $_POST['excelDate'];
// 		// $mydatess= $_POST['dateMonth'];
// 		// var_dump($mydatess);
// 		$out = $this->TRExportExcelGASS($mydate);
// 		ob_end_clean();
// 		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
// 		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
// 		header("Pragma: no-cache");
// 		header("Expires: 0");
// 		echo $out;
// 		die();
// 		exit;
// 	}



// 	public function TRExportExcelGASS($mydate){
// 		$thedate = $mydate;

							
// 		//var_dump("-->".$thedate); 

// 							//$url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataBAF306B/date/".$thedate;
// 							$url = $GLOBALS['backend_URL']."ORS/API/v1/getSUMMARY/type/1/".$thedate;
//       						$ret = Helper::serviceGet($url);
//       						$arrsSTD = json_decode($ret);
//       						$arrs = json_decode( json_encode($arrsSTD), true);
//       						// echo '<pre>' . var_export($arrs, true) . '</pre>';

// 			$topheader = 	'<table style="font-weight:bold;">' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h3>Administration of AMCFP</h3></td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(PERSONEL SERVICES & MOOE)</td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(Allotment Class)</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">Name of Agency</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h2 style="color:blue;">'.$thedate.'</h2></td>' .

// 							'</tr>'.		
// 							'</table>';


// 				$grandchildtitle = $arrs['ResponseResult'][0]['TAXList'];
// 				$child = $arrs['ResponseResult'];
//       						//echo '<pre>' . var_export(json_encode($child), true) . '</pre>'; // 
// 				$headerTBL = '';
// 				$columndataApp='';
// 				foreach ($grandchildtitle as $x => $value) {	
// 						$headerTBL = $headerTBL . '<td >'. $grandchildtitle[$x]['expendName'] .'</td>';
// 						$columndataApp = $columndataApp .'<td>'. $grandchildtitle[$x]['allotmentFund']['af'].'</td>';
// 				}
// 				$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
// 				$rawdataGASSDATA = [];
// 				$data = '';

// 				foreach ($months as $a => $valuedata) {	
// 					$rowdata ='';
// 					$rowdataTotal ='';
// 					$rowdataRemain ='';
// 					$columndataTotal='';
// 					$columndataRemain ='';

// 					$grandchild = $arrs['ResponseResult'][$a]['TAXList'];
// 						foreach ($grandchild as $b => $valuedata) {
// 							if($b == 0){
// 								// $columndataApp = $columndataApp . '<td>APPROPRIATION</td><td >'. $grandchild[$b]['allotmentFund']['af'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>Total ORS of Month of '. $months[$a] .'</td><td >'. $grandchild[$b]['obligationFund']['af'].'</td>';
// 								$columndataRemain = $columndataRemain. '<td> Remaining balance of '.$months[$a].'</td><td >'. $grandchild[$b]['balanceFund']['af'].'</td>';

// 							}else{
// 								//$columndataApp = $columndataApp .'<td>'. $grandchild[$b]['allotmentFund']['af'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>'. $grandchild[$b]['obligationFund']['af'].'</td>';
// 								$columndataRemain = $columndataRemain . '<td>'. $grandchild[$b]['balanceFund']['af'].'</td>';
// 							}
// 						}
// 						$rowdata = $rowdata . '<tr style="background:yellow;">'. $columndataApp.'</tr>';
// 						$rowdataTotal = $rowdataTotal .'<tr>'. $columndataTotal.'</tr>';
// 						$rowdataRemain = $rowdataRemain .'<tr>'. $columndataRemain.'</tr><tr></tr>';
// 						$data = $data .''.$rowdataTotal.''.$rowdataRemain;
// 						}
				

// 				$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
// 							  <thead>
//                    		  		<tr>
//                    		  		<td></td>'
//                    		  		.$headerTBL.
//                         		 '</tr>.
//                				 </thead>
// 							<tbody>';

// 					$tabledata = '';
// 					$dataBody = $dataBody.'<tr><td>APPROPRIATION</td>'.$columndataApp.'</tr><tr></tr>'.$data;
// 					$tableend = '</tbody></table>';

// 					return $topheader . $tablehead . $dataBody . $tableend;




		
// 	}
// //===============================================================================================================================

// 	public function exportExcelAMCf(){
// 	    ob_start();
// 	    $mydate = $_POST['excelDate'];
// 		// var_dump($mydate); die();
// 		$out = $this->TRExportExcelAMCf($mydate);
// 		ob_end_clean();
// 		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
// 		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
// 		header("Pragma: no-cache");
// 		header("Expires: 0");
// 		echo $out;
// 		die();
// 		exit;
// 	}



// 	public function TRExportExcelAMCf($mydate){
// 		$thedate = $mydate;			//$url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataBAF306B/date/".$thedate;
// 							$url = $GLOBALS['backend_URL']."ORS/API/v1/getSUMMARY/type/1/".$thedate;
//       						$ret = Helper::serviceGet($url);
//       						$arrsSTD = json_decode($ret);
//       						$arrs = json_decode( json_encode($arrsSTD), true);
							

// 			$topheader = 	'<table style="font-weight:bold;">' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h3>Administration of AMCFP</h3></td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(PERSONEL SERVICES & MOOE)</td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(Allotment Class)</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">Name of Agency</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h2 style="color:blue;">'.$thedate.'</h2></td>' .

// 							'</tr>'.	
// 							'</table>';
// 	$grandchildtitle = $arrs['ResponseResult'][0]['TAXList'];
// 				$child = $arrs['ResponseResult'];
//       						//echo '<pre>' . var_export(json_encode($child), true) . '</pre>'; // 
// 				$headerTBL = '';
// 				$columndataApp='';
// 				foreach ($grandchildtitle as $x => $value) {	
// 						$headerTBL = $headerTBL . '<td >'. $grandchildtitle[$x]['expendName'] .'</td>';
// 						$columndataApp = $columndataApp .'<td>'. $grandchildtitle[$x]['allotmentFund']['am'].'</td>';
// 				}
// 				$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
// 				$rawdataGASSDATA = [];
// 				$data = '';

// 				foreach ($months as $a => $valuedata) {	
// 					$rowdata ='';
// 					$rowdataTotal ='';
// 					$rowdataRemain ='';
// 					$columndataTotal='';
// 					$columndataRemain ='';

// 					$grandchild = $arrs['ResponseResult'][$a]['TAXList'];
// 						foreach ($grandchild as $b => $valuedata) {
// 							if($b == 0){
// 								// $columndataApp = $columndataApp . '<td>APPROPRIATION</td><td >'. $grandchild[$b]['allotmentFund']['af'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>Total ORS of Month of '. $months[$a] .'</td><td >'. $grandchild[$b]['obligationFund']['am'].'</td>';
// 								$columndataRemain = $columndataRemain. '<td> Remaining balance of '.$months[$a].'</td><td >'. $grandchild[$b]['balanceFund']['am'].'</td>';

// 							}else{
// 								//$columndataApp = $columndataApp .'<td>'. $grandchild[$b]['allotmentFund']['am'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>'. $grandchild[$b]['obligationFund']['am'].'</td>';
// 								$columndataRemain = $columndataRemain . '<td>'. $grandchild[$b]['balanceFund']['am'].'</td>';
// 							}
// 						}
// 						$rowdata = $rowdata . '<tr style="background:yellow;">'. $columndataApp.'</tr>';
// 						$rowdataTotal = $rowdataTotal .'<tr>'. $columndataTotal.'</tr>';
// 						$rowdataRemain = $rowdataRemain .'<tr>'. $columndataRemain.'</tr><tr></tr>';
// 						$data = $data .''.$rowdataTotal.''.$rowdataRemain;
// 						}
				

// 				$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
// 							  <thead>
//                    		  		<tr>
//                    		  		<td></td>'
//                    		  		.$headerTBL.
//                         		 '</tr>.
//                				 </thead>
// 							<tbody>';

// 					$tabledata = '';
// 					$dataBody = $dataBody.'<tr><td>APPROPRIATION</td>'.$columndataApp.'</tr><tr></tr>'.$data;
// 					$tableend = '</tbody></table>';

// 					return $topheader . $tablehead . $dataBody . $tableend;




		
// 	}
// //===============================================================================================================================
// 		public function exportExcelFORMEQ(){
// 	    ob_start();
// 	    $mydate = $_POST['excelDate'];
// 		// var_dump($mydate); die();
// 		$out = $this->TRExportExcelFORMEQ($mydate);
// 		ob_end_clean();
// 		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
// 		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
// 		header("Pragma: no-cache");
// 		header("Expires: 0");
// 		echo $out;
// 		die();
// 		exit;
// 	}



// 	public function TRExportExcelFORMEQ($mydate){
// 		$thedate = $mydate;

// 							$url = $GLOBALS['backend_URL']."ORS/API/v1/getSUMMARY/type/1/".$thedate;
//       						$ret = Helper::serviceGet($url);
//       						$arrsSTD = json_decode($ret);
//       						$arrs = json_decode( json_encode($arrsSTD), true);
							

// 			$topheader = 	'<table style="font-weight:bold;">' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h3>Administration of AMCFP</h3></td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(PERSONEL SERVICES & MOOE)</td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(Allotment Class)</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">Name of Agency</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h2 style="color:blue;">'.$thedate.'</h2></td>' .

// 							'</tr>'.	
// 							'</table>';

// 			$grandchildtitle = $arrs['ResponseResult'][0]['TAXList'];
// 				$child = $arrs['ResponseResult'];
//       						//echo '<pre>' . var_export(json_encode($child), true) . '</pre>'; // 
// 				$headerTBL = '';
// 				$columndataApp='';
// 				foreach ($grandchildtitle as $x => $value) {	
// 						$headerTBL = $headerTBL . '<td >'. $grandchildtitle[$x]['expendName'] .'</td>';
// 						$columndataApp = $columndataApp .'<td>'. $grandchildtitle[$x]['allotmentFund']['os'].'</td>';
// 				}
// 				$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
// 				$rawdataGASSDATA = [];
// 				$data = '';

// 				foreach ($months as $a => $valuedata) {	
// 					$rowdata ='';
// 					$rowdataTotal ='';
// 					$rowdataRemain ='';
// 					$columndataTotal='';
// 					$columndataRemain ='';

// 					$grandchild = $arrs['ResponseResult'][$a]['TAXList'];
// 						foreach ($grandchild as $b => $valuedata) {
// 							if($b == 0){
// 								// $columndataApp = $columndataApp . '<td>APPROPRIATION</td><td >'. $grandchild[$b]['allotmentFund']['af'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>Total ORS of Month of '. $months[$a] .'</td><td >'. $grandchild[$b]['obligationFund']['os'].'</td>';
// 								$columndataRemain = $columndataRemain. '<td> Remaining balance of '.$months[$a].'</td><td >'. $grandchild[$b]['balanceFund']['os'].'</td>';

// 							}else{
// 								//$columndataApp = $columndataApp .'<td>'. $grandchild[$b]['allotmentFund']['os'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>'. $grandchild[$b]['obligationFund']['os'].'</td>';
// 								$columndataRemain = $columndataRemain . '<td>'. $grandchild[$b]['balanceFund']['os'].'</td>';
// 							}
// 						}
// 						$rowdata = $rowdata . '<tr style="background:yellow;">'. $columndataApp.'</tr>';
// 						$rowdataTotal = $rowdataTotal .'<tr>'. $columndataTotal.'</tr>';
// 						$rowdataRemain = $rowdataRemain .'<tr>'. $columndataRemain.'</tr><tr></tr>';
// 						$data = $data .''.$rowdataTotal.''.$rowdataRemain;
// 						}
				

// 				$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
// 							  <thead>
//                    		  		<tr>
//                    		  		<td></td>'
//                    		  		.$headerTBL.
//                         		 '</tr>.
//                				 </thead>
// 							<tbody>';

// 					$tabledata = '';
// 					$dataBody = $dataBody.'<tr><td>APPROPRIATION</td>'.$columndataApp.'</tr><tr></tr>'.$data;
// 					$tableend = '</tbody></table>';

// 					return $topheader . $tablehead . $dataBody . $tableend;




		
// 	}
// //===============================================================================================================================
// 			public function exportExcelSUMMARY(){
// 	    ob_start();
// 	    $mydate = $_POST['excelDate'];
// 		// var_dump($mydate); die();
// 		$out = $this->TRExportExcelSUMMARY($mydate);
// 		ob_end_clean();
// 		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
// 		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
// 		header("Pragma: no-cache");
// 		header("Expires: 0");
// 		echo $out;
// 		die();
// 		exit;
// 	}



// 	public function TRExportExcelSUMMARY($mydate){
// 		$thedate = $mydate;

// 							$url = $GLOBALS['backend_URL']."ORS/API/v1/getSUMMARY/type/1/".$thedate;
//       						$ret = Helper::serviceGet($url);
//       						$arrsSTD = json_decode($ret);
//       						$arrs = json_decode( json_encode($arrsSTD), true);
							

// 			$topheader = 	'<table style="font-weight:bold;">' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h3>Administration of AMCFP</h3></td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(PERSONEL SERVICES & MOOE)</td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">(Allotment Class)</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22">Name of Agency</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="22"><h2 style="color:blue;">'.$thedate.'</h2></td>' .

// 							'</tr>'.	
// 							'</table>';
// $grandchildtitle = $arrs['ResponseResult'][0]['TAXList'];
// 				$child = $arrs['ResponseResult'];
//       						//echo '<pre>' . var_export(json_encode($child), true) . '</pre>'; // 
// 				$headerTBL = '';
// 				$columndataApp='';
// 				foreach ($grandchildtitle as $x => $value) {	
// 					$allotmentTotal = $grandchildtitle[$x]['allotmentFund']['af'] + $grandchildtitle[$x]['allotmentFund']['am'] + $grandchildtitle[$x]['allotmentFund']['os'];
// 						$headerTBL = $headerTBL . '<td >'. $grandchildtitle[$x]['expendName'] .'</td>';
// 						$columndataApp = $columndataApp .'<td>'. $allotmentTotal.'</td>';
// 				}
// 				$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
// 				$rawdataGASSDATA = [];
// 				$data = '';

// 				foreach ($months as $a => $valuedata) {	
// 					$rowdata ='';
// 					$rowdataTotal ='';
// 					$rowdataRemain ='';
// 					$columndataTotal='';
// 					$columndataRemain ='';

// 					$grandchild = $arrs['ResponseResult'][$a]['TAXList'];

// 						foreach ($grandchild as $b => $valuedata) {
// 						$obligationtotal = $grandchild[$b]['obligationFund']['af'] + $grandchild[$b]['obligationFund']['am'] + $grandchild[$b]['obligationFund']['os']; 
// 						$Remainingtotal = $grandchild[$b]['balanceFund']['af'] + $grandchild[$b]['balanceFund']['am'] + $grandchild[$b]['balanceFund']['os']; 
// 							if($b == 0){
// 								// $columndataApp = $columndataApp . '<td>APPROPRIATION</td><td >'. $grandchild[$b]['allotmentFund']['af'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>Total ORS of Month of '. $months[$a] .'</td><td >'. $obligationtotal.'</td>';
// 								$columndataRemain = $columndataRemain. '<td> Remaining balance of '.$months[$a].'</td><td >'. $Remainingtotal.'</td>';

// 							}else{
// 								//$columndataApp = $columndataApp .'<td>'. $grandchild[$b]['allotmentFund']['am'].'</td>';
// 								$columndataTotal = $columndataTotal . '<td>'. $obligationtotal.'</td>';
// 								$columndataRemain = $columndataRemain . '<td>'. $Remainingtotal.'</td>';
// 							}
// 						}
// 						$rowdata = $rowdata . '<tr style="background:yellow;">'. $columndataApp.'</tr>';
// 						$rowdataTotal = $rowdataTotal .'<tr>'. $columndataTotal.'</tr>';
// 						$rowdataRemain = $rowdataRemain .'<tr>'. $columndataRemain.'</tr><tr></tr>';
// 						$data = $data .''.$rowdataTotal.''.$rowdataRemain;
// 						}
				

// 				$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
// 							  <thead>
//                    		  		<tr>
//                    		  		<td></td>'
//                    		  		.$headerTBL.
//                         		 '</tr>.
//                				 </thead>
// 							<tbody>';

// 					$tabledata = '';
// 					$dataBody = $dataBody.'<tr><td>APPROPRIATION</td>'.$columndataApp.'</tr><tr></tr>'.$data;
// 					$tableend = '</tbody></table>';

// 					return $topheader . $tablehead . $dataBody . $tableend;




		
// 	}
// //===============================================================================================================================
// 			public function exportExcelUTIL(){
// 	    ob_start();
// 	    $mydate = $_POST['excelDate'];
// 		//var_dump($mydate); die();
// 		$out = $this->TRExportExcelUTIL($mydate);
// 		ob_end_clean();
// 		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
// 		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
// 		header("Pragma: no-cache");
// 		header("Expires: 0");
// 		echo $out;
// 		die();
// 		exit;
// 	}



// 	public function TRExportExcelUTIL($mydate){
// 		$thedate = $mydate;

// 			$topheader = 	'<table style="font-weight:bold;">' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="13"><h3>REGISTRY OF ALLOTMENTS, OBLIGATIONS AND DISBURSEMENTS (RAOD)</h3></td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="13">(BUDGET SUMMARY - PS)</td>' .
// 							'</tr>' .
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="13">(Allotment Class)</td>' .
// 							'</tr>'.
// 							'<tr style="text-align:left;">' .
// 								'<td colspan="13">Name of Agency</td>' .
// 							'</tr>'.
// 							'<tr>' .
// 								'<td><h2 style="color:blue;">'.$thedate.'</h2></td>' .

// 							'</tr>'.	
// 							'</table>';

// 				$title = 	'<table style="font-weight:bold; font-size: 25px;">' .
// 							'<tr style="text-align:center;">' .
// 								'<td colspan="20">BUDGET UTILIZE</td>' .
// 							'</tr>' .	
// 							'</table>';



// 				$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
// 							  <thead>
//                            <tr>
//                                 <th colspan="4" class="no-sort">BAF306B Obligation</th>     
//                                 <th colspan="4"  style="text-align: center;">TAX</th>
//                                 <th colspan="4" style="text-align: center;">DISBURSEMENTS</th> 
//                             </tr>
//                             <tr>
                       
//                                 <th class="no-sort">Expend Code</th>
//                                 <th class="no-sort">I.a.1</th>
//                                 <th class="no-sort">II.a.1</th>
//                                 <th class="no-sort">II.a.2</th>
//                                 <th class="no-sort">I.a.1</th>
//                                 <th class="no-sort">II.a.1</th>
//                                 <th class="no-sort">II.a.2</th>
//                                 <th class="no-sort">TOTAL</th>
//                                 <th class="no-sort">I.a.1</th>
//                                 <th class="no-sort">II.a.1</th>
//                                 <th class="no-sort">II.a.2</th>
//                                 <th class="no-sort">TOTAL</th>
                               
//                             </tr>
//                     </thead>
// 							<tbody>';

// 					$tabledata = '';
// 					$data = '';
					
					
						
// 							// $json_string =  file_get_contents( $GLOBALS['backend_URL']."ORS/API/v1/getTAX/date/".$mydate);
// 							// $arrs = json_decode($json_string, true);
							

// 							$url = $GLOBALS['backend_URL']."ORS/API/v1/getTAX/date/".$mydate;
//       						$ret = Helper::serviceGet($url);
//       						$arrsSTD = json_decode($ret);
//       						$arrs = json_decode( json_encode($arrsSTD), true);

//       						$obligationFundaf = 0;
//       						$obligationFundam = 0;
//       						$obligationFundos = 0;

//       						$taxFundaf = 0;
//       						$taxFundam = 0;
//       						$taxFundos = 0;
//       						$taxFundtotal = 0;

//       						$disbursementFundaf = 0;
//       						$disbursementFundam = 0;
//       						$disbursementFundos = 0;
//       						$disbursementFundtotal = 0;


//       						$child = $arrs['ResponseResult'][0]['TAXList'];

// 						  // var_dump($arrs); die();
			
// 								foreach ($child as $key => $value) {
// 									$obligationFundaf = $obligationFundaf +	$child[$key]['obligationFund']['af'];
// 									$obligationFundam = $obligationFundam +	$child[$key]['obligationFund']['am'];
// 									$obligationFundos = $obligationFundos +	$child[$key]['obligationFund']['os'];

// 									$taxFundaf = $taxFundaf +	$child[$key]['taxFund']['af'];
// 									$taxFundam = $taxFundam +	$child[$key]['taxFund']['am'];
// 									$taxFundos = $taxFundos +	$child[$key]['taxFund']['os'];
// 									$taxFundtotal = $taxFundtotal +	$child[$key]['taxFund']['total'];


// 									$disbursementFundaf = $disbursementFundaf +	$child[$key]['disbursementFund']['af'];
// 									$disbursementFundam = $disbursementFundam +	$child[$key]['disbursementFund']['am'];
// 									$disbursementFundos = $disbursementFundos +	$child[$key]['disbursementFund']['os'];
// 									$disbursementFundtotal = $disbursementFundtotal +	$child[$key]['disbursementFund']['total'];
// 							//var_dump($child[$key]['obligationFund']['af']); die();
								
// 											$data = $data . '<tr>'.
// 											'<td >'. $child[$key]['code'].'</td>' .
// 											'<td>'. $child[$key]['obligationFund']['af'].'</td>' .
// 											'<td>'. $child[$key]['obligationFund']['am'].'</td>' .
// 											'<td>'. $child[$key]['obligationFund']['os'].'</td>' .

// 											'<td>'. $child[$key]['taxFund']['af'].'</td>' .
// 											'<td>'. $child[$key]['taxFund']['am'].'</td>' .
// 											'<td>'. $child[$key]['taxFund']['os'].'</td>' .
// 											'<td>'. $child[$key]['taxFund']['total'].'</td>' .


// 											'<td>'. $child[$key]['disbursementFund']['af'].'</td>' .
// 											'<td>'. $child[$key]['disbursementFund']['am'].'</td>' .
// 											'<td>'. $child[$key]['disbursementFund']['os'].'</td>' .
// 											'<td>'. $child[$key]['disbursementFund']['total'].'</td>' .

										
// 											'</tr>';

// 				}
// 					$tablefooter = '<tfoot>
//                             <tr>
                       
//                                 <th></th>
//                                 <th class="no-sort">'.$obligationFundaf.'</th>
//                                 <th class="no-sort">'.$obligationFundam.'</th>
//                                 <th class="no-sort">'.$obligationFundos.'</th>
//                                 <th class="no-sort">'.$taxFundaf.'</th>
//                                 <th class="no-sort">'.$taxFundam.'</th>
//                                 <th class="no-sort">'.$taxFundos.'</th>
//                                 <th class="no-sort">'.$taxFundtotal.'</th>
//                                 <th class="no-sort">'.$disbursementFundaf.'</th>
//                                 <th class="no-sort">'.$disbursementFundam.'</th>
//                                 <th class="no-sort">'.$disbursementFundos.'</th>
//                                 <th class="no-sort">'.$disbursementFundtotal.'</th>

//                             </tr>
//                     <tfoot>';	
// 					$tableend = '</tbody></table>';

					
// 				//die();
// 					return $topheader .  $title . $tablehead . $data . $tablefooter . $tableend;




		
// 	}
// //===============================================================================================================================		
	
 }
?>