  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }

//=======================================================
      public function getNCAData(){
        //var_dump($_POST['date']); die();
        $theDate = $_POST['date'];
     $url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataByMonthAndYearNCAINACTIVE/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        //var_dump($ret); die();
        return $ret;

    }
  //=======================================================
 public function getAllots(){
       // $url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataBAF306B/date/2018-04-10";
     $date = $_POST['date'];

     //var_dump($date);
        $url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataByMonthAndYearINACTIVE/date/".$date;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($ret); die();
        return $ret;



    }
//   //=======================================================
 public function getOrsdata(){
      $theDate = $_POST['date'];
        $url = $GLOBALS['backend_URL']."ORS/API/v1/getDataByMonthAndYearINACTIVE/date/".$theDate;
        $ret = Helper::serviceGet($url);
        return $ret;



    }
//========================================================
     public function restoreOrsData(){
      $refId = $_POST['refId'];
      $url = $GLOBALS['backend_URL']."ORS/API/v1/activateORS/refid/".$refId;     
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);  
      //var_dump($ret); die();     
      return $ret;


    }
//========================================================
     public function restoreNCAData(){
      $refId = $_POST['refId'];
      $url = $GLOBALS['backend_URL']."ORSF/API/v1/activateNCA/refid/".$refId;   
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);  
      //var_dump($ret); die();     
      return $ret;


    }

//========================================================
     public function restoreAllotData(){
      $refId = $_POST['refId'];

     // var_dump($refId); die();
     $url =  $GLOBALS['backend_URL']."ORSF/API/v1/activateORSF/refid/".$refId;   
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);  
      //var_dump($ret); die();     
      return $ret;


    }

// //=======================================================
//     public function activateuser($id){
//         $data = array(
//             "status"=>"ACTIVE"
//             );
//         $this->db->where("userid",$id);
//         return $this->db->update("webusers",$data);
//     }
    
//     public function deactivateuser($id){
//         $data = array(
//             "status"=>"INACTIVE"
//             );
//         $this->db->where("userid",$id);
//         return $this->db->update("webusers",$data);
//     }

}

?>