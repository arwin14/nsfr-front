<?php
class UserLevelConfigCollection extends Helper {


 public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");

    }

    public function getuserlevels(){
        $url = $GLOBALS['userlevel']."UserLevel/API/v1/getAllData";
        $ret = Helper::serviceGet($url);
        //var_dump($ret); die();
        $result = json_encode($ret);
        return $ret;
    }

    public function getModules(){
        $url = $GLOBALS['userlevel']."UserLevelModule/API/v1/getAllData";
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }

    public function getUserModule(){
        //var_dump($GLOBALS['userlevel']."UserLevel/API/v1/selectUserlevel/id/".$_POST['idbase']); die();
        $url = $GLOBALS['userlevel']."UserLevel/API/v1/selectUserlevel/id/".$_POST['idbase'];
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        return $result;
    }

     public function addDatauserlevel($datajson){
      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['userlevel']."UserLevel/API/v1/insertlevel");
      echo json_encode($ret);
    }
    
    public function updateDatauserlevel($datajson){
      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['userlevel']."UserLevel/API/v1/updatelevel");
      //var_dump($ret); die();
      echo json_encode($ret);
    }

       public function deleteUsers($postdata){
        $id = $postdata['idbase'];
        $url = $GLOBALS['login_url']."UserLevel/API/v1/deactivate/id/".$id;     
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url);    
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
        $ret  = curl_exec($ch);  
        var_dump($ret); die();
        return $ret;
    }
   
}
//echo '<pre>' . var_export($_POST, true) . '</pre>'; die();
?>

