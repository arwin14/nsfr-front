  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


     public function addUsers(){
       $arr =
        array(

            "username" => $_POST['username'],
            "userlevel" =>  $_POST['user_level'],
            "firstname" =>  $_POST['fname'],
            "middlename" =>  $_POST['mname'],
            "lastname" =>  $_POST['lname'],
            "gender" =>  $_POST['gender'],
            //"email" => $_POST['email_address'],
            "filepath" => $pathtosend

            );

      $result = json_encode($arr);
      var_dump($result); die();
      $ret = Helper::serviceCallBudget($result,  $GLOBALS['login_url']."API/v1/insert");
      echo json_encode($ret);

     }



       public function updateUsers(){
       $arr =
        array(

            "id" => $_POST['id'],
            "userlevel" =>  $_POST['user_level'],
            "firstname" =>  $_POST['fname'],
            "middlename" =>  $_POST['mname'],
            "lastname" =>  $_POST['lname'],
            "gender" =>  $_POST['gender'],
            "email" => $_POST['email_address']

            );
      $result = json_encode($arr);
      $ret = Helper::serviceCallBudget($result,  $GLOBALS['login_url']."API/v1/update");
      echo json_encode($ret);

     }

     public function getuserlevels(){
        $url = $GLOBALS['userlevel']."UserLevel/API/v1/getAllData";
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        return $result;
    }


    // //========================================================
   public function getUsers(){
        $url = $GLOBALS['login_url']."API/v1/getAllData";
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;

    }

    // //========================================================
   public function deleteUsers($postdata){
        $id = $postdata['idbase'];
        $url = $GLOBALS['login_url']."API/v1/deactivate/id/".$id;     
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url);    
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
        $ret  = curl_exec($ch);
      //var_dump($url); die();       
      return $ret;


    }


    // //========================================================
   public function getSpecific(){
        $url = $GLOBALS['login_url']."API/v1/getDataById/".$_POST['id'];
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        //var_dump($ret); die();
        return $result;

    }
}
?>