<style type="text/css">
   #background{
   background: rgba(255,255,255,1);
   width: 100%;
   height: 420px;
   border-radius: 100px;
   /*background: 
   linear-gradient(
   rgba(0, 0, 0, 0.45), 
   rgba(0, 0, 0, 0.45)
   ),
   url("<?php echo base_url(); ?>assets/img/background/paper.png");
   width: 100%;
   height: 390px;
   background-size: cover;*/
   }
   .img-preview{
   width: 180px;
   height: 180px;
   border-radius: 100px;
   background-size: cover;
   border: 5px solid lightgrey;
   }
   #imgInp{
   opacity: 0;
   position: relative; 
   top: -180px; 
   height: 190px; 
   width: 190px;
   cursor: pointer;
   margin-bottom: -200px;
   }
   #image_upload{
   height: 180px;
   }
</style>
<div class="main-content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-8">
            <div class="card">
               <div class="header">
                  <h4 class="title">Edit Profile</h4>
               </div>
               <div class="content">
                  <form id="accountInfo">
                     <div class="row">
                        <div class="col-md-5">
                           <div class="form-group">
                              <label>Company (disabled)</label>
                              <input type="text" class="form-control" disabled placeholder="Company" value="MUFG BANK">
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label>Position</label>
                              <input type="text" class="form-control" name="position" value="<?php echo $_SESSION['userlevel']; ?>" readonly>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="exampleInputEmail1">Gender</label>
                              <select name="gender" class="selectpicker" data-title="Select Gender" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
                                 <?php  
                                    if($_SESSION['gender'] == "MALE"){
                                    ?>
                                 <option value="MALE" selected="">Male</option>
                                 <option value="FEMALE">Female</option>
                                 <?php
                                    }else{
                                    ?>
                                 <option value="MALE">Male</option>
                                 <option value="FEMALE" selected="">Female</option>
                                 <?php   
                                    }
                                    ?>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="form-group">
                              <label>First Name</label>
                              <input type="text" class="form-control" placeholder="fname" name="fname" value="<?php echo $_SESSION['firstname']; ?>" required>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label>Middle Name</label>
                              <input type="text" class="form-control" placeholder="mname" name="mname" value="<?php echo $_SESSION['middlename']; ?>" required>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label>Last Name</label>
                              <input type="text" class="form-control" placeholder="lname" name="lname"    value="<?php echo $_SESSION['lastname']; ?>" required>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Email Address</label>
                              <input type="email" class="form-control" placeholder="lname" name="email" value="<?php echo $_SESSION['email']; ?>" required>
                           </div>
                        </div>
                     </div>
                     <input type="hidden" id="userid" name="userid" value="<?php echo $_SESSION['userid']; ?>">
                     <button type="submit" id="btnaccInfo" class="btn btn-info btn-fill pull-right">Update Profile</button>
                     <div class="clearfix"></div>
                  </form>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            
            <div class="card card-user">
               <div class="image">
                  <img src="<?php echo base_url(); ?>assets/img/Mr.png" alt="..."/>
               </div>
               <div class="content">
                  <form id="image_upload" runat="server" enctype="multipart/form-data">
                     <div class="author">
                        <?php if(Helper::get('photopath') == "N/A"): ?>
                        <img src="<?php echo base_url(); ?>assets/img/faces/Mr.png" class = "avatar border-gray img-preview">
                        <?php endif; ?>
                        <?php if(Helper::get('photopath') != "N/A"): ?>
                        <img src="<?php echo base_url(); ?>assets/img/Mr.png" class = "avatar border-gray img-preview ">
                        <?php endif; ?>
                        <input type='file' id="imgInp" name  = "image_file" accept="image/*" required>  
                        <h4 class="title"><?php  echo $_SESSION['firstname'].' '.$_SESSION['lastname']; ?><br />
                           <small>michael24</small>
                        </h4>
                     </div>
                  </form>
                  <center>
                     <button class="btn btn-social btn-simple btn-twitter" data-toggle="collapse" data-target="#passwordForm">
                     <i class="pe-7s-lock"></i> Change Password
                     </button>
                  </center>
                  <div class="collapse" id="passwordForm">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <center> <label>Current Password</label> </center>
                              <input type="password" placeholder="Enter Your Existing Password" class="form-control">
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <center> <label>New Password</label> </center>
                              <input type="password" placeholder="Enter New Password" class="form-control" placeholder="Country">
                           </div>
                        </div>
                     </div>
                     <button class="btn btn-primary btn-fill" style="width:100%;">Submit</button>
                     </right>
                  </div>
               </div>
               <hr>
               <div class="text-center">
                  <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                  <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                  <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/usermanagement/useraccount.js"></script>