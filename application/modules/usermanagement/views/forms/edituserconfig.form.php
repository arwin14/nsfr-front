
<form role="form" id="updateForm" action="<?php echo base_url().'usermanagement/UserConfig/updateUserConfig'; ?>" data-parsley-validate=""  >
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="form-group form-float">
				<label class="form-label">Employee ID <span class="text-danger">*</span></label>
				<div class="form-line">
					<input type="text" name="empid" id="empid" class="form-control" placeholder="Employee ID" data-parsley-required/>
				</div>
			</div>                                            
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="form-group form-float">
				<label class="form-label">Username <span class="text-danger">*</span></label>
				<div class="form-line">
					<input type="text" name="username" id="username" class="form-control" placeholder="Username" style="text-transform: uppercase;" readonly />
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
			<div class="form-group form-float">
				<label class="form-label">User Level <span class="text-danger">*</span></label>
				<div class="form-line">
					<select name="userlevel" id="userlevel" class="form-control"></select>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Area ID <span class="text-danger">*</span></label>
							<div class="form-line">
								<select class="required form-control" id="area" name="area">
								</select>
							</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
				<div class="form-group form-float">						

						<label class="form-label">Device ID <span class="text-danger">*</span></label>
							<div class="form-line">
								<input type="text" class="form-control" id="device" name="device" required="" placeholder="Device ID" required pattern="[0-9]+">								
							</div>
				</div>
			</div>
	</div>
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label class="form-label">First Name <span class="text-danger">*</span></label>
				<div class="form-line">
					<input type="text" name="fname" id="fname" class="form-control" placeholder="First Name" data-parsley-pattern="/^[a-zA-Z\s ]*$/" data-parsley-pattern-message="Must only contain characters and spaces." data-parsley-required/>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label class="form-label">Middle Name</label>
				<div class="form-line">
					<input type="text" name="mname" id="mname" class="form-control" placeholder="Middle Name" data-parsley-pattern="/^[a-zA-Z\s ]*$/" data-parsley-pattern-message="Must only contain characters and spaces."/>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group form-float">
				<label class="form-label">Last Name <span class="text-danger">*</span></label>
				<div class="form-line ">
					<input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name" data-parsley-pattern="/^[a-zA-Z\s ]*$/" data-parsley-pattern-message="Must only contain characters and spaces." data-parsley-required/>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group form-float">
				<label class="form-label">Suffix</label>
				<div class="form-line">
					<select name="suffix" id="suffix" class="form-control" >
						<option value="">-- Suffix --</option>
						<option value="JR">Jr.</option>
						<option value="SR">Sr.</option>
						<option value="II">II</option>
						<option value="III">III</option>
						<option value="IV">IV</option>
						<option value="V">V</option>
						<option value="VI">VI</option>
						<option value="VII">VII</option>
						<option value="VIII">VIII</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<div class="form-group form-float">
				<label class="form-label">Gender <span class="text-danger">*</span></label>
				<div class="form-line">
					<select name="gender" id="gender" class="form-control">
						<option value="">-- Gender --</option>
						<option value="M">Male</option>
						<option value="F">Female</option>
					</select>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<div class="form-group form-float">
				<label class="form-label">Email Address </label>
				<div class="form-line ">
					<input type="email" name="email" id="email" class="form-control" placeholder="Email" data-parsley-type="email" data-parsley-required>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<div class="form-group form-float">
				<label class="form-label">Position </label>
				<div class="form-line ">
					<input type="text" name="position" id="position" class="form-control" placeholder="Last Name" data-parsley-pattern="/^[a-zA-Z\s ]*$/" data-parsley-pattern-message="Must only contain characters and spaces." data-parsley-required/>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Classification <span class="text-danger">*</span></label>
					<div class="form-line">
						<select class="required form-control" name="classification" id="classification">
							<option value="" selected>-- Classify --</option>
							<option value="Job Order">Job Order</option>
							<option value="Casual">Casual</option>
						</select>
					</div>
				</div>
			</div>
	</div>

	<!--<div class="row pull-right">
		<div class="col-md-12">
			<?php if(Helper::role(ModuleRels::UPDATE_USER)): ?>
			<button id="bEdit" type="button" class="btn btn-sm btn-info btn-fill"><i class="material-icons">mode_edit</i> Update</button>
			<?php endif; ?>
			<?php if(Helper::role(ModuleRels::VIEW_PASSWORD)): ?>
			<button id="bPassword" type="button" class="btn btn-sm btn-warning btn-fill"><i class="material-icons">visibility</i> Password</button>
			<?php endif; ?>
			<?php if(Helper::role(ModuleRels::RESTART_SESSION)): ?>
			<button id="bRestart" type="button" class="btn btn-sm btn-success btn-fill"><i class="material-icons">cached</i> Restart</button>
			<?php endif; ?>
			<?php if(Helper::role(ModuleRels::ACTIVATE_DEACTIVATE_USER)): ?>
			<button id="bActivate" type="button" class="btn btn-sm btn-success btn-fill"><i class="material-icons">add_circle_outline</i> Activate</button>
			<button id="bDeactivate" type="button" class="btn btn-sm btn-fill"><i class="material-icons">remove_circle_outline</i> Deactivate</button>
			<?php endif; ?>
			<?php if(Helper::role(ModuleRels::UNLOCK_LOCK_USER)): ?>
			<button id="bLock" type="button" class="btn btn-sm btn-danger btn-fill"><i class="material-icons">lock_outline</i> Lock</button>			
			<button id="bUnlock" type="button" class="btn btn-sm btn-primary btn-fill"><i class="material-icons">lock_open</i> Unlock</button>
			<?php endif; ?>
			<button type="button" class="btn btn-warning btn-sm waves-effect" data-dismiss="modal"><i class="material-icons">close</i> Close</button>
		</div>
	</div>-->
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-5">
				<label>Created By: &nbsp;</label><span id="createdBy"></span>
			</div>
			<div class="col-md-7">
				<label>Creation Timestamp: &nbsp;</label><span id="dateCreated"></span>
			</div>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-12" style="margin-left: -8px;">
			<div class="col-md-4">
			<?php if(Helper::role(ModuleRels::UPDATE_USER)): ?>
			<button id="bEdit" type="button" class="btn btn-sm btn-info btn-fill" style="width: 170px;"><i class="material-icons">mode_edit</i> Update</button>
			<?php endif; ?>
			</div>
			<div class="col-md-4">
			<?php if(Helper::role(ModuleRels::VIEW_PASSWORD)): ?>
			<button id="bPassword" type="button" class="btn btn-sm btn-warning btn-fill" style="width: 170px;"><i class="material-icons">visibility</i> Password</button>
			<?php endif; ?>
			</div>
			<div class="col-md-4">
			<?php if(Helper::role(ModuleRels::VIEW_OR_RANGE)): ?>
			<button id="bRange" type="button" class="btn btn-sm waves-effect" style="width: 170px; background-color: #39a88e !important; color:white;"><i class="material-icons">settings</i> OR Range</button>
			<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-left: -8px;">
			<div class="col-md-4">
			<?php if(Helper::role(ModuleRels::ACTIVATE_DEACTIVATE_USER)): ?>
			<button id="bActivate" type="button" class="btn btn-sm btn-success btn-fill" style="width: 170px;"><i class="material-icons">add_circle_outline</i> Activate</button>
			<button id="bDeactivate" type="button" class="btn btn-sm btn-fill" style="width: 170px;"><i class="material-icons">remove_circle_outline</i> Deactivate</button>
			<?php endif; ?>
			</div>
			<div class="col-md-4">
			<?php if(Helper::role(ModuleRels::UNLOCK_LOCK_USER)): ?>
			<button id="bLock" type="button" class="btn btn-sm btn-danger btn-fill" style="width: 170px;"><i class="material-icons">lock_outline</i> Lock</button>			
			<button id="bUnlock" type="button" class="btn btn-sm btn-primary btn-fill" style="width: 170px;"><i class="material-icons">lock_open</i> Unlock</button>
			<?php endif; ?>
			</div>
			<div class="col-md-4">
			<?php if(Helper::role(ModuleRels::RESTART_SESSION)): ?>
			<button id="bRestart" type="button" class="btn btn-sm btn-success btn-fill" style="width: 170px;"><i class="material-icons">cached</i> Restart</button>
			<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>
	<input type="hidden" name="userid" id="userid" />
	<input type="hidden" name="status" id="status" />
	<input type="hidden" name="userlevelval" id="userlevelval" />
	<input type="hidden" name="areaval" id="areaval" />
</form>