<form id="adduserlevelrecordform" data-direct="UserLevelConfig/adduserlevel" data-function="InitData" data-form="#adduserlevelrecordform" class="uppercase" accept-charset="utf-8">
   <div class = "row">
      <div class = "col-md-4">
         <div class = "form-group">
            <label>User Level Name:</label>
            <input type="text" class = "form-control" name="userlevelname" id = "userlevelname" required>
         </div>
         <div class = "form-group">
            <label>Description:</label>
            <input type="text" class = "form-control" name="description" id = "description" required>
         </div>
         <div class = "form-group">
            <label>Status:</label>
            <select class="form-control" id = "status" name = "mstatus" required>
               <option value="ACTIVE">Active</option>
               <option value="INACTIVE">Inactive</option>
            </select>
         </div>
         <div class = "form-group">
            <button type="submit" id = "btn-adduserlevel" class="btn btn-success" style="width: 100%;">Add User Level</button>
         </div>
      </div>
      <div class = "col-md-8" style="">
         <div class="alert alert-warning" id="validateError">
            <span><b> Warning - </b> This is a regular notification made with ".alert-warning"</span>
         </div>
         <div class="table-responsive" >
            <table id="addroletbl" class="table table-hover table-striped" style="width:100%;">
               <thead>
                  <tr>
                     <th></th>
                     <th>Description</th>
                     <th>Status</th>
                     <th class="text-center">
                        <label class="checkbox">
                        <input type="checkbox" id="checkboxall" data-key=".ckdata"  type="checkbox">
                        <label for="checkboxall">
                        </label>
                        </label>
                     </th>
                  </tr>
               </thead>
               <tbody>
                  <?php 
                     foreach ($tbldataAvail as $index => $value) {                      
                         $id = $tbldataAvail[$index]['id'];
                         ?>
                  <tr>
                     <td><?php echo $tbldataAvail[$index]['module']; ?></td>
                     <td><?php echo $tbldataAvail[$index]['description']; ?></td>
                     <td><?php echo $tbldataAvail[$index]['status']; ?></td>
                     <td>
                        <label class="checkbox">
                        <input type="checkbox" id="checkbox<?php echo $id; ?>" name="module[]" class="ckdata" type="checkbox" value="<?php echo $tbldataAvail[$index]['module']; ?>">
                        <label for="checkbox<?php echo $id; ?>">
                        </label>
                        </label>
                     </td>
                  </tr>
                  <?php }
                     ?>
               </tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   </div>
   </div>
</form>