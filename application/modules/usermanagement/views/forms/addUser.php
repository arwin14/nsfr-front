<?php
   $userlevel = $dropdown->ResponseResult;
 ?>
<style type="text/css">
   #background{
   background: rgba(255,255,255,1);
   width: 100%;
   height: 420px;
   border-radius: 100px;
   /*background: 
   linear-gradient(
   rgba(0, 0, 0, 0.45), 
   rgba(0, 0, 0, 0.45)
   ),
   url("<?php echo base_url(); ?>assets/img/background/paper.png");
   width: 100%;
   height: 390px;
   background-size: cover;*/
   }
   .img-preview{
   width: 180px;
   height: 180px;
   border-radius: 100px;
   background-size: cover;
   border: 5px solid lightgrey;
   }
   #imgInp{
   opacity: 0;
   position: relative; 
   top: -180px; 
   height: 190px; 
   width: 190px;
   cursor: pointer;
   margin-bottom: -200px;
   }
   #image_upload{
   height: 180px;
   }
</style>
<form id = "form_add_user" class="uppercase" accept-charset="utf-8">
   <div class="row" style="background:;">
      <div class="col-md-6">
         <br><br><br>
         <div class="card card-user" style="height:200px; background:;">
            <div class="content">
               <div class="author">
                  <?php if(Helper::get('photopath') == "N/A"): ?>
                  <img src="<?php echo base_url(); ?>assets/img/faces/face-0.jpg" class = "avatar border-gray img-preview">
                  <?php endif; ?>
                  <?php if(Helper::get('photopath') != "N/A"): ?>
                  <img src="<?php echo base_url(); ?>assets/img/Ana1.png" class = "avatar border-gray img-preview ">
                  <?php endif; ?>
                  <input type='file' id="imgInp" name  = "image_file" accept="image/*" >  
                  <h4 class="title">Upload Image<br />
                  </h4>
               </div>
            </div>
            <div class="text-center">
            </div>
            <button type="submit" id="btnadd" class = "btn btn-success waves-effect" style="width: 100%;">Submit</button>
         </div>
      </div>
      <div class="col-md-6">
         <div class = "container-fluid" style="width:95%;">
            <div class = "form-group">
               <label>Username:</label>
               <div class = "form-line">
                  <input type="text" class="form-control" id = "username" name="username" placeholder="Username:" required>
               </div>
            </div>
            <br>
            <div class = "form-group">
               <label>Password:</label>
               <div class = "form-line">
                  <input type="password" id = "password" class="form-control" name="password" placeholder="Password:" required>
               </div>
            </div>
            <br>
            <div class = "form-group">
               <label>User Level:</label>
               <div class = "form-line">
                  <select class="form-control" id = "user_level" name="user_level" required>
                     <?php foreach ($userlevel as $num => $myval) { 
                        ?> 
                     <option value="<?php echo $userlevel[$num]->name;  ?>"><?php echo $userlevel[$num]->name;  ?></option>
                     <?php } ?>
                  </select>
                  </select>
               </div>
            </div>
            <br>
            <div class = "form-group">
               <label>Email Address:</label>
               <div class = "form-line">
                  <input type="email" class="form-control" id = "email_address" name="email_address" placeholder="Email Address:" required>
               </div>
            </div>
            <br>
            <div class = "form-group">
               <label>First Name:</label>
               <div class = "form-line">
                  <input type="text" class="form-control" name="fname" id = "fname" placeholder="First Name:" required>
               </div>
            </div>
            <br>
            <div class = "form-group">
               <label>Middle Name:</label>
               <div class = "form-line">
                  <input type="text" class="form-control" name="mname" id = "mname" placeholder="Middle Name:" required>
               </div>
            </div>
            <br>
            <div class = "form-group">
               <label>Last Name:</label>
               <div class = "form-line">
                  <input type="text" class="form-control" name="lname" id = "lname" placeholder="Last Name:" required>
               </div>
            </div>
            <br>
            <div class = "form-group">
               <label>Gender:</label>
               <div class = "form-line">
                  <select class="form-control" id = "gender" name = "gender" required>
                     <option value="MALE">Male</option>
                     <option value="FEMALE">Female</option>
                  </select>
               </div>
            </div>
         </div>
         <br>
         <div class = "container-fluid">
         </div>
      </div>
</form>
</div>