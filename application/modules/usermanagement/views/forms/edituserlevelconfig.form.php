<?php
   $name = $userModule->ResponseResult->name;
   $description = $userModule->ResponseResult->description;
   $status = $userModule->ResponseResult->status;
   $module = $userModule->ResponseResult->modules;
   $userid = $userModule->ResponseResult->id;
      //var_dump($userid);
   ?>
<form id="updateuserlevelrecordform" data-direct="UserLevelConfig/updateuserlevel" data-function="InitData" data-form="#updateuserlevelrecordform" class="uppercase" accept-charset="utf-8">
   <div class="container-fluid">
      <div class="card">
         <div class="content">
            <div class="row">
               <div class = "col-md-4">
                  <div class = "form-group">
                     <label>User Level Name:</label>
                     <input type="text" class = "form-control" name="userlevelname" id = "userlevelname"  value="<?php echo $userModule->ResponseResult->name; ?>" required>
                  </div>
                  <div class = "form-group">
                     <label>Description:</label>
                     <input type="text" class = "form-control" name="description" id = "description" value="<?php echo $userModule->ResponseResult->description; ?>" required>
                  </div>
                  <div class = "form-group">
                     <label>Status:</label>
                     <select class="form-control" id = "status" name = "mstatus" required>
                        <?php
                           if($status == "ACTIVE"){
                        ?>          
                        <option value="ACTIVE" selected>Active</option>
                        <option value="INACTIVE">Inactive</option>
                         <?php }else{ ?>
                        <option value="ACTIVE" selected>Active</option>
                        <option value="INACTIVE">Inactive</option>
                        <?php }
                        ?>
                     </select>
                     <input type="hidden" name="userid" value="<?php echo $userid; ?>">
                  </div>
                  <div class = "form-group">
                     <button type="submit" id = "btn-adduserlevel" class="btn btn-success" style="width: 100%;">Update User Level</button>
                  </div>
               </div>
               <div class = "col-md-8" style="">
                  <div class="alert alert-warning" id="updatevalidateError" style="display:none;">
                     <span><b> Warning - </b>Provide module role at least one</span>
                  </div>
                  <div class="table-responsive"  >
                     <table id="updateroletbl" class="table table-hover table-striped" style="width:100%; height:100px;">
                        <thead>
                           <tr>
                              <th></th>
                              <th>Description</th>
                              <th>Status</th>
                              <th class="text-center">
                                 <label class="checkbox">
                                 <input type="checkbox"  data-key=".ckdatamoduleupdate" class="ckdatamoduleupdate" id="checkboxallUpdate">
                                 <label for="checkboxallUpdate">
                                 </label>
                                 </label>
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 
                              foreach ($tbldataAvail as $index => $value) {                      
                                  $id = $tbldataAvail[$index]['id'];
                                  $chstatus = (in_array($tbldataAvail[$index]['module'], $module))? "checked":""
                              ?>
                           <tr>
                              
                              <td><?php echo $tbldataAvail[$index]['module']; ?></td>
                              <td><?php echo $tbldataAvail[$index]['description']; ?></td>
                              <td><?php echo $tbldataAvail[$index]['status']; ?></td>
                              <td>
                                 <label class="checkbox">
                                 <input type="checkbox" id="checkboxupdate<?php echo $id; ?>" name="moduleupdate[]" class="ckdatamoduleupdate" type="checkbox" value="<?php echo $tbldataAvail[$index]['module']; ?>" <?php echo $chstatus; ?> >
                                 <label for="checkboxupdate<?php echo $id; ?>">
                                 </label>
                                 </label>
                              </td>
                           </tr>
                           <?php }
                              ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>