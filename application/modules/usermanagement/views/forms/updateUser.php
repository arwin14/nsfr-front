<?php
    $value = $data->ResponseResult; 
    $userlevel = $dropdown->ResponseResult;
?>
<form id = "form_update_user" class="uppercase" accept-charset="utf-8">
   <div class="row" style="background:;">
      <div class="col-md-6 " >
         <br><br><br>
         <div class="card card-user">
            <div class="content">
               <div class="author">
                  <a href="#">
                     <img class="avatar border-gray"  src="<?php echo base_url(); ?>assets/img/full-screen-image-3.jpg" alt="..."/>
                     <h4 class="title" id="wholeName">Tania Andrew<br />
                     </h4>
                     <p id="titleUsername">michael24</p>
                  </a>
               </div>
               <p class="description text-center" id="titlePosition"> ADMIN <br>
               </p>
            </div>
            <div class="text-center">
            </div>

         </div>
          <button type="button" id="btnupdate" data-id="" class = "btn btn-success waves-effect" style="width: 100%;">Submit</button>
       <!--  
          <button class="btn btn-success btn-fill btn-wd" style="width:100%;">Success</button>
          <button class="btn btn-warning btn-fill btn-wd" style="width:100%;">Success</button>
            <div class="row" >
          <div class="col-md-6 content">
            <button class="btn btn-success btn-fill btn-wd " style="width:100%;">Success</button>
          <button class="btn btn-warning btn-fill btn-wd" style="width:100%;">Success</button>
          </div>
          <div class="col-md-6 content">
            <button class="btn btn-success btn-fill btn-wd" style="width:100%;">Success</button>
          <button class="btn btn-warning btn-fill btn-wd" style="width:100%;">Success</button>
          </div>
      </div> -->
      </div>

      <div class="col-md-6 col-md-offset-s1">
         <div class = "form-group">
            <label>Username:</label>
            <div class = "form-line">
               <input type="text" class="form-control" id = "username" name="username" value ="<?php echo $value->username; ?>" placeholder="Username:">
            </div>
         </div>
         <br>
         <div class = "form-group">
            <label>User Level:</label>
            <div class = "form-line">
               <select class="form-control" id = "user_level" name="user_level">
                <?php foreach ($userlevel as $num => $myval) { 
                  if($value->username == $userlevel[$num]->name){ 
                  ?> <option value="<?php echo $userlevel[$num]->name;  ?>" selected><?php echo $userlevel[$num]->name;  ?></option>
                  <?php
                    }else{
                   ?>
                  <option value="<?php echo $userlevel[$num]->name;  ?>"><?php echo $userlevel[$num]->name;  ?></option>
                 <?php }} ?>
               </select>
            </div>
         </div>
         <br>

         <div class = "form-group">
            <label>Email Address:</label>
            <div class = "form-line">
               <input type="email" class="form-control" id = "email_address" name="email_address" value="<?php echo $value->email; ?>"  placeholder="Email Address:">
            </div>
         </div>
         <br>
         <div class = "form-group">
            <label>First Name:</label>
            <div class = "form-line">
               <input type="text" class="form-control" name="fname" id = "fname" value ="<?php echo $value->firstname; ?>" placeholder="First Name:">
            </div>
         </div>
         <br>
         <div class = "form-group">
            <label>Middle Name:</label>
            <div class = "form-line">
               <input type="text" class="form-control" name="mname" id = "mname" value ="<?php echo $value->middlename; ?>" placeholder="Middle Name:">
            </div>
         </div>
         <br>
         <div class = "form-group">
            <label>Last Name:</label>
            <div class = "form-line">
               <input type="text" class="form-control" name="lname" id = "lname" value ="<?php echo $value->lastname; ?>" placeholder="Last Name:">
            </div>
         </div>
         <br>
         <div class = "form-group">
            <label>Gender:</label>
            <div class = "form-line">
                <?php 
                //$status = ( $value->gender == 'M'):
                var_dump($value->gender);
                ?>
               <select class="form-control" id = "gender" name = "gender">
                  <?php if( $value->gender == "MALE"){ ?>
                  <option value="MALE" selected>Male</option>
                  <option value="FEMALE">Female</option>
                  <?php }else{ ?>
                  <option value="MALE">Male</option>
                  <option value="FEMALE" selected>Female</option>
                  <?php
                }
                ?>
               </select>
            </div>
         </div>
         <br>
         <div class = "form-group">
            <label>Status:</label>
            <div class = "form-line">
               <select class="form-control" id = "status" name = "status">
                  <option value="GEN_PW">ACTIVE</option>
                  <option value="INACTIVE">INACTIVE</option>
                
               </select>
            </div>
         </div>
      </div>
      <br>
      <hr>
   </div>
   </div>
   <br>
   </div>
   <input type="hidden" class="form-control" value ="<?php echo $value->id; ?>" name="id" id = "id" placeholder="Last Name:">
   <div class = "container-fluid">
     
   </div>
</form>