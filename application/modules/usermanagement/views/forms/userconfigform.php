<?php 
	//var_dump($data);die();
	$userid = (isset($data->Data->details[0]->userid))?$data->Data->details[0]->userid:"";
	$area_id = (isset($data->Data->details[0]->area_id))?$data->Data->details[0]->area_id:"";
	$device_id = (isset($data->Data->details[0]->device_id))?$data->Data->details[0]->device_id:"";
	$entityid = (isset($data->Data->details[0]->entityid))?$data->Data->details[0]->entityid:"";
	$username = (isset($data->Data->details[0]->username))?$data->Data->details[0]->username:"";
	$firstname = (isset($data->Data->details[0]->firstname))?$data->Data->details[0]->firstname:"";
	$middlename = (isset($data->Data->details[0]->middlename) && $data->Data->details[0]->middlename != "n/a")?$data->Data->details[0]->middlename:"";
	$lastname = (isset($data->Data->details[0]->lastname))?$data->Data->details[0]->lastname:"";
	$suffix = (isset($data->Data->details[0]->suffix) && $data->Data->details[0]->suffix != "n/a")?$data->Data->details[0]->suffix:"";
	$email = (isset($data->Data->details[0]->email))?$data->Data->details[0]->email:"";
	$gender = (isset($data->Data->details[0]->gender))?$data->Data->details[0]->gender:"";	
	$userlevelname = (isset($data->Data->details[0]->userlevelname))?$data->Data->details[0]->userlevelname:"";
	$position = (isset($data->Data->details[0]->position))?$data->Data->details[0]->position:"";
	$status = (isset($data->Data->details[0]->status))?$data->Data->details[0]->status:"";

    $display = "show";
    if($key == "updateUserConfig"){
        $display = "none";
    }
?>
<form id="<?php echo $key ?>" action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$key; ?>" method="POST">
    <div class="form-elements-container" style="display:<?php echo $display; ?>">
    	<input type="hidden" name="UserId" value="<?php echo $userid; ?>">
    	<input type="hidden" name="Status" value="<?php echo ($status == 'ACTIVE')?'1':'0'; ?>">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Employee ID <span class="text-danger">*</span></label>
					<div class="form-line ">
						<input type="text" class="empid required form-control" placeholder="Employee ID" name="EmpId" id="EmpId" required="" aria-required="true">
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Username <span class="text-danger">*</span></label>
					<div class="form-line ">
						<input type="text" class="required form-control font-italic" pattern=".{4,}" style="text-transform:uppercase;" placeholder="USERNAME (MIN. 4 CHARS)" id="username" name="Username" required="" aria-required="true">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">User Level <span class="text-danger">*</span></label>
					<div class="form-line">
						<select class="required form-control" name="UserLevelId">
						</select>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Area ID <span class="text-danger">*</span></label>
							<div class="form-line">
								<select class="required form-control" name="AreaId">
								</select>
							</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
				<div class="form-group form-float">						

						<label class="form-label">Device ID <span class="text-danger">*</span></label>
							<div class="form-line">
								<input type="text" class="form-control" name="DeviceId" placeholder="Device ID" required="" required pattern="[0-9]+">								
							</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">First Name <span class="text-danger">*</span></label>
					<div class="form-line ">
						<input type="text" class="required form-control full-name" name="FirstName" placeholder="First Name" required="" aria-required="true">
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Middle Name</label>
					<div class="form-line ">
						<input type="text" class="form-control full-name" name="MiddleName" placeholder="Middle Name">
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Last Name <span class="text-danger">*</span></label>
					<div class="form-line ">
						<input type="text" class="required form-control full-name" name="LastName" placeholder="Last Name" required="" aria-required="true">
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
	            <div class="form-group form-float">
	            	<label class="form-label">Suffix</label>
	                <div class="form-line">
	                    <select class="form-control" name="Suffix">
	                    	<?php $suffix = Helper::get('suffix'); ?>
	                        <option value="" selected>-- Suffix --</option>
	                        <option value="JR">JR</option>
							<option value="SR">SR</option>
							<option value="II">II</option>
							<option value="III">III</option>
							<option value="IV">IV</option>
							<option value="V">V</option>
							<option value="VI">VI</option>
							<option value="VII">VII</option>
							<option value="VIII">VIII</option>
	                    </select>
	                </div>
	            </div>
	        </div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Gender <span class="text-danger">*</span></label>
					<div class="form-line">
						<select class="required form-control" name="Gender">
							<option value="" selected>-- Gender --</option>
							<option value="M">Male</option>
							<option value="F">Female</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Email Address </label>
					<div class="form-line ">
						<input type="text" class="email form-control" name="Email" placeholder="Email Address">
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Position </label>
					<div class="form-line ">
						<input type="text" class="required form-control position" name="Position" placeholder="Position">
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<div class="form-group form-float">
					<label class="form-label">Classification <span class="text-danger">*</span></label>
					<div class="form-line">
						<select class="required form-control" name="Classification" required="" aria-required="true">
							<option value="" selected>-- Classify --</option>
							<option value="Job Order">Job Order</option>
							<option value="Casual">Casual</option>
						</select>
					</div>
				</div>
			</div>
		</div> 
    </div>

    <div class="row">
    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    	<div class="form-check">
			    <input type="checkbox" class="form-check-input" name="ORRange" id="ORRange">
			    <label class="form-check-label" for="ORRange">OR Number Range</label>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		    <div class="text-right" style="width:100%;">
		        <button id="saveUserConfig" class="btn btn-primary btn-sm waves-effect" type="submit" style="display:<?php echo $display; ?>"
		        	data-toggle="tooltip" data-placement="top" title="Register New User">
		            <i class="material-icons">person_add</i>
		        </button>
		        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class=""></i>Close</button>
		    </div>
		</div>
	</div>
</form>

