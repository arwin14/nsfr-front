<?php
class UserConfig extends MX_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}









	public function addModal(){
	$retuserlevel = $this->UserConfigCollection->getuserlevels($_POST);
	$data['dropdown'] = $retuserlevel;
	
	$result = array();
	$result['form'] = $this->load->view('forms/addUser.php', $data, TRUE);
	//var_dump($result); die();
		echo json_encode($result);
	}

	public function UpdateModal(){
		$ret = $this->UserConfigCollection->getSpecific($_POST);
		$retuserlevel = $this->UserConfigCollection->getuserlevels($_POST);
		$data['dropdown'] = $retuserlevel;
		$data['data'] = $ret;
		$result = array();
		$result['form'] = $this->load->view('forms/updateUser.php', $data, TRUE);
		echo json_encode($result);
	}

	public function addUser(){
		$ret = $this->UserConfigCollection->addUsers($_POST);
		echo $ret;
	}

	public function updateUser(){
		$ret = $this->UserConfigCollection->updateUsers($_POST);
		echo $ret;
	}

	public function deleteUser(){
		$ret = $this->UserConfigCollection->deleteUsers($_POST);
		echo $ret;
	}


	// public function getSpecific(){
	// 	$ret = $this->UserConfigCollection->getSpecific($_POST);
	// 	echo $ret;
	// }


	public function getUser(){
		$ret = $this->UserConfigCollection->getUsers($_POST);


		$arrsSTD = json_decode($ret);
		$arrs = json_decode( json_encode($arrsSTD), true);
		$result = $arrs['ResponseResult'];
		$return = array();
		//var_dump($result); die();
		if($result == "NO DATA FOUND"){
			$return[] = array(
			 "id" => '',
			 "username" => 'no data',
			 "userlevel" => 'no data',
			 "fname" => 'no data',
			 "mname" => 'no data',
			 "lname" => 'no data',
			 "email" => 'no data',
			 "status" => 'no data',
			 "btn" => 'no data'
			);
		}else{	
		foreach ($result as $key => $value) {
		$return[] = array(
			 "id" => $result[$key]['id'],
			 "username" => $result[$key]['username'],
			 "userlevel" => $result[$key]['userlevel'],
			 "fname" => $result[$key]['firstname'],
			 "lname" => $result[$key]['lastname'],
			 "email" => $result[$key]['email'],
			 "status" => $result[$key]['status'],
			 "btn" => "<a class='btn btn-simple btn-warning btn-icon editbtn' data-id='".$result[$key]['id']."' data-type='".$result[$key]['id']."'><i class='fa fa-edit'></i></a>".
			 "<a  class='btn btn-simple btn-danger btn-icon deletebtn' data-id='".$result[$key]['id']."'><i class='fa fa-times'></i></a>"
			 // "btn" => "<a class='btn btn-xs btn-social btn-fill btn-reddit editbtn' data-id='".$result[$key]['id']."' data-type='".$result[$key]['id']."'>
    //                                         <i class='fa fa-edit'></i> Select
    //                                     </a>"
			);
		}
		}

		$result = json_encode($return);
		//var_dump($result); die();
		echo $result;
		
			}
}

?>