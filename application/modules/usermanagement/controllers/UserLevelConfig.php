	<?php




	class UserLevelConfig extends MX_Controller
	{

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
	    // $this->load->model('ModuleRels');
	    $this->load->model('UserLevelConfigCollection');
	}

	public function index()
	{
	    // var_dump("test");die();
	    $this->Helper->sessionEndedHook('Session');
	    $this->Helper->setTitle('User Configuration');
	    $this->Helper->setView('userlevelconfig.form', '', FALSE);
	    $this->Helper->setTemplate('templates/mastertemplate');
	}



	public function getuserlevel()
	{
	    $ret     = $this->UserLevelConfigCollection->getuserlevels($_POST);
	    $arrsSTD = json_decode($ret);
	    $arrs    = json_decode(json_encode($arrsSTD), true);
	    $result  = $arrs['ResponseResult'];
	    $return  = array();
	    // var_dump($result); die();
	    if ($result == "NO DATA FOUND") {
	        $return[] = array(
	            "id" => '',
	            "name" => 'No Data Found',
	            "description" => '',
	            "status" => ''
	        );
	    } else {
	        foreach ($result as $key => $value) {
	            //var_dump($result[$key]['name']);
	            $return[] = array(
	                "id" => $result[$key]['id'],
	                "name" => $result[$key]['name'],
	                "description" => $result[$key]['description'],
	                "status" => $result[$key]['status'],
	                "btn" => "<a class='btn btn-simple btn-warning btn-icon editbtn' data-refid='" . $result[$key]['id'] . "' data-direct='UserLevelConfig/UpdateModal'><i class='fa fa-edit'></i></a>" . "<a  class='btn btn-simple btn-danger btn-icon remove'  data-refid='" . $result[$key]['id'] . "' ><i class='fa fa-times'></i></a>"
	            );
	        }
	    }
	    
	    $result = json_encode($return);
	    echo $result;
	}

	public function bootformStart()
	{
	    
	    $ret = $this->UserLevelConfigCollection->getModules($_POST);
	    
	    $result  = array();
	    $arrsSTD = json_decode($ret);
	    $arrs    = json_decode(json_encode($arrsSTD), true);
	    $result  = $arrs['ResponseResult'];
	    $return  = array();
	    foreach ($result as $a => $value) {
	        $return['tbldataAvail'][$a] = array(
	            "id" => $result[$a]['id'],
	            "module" => $result[$a]['module'],
	            "description" => $result[$a]['description'],
	            "status" => $result[$a]['status']
	        );
	    }
	    
	    
	    $result['form'] = $this->load->view('forms/userlevelconfigform.php', $return, TRUE);
	    echo json_encode($result);
	}


	public function UpdateModal()
	{
	    $data    = $this->UserLevelConfigCollection->getUserModule($_POST);
	    $ret     = $this->UserLevelConfigCollection->getModules($_POST);
	    $result  = array();
	    $arrsSTD = json_decode($ret);
	    $arrs    = json_decode(json_encode($arrsSTD), true);		
	    $result  = $arrs['ResponseResult'];
	    $return  = array();
	    
	    //var_dump($data->); die();
	    foreach ($result as $a => $value) {
	        $return['tbldataAvail'][$a] = array(
	            "id" => $result[$a]['id'],
	            "module" => $result[$a]['module'],
	            "description" => $result[$a]['description'],
	            "status" => $result[$a]['status']
	        );
	    }
	    $return['userModule'] = $data;
	    
	    
	    $result['form'] = $this->load->view('forms/edituserlevelconfig.form.php', $return, TRUE);
	    //var_dump($result); die();
	    echo json_encode($result);
	}



public function deleteUser(){
        $ret = $this->UserLevelConfigCollection->deleteUsers($_POST);
        echo $ret;
    }



	public function adduserlevel()
	{
	    
	    $ret = new UserLevelConfigCollection();
	    
	    $requestdata = array(
	        'id' => $_SESSION['userid'],
	        'name' => $_POST['userlevelname'],
	        'description' => $_POST['description'],
	        'module' => $_POST['module'],
	        'status' => $_POST['mstatus']
	    );
	    
	    $datajson = json_encode($requestdata);
	    //var_dump($datajson); die();
	    $ret  = $this->UserLevelConfigCollection->addDatauserlevel($datajson);
	    echo $ret;
	}

	public function updateuserlevel()
	{
	    $requestdata = array(
	        'userleveid' => $_POST['userid'],
	        'name' => $_POST['userlevelname'],
	        'description' => $_POST['description'],
	        'module' => $_POST['moduleupdate'],
	        'status' => $_POST['mstatus']
	    );
	    
	    $datajson = json_encode($requestdata);
	    $ret  = $this->UserLevelConfigCollection->updateDatauserlevel($datajson);
	    echo $ret;
	}

	}
	?>