<style>
   td.details-control {
   background: url('<?php echo base_url(); ?>assets/thirdparty/img/reference/details_open.png') no-repeat center center;
   cursor: pointer;
   }
   tr.shown td.details-control {
   background: url('<?php echo base_url(); ?>assets/thirdparty/img/reference/details_close.png') no-repeat center center;
   }
   .mycssvalid {
   -webkit-transition: all 0.30s ease-in-out;
   -moz-transition: all 0.30s ease-in-out;
   -ms-transition: all 0.30s ease-in-out;
   -o-transition: all 0.30s ease-in-out;
   outline: none;
   padding: 3px 0px 3px 3px;
   margin: 5px 1px 3px 0px;
   border: 1px solid #DDDDDD;
   }
   .mycssvalid{
   box-shadow: 0 0 5px rgba(255, 201, 14, 1);
   padding: 3px 0px 3px 3px;
   margin: 5px 1px 3px 0px;
   border: 1px solid rgba(255, 201, 14, 0);
   }
   .highlightthis{
   color: red;
   }
</style>
<div class="row clearfix" style="position:relative; top:0px;">
   <div class="col-md-12">
      <div class="card">
         <div class="header ">
            <legend>
               <div class="row">
                  <div class="col-md-6">
                     ARCHIVES - PENDED
                  </div>
                  <!--   <div class="col-md-6" style="position:fixed;">
                     CALCULATION OF NET STABLE FUNDING RATIO
                     </div> -->
                  <div class="col-md-6">
                     <div class="pull-right">
                        <button type="button" id="btnDateSearch" class="btn btn-round btn-round btn-sm btn-warning" style="margin-bottom: 10px;" data-toggle="collapse" data-target="#dateSearch">
                        <span class="btn-label">
                        <i class="pe-7s-date"></i>
                        </span>
                        Search
                        </button>
                     </div>
                  </div>
               </div>
            </legend>
            <div class="collapse" id="dateSearch">
                <div class="row clearfix">
                  <div class="col-md-4">
                     <div class="form-group form-float">
                        <label>SPECIFY DATE</label>
                        <div class='input-group date' id='datetimepicker7'>
                           <input type="text" id="FindDate" class="form-control" name="daterange" />
                           <!-- <input type="text" id="FindDate" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>"> -->
                           <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <input type="text" name="currentuseridbudget" id="currentuseridbudget" value="<?php echo $_SESSION['userid'] ?>" style="visibility: hidden">
         <input type="hidden" name="statModule" id="statModule" value="<?php echo ModuleRels::SIMULATION_STATUS; ?>" style="">
         <input type="hidden" name="UpdateSimulationModule" id="UpdateSimulationModule" value="<?php echo ModuleRels::SIMULATION_UPDATE; ?>">
        <!--  <input type="hidden" name="addreason" id="addreason" value="<?php //echo ModuleRels::SIMULATION_ADDREASON; ?>" style=""> -->
         <div class="content">
            <div class="table-responsive">
               <form class="nsfrForm">
                  <table id="tblArchive" class="table table-no-bordered dataTable js-exportable dtr-inline " style=" width:100%;">
                     <thead>
                        <tr style="background:rgb(244,149,66) ;">
                           <th></th>
                           <th style="color:white;">NSFR</th>
                           <th style="color:white;">Available Stable Fund</th>
                           <th style="color:white;">Required Stable Fund</th>

                            <th style="color:white;">Fund Date</th>
                            <th style="color:white;">Date Created</th>  
                        </tr>
                     </thead>
                  
                  </table>
            </div>
         </div>
      </div>
   </div>
   <!-- end card -->
</div>



<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/archive/pending.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>