<div class="container-fluid">
   <?php
      $a = 1;
      $b = 1;
        foreach ($data as $key => $value) {
          $a++;
      ?>
   <div class="panel-heading" style="width:;">
      <h4 class="panel-title"> 
         <a data-target="#bal<?php echo $a; ?>" data-toggle="collapse" class="collapsed" aria-expanded="false"> 
         <b><?php echo $key; ?></b> 
         <b class="caret"></b> 
         </a> 
      </h4>
   </div>
   <div id="bal<?php echo $a; ?>" class="collapse" style="width:95%; margin:auto;">
      <?php
         foreach ($data[$key] as $seckey => $secvalue) {
           $b++;
         ?>
      <div  class="panel panel-default" style="display:; margin:auto;">
         <div class="panel-heading">
            <h4 class="panel-title"><a data-target=".spec<?php echo $b; ?>" data-toggle="collapse" class="collapsed" aria-expanded="false"><?php echo $seckey; ?><b class="caret"></b></a></h4>
         </div>
         <div id="spec<?php echo $b; ?>" class="spec<?php echo $b; ?> panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
               <div class="table-responsive">
                  <div id="tblCapital-50_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                     <table id="tblCapital-50" class="tableclass-50 table table-bordered table-striped js-exportable no-footer dataTable" style="width: 100%;" role="grid" aria-describedby="tblCapital-50_info">
                        <thead>
                           <tr role="row">
                              <th style="width: 0px;" class="sorting" >Original Amount</th>
                              <th class="sorting" tabindex="0" aria-controls="tblCapital-50" rowspan="1" colspan="1" aria-label="Code: activate to sort column ascending" style="width: 0px;">Code</th>
                              <th style="width: 0px;" class="sorting" tabindex="0" aria-controls="tblCapital-50" rowspan="1" colspan="1" aria-label="Item Name: activate to sort column ascending">Item Name</th>
                              <th >Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              $child = $data[$key][$seckey];
                                 foreach ($child as $thirdkey => $thirdvalue) {
                              ?>
                           <tr>
                              <td><?php echo $child[$thirdkey]->amount; ?></td>
                              <td><?php echo $child[$thirdkey]->code; ?></td>
                              <td><?php echo $child[$thirdkey]->name; ?></td>
                              <td><?php echo $child[$thirdkey]->status; ?></td>
                           </tr>
                           <?php
                              }
                              ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php
         }
         ?>
   </div>
   <hr>
   <?php
      }
      ?>
</div>