  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class ShortfallConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }



  public function getArchicves($date){
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getAllNotesSHORTFALL/date/".$date;
        $ret = Helper::serviceGet($url);
        $ret = json_decode($ret);
        //var_dump($ret);
        if($ret != null) {
            if($ret->StatusCode == 100) {
                $this->ModelResponse($ret->StatusCode, 'Data Received', $ret->ResponseResult);
                    return true;
            } else {
                $this->ModelResponse($ret->StatusCode, $ret->ResponseResult);
            }
        }
        
        return false;
    }





public function getOrigData($data){
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getSpecDataNSFRA/refid/".$data['refid'];
        $ret = Helper::serviceGet($url);
        $ret = json_decode($ret);
        return $ret;
    }

public function getReasonSpecific($data){
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/selectNotes/id/".$data['id'].'/refid/'.$data['refid'];
        $ret = Helper::serviceGet($url);
        $ret = json_decode($ret);
        //var_dump($ret);
        return $ret;
    }


 public function updateArchicves($data){

  
        $ret = Helper::serviceCallBudget($data,  $GLOBALS['backend_URL']."NSFRA/API/v1/updateNOTES");
        //$ret = json_decode($ret);
        //var_dump($ret); die();
        if($ret != null) {
            if($ret->ResponseResult == 'Update Successful') {
            //var_dump("ret"); die();
                $this->ModelResponse('100', $ret->ResponseResult);
                    return true;
            } else {
                $this->ModelResponse('101', $ret->ResponseResult);
            }
        }
        
        return false;
    }









}

?>