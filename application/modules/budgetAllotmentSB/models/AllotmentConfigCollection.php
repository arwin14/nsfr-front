  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class AllotmentConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }





       public function getOptionDataAdd(){
       // http://127.0.0.1:8080/NSFRAccountingAPI/BUDGETService/NSFRA/API/v1/getAllExpenditureType/type

        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllExpenditureType/type';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
      }  
//========================================================
   public function getExpendData(){
         $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getExpenditureType/type/'.$_POST['id'];
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;



    }

//=========================================================
       public function getNatureofItem(){
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getAllExpenditures/expenditures/";
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        return $result;



    }
// //========================================================
   public function getExpAllots(){
     $date = $_POST['date'];
        //$url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataByDayAndYearNSFRA/date/".$date;
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataSimulation/date/".$date."/type/3";
        $ret = Helper::serviceGet($url);
        //var_dump($ret);
        $result = json_decode($ret);
        return $result;
    }
// //========================================================
   public function getOrigsTotal(){
    
     $date = $_POST['date'];
     $date = explode(" ",$date);
     $date = $date[0];
     
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getOrigDataNSFRA/date/".$date."/type/0";
       // var_dump($url);
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        //var_dump($result); die();
        return $result;
    }
// //========================================================
   public function getSpecifics(){
     $refid = $_POST['refId'];
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getSpecDataNSFRA/refid/".$refid;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;

    }

   public function getSpecificsValidate(){
     $refid = $_POST['refId'];
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getSpecDataNSFRA/refid/".$refid;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
       // var_dump($ret); die();
        return $ret;

    }


    public function getValidateSpecifics(){
     $refid = $_POST['refId'];
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getSpecDataNSFRA/refid/".$refid;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;

    }



     public function getSpecificsforTotal(){
       // $url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataBAF306B/date/2018-04-10";
     $refid = $_POST['refId'];

        //var_dump($refid);
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getSpecDataNSFRA/refid/".$refid;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
     //   var_dump($ret); die();
        return $ret;



    }
        

// //========================================================

    //===========ADDED BY JOMER=========
    public function addAllotmentsbyCSVModel($datajson){
      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['backend_URL']."NSFRA/API/v1/insertNSFRA");
      //var_dump($ret); die();
      $result = json_encode($ret);
     // var_dump($result); die();
      return $result;


      /*var_dump("DIE");
      var_dump($datajson);die();*/

    }

  //===========END ADD BY JOMER========


    //  public function getDataUpdate(){
    //   $refId = $_POST['refId'];
    //   var_dump($refId); die();
    //   $url = "http://180.150.134.136:48082/ACPCAccountingAPI/BUDGETService/ORS/API/v1/deactivateORS/refid/".$refId;
    //    $json_string =  file_get_contents( $GLOBALS['backend_URL']."ORS/API/v1/deactivateORS/refid/".$refId);
    //     // $ret = Helper::serviceCall("http://180.150.134.136:48082/ACPCAccountingAPI/BUDGETService/ORS/API/v1/getSpecData/refid/".$refId);
    //    //return json_decode($json_string);

    // }

// //========================================================


     public function removeBudgets(){
      //var_dump("=== ".$_POST['idbase']); die();
      $refId = $_POST['idbase'];
       $url =  $GLOBALS['backend_URL']."NSFRA/API/v1/deactivateNSFRA/refid/".$refId;     
     //var_dump($url);
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   
      //var_dump($ret); die();
      return $ret;


    }

  
// //========================================================
  public function addAllotments($data){
      $ret = Helper::serviceCallBudget($data,  $GLOBALS['backend_URL']."NSFRA/API/v1/insertNSFRA");
      //var_dump($ret); die();
      echo json_encode($ret);

     }

// //========================================================
  public function addApprovals($data){
    //var_dump($data); die(); 
      $ret = Helper::serviceCallBudget($data,  $GLOBALS['backend_URL']."NSFRA/API/v1/insertNotes");
      echo json_encode($ret);

     }




//========================================================

     public function updateAllotments($data){
       //var_dump($data); die();
      $ret = Helper::serviceCallBudget($data,  $GLOBALS['backend_URL']."NSFRA/API/v1/updateNSFRA");
      //var_dump($ret); die();
      echo json_encode($ret);
      // $ret = Helper::serviceCallBudget($data,  $GLOBALS['backend_URL']."NSFRA/API/v1/insertNSFRA");
      // echo json_encode($ret);
     }

//========================================================

     public function ChangesStatus(){
      $url = $GLOBALS['backend_URL']."NSFRA/API/v1/statusNSFRA/refid/".$_POST['Refid'].'/status/'.$_POST['Status'];
    //  var_dump($url); die();
      
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   

      //var_dump($ret); die();
      echo json_encode($ret);

     }

//========================================================

    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>