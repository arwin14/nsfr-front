<?php

class AllotmentConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('AllotmentConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}






	public function upload()
	{
		// var_dump($_POST['date']); die();
	$columns_selected = [2,3];
	$row_starts = 1;
	$row = 0;
	$file_handle = fopen($_FILES['file']['tmp_name'], 'r');
    while (!feof($file_handle) ) {
    	$selecteddetails = array();
    	$readline = fgetcsv($file_handle, 1024);
    	if (empty($readline) || $row < $row_starts) {

    	} else {
    		$j = 0;
    		foreach($readline as $key => $val) {
	    		for ($i=0; $i < sizeof($columns_selected); $i++) { 
	    			if ($key == $columns_selected[$i]) {
	    				$selecteddetails[$j] = $val;
	    				$j++;
	    			}
	    		}
			}
    		$line_of_text[] = $selecteddetails;
    	}
    	$row++;
    }





    fclose($file_handle);

    if (empty($line_of_text)) {
     $returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '1'
    					);
	} else {
		$returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '0'
    				);
	}
    

$populate = $returndata['CSVData'];
//var_dump($populate); die();
$nsfraContentList = array();
	foreach ($populate as $k3 => $v3) {
		$nsfraContentList[] = array(
									"expendCode" => $populate[$k3][0]
									,"amount" => $populate[$k3][1]
								);
	}


	$requestdata = array(
						'fundDate'=>$_POST['date'],
						'fund_type'=>'0',
						'createdBy'=>$_SESSION['userid'],
						'nsfraContentList'=>$nsfraContentList,
					    );	
	$jsonlineoftext = json_encode($requestdata);

	$ret = $this->AllotmentConfigCollection->addAllotmentsbyCSVModel($jsonlineoftext);
	echo $ret;


	}




	public function readCSVfile(){
	$columns_selected = [2,3];
	$row_starts = 1;
	$row = 0;
	$file_handle = fopen($_FILES["CSVfile"]["tmp_name"], 'r');
    while (!feof($file_handle) ) {
    	$selecteddetails = array();
    	$readline = fgetcsv($file_handle, 1024);
    	if (empty($readline) || $row < $row_starts) {

    	} else {
    		$j = 0;
    		foreach($readline as $key => $val) {
	    		for ($i=0; $i < sizeof($columns_selected); $i++) { 
	    			if ($key == $columns_selected[$i]) {
	    				$selecteddetails[$j] = $val;
	    				$j++;
	    			}
	    		}
			}
    		$line_of_text[] = $selecteddetails;
    	}
    	$row++;
    }

    fclose($file_handle);

    if (empty($line_of_text)) {
     $returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '1'
    				);
	} else {
		$returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '0'
    				);
	}
    
    $jsonlineoftext = json_encode($returndata);
    echo $jsonlineoftext;
}


	public function remove()
	{
		$file = $this->input->post("file");
		if ($file && file_exists($this->upload_path . "/" . $file)) {
			unlink($this->upload_path . "/" . $file);
		}
	}

	public function list_files()
	{
		// $this->load->helper("file");
		// $files = get_filenames($this->upload_path);
		// // we need name and size for dropzone mockfile
		// foreach ($files as &$file) {
		// 	$file = array(
		// 		'name' => $file,
		// 		'size' => filesize($this->upload_path . "/" . $file)
		// 	);
		// }
	}














	public function addAllotment(){
			foreach ($_POST['amount'] as $k3 => $v3) {
				$amount = ($_POST['amount'][$k3] == "" || $_POST['amount'][$k3] < 1)? 0 : $_POST['amount'][$k3];
				$nsfraContentList[] = array(
									"expendCode" => $_POST['code'][$k3]
									,"amount" => $amount
								);
			}
			$requestdata = array(
						'fundDate'=>$_POST['dateCreated'][0],
						'fund_type'=>'0',
						'createdBy'=>$_SESSION['userid'],
						'nsfraContentList'=>$nsfraContentList,
					    );	
			// 			var_dump($requestdata);

			// die();
				$jsonlineoftext = json_encode($requestdata);
				var_dump($jsonlineoftext); die();
			$ret = $this->AllotmentConfigCollection->addAllotments($jsonlineoftext);
			echo $ret;
	}



	public function addSimulation(){	
	//var_dump($_POST['date']); die();	
		$data = $_POST['data'];
			//	var_dump( $_POST['data'][0]["amount"]); die();
			foreach ($data as $k3 => $v3) {
				$amount = ($data[$k3]["amount"] == "" || $data[$k3]["amount"] < 1)? 0 : $data[$k3]["amount"];
				$nsfraContentList[] = array(
									"expendCode" => $data[$k3]["expendCode"]
									,"amount" => $amount
								);
			}


			$requestdata = array(
						'fundDate'=> $_POST['date'],
						'fund_type'=>'1',
						'createdBy'=>$_SESSION['userid'],
						'nsfraContentList'=>$nsfraContentList,
					    );	
			// 			var_dump($requestdata);

			// die();
				$jsonlineoftext = json_encode($requestdata);
			$ret = $this->AllotmentConfigCollection->addAllotments($jsonlineoftext);
			echo $ret;
	}



	public function getUpdate(){
		$ret = $this->AllotmentConfigCollection->getDataUpdate($_POST);
		echo $ret;
			}


	public function getSpecific(){
		
		$ret = $this->AllotmentConfigCollection->getSpecifics($_POST);
		echo $ret;
			}
	public function getSpecificforTotal(){
		
	$ret = $this->AllotmentConfigCollection->getSpecificsforTotal($_POST);
	echo $ret;
		}


	public function getSpecificValidate(){
		
		$ret = $this->AllotmentConfigCollection->getSpecificsValidate($_POST);
		echo $ret;
			}

   public function getValidateSpecific(){
		
		$ret = $this->AllotmentConfigCollection->getValidateSpecifics($_POST);
		echo $ret;
			}


	public function getModule(){
		
		if(Helper::role($_POST['module'])){
		echo "1";
		}else{
		echo "0"; 
		}
		//echo $jsonlineoftext;
	}


	public function getOptionAdd(){
		$ret = $this->AllotmentConfigCollection->getOptionDataAdd($_POST);
		echo $ret;
		
			}

	public function removeBudget(){
		$ret = $this->AllotmentConfigCollection->removeBudgets($_POST);
		echo $ret;
			}

	public function getOrigTotal(){
		$result = $this->AllotmentConfigCollection->getOrigsTotal($_POST);
		$ret = json_encode($result);
		echo $ret;
			}
	
	public function submitApproval(){
	//$ret = $this->AllotmentConfigCollection->ChangesStatus($_POST);
	//echo $ret;
		$requestedData = array();
		$requestedData['form'] = $_POST;
		$result = array();
		$result['form'] = $this->load->view('forms/addReason.php', $requestedData, TRUE);
		echo json_encode($result);
	}



	public function addApproval(){
		$arr = array(
			"refid" => $_POST['refid'],
			"reason" => $_POST['reason'],
			"action" => $_POST['action'],
			"remarks" => $_POST['remarks'],
			"duration" => $_POST['duration'],
			"nsfr" => round($_POST['NSFR'] * 100, 2),
			"asf" => $_POST['ASF'],
			"rsf" => $_POST['RSF'],
			"measurement_date" => $_POST['fund_date'],
			"noteStatus" =>	strtoupper($_POST['status'])
		);
		//var_dump($arr);
		$ret = new AllotmentConfigCollection();
		$Request = json_encode($arr);
		$ret = $this->AllotmentConfigCollection->addApprovals($Request);
		echo $ret;
	}



	public function getExpAllotStat(){
		$getExpend = new AllotmentConfigCollection();
		$ret = $getExpend->getExpAllots($_POST)->ResponseResult;
		$res = ($ret == "NO DATA FOUND")? 1:0;
		//var_dump($res); die();	
		echo $res;
	}


	public function getExpAllot(){
		$allocateData = new AllotmentConfigCollection();
		$partThreeRet = new AllotmentConfigCollection();
		$result = array();
		$ret = $allocateData->getExpAllots($_POST)->ResponseResult;
		$result['original'] = array(
				"nsfr" => "111.23123",
				"asf" => "2222.123123",
				"rsf" => "33333.123213"

		);
		if($ret == "NO DATA FOUND"){
			$return = "";
		}else{	
		foreach ($ret as $key => $value) {
		$btn = ($ret[$key]->fund_type == 0)? "<a  class='btn btn-simple btn-danger btn-icon'><i class='fa fa-times' style='color:black;'></i></a>":"<a  class='btn btn-simple btn-danger btn-icon remove' data-id='".$ret[$key]->refid."' data-functionname = 'tblAllocate' data-direct='AllotmentConfig/removeBudget' data-date='".substr($ret[$key]->dateCreated,0,-10)."' ><i class='fa fa-times'></i></a>";
		$result['allocate'][] = array(
			 "refference" => $ret[$key]->refid,
			 "Status" => $ret[$key]->status_approval,
			 "SimulationStat" => ($ret[$key]->fund_type == 0)? "Original" : "Simulated",
			 //"Eexpendtype" => $ret[$key]->title,
			 "nsfraStatus" => $ret[$key]->nsfraStatus,
			 "date" => $ret[$key]->fund_date,
		 	 "btn" => $btn
			);
		}
		}

	  $ret = json_encode($result);
	  //var_dump($ret); 
	  echo $ret;
		
	}









//====================================================================
public function addModal(){
	//$myfirstFunction = $this->gg();

	$ret = $this->AllotmentConfigCollection->getNatureofItem($_POST);
	$data = $ret->ResponseResult;
	$dataTittle = $this->GroupbyTitle($data);

	$requestData = array();
	foreach ($dataTittle as $key => $value) {
			$populatedetail = array();
			foreach ($data as $datakey => $datavalue) {
				if($data[$datakey]->title ==  $dataTittle[$key]){
					$populatedetail[] = array(
						"name" => $data[$datakey]->name,
						"id"   => $data[$datakey]->code
					);
				}else{
				}
			}
			$requestData[] = array(
				'title' => $dataTittle[$key],
				'content' => $populatedetail
			);
	}

	$requestedData = array();
	$requestedData['form'] = $requestData;


	$result = array();
	$result['form'] = $this->load->view('forms/addAllotment.php', $requestedData, TRUE);
		echo json_encode($result);
	}

	private function GroupbyTitle($data){
		$title = [];
		foreach ($data as $key => $value) {
				array_push($title,$data[$key]->title);
		}
		$title = array_unique($title);
		return $title;
	}
//====================================================================















public function UpdateModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/updateAllotment.php', "updaterecord", TRUE);
		echo json_encode($result);
	//var_dump($result);
}


//added by jomer
public function addImportCSVModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/importcsvmodal.php', "addcsvrecord", TRUE);
		echo json_encode($result);
}

public function tablecollapseview(){
	$result = array();
	$result['form'] = $this->load->view('forms/tablecollapseview.php', "tablecollapseview", TRUE);
		echo json_encode($result);
}



public function submitCSVData(){

	//$ret = new AllotmentConfigCollection();


	$nsfraContentList = array();
	foreach ($_POST['codefield'] as $k3 => $v3) {
		$nsfraContentList[] = array(
									"expendCode" => $_POST['codefield'][$k3]
									,"amount" => $_POST['amountfield'][$k3]
								);
	}

	$requestdata = array(
						'fundDate'=>$_POST['funddate'],
						'createdBy'=>$_SESSION['userid'],
						'nsfraContentList'=>$nsfraContentList,
					    );

	$datajson = json_encode($requestdata);
	$ret = $this->AllotmentConfigCollection->addAllotmentsbyCSVModel($datajson);
	
	echo $ret;


	// $ret = $this->AllotmentConfigCollection->addAllotments($_POST);
	// echo $ret;
}

public function submitchangesdata(){


	$fulldate = (explode (" ", $_POST['funddate']));
	$date = $fulldate[0];
     foreach($_POST['simulatedtxt'] as $k3 => $v3){
         $formdata[] = 

           array(
          'expendId' => $_POST['expendid'][$k3],
          'expendCode'=>$_POST['expendCode'][$k3],
          'amount'=>($_POST['totalreponse'][$k3] == '' || $_POST['totalreponse'][$k3] == '0') ? '0': $_POST['totalreponse'][$k3],
          'nsfrStatus'=>'ACTIVE'
             );                  
      
          }



      $arr =
        array(
             "refid" => $_POST['refid'],
            "fundDate" => $date,
            "fund_type" => ($_POST['changesStatus'] == "Original")? '0':'1',
            "updatedBy" => $_SESSION['userid'],
            "createdBy" =>  $_SESSION['userid'],
            "nsfraStatus" => 'ACTIVE',
            "nsfraContentList" => $formdata
            );



	$ret = new AllotmentConfigCollection();
	$Request = json_encode($arr);
	$ret = $this->AllotmentConfigCollection->updateAllotments($Request);
	echo $ret;
	//var_dump($Request); die();

}



	// public function updateAllotment(){	
	// 	$ret = $this->AllotmentConfigCollection->updateAllotments($_POST);
	// 	echo $ret;
	// }



	public function getExpend(){

		// var_dump($_POST['date']); die();
		$ret = $this->AllotmentConfigCollection->getExpendData($_POST);
		echo $ret;
		
			}

	// public function getExpAllot(){

	// 	// var_dump($_POST['date']); die();
	// 	$ret = $this->AllotmentConfigCollection->getExpAllots($_POST);
	// 	echo $ret;
		
	// 		}
//==========================================================================================================
	
	
}
?>