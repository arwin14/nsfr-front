<?php

class AllotmentConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('AllotmentConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('Config.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}






	public function upload()
	{
	//	 var_dump($_POST); die();
	$columns_selected = [2,3];
	$row_starts = 1;
	$row = 0;
	$file_handle = fopen($_FILES['file']['tmp_name'], 'r');
    while (!feof($file_handle) ) {
    	$selecteddetails = array();
    	$readline = fgetcsv($file_handle, 1024);
    	if (empty($readline) || $row < $row_starts) {

    	} else {
    		$j = 0;
    		foreach($readline as $key => $val) {
	    		for ($i=0; $i < sizeof($columns_selected); $i++) { 
	    			if ($key == $columns_selected[$i]) {
	    				$selecteddetails[$j] = $val;
	    				$j++;
	    			}
	    		}
			}
    		$line_of_text[] = $selecteddetails;
    	}
    	$row++;
    }





    fclose($file_handle);

    if (empty($line_of_text)) {
     $returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '1'
    				);
	} else {
		$returndata = array(
    					'CSVData'=>$line_of_text
    					,'Code' => '0'
    				);
	}
    

$populate = $returndata['CSVData'];
//var_dump($populate); die();
$nsfraContentList = array();
	foreach ($populate as $k3 => $v3) {
		$nsfraContentList[] = array(
									"expendCode" => $populate[$k3][0]
									,"amount" => $populate[$k3][1]
								);
	}


	$requestdata = array(
						'fundDate'=>$_POST['date'],
						'createdBy'=>$_SESSION['userid'],
						'nsfraContentList'=>$nsfraContentList,
					    );	
	$jsonlineoftext = json_encode($requestdata);

	$ret = $this->ConfigCollection->addAllotmentsbyCSVModel($jsonlineoftext);

	echo $ret;


	}




// 	public function readCSVfile(){
// 	$columns_selected = [2,3];
// 	$row_starts = 1;
// 	$row = 0;
// 	$file_handle = fopen($_FILES["CSVfile"]["tmp_name"], 'r');
//     while (!feof($file_handle) ) {
//     	$selecteddetails = array();
//     	$readline = fgetcsv($file_handle, 1024);
//     	if (empty($readline) || $row < $row_starts) {

//     	} else {
//     		$j = 0;
//     		foreach($readline as $key => $val) {
// 	    		for ($i=0; $i < sizeof($columns_selected); $i++) { 
// 	    			if ($key == $columns_selected[$i]) {
// 	    				$selecteddetails[$j] = $val;
// 	    				$j++;
// 	    			}
// 	    		}
// 			}
//     		$line_of_text[] = $selecteddetails;
//     	}
//     	$row++;
//     }

//     fclose($file_handle);

//     if (empty($line_of_text)) {
//      $returndata = array(
//     					'CSVData'=>$line_of_text
//     					,'Code' => '1'
//     				);
// 	} else {
// 		$returndata = array(
//     					'CSVData'=>$line_of_text
//     					,'Code' => '0'
//     				);
// 	}
    
//     $jsonlineoftext = json_encode($returndata);
//     echo $jsonlineoftext;
// }


	// public function remove()
	// {
	// 	$file = $this->input->post("file");
	// 	if ($file && file_exists($this->upload_path . "/" . $file)) {
	// 		unlink($this->upload_path . "/" . $file);
	// 	}
	// }

	// public function list_files()
	// {
	// 	// $this->load->helper("file");
	// 	// $files = get_filenames($this->upload_path);
	// 	// // we need name and size for dropzone mockfile
	// 	// foreach ($files as &$file) {
	// 	// 	$file = array(
	// 	// 		'name' => $file,
	// 	// 		'size' => filesize($this->upload_path . "/" . $file)
	// 	// 	);
	// 	// }
	// }















	public function dummy()
	{
		$data = array_map(function($x,$y){
			return [
				'dummy'=>$x,
				'text'=>$y
			];
		}, $_POST['dummy'], $_POST['simulatedtxt']);
	
		print_r($data);
	}


	public function getOptionAdd(){
		$ret = $this->ConfigCollection->getOptionDataAdd($_POST);
		echo $ret;
		
			}

	public function getUpdate(){
		$ret = $this->ConfigCollection->getDataUpdate($_POST);
		echo $ret;
			}


	public function getSpecific(){
		
		$ret = $this->ConfigCollection->getSpecifics($_POST);
		echo $ret;
			}
	public function getSpecificValidate(){
		
		$ret = $this->ConfigCollection->getSpecificsValidate($_POST);
		echo $ret;
			}

   public function getValidateSpecific(){
		
		$ret = $this->ConfigCollection->getValidateSpecifics($_POST);
		echo $ret;
			}
	public function getSpecificforTotal(){
		
	$ret = $this->ConfigCollection->getSpecificsforTotal($_POST);
	echo $ret;
		}



	public function removeBudget(){
		//var_dump($_POST); die();
		$ret = $this->ConfigCollection->removeBudgets($_POST);
		echo $ret;
			}
	

	public function addAllotment(){
		$ret = $this->ConfigCollection->addAllotments($_POST);
		echo $ret;
	}





	public function getExpAllot(){
		$ret = $this->ConfigCollection->getExpAllots($_POST);


		$arrsSTD = json_decode($ret);
		$arrs = json_decode( json_encode($arrsSTD), true);
		$result = $arrs['ResponseResult'];
		$return = array();
		/*var_dump($result); die();*/
		if($result == "NO DATA FOUND"){
			$return[] = array(
			 "refference" => '',
			 "Eexpendtype" => 'No Data Found',
			 "date" => '',
			 "nsfraStatus" => '',
			 "btn" => ''
			);
		}else{	
		foreach ($result as $key => $value) {
		$return[] = array(
			 "refference" => $result[$key]['refid'],
			 "Eexpendtype" => $result[$key]['title'],
			 "nsfraStatus" => $result[$key]['nsfraStatus'],
			 "date" => $result[$key]['dateCreated'],
			 "btn" =>/* "<a href='#' class='btn btn-simple btn-info btn-icon like'><i class='fa fa-eye'></i></a>".
			 		  "<a class='btn btn-simple btn-warning btn-icon editbtn' data-refid='".$result[$key]['refid']."' data-type='".$result[$key]['title']."'><i class='fa fa-edit'></i></a>".*/
			          "<a  class='btn btn-simple btn-danger btn-icon remove' data-id='".$result[$key]['refid']."' data-functionname = 'tblAllocate' data-direct='Config/removeBudget' data-date='".substr($result[$key]['dateCreated'],0,-10)."'><i class='fa fa-times'></i></a>"
			);
		}
		}

		$result = json_encode($return);
		//var_dump($result); die();
		echo $result;
		
	}





public function addModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/addAllotment.php', "addrecord", TRUE);
		echo json_encode($result);
	//var_dump($result);
}

public function UpdateModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/updateAllotment.php', "updaterecord", TRUE);
		echo json_encode($result);
	//var_dump($result);
}


//added by jomer
public function addImportCSVModal(){
	$result = array();
	$result['form'] = $this->load->view('forms/importcsvmodal.php', "addcsvrecord", TRUE);
		echo json_encode($result);
}

public function tablecollapseview(){
	$result = array();
	$result['form'] = $this->load->view('forms/tablecollapseview.php', "tablecollapseview", TRUE);
		echo json_encode($result);
}



// public function submitCSVData(){

// 	//$ret = new ConfigCollection();


// 	$nsfraContentList = array();
// 	foreach ($_POST['codefield'] as $k3 => $v3) {
// 		$nsfraContentList[] = array(
// 									"expendCode" => $_POST['codefield'][$k3]
// 									,"amount" => $_POST['amountfield'][$k3]
// 								);
// 	}

// 	$requestdata = array(
// 						'fundDate'=>$_POST['funddate'],
// 						'createdBy'=>$_SESSION['userid'],
// 						'nsfraContentList'=>$nsfraContentList,
// 					    );

// 	$datajson = json_encode($requestdata);
// 	$ret = $this->ConfigCollection->addAllotmentsbyCSVModel($datajson);
	
// 	echo $ret;


// 	// $ret = $this->ConfigCollection->addAllotments($_POST);
// 	// echo $ret;
// }

public function submitchangesdata(){
	$fulldate = (explode (" ", $_POST['funddate']));
	$date = $fulldate[0];
     foreach($_POST['simulatedtxt'] as $k3 => $v3){
         $formdata[] = 

           array(
          'expendId' => $_POST['expendid'][$k3],
          'expendCode'=>$_POST['expendCode'][$k3],
          'amount'=>($_POST['simulatedtxt'][$k3] == '' || $_POST['simulatedtxt'][$k3] == '0') ? '0': $_POST['simulatedtxt'][$k3],
          'nsfrStatus'=>'ACTIVE'
             );                  
      
          }



      $arr =
        array(
             "refid" => $_POST['refid'],
            "fundDate" => $date,
            "updatedBy" => $_SESSION['userid'],
            "createdBy" =>  $_SESSION['userid'],
            "nsfraStatus" => 'ACTIVE',
            "nsfraContentList" => $formdata
            );







	$ret = new ConfigCollection();
	$Request = json_encode($arr);
		// var_dump($Request);
		// die();
	$ret = $this->ConfigCollection->updateAllotments($Request);
	echo $ret;
	//var_dump($Request); die();

}



	// public function updateAllotment(){	
	// 	$ret = $this->ConfigCollection->updateAllotments($_POST);
	// 	echo $ret;
	// }



	public function getExpend(){

		// var_dump($_POST['date']); die();
		$ret = $this->ConfigCollection->getExpendData($_POST);
		echo $ret;
		
			}

	// public function getExpAllot(){

	// 	// var_dump($_POST['date']); die();
	// 	$ret = $this->ConfigCollection->getExpAllots($_POST);
	// 	echo $ret;
		
	// 		}
//==========================================================================================================
	
	
}
?>