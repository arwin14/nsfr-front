<style>
   td.details-control {
   background: url('<?php echo base_url(); ?>assets/thirdparty/img/reference/details_open.png') no-repeat center center;
   cursor: pointer;
   }
   tr.shown td.details-control {
   background: url('<?php echo base_url(); ?>assets/thirdparty/img/reference/details_close.png') no-repeat center center;
   }
   .mycssvalid {
   -webkit-transition: all 0.30s ease-in-out;
   -moz-transition: all 0.30s ease-in-out;
   -ms-transition: all 0.30s ease-in-out;
   -o-transition: all 0.30s ease-in-out;
   outline: none;
   padding: 3px 0px 3px 3px;
   margin: 5px 1px 3px 0px;
   border: 1px solid #DDDDDD;
   }
   .mycssvalid{
   box-shadow: 0 0 5px rgba(255, 201, 14, 1);
   padding: 3px 0px 3px 3px;
   margin: 5px 1px 3px 0px;
   border: 1px solid rgba(255, 201, 14, 0);
   }
   .highlightthis{
   color: red;
   }
   .valueInputed{
      width:100%;
       border: 0;
    background: transparent;
   }

</style>
<?php
date_default_timezone_set('Asia/Manila');
?>
<div class="row clearfix" style="position:relative; top:0px;">
   <div class="col-md-12">
      <div class="card">
         <div class="header ">
            <legend>
               <div class="row">
                  <div class="col-md-6">
                     CALCULATION OF NET STABLE FUNDING RATIO
                  </div>
                  <!--   <div class="col-md-6" style="position:fixed;">
                     CALCULATION OF NET STABLE FUNDING RATIO
                     </div> -->

                  <div class="col-md-6">

                     <div class="pull-right">
                        <button type="button" id="btnDateSearch" class="btn btn-round btn-round btn-sm btn-warning" style="margin-bottom: 10px;" data-toggle="collapse" data-target="#dateSearch">
                        <span class="btn-label">
                        <i class="pe-7s-date"></i>
                        </span>
                        Search
                        </button>
                        <?php if(Helper::role(ModuleRels::SIMULATION_ADD)){ ?>
                        <button type="button" id="adding" class="btn btn-round btn-sm btn-success" style="margin-bottom: 10px;" data-holder="">
                        <input type="hidden" name="exportExceltxt" id="exportExceltxt" value>
                        <span class="btn-label">
                        <i class="pe-7s-print"></i>
                        </span>
                        ADD
                        </button>
                        <?php 
                        }else{ echo""; } 
                           if(Helper::role(ModuleRels::SIMULATION_IMPORT)){
                        ?>
                        <!-- added by Jomer -->
                        <button type="button" id="btnimportcsv" class="btn btn-round btn-sm btn-warning" style="margin-bottom: 10px;">
                        <span class="btn-label">
                        <i class="pe-7s-upload"></i>
                        </span>
                        Import CSV
                        </button> 
                        <?php
                           }else{
                              echo "";
                           }
                        ?> 
                     </div>
                  </div>
               </div>
            </legend>
            <div class="collapse" id="dateSearch">
               <div class="row clearfix">
                  <div class="col-md-4">
                     <div class="form-group form-float">
                        <label>Specify Date</label>
                        <div class='input-group date' id='datetimepicker7'>
                           <input type="text" id="FindDate" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                             <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="container-fluid">
                     <div class="col-md-2">
                     <button type="button" id="addSimulate" class="btn btn-round btn-sm btn-warning" style="margin-bottom: 10px;">
                        <span class="btn-label">
                        <i class="pe-7s-upload"></i>
                        </span>
                        Add Simulation
                     </button>
                  </div>
                     <div class="col-md-1" style="background:none; display:;" id="loader">
                        <div class="lds-ellipsis" style="margin:auto; position:relative; top:-18px; left:-90%;"><div></div><div></div><div></div><div></div></div>
                     </div>


         </div>

         <input type="text" name="currentuseridbudget" id="currentuseridbudget" value="<?php echo $_SESSION['userid'] ?>" style="visibility: hidden">
         <input type="hidden" name="statModule" id="statModule" value="<?php echo ModuleRels::SIMULATION_STATUS; ?>" style="">
         <input type="hidden" name="UpdateSimulationModule" id="UpdateSimulationModule" value="<?php echo ModuleRels::SIMULATION_UPDATE; ?>">
        <!--  <input type="hidden" name="addreason" id="addreason" value="<?php //echo ModuleRels::SIMULATION_ADDREASON; ?>" style=""> --> 
         <div class="content">
            <div class="table-responsive">
               <form class="nsfrForm">
                  <table id="tblAllocate" class="table table-no-bordered dataTable js-exportable dtr-inline " style=" width:100%;">
                     <thead>
                        <tr style="background:rgb(244,149,66) ;">
                           <th></th>
                           <th class="no-sort" style="color:white;">Reference</th>
                           <th style="color:white;">Date</th>
                           <th style="color:white;">Status</th>
                           <th style="width: 10%; color:white;">Action</th>
                        </tr>
                     </thead>
                  </table>
            </div>
         </div>
      </div>
   </div>
   <!-- end card -->
</div>
<div class="collapse col-md-11" style="background:; position:fixed; top:0%; left:245px;" id="mysimulation">
<div class="card">
<div class="header">
<legend>
<div class="row" id="exitme">
<div class="col-md-10" style="height:50px;">
SIMULATION
</div>
<div class="col-md-2">
<a class="dropdown-toggle" id="simulatebtn" data-toggle="collapse" data-target="#simulateContent">
<i class="fa fa-arrows-h" style="color:black;"></i>
<p class="hidden-md hidden-lg">
Actions
<b class="caret"></b>
</p>
</a>
</div>
</div>
</legend>
</div>
<div>

<div class="content collapse" id="simulateContent" style="background:rgb(250,197,137);">
   <table style="width:1000px;">
   <thead>
   <th></th>
   <th>Net Stable Funding Ratio</th>
   <th>Available Stable Funding</th>
   <th>Required Stable Funding</th>
   </thead>
   <tbody>
      <tr>
         <td align="center"><label>Simulated</label></td>
         <td id="nsfr" style="font-size: 20pt;"></td>
         <td id="asf" style="font-size: 20pt;"></td>
         <td id="rsf" style="font-size: 20pt;"></td>
      </tr>
      <tr style="background:;">
         <td align="center"><label>Original</label></td>
         <td id="Orignsfr" style="font-size: 20pt;"></td>
         <td id="Origasf" style="font-size: 20pt;"></td>
         <td id="Origrsf" style="font-size: 20pt;"></td>
      </tr>
   
   </tbody>
   </table>
</form>
</div>
</div>
</div>
</div>


<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/budgetAllotment/allotment.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>