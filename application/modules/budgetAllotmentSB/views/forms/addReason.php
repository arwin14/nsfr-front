
<div style="background:rgb(244,234,159);">
   <br>
   <table style="width:90%; margin:auto;">
      <thead>
         <th>Net Stable Funding Ratio</th>
         <th>Available Stable Funding</th>
         <th>Required Stable Funding</th>
      </thead>
      <tbody>
         <tr>
            <td id="nsfr" style="font-size: 20pt;"><?php echo round($form['NSFR'] * 100, 2).'%'; ?></td>
            <td id="asf" style="font-size: 20pt;"><?php echo  number_format($form['ASF'], 2); ?></td>
            <td id="rsf" style="font-size: 20pt;"><?php echo  number_format($form['RSF'], 2); ?></td>
         </tr>
      </tbody>
   </table>
   <br>
</div>
<br>
<div class="container-fluid">
   <form id="reasonForm" data-id="<?php echo $form['refid']; ?>">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="content">
                  <div class="">
                     <div class="row">
                        <div class="col-md-6">
                           <?php
   //var_dump($form);
   ?>
                           <h4 class="title">Provide Reason</h4>
                           <p class="category"></p>
                        </div>
                        <div class="col-md-6">
                           <label class="form-label" style="font-size: 1.25rem">Duration:</label><br>
                           <div class="form-group form-float">
                              <div class="form-line">
                                 <input type="text" id="duration" name="duration" class="form-control ">
                              </div>
                           </div>
                           <br>
                        </div>
                     </div>
                     <div id="content">

                        <div class="form-group">
                           <label for="comment">Comment:</label>
                           <textarea class="form-control" rows="5" name="reason" id="reason"></textarea>
                        </div>
                        <div class="form-group">
                           <label for="comment">Action:</label>
                           <textarea class="form-control" rows="5" name="action" id="action"></textarea>
                        </div>
                         <div class="form-group">
                           <label for="comment">Remarks:</label>
                           <textarea class="form-control" rows="5" name="renarks" id="remarks"></textarea>
                        </div>
                 <!--   <p class="input-group">
                           <input type="text" id="from" class="form-control" style="width:30%;" />
                           <span class="input-group-btn">
                         <select style="width:40%; margin-right:400px;">
                          	<option>ww</option>
                          	<option>ww</option>
                          	<option>ww</option>
                          	<option>ww</option>
                          </select>
                           </span>
                        </p> -->
                     </div>
                  </div>
                  <br>
               </div>
            </div>
            <div class="modal-footer" style="padding: 0; margin: 0">
               <input type="hidden" name="AllotmentDate" id="AllotmentDate" value="<?php echo date('Y-m-d'); ?>">
               <div class="row clearfix">
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-lg btn-success btn-block waves-effect" id="importCSV" name="importRawDTR" >
                     <i class="fa fa-paper-plane fa-fw"></i>
                     <strong>SUBMIT</strong>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>