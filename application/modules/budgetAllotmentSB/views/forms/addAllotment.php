<style type="text/css">
   / CHANGE COLOR HERE / 
   ol.etapier li.done {
   border-color: yellowgreen ;
   }
   / CHANGE COLOR HERE / 
   ol.etapier li.done:before {
   background-color: yellowgreen;
   border-color: yellowgreen;
   }
   ol.etapier {
   display: table;
   list-style-type: none;
   margin: 0 auto 20px auto;
   padding: 0;
   table-layout: fixed;
   width: 100%;
   }
   ol.etapier a {
   display: table-cell;
   text-align: center;
   white-space: nowrap;
   position: relative;
   }
   ol.etapier a li {
   display: block;
   text-align: center;
   white-space: nowrap;
   position: relative;
   }
   ol.etapier li {
   display: table-cell;
   text-align: center;
   padding-bottom: 10px;
   white-space: nowrap;
   position: relative;
   }
   ol.etapier li a {
   color: inherit;
   }
   ol.etapier li {
   color: silver; 
   border-bottom: 4px solid silver;
   }
   ol.etapier li.done {
   color: black;
   }
   ol.etapier li:before {
   position: absolute;
   bottom: -11px;
   left: 50%;
   margin-left: -7.5px;
   color: white;
   height: 15px;
   width: 15px;
   line-height: 15px;
   border: 2px solid silver;
   border-radius: 15px;
   }
   ol.etapier li.done:before {
   content: "\2713";
   color: white;
   }
   ol.etapier li.todo:before {
   content: " " ;
   background-color: white;
   }
</style>
<div class="content">
   <?php
      ?>
   <div class="row">
      <div class="col-md-12" style="background:rgb(244,234,159)">
         <div class="nav-container">
            <ul class="nav nav-icons" role="tablist">
               <li class="active">
                  <a href="#0" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-calculator" style="font-size:20pt;"></i><br>
                  Capital <br><br><br>
                  </a>
               </li>
               <li class="">
                  <a href="#1" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-upload" style="font-size:20pt;"></i><br>
                  Retail<br>Deposit<br><br>
                  </a>
               </li>
               <li class="">
                  <a href="#2" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-box1" style="font-size:20pt;"></i><br>
                  Whole Sale<br> Deposit<br><br>
                  </a>
               </li>
               <li class="">
                  <a href="#3" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-cash" style="font-size:20pt;"></i><br>
                  Secured<br> Usecured<br>Funding<br>
                  </a>
               </li>
               <li class="">
                  <a href="#4" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-id" style="font-size:20pt;"></i><br>
                  Other<br>Liabilities<br>and Equalities<br>
                  </a>
               </li>
               <li class="">
                  <a href="#5" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-graph1" style="font-size:20pt;"></i><br>
                  NSFR<br>HQLS<br><br>
                  </a>
               </li>
               <li class="">
                  <a href="#6" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-upload" style="font-size:20pt;"></i><br>
                  Deposit<br>at other<br>Financial<br>
                  </a>
               </li>
               <li class="">
                  <a href="#7" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-news-paper" style="font-size:20pt;"></i><br>
                  Performing <br>Loans Non-<br>HQLA Sec.<br>
                  </a>
               </li>
               <li class="">
                  <a href="#8" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-albums" style="font-size:20pt;"></i><br>
                  Other <br>Assets<br><br>
                  </a>
               </li>
               <li class="">
                  <a href="#9" role="tab" data-toggle="tab" style="font-size:11pt;">
                  <i class="pe-7s-news-paper" style="font-size:20pt;"></i><br>
                  Off Balance <br>Sheets<br>Exposure<br>
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </div>
</div>
<br>  
<form id="addrecordform" class="uppercase" accept-charset="utf-8">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="body">
                  <br><br>
                  <div class="container-fluid">
                     <div class="col-md-12">
                        <div class="card">
                           <div class="header">
                           </div>
                           <div class="content">
                              <div class="form-group">
                                 <div class="tab-content">
                                    <?php
                                       foreach ($form as $key => $value) {
                                           $stat = ($key == 0)? "active":"";
                                       ?>
                                    <div class="tab-pane <?php echo $stat; ?>" id="<?php echo $key; ?>">
                                       <div class="col-md-6">
                                          <h4 class="title"><?php echo $form[$key]['title'] ?></h4>
                                          <p class="category">More here</p>
                                       </div>
                                       <div class="col-md-6">
                                          <label class="form-label" style="font-size: 1.25rem">Date Created:</label><br>
                                          <div class="form-group form-float">
                                             <div class="form-line">
                                                <input type="text" id="dateCreated" name="dateCreated[]" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                                             </div>
                                          </div>
                                          <br>
                                       </div>
                                       <div class="content">
                                          <?php                                       
                                             foreach ($form[$key]['content'] as $formkey => $formvalue) {
                                             ?>
                                          <div class="col-md-6">
                                             <label class="control-label">
                                             <?php
                                                 $rawlabel = $form[$key]['content'][$formkey]['name'];
                                                 $label = (strlen($rawlabel) > 40)? substr($rawlabel, 0,40)."..." : $rawlabel;
                                                echo $label;
                                                ?> 
                                             </label>
                                             <input class="form-control amount" name="amount[]" type="text" />
                                             <input class="form-control" name="code[]" type="hidden" value="<?php echo $form[$key]['content'][$formkey]['id']; ?>" />
                                          </div>
                                          <?php
                                             }
                                          ?>
                                       </div>
                                    </div>
                                    <?php
                                       }
                                       ?>
                                    <br>
                                 </div>
                              </div>
                           </div>
                           <div class="footer">
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <br>
            </div>
         </div>
         <div class="modal-footer" style="padding: 0; margin: 0">
            <input type="hidden" name="AllotmentDate" id="AllotmentDate" value="<?php echo date('Y-m-d'); ?>">
            <div class="row clearfix">
               <div class="col-md-6">
                  <button type="button" class="btn btn-lg btn-success btn-block waves-effect" id="btnsubmitadd" name="btnsubmitadd" >
                  <i class="fa fa-paper-plane fa-fw"></i>
                  <strong>ADD</strong>
                  </button>
               </div>
               <div class="col-md-6">
                  <button id="btnclear" name="btnclear" type="button" class="btn btn-lg btn-danger btn-block waves-effect" >
                  <i class="fa fa-trash fa-fw"></i>
                  <strong>CLEAR</strong>
                  </button>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>