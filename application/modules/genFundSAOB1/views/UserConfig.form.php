<style type="text/css">
   .displaynone{
   display: none;
   }
   .no-sort::after { display: none!important; }
   .no-sort { 
   pointer-events: none!important; cursor: default!important; 
   }
   #tlbmonthlyData tbody td {
   white-space: nowrap;
   }
   div.form-group{
   margin-bottom: -.5em;
   }
   .displaynone{
   display: none;
   }
   .btn:focus,
   .btn:active,
   .btn:active:focus {
   box-shadow: none;
   outline: none;
   }
   .btn-modal {
   position: absolute;
   top: 50%;
   left: 50%;
   margin-top: -20px;
   margin-left: -100px;
   width: 200px;
   }
   .btn-primary:active,
   .btn-primary:hover:active,
   .btn-primary:focus:active,
   .btn-primary:active:active {
   border-bottom: 1px solid #36a940;
   }
   .btn-default:active,
   .btn-default:hover:active,
   .btn-default:focus:active,
   .btn-default:active:active {
   border-bottom: 1px solid #a2aab8;
   }
   .btn-secondary,
   .btn-secondary:hover,
   .btn-secondary:focus,
   .btn-secondary:active {
   color: #cc7272;
   background: transparent;
   border: 0;
   }
   h1:first-child,
   h2:first-child,
   h3:first-child {
   margin-top: 0;
   }
   .form-label{
   font-size:8pt;
   }
   .number{
   background:rgba(0,0,0,0);
   }
   .loaders {
   border: 6px solid #f3f3f3;
   border-radius: 50%;
   border-top: 6px solid #3498db;
   width: 50px;
   height: 50px;
   -webkit-animation: spin 2s linear infinite; /* Safari */
   animation: spin 2s linear infinite;
   }

    .loader{
   width: 70px;
   height: 70px;
   margin: 40px auto;
   }
   .loader p{
   font-size: 16px;
   color: #777;
   }
   .loader .loader-inner{
   display: inline-block;
   width: 15px;
   border-radius: 15px;
   background: #74d2ba;
   }
   .loader .loader-inner:nth-last-child(1){
   -webkit-animation: loading 1.5s 1s infinite;
   animation: loading 1.5s 1s infinite;
   }
   .loader .loader-inner:nth-last-child(2){
   -webkit-animation: loading 1.5s .5s infinite;
   animation: loading 1.5s .5s infinite;
   }
   .loader .loader-inner:nth-last-child(3){
   -webkit-animation: loading 1.5s 0s infinite;
   animation: loading 1.5s 0s infinite;
   }
   @-webkit-keyframes loading{
   0%{
   height: 15px;
   }
   50%{
   height: 35px;
   }
   100%{
   height: 15px;
   }
   }
   @keyframes loading{
   0%{
   height: 15px;
   }
   50%{
   height: 35px;
   }
   100%{
   height: 15px;
   }
   /* Safari */
   @-webkit-keyframes spin {
   0% { -webkit-transform: rotate(0deg); }
   100% { -webkit-transform: rotate(360deg); }
   }
   @keyframes spin {
   0% { transform: rotate(0deg); }
   100% { transform: rotate(360deg); }
   }
</style>
<div class="container-fluid" style="font-size:8pt;">
   <div class="block-header">
      <h2>STATEMENT OF APPROVED BUDGET, UTILIZATION AND BALANCES</h2>
   </div>
   <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="card">
            <div class="header bg-green">
               <h2 style="font-size: 2.2rem">
                  STATEMENT OF APPROVED BUDGET, UTILIZATION AND BALANCES
                  <small>BAF306B</small> 
               </h2>
               <ul class="header-dropdown m-r-0">
                  <li>
                      <div class="loaders col-lg-2 col-centered" id="loaders"></div>
                     <button class="btn bg-light-green btn-circle-lg waves-effect waves-circle waves-float" data-toggle="modal" data-target="#UpdateModal" class="updateAllots" id='updateAllot' value="">
                     <i class="material-icons">account_balance_wallet</i>
                     </button>
                     <button type="button" id="btnDateSearch" class="btn bg-light-green btn-circle-lg waves-effect waves-circle waves-float" data-toggle="collapse" data-target="#dateSearch">
                     <i class="material-icons">date_range</i>
                     </button>
                  </li>
               </ul>
            </div>
            <div class="body">
               <div class="collapse" id="dateSearch">
                  <div class="row clearfix">
                     <div class="col-md-6">
                        <button id="exportExcelButton" class="btn btn-info" >
                        Export Excel
                        </button>  
                        <input type="hidden" name="exportExceltxt" id="exportExceltxt" value>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group form-float">
                           <div class="form-line">
                              <input type="text" class="form-control" id="searchDate" name="search_table_textbox" value="<?php echo date('Y-m-d'); ?>">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-2">
                        <button type="button" class="btn btn-sm btn-block bg-green waves-effect" id="dateGen" name="dateGen"
                           data-toggle="collapse" data-target="#tableOptionsCollapse">
                        <i class="material-icons">filter_list</i>
                        <span>
                        <strong>Search</strong>
                        </span>
                        </button>
                     </div>
                  </div>
               </div>
               <div class = "row clearfix">
                  <div class="col-md-12">
                     <div class="table-responsive">
                        <br><br>
                        <table id="specuBudTbl" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                           <thead>
                              <tr>
                                 <th rowspan="3" class="no-sort">Expenditures</th>
                                 <th rowspan="3" class="no-sort">UACS</th>
                                 <th colspan="3" style="text-align: center;" class="no-sort">Last Quarter</th>
                                 <th></th>
                                 <th colspan="3" style="text-align: center;" class="no-sort">This  Quarter</th>
                                 <th></th>
                                 <th colspan="4" style="text-align: center;" class="no-sort">ALLOTMENT</th>
                                 <th colspan="12" style="text-align: center;" class="no-sort">OBLIGATIONS INCURRED</th>
                                 <th colspan="4" style="text-align: center;" class="no-sort">BALANCES</th>
                              </tr>
                              <tr>
                                 <th rowspan="2">I0.A.1</th>
                                 <th rowspan="2">II.A.1</th>
                                 <th rowspan="2">II.A.2</th>
                                 <th rowspan="2">Total</th>
                                 <th rowspan="2">I.A.1</th>
                                 <th rowspan="2">II.A.1</th>
                                 <th rowspan="2">II.A.2</th>
                                 <th rowspan="2">Total</th>
                                 <th rowspan="2">I.A.1</th>
                                 <th rowspan="2">II.A.1</th>
                                 <th rowspan="2">II.A.2</th>
                                 <th rowspan="2">Total</th>
                                 <th colspan="4" style="text-align: center;">Prev Month</th>
                                 <th colspan="4" style="text-align: center;">This Month - <label id='curHeadDate'></label></th>
                                 <th rowspan="2">I.A.1</th>
                                 <th rowspan="2">II.A.1</th>
                                 <th rowspan="2">II.A.2</th>
                                 <th rowspan="2">Total</th>
                                 <th rowspan="2">I.A.1</th>
                                 <th rowspan="2">II.A.1</th>
                                 <th rowspan="2">II.A.2</th>
                                 <th rowspan="2">Total</th>
                              </tr>
                              <tr>
                                 <th>I.A.1</th>
                                 <th>II.A.1</th>
                                 <th>II.A.2</th>
                                 <th>Total</th>
                                 <th>I.A.1</th>
                                 <th>II.A.1</th>
                                 <th>II.A.2</th>
                                 <th>Total11 </th>
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<div class="modal fade" id="UpdateModal" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg" role="document" style="width: 90%">
   <div class="modal-content">
      <div class="modal-header bg-green" style="padding: 12px">
         <div class="row">
            <div class="col-md-10">
               <h4 style="font-size:2.5rem" class="modal-title" id="addRecordModalLabel">
                  <i class="fa fa-plus fa-fw"></i>
                  Add Budget Allotment
               </h4>
            </div>
            <div class="col-md-2">
               <a id="dateMod" class="btn bg-light-green" href="#modify_date_container" data-toggle="collapse">
               <i class="glyphicon glyphicon-wrench"></i>
               <span>Modify Date</span>
               </a>
               <div id="modify_date_container" class="collapse">
                  <p></p>
                  <input class="form-control input-sm" type="text" value="<?php echo date('Y-m-d'); ?>" style="width: 120px" id="modalDate" name="modalDate">
               </div>
            </div>
         </div>
      </div>
      <div class="modal-body">
         <form id="addrecordform" class="uppercase" accept-charset="utf-8">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="body">
                           <div class="row clearfix">
                              <div class="col-md-12">
                                 <label class="form-label" style="font-size: 1.25rem">Specify Budget Allotment Form Type:</label><br>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                       <div class="form-line" id="budget_listModalhandle">
                                          <select class="form-control" id="budget_listModal" name="budget_listModal" required>
                                             <option value="--"  selected>Budget List</option>
                                             <option value="PERSONNEL SERVICES">PS</option>
                                             <option value="MAINTENANCE AND OTHER OPERATING EXPENSES">MOOE</option>
                                             <option value="CAPITAL OUTLAY">CO</option>
                                             <option value="FINANCIAL EXPENSE">FE</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                       <div class="form-line" id="budget_classificationModalhandle">
                                          <select class="form-control" id="budget_classificationModal" name="budget_classificationModal" required>
                                             <option value="--" selected>Budget Classification</option>
                                             <option value="GAS">GAS l.A.1</option>
                                             <option value="AMCFP">AMCFP ll.A.1</option>
                                             <option value="FORMULATING & EQUIPMENT">FORMULATING & EQUIPMENT ll.A.2</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row clearfix">
                              <div class="alert alert-danger fade in" id="searchMssgModal">
                                 <a href="#" class="close" data-dismiss="alert">&times;</a>
                                 <strong>Error!</strong> Should filled this form, that will allow to generate forms.
                              </div>
                              <div class="col-md-12">
                                 <button type="button" class="btn btn-block" id="generate_form" name="generate_form">
                                 <strong>GENERATE FORM</strong>
                                 </button>
                              </div>
                           </div>
                           <div id="more_information" class="">
                              <div class="container">
                                 <div class="row"  id="formloader">
                                    <div class="col-md-12">
                                       <div class="loader">
                                          <p>Loading...</p>
                                          <div class="loader-inner"></div>
                                          <div class="loader-inner"></div>
                                          <div class="loader-inner"></div>
                                       </div>
                                    </div>
                                    <label class="form-label" style="font-size: 1.25rem">Specify Budget Allotment Form Type:</label><br>
                                 </div>
                              </div>
                              <div class="alert alert-danger fade in" id="AllotmentMssgModal">
                                 <a href="#" class="close" data-dismiss="alert">&times;</a>
                                 <strong>Error!</strong> Should filled this form, that will allow to generate form.
                              </div>
                              <div class="row clearfix" id="formGen">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer" style="padding: 0; margin: 0">
                        <input type="hidden" name="AllotmentDate" id="AllotmentDate" value="<?php echo date('Y-m-d'); ?>">
                        <div class="row clearfix">
                           <div class="col-md-6">
                              <button type="button" class="btn btn-lg btn-success btn-block waves-effect" id="btnsubmitadd" name="btnsubmitadd" disabled>
                              <i class="fa fa-paper-plane fa-fw"></i>
                              <strong>SUBMIT</strong>
                              </button>
                           </div>
                           <div class="col-md-6">
                              <button id="btnclear" name="btnclear" type="button" class="btn btn-lg btn-danger btn-block waves-effect" disabled>
                              <i class="fa fa-trash fa-fw"></i>
                              <strong>CLEAR</strong>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </form>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/genSAOB1/budget.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>

