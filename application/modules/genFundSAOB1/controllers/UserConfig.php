<?php

				
class UserConfig extends MX_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}





		public function getBaf(){
		$ret = $this->UserConfigCollection->getBafs($_POST);
		echo $ret;
		
			}

		public function getBafOption(){
		$ret = $this->UserConfigCollection->getBafsOption($_POST);
		echo $ret;
		
			}

		// public function getOptionData(){
		// $ret = $this->UserConfigCollection->getOptionsData($_POST);
		// echo $ret;
		
		// 	}
		// public function rightgetOptionData(){
		// $ret = $this->UserConfigCollection->rightgetOptionsData($_POST);
		// echo $ret;
		
		// 	}
		// public function getOptionDataType(){
		// $ret = $this->UserConfigCollection->getOptionDataTypes($_POST);
		// echo $ret;
		
		// 	}
		// public function rightgetOptionDataType(){
		
		// $ret = $this->UserConfigCollection->rightgetOptionDataTypes($_POST);
		// echo $ret;
		
		// 	}
		public function GetallotmentBalance(){
			//var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->GetallotmentBalances($_POST);
		echo $ret;
		
			}
		public function GetallotmentBalanceCur(){
			//var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->GetallotmentBalancesCur($_POST);
		echo $ret;
		
			}
		public function GetallotmentBalanceCurCheck(){
			//var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->GetallotmentBalancesCurCheck($_POST);
		echo $ret;
		
			}
		public function GetallotmentBalanceCurCheckRight(){
			//var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->GetallotmentBalancesCurCheckRight($_POST);
		echo $ret;
		
			}

		public function addAllotment(){
		$ret = $this->UserConfigCollection->addAllotments($_POST);
		echo $ret;
	}





	public function exportExcel(){
	    ob_start();
	    $mydate = $_POST['excelDate'];
		$out = $this->TRExportExcel($mydate);
		ob_end_clean();
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $out;
		die();
		exit;
		//$gg = $_POST['dateMonth'];
		//var_dump($gg);
	}



	public function TRExportExcel($mydate){
		$thedate = $mydate;

		
			$topheader = 	'<table style="font-weight:bold;">' .
							'<tr>' .
								'<td colspan="30" align="center">STATEMENT OF ALLOTMENTS/OBLIGATIONS AND BALANCES</td>' .
							'</tr>'.
							'<tr>' .
								'<td colspan="30" align="center">BY ACTIVITY</td>' .
							'</tr>'.
							'<tr>' .
								'<td colspan="30" align="center">As of: '.$thedate.'</td>' .
							'</tr>'.
							// '<tr>' .
							// 	'<td><h2>'.$thedate.'<h2></td>' .

							// '</tr>'.
							'<tr>' .
								'<td>Department</td>' .
								'<td>:</td>' .
								'<td>Agriculture</td>' .
							'</tr>'.
							'<tr>' .
								'<td>Agency/Bureau/Office</td>' .
								'<td>:</td>' .
								'<td>Agricultural Credit Policy Council</td>' .
							'</tr>'.
							'<tr>' .
								'<td>Fund Title</td>' .
								'<td>:</td>' .
								'<td>General Fund</td>' .
							'</tr>'.	
							'</table>';




				$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
							 <thead>
                                      <tr>
                                          <th rowspan="3" class="no-sort">Expenditures</th>
                                          <th rowspan="3" class="no-sort">UACS</th> 
                                          <th colspan="3" style="text-align: center;" class="no-sort">Last Quarter</th>
                                          <th></th>
                                          <th colspan="3" style="text-align: center;" class="no-sort">This  Quarter</th>
                                          <th></th>
                                          <th colspan="4" style="text-align: center;" class="no-sort">ALLOTMENT</th>
                                          <th colspan="12" style="text-align: center;" class="no-sort">OBLIGATIONS INCURRED</th>
                                          <th colspan="4" style="text-align: center;" class="no-sort">BALANCES</th>
                                      </tr>
                                      <tr style="background:;">
                                         
                                          <th rowspan="2">I0.A.1</th> 
                                          <th rowspan="2">II.A.1</th> 
                                          <th rowspan="2">II.A.2</th> 
                                          <th rowspan="2">Total</th>
                                          <th rowspan="2">I.A.1</th> 
                                          <th rowspan="2">II.A.1</th> 
                                          <th rowspan="2">II.A.2</th> 
                                          <th rowspan="2">Total</th>
                                          <th rowspan="2">I.A.1</th> 
                                          <th rowspan="2">II.A.1</th> 
                                          <th rowspan="2">II.A.2</th> 
                                          <th rowspan="2">Total</th>
                                          <th colspan="4" style="text-align: center;">Month AS OF FEB</th>
                                          <th colspan="4" style="text-align: center;">This Month - MARCH - 2018</th>
                                          <th rowspan="2">I.A.1</th> 
                                          <th rowspan="2">II.A.1</th> 
                                          <th rowspan="2">II.A.2</th> 
                                          <th rowspan="2">Total</th>
                                          <th rowspan="2">I.A.1</th> 
                                          <th rowspan="2">II.A.1</th> 
                                          <th rowspan="2">II.A.2</th> 
                                          <th rowspan="2">Total</th>
                                     
                                      </tr>
                                      <tr>
                                          <th>I.A.1</th> 
                                          <th>II.A.1</th> 
                                          <th>II.A.2</th>
                                          <th>Total</th>

                                          <th>I.A.1</th> 
                                          <th>II.A.1</th> 
                                          <th>II.A.2</th>
                                          <th>Total11 </th>
                                      </tr>
                                  </thead> 
							<tbody>';

					$tabledata = '';
					$data = '';
					
							// $json_string =  file_get_contents( $GLOBALS['backend_URL']."ORS/API/v1/getDataBAF306B/date/".$thedate);
							// $arrs = json_decode($json_string, true);
							
							//$theDate = $_POST['date'];
      						$url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$thedate;
      						$ret = Helper::serviceGet($url);
      						$arrsSTD = json_decode($ret);
      						$arrs = json_decode( json_encode($arrsSTD), true);
      						// $arrs = json_decode( json_encode($ret), true);

      						//$arrs = serialize($strarrs);

      						//var_dump($arrs); die();
	     					
							$response = $arrs["ResponseResult"];
							$prev = 0;
							$count = 0;
							function sortById($x, $y) {
							return  $x['code'] - $y['code'];
							}
							foreach ($response as $i => $value) {	
								$child = $response[$i]['BAFList'];
								usort($child, 'sortById');

								${"lastQuarterFundTotalaf" . $i} = 0;
								${"lastQuarterFundTotalam" . $i} = 0;
								${"lastQuarterFundTotalos" . $i} = 0;
								${"lastQuarterFundTotaltotal" . $i} = 0;
								
								${"thisQuarterFundTotalaf" . $i} = 0;
								${"thisQuarterFundTotalam" . $i} = 0;
								${"thisQuarterFundTotalos" . $i} = 0;
								${"thisQuarterFundTotaltotal" . $i} = 0;

								${"allotmentFundTotalaf" . $i} = 0;
								${"allotmentFundTotalam" . $i} = 0;
								${"allotmentFundTotalos" . $i} = 0;
								${"allotmentFundTotaltotal" . $i} = 0;
								
								${"previousMonthFundTotalaf" . $i} = 0;
								${"previousMonthFundTotalam" . $i} = 0;
								${"previousMonthFundTotalos" . $i} = 0;
								${"previousMonthFundTotaltotal" . $i} = 0;
								
								${"selectedMonthFundTotalaf" . $i} = 0;
								${"selectedMonthFundTotalam" . $i} = 0;
								${"selectedMonthFundTotalos" . $i} = 0;
								${"selectedMonthFundTotaltotal" . $i} = 0;
								
								${"totalMonthFundTotalaf" . $i} = 0;
								${"totalMonthFundTotalam" . $i} = 0;
								${"totalMonthFundTotalos" . $i} = 0;
								${"totalMonthFundTotaltotal" . $i} = 0;
								
								${"balanceFundTotalaf" . $i} = 0;
								${"balanceFundTotalam" . $i} = 0;
								${"balanceFundTotalos" . $i} = 0;
								${"balanceFundTotaltotal" . $i} = 0;


								$childnum = count($response[$i]['BAFList']);
								var_dump($childnum);
										 foreach ($child as $a => $value1) {
											
										 	$count = $count + 1;
										 	${"lastQuarterFundTotalaf" . $i} = ${"lastQuarterFundTotalaf" . $i} +  $child[$a]['lastQuarterFund']['af'];
										 	${"lastQuarterFundTotalam" . $i} = ${"lastQuarterFundTotalam" . $i} +  $child[$a]['lastQuarterFund']['am'];
										 	${"lastQuarterFundTotalos" . $i} = ${"lastQuarterFundTotalos" . $i} +  $child[$a]['lastQuarterFund']['os'];
										 	${"lastQuarterFundTotaltotal" . $i} = ${"lastQuarterFundTotaltotal" . $i} +  $child[$a]['lastQuarterFund']['total'];

										 	${"thisQuarterFundTotalaf" . $i} = ${"thisQuarterFundTotalaf" . $i} +  $child[$a]['thisQuarterFund']['af'];
										 	${"thisQuarterFundTotalam" . $i} = ${"thisQuarterFundTotalam" . $i} +  $child[$a]['thisQuarterFund']['am'];
										 	${"thisQuarterFundTotalos" . $i} = ${"thisQuarterFundTotalos" . $i} +  $child[$a]['thisQuarterFund']['os'];
										 	${"thisQuarterFundTotaltotal" . $i} = ${"thisQuarterFundTotaltotal" . $i} +  $child[$a]['thisQuarterFund']['total'];

										 	${"allotmentFundTotalaf" . $i} = ${"allotmentFundTotalaf" . $i} +  $child[$a]['allotmentFund']['af'];
										 	${"allotmentFundTotalam" . $i} = ${"allotmentFundTotalam" . $i} +  $child[$a]['allotmentFund']['am'];
										 	${"allotmentFundTotalos" . $i} = ${"allotmentFundTotalos" . $i} +  $child[$a]['allotmentFund']['os'];
										 	${"allotmentFundTotaltotal" . $i} = ${"allotmentFundTotaltotal" . $i} +  $child[$a]['allotmentFund']['total'];

										 	${"previousMonthFundTotalaf" . $i} = ${"previousMonthFundTotalaf" . $i} +  $child[$a]['previousMonthFund']['af'];
										 	${"previousMonthFundTotalam" . $i} = ${"previousMonthFundTotalam" . $i} +  $child[$a]['previousMonthFund']['am'];
										 	${"previousMonthFundTotalos" . $i} = ${"previousMonthFundTotalos" . $i} +  $child[$a]['previousMonthFund']['os'];
										 	${"previousMonthFundTotaltotal" . $i} = ${"previousMonthFundTotaltotal" . $i} +  $child[$a]['previousMonthFund']['total'];

										 	${"selectedMonthFundTotalaf" . $i} = ${"selectedMonthFundTotalaf" . $i} +  $child[$a]['previousMonthFund']['af'];
										 	${"selectedMonthFundTotalam" . $i} = ${"selectedMonthFundTotalam" . $i} +  $child[$a]['previousMonthFund']['am'];
										 	${"selectedMonthFundTotalos" . $i} = ${"selectedMonthFundTotalos" . $i} +  $child[$a]['previousMonthFund']['os'];
										 	${"selectedMonthFundTotaltotal" . $i} = ${"selectedMonthFundTotaltotal" . $i} +  $child[$a]['previousMonthFund']['total'];

										 	${"totalMonthFundTotalaf" . $i} = ${"totalMonthFundTotalaf" . $i} +  $child[$a]['totalMonthFund']['af'];
										 	${"totalMonthFundTotalam" . $i} = ${"totalMonthFundTotalam" . $i} +  $child[$a]['totalMonthFund']['am'];
										 	${"totalMonthFundTotalos" . $i} = ${"totalMonthFundTotalos" . $i} +  $child[$a]['totalMonthFund']['os'];
										 	${"totalMonthFundTotaltotal" . $i} = ${"totalMonthFundTotaltotal" . $i} +  $child[$a]['totalMonthFund']['total'];

										 	${"balanceFundTotalaf" . $i} = ${"balanceFundTotalaf" . $i} +  $child[$a]['balanceFund']['af'];
										 	${"balanceFundTotalam" . $i} = ${"balanceFundTotalam" . $i} +  $child[$a]['balanceFund']['am'];
										 	${"balanceFundTotalos" . $i} = ${"balanceFundTotalos" . $i} +  $child[$a]['balanceFund']['os'];
										 	${"balanceFundTotaltotal" . $i} = ${"balanceFundTotaltotal" . $i} +  $child[$a]['balanceFund']['total'];



										 	if($count < $childnum){
										 		//echo '<pre>' .$count.' = '.$childnum . '</pre>';
									 		$data = $data . '<tr>'.
												'<td>'.$child[$a]['expendName'].'</td>' .
												'<td style="width:50px;">'.$child[$a]['code'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['af'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['am'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['os'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['total'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['af'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['am'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['os'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['total'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['af'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['am'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['os'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['total'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['af'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['am'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['os'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['total'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['af'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['am'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['os'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['total'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['af'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['am'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['os'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['total'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['af'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['am'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['os'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['total'].'</td>' .
												'</tr>';
											}else{
													$count = 0;
													//echo '<pre> else' .$count.' = '.$childnum . '</pre>';
													$data = $data . 

												'<tr>'.
												'<td>'.$child[$a]['expendName'].'</td>' .
												'<td style="width:50px;">'.$child[$a]['code'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['af'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['am'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['os'].'</td>' .
												'<td>'.$child[$a]['lastQuarterFund']['total'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['af'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['am'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['os'].'</td>' .
												'<td>'.$child[$a]['thisQuarterFund']['total'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['af'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['am'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['os'].'</td>' .
												'<td>'.$child[$a]['allotmentFund']['total'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['af'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['am'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['os'].'</td>' .
												'<td>'.$child[$a]['previousMonthFund']['total'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['af'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['am'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['os'].'</td>' .
												'<td>'.$child[$a]['selectedMonthFund']['total'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['af'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['am'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['os'].'</td>' .
												'<td>'.$child[$a]['totalMonthFund']['total'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['af'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['am'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['os'].'</td>' .
												'<td>'.$child[$a]['balanceFund']['total'].'</td>' .
												'</tr>'.
												'<tr style="background: yellow;  border: 3px solid red; font-weight: bold;">'.
												'<td colspan="2">Total</td>' .
												'<td>'.${"lastQuarterFundTotalaf" . $i}.'</td>' .
												'<td>'.${"lastQuarterFundTotalam" . $i}.'</td>' .
												'<td>'.${"lastQuarterFundTotalos" . $i}.'</td>' .
												'<td>'.${"lastQuarterFundTotaltotal" . $i}.'</td>' .
												'<td>'.${"thisQuarterFundTotalaf" . $i}.'</td>' .
												'<td>'.${"thisQuarterFundTotalam" . $i}.'</td>' .
												'<td>'.${"thisQuarterFundTotalos" . $i}.'</td>' .
												'<td>'.${"thisQuarterFundTotaltotal" . $i}.'</td>' .
												'<td>'.${"allotmentFundTotalaf" . $i}.'</td>' .
												'<td>'.${"allotmentFundTotalam" . $i}.'</td>' .
												'<td>'.${"allotmentFundTotalos" . $i}.'</td>' .
												'<td>'.${"allotmentFundTotaltotal" . $i}.'</td>' .
												'<td>'.${"previousMonthFundTotalaf" . $i}.'</td>' .
												'<td>'.${"previousMonthFundTotalam" . $i}.'</td>' .
												'<td>'.${"previousMonthFundTotalos" . $i}.'</td>' .
												'<td>'.${"previousMonthFundTotaltotal" . $i}.'</td>' .
												'<td>'.${"selectedMonthFundTotalaf" . $i}.'</td>' .
												'<td>'.${"selectedMonthFundTotalam" . $i}.'</td>' .
												'<td>'.${"selectedMonthFundTotalos" . $i}.'</td>' .
												'<td>'.${"selectedMonthFundTotaltotal" . $i}.'</td>' .
												'<td>'.${"totalMonthFundTotalaf" . $i}.'</td>' .
												'<td>'.${"totalMonthFundTotalam" . $i}.'</td>' .
												'<td>'.${"totalMonthFundTotalos" . $i}.'</td>' .
												'<td>'.${"totalMonthFundTotaltotal" . $i}.'</td>' .
												'<td>'.${"balanceFundTotalaf" . $i}.'</td>' .
												'<td>'.${"balanceFundTotalam" . $i}.'</td>' .
												'<td>'.${"balanceFundTotalos" . $i}.'</td>' .
												'<td>'.${"balanceFundTotaltotal" . $i}.'</td>' .
												'</tr>';
											}
								}

					}

					$tableend = '</tbody></table>';
					
				//die();
					return $topheader . $tablehead . $data . $tableend;




		
	}


	
}
?>