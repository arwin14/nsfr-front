  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


//========================================================
   public function getBafs(){
      $theDate = $_POST['date'];
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        //var_dump($ret); die();
        return $ret;
        //var_dump($content); die();

    }
//========================================================
   public function getBafsOption(){
      $theDate = $_POST['date'];
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        //var_dump($ret); die();
        return $ret;
        //var_dump($content); die();

    }

//========================================================

   public function rightgetOptionDataTypes(){
    //var_dump($_POST['date']);
      $theDate = $_POST['date'];
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        //var_dump($ret); die();
        return $ret;
        //var_dump($content); die();

    }
//========================================================
   public function GetallotmentBalances(){
      $theDate = $_POST['date'];
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        return $ret;

    }
//========================================================
   public function GetallotmentBalancesCur(){
      $theDate = $_POST['date'];
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        return $ret;

    }
//========================================================
   public function GetallotmentBalancesCurCheck(){
      $theDate = $_POST['date'];
      //var_dump($theDate); die();
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        return $ret;

    }

//========================================================
   public function GetallotmentBalancesCurCheckRight(){
      $theDate = $_POST['date'];
      //var_dump($theDate); die();
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataBAF306B/date/".$theDate;
      $ret = Helper::serviceGet($url);
      $result = json_encode($ret);
        return $ret;

    }

//========================================================

 public function addAllotments(){
   $post = $this->input->post();
      $post_data = array();

      foreach ($post as $k => $v) {
        $post_data[$k] = $this->input->post($k,true);

      }


      $orsContentList = array();
      $budget_listModal = $_POST['budget_listModal'];
      $budget_classificationModal = $_POST['budget_classificationModal'];
      foreach($post_data['allotmentTxt'] as $k3 => $v3){
         
            // $budgetList = 'I.A.1';
          if($_POST['allotmentTxt'][$k3] == ""){
              $allotment = '0';
          }else{
               $allotment =$_POST['allotmentTxt'][$k3];
          }



                if($budget_listModal == 'PERSONNEL SERVICES'){
                   $listbudget = array(
                     "fundCategory" => "II.A.1"
                    ,"ps" => $allotment
                    ,"mooe" => "0"
                    ,"co" => "0"
                    ,"fe" => "0"
                   );
                }else if($budget_listModal == 'MAINTENANCE AND OTHER OPERATING EXPENSES'){
                    $listbudget = array(
                     "fundCategory" => "II.A.1"
                    ,"ps" => "0"
                    ,"mooe" => $allotment
                    ,"co" => "0"
                    ,"fe" => "0"
                   );
                }else if($budget_listModal == 'CAPITAL OUTLAY'){
                    $listbudget = array(
                     "fundCategory" => "II.A.1"
                    ,"ps" => "0"
                    ,"mooe" => "0"
                    ,"co" => $allotment
                    ,"fe" => "0"
                   );
                }else if($budget_listModal == 'FINANCIAL EXPENSE'){
                    $listbudget = array(
                     "fundCategory" => "II.A.2"
                    ,"ps" => "0"
                    ,"mooe" => "0"
                    ,"co" => "0"
                    ,"fe" => $allotment
                   );
                }

               
      if($budget_classificationModal == 'GAS'){
        $formdata[] = 
          array(
          'expendCode'=>$_POST['expendCode'][$k3],
          'tax'=>"0",
          "ocdAF" =>  $listbudget
          ,"ocdAM" => array(
                "fundCategory" => "II.A.1"
                ,"ps" => "0"
                ,"mooe" => "0"
                ,"co" => "0" 
                ,"fe" => "0"      
            ) 
          ,"ocdOS" => array(
                "fundCategory" => "II.A.2"
                 ,"ps" => "0"
                ,"mooe" => "0"
                ,"co" => "0" 
                ,"fe" => "0"         
              )

        );                  
        }else if ($budget_classificationModal == 'AMCFP'){
        $formdata[] = 
          array(
          'expendCode'=>$_POST['expendCode'][$k3],
          'tax'=>"0",
          "ocdAF" => array(
                "fundCategory" => "I.A.1"
                ,"ps" => "0"
                ,"mooe" => "0"
                ,"co" => "0" 
                ,"fe" => "0"      
            ) 
          ,"ocdAM" => $listbudget
          ,"ocdOS" => array(
                "fundCategory" => "II.A.2"
                 ,"ps" => "0"
                ,"mooe" => "0"
                ,"co" => "0" 
                ,"fe" => "0"         
              )

        );     
        }else if ($budget_classificationModal == 'FORMULATING & EQUIPMENT'){
        $formdata[] = 
          array(
          'expendCode'=>$_POST['expendCode'][$k3],
          'tax'=>"0",
          "ocdAF" => array(
                "fundCategory" => "I.A.1"
                ,"ps" => "0"
                ,"mooe" => "0"
                ,"co" => "0" 
                ,"fe" => "0"      
            ) 
          ,"ocdAM" =>array(
                "fundCategory" => "II.A.2"
                ,"ps" => "0"
                ,"mooe" => "0"
                ,"co" => "0" 
                ,"fe" => "0"      
            ) 
          ,"ocdOS" => $listbudget

        );       
        }
         
         


      }



      $arr =
        array(

            "fundConsumer" => 'ADJUSTMENT',
            "expend_type" => $budget_listModal,
            "fund_category" => $budget_classificationModal,
            "fundDate" => $_POST['AllotmentDate'],
            "createdBy" =>  $_SESSION['userid'],//============================================
            "refid2" => 1,
            "orsfContentList" => $formdata
            );
      $result = json_encode($arr);
        //var_dump($result);die();
    //var_dump($result); die();



      $ret = Helper::serviceCallBudget($result,  $GLOBALS['backend_URL']."FARF/API/v1/insert");
      echo json_encode($ret);

     }




//========================================================
    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>