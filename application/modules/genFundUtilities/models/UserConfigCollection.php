  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


//=======================================================
       public function getOptionData(){
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllExpenditureType/type';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }
//=======================================================
       public function getExpendsSaob(){

        $urlOSF = $GLOBALS['backend_URL'].'NSFRA/API/v1/getExpenditureFundType/fundtype/1';
        $retOSF = Helper::serviceGet($urlOSF);
        $resultOSF = json_encode($retOSF);

         $urlRSF = $GLOBALS['backend_URL'].'NSFRA/API/v1/getExpenditureFundType/fundtype/2';
        $retRSF = Helper::serviceGet($urlRSF);
        $resultRSF = json_encode($retRSF);
        
        $expendType['OSF'] = $retOSF;
        $expendType['RSF'] = $retRSF;
        //var_dump($ret);
          //    
        return $expendType;
    }
//=======================================================
       public function getOptionsSaobUpdate(){

        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllExpenditureType/type';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;
    }
//=======================================================
       public function getExpendsFar(){
       $url = $GLOBALS['backend_URL'].'FAR/API/v1/getAllExpenditures/expenditures/';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }
//=======================================================
       public function getUpdatesAvail(){
        $id = $_POST['id'];
        //http://127.0.0.1:8080/NSFRAccountingAPI/BUDGETService/NSFRA/API/v1/selectEXP/id/1
       $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/selectEXP/id/'.$id;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }
//=======================================================
       public function getUpdatesFAR(){
        $id = $_POST['id'];
       $url = $GLOBALS['backend_URL'].'FAR/API/v1/selectEXP/id/'.$id;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }
//========================================================
  public function addExpendsAvail(){

      $arr =
        array(
            "code" => $_POST['expenCodeAvail'],
            "name" => $_POST['expenNameAvail'],
            "factor" => $_POST['factorAvail'],
            "expend_type" => $_POST['expenTypeAvail']
            );
      $result = json_encode($arr);
      var_dump($result);die();


      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."NSFRA/API/v1/insertEXP");
        echo json_encode($ret);
          // die();
          //return $ret;
      
     }
//========================================================
  public function addNewExpendsFar(){

      $arr =
        array(
            "code" => $_POST['expenCodeFar'],
            "name" => $_POST['expenNameFar'],
            "expend_type" => $_POST['expenTypeFar']
            );
      $result = json_encode($arr);
      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."FAR/API/v1/insertEXP");
      echo json_encode($ret);
        // die();
        //return $ret;
      
     }
//========================================================
  public function UpdateExpendsAvail(){

      $arr =
        array(
            "id" => $_POST['theid'],
            "code" => $_POST['codeAvail'],
            "name" => $_POST['budgetNAvail'],
            "factor" => $_POST['factorAvailUpdate'],
            "expend_type" => $_POST['codeCategAvail'],
            "status" => 'ACTIVE'
            );
        $result = json_encode($arr);
        //var_dump($result); die();
      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."NSFRA/API/v1/updateEXP");
      echo json_encode($ret);
        // die();
        //return $ret;
      
     }
//========================================================
  public function UpdateExpendsReq(){

      $arr =
        array(
            "id" => $_POST['theid'],
            "code" => $_POST['codeReq'],
            "name" => $_POST['budgetNReq'],
            "factor" => $_POST['factorReqUpdate'],
            "expend_type" => $_POST['codeCategReq'],
            "status" => 'ACTIVE'
            );
      $result = json_encode($arr);
      //var_dump($result);
      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."NSFRA/API/v1/updateEXP");
      echo json_encode($ret);
      
     }

//========================================================
     public function removeReqExpends(){
      $code = $_POST['idbase'];
       $url = $GLOBALS['backend_URL']."NSFRA/API/v1/deactivateEXP/id/".$code; 

      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   
      //var_dump($ret); die();    
      return $ret;


    }

//========================================================
     public function removeAvailExpends(){
      $code = $_POST['idbase'];
       $url = $GLOBALS['backend_URL']."NSFRA/API/v1/deactivateEXP/id/".$code; 

      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   
      //var_dump($ret); die();    
      return $ret;


    }

//========================================================
    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>