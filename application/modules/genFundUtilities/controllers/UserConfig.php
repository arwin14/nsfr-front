<?php

class UserConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
		$viewData = array();
		$ret = $this->UserConfigCollection->getExpendsSaob($_POST);

		$arrsSTD_osf = json_decode($ret['OSF']);
		$arrs_osf = json_decode( json_encode($arrsSTD_osf), true);
		$result_osf = $arrs_osf['ResponseResult'];

		$arrsSTD_rsf = json_decode($ret['RSF']);
		$arrs_rsf = json_decode( json_encode($arrsSTD_rsf), true);
		$result_rsf = $arrs_rsf['ResponseResult'];
		//var_dump($result_osf); die();















		$ydata['ydata'] = "yform";
		$return = array();
		// var_dump($result[0]['name']); die();
		if($result_osf == "NO DATA FOUND" || $result_rsf == "NO DATA FOUND"){
			$return[] = array(
			 "code" => '',
			 "name" => 'no data',
			 "amount" => '',
			 "btn" => ''
			);
		}else{	
			foreach ($result_osf as $a => $value) {
			//if($a > 0 && $a < 45){
				$return['tbldataAvail'][$a] = array(
			 		"btn" => "<a class='btn btn-simple btn-warning btn-icon btnUpdateAvail' data-code='".$result_osf[$a]['id']."' data-form='updateAvail' data-name='#budgetNAvail' data-codeavail='#codeAvail' data-categ='#codeCategAvail' data-factor='#factorAvailUpdate' data-id='#theid' data-addcateg='#expenTypeAvail'><i class='fa fa-edit'></i></a>".
			  				 "<a  data-code='".$result_osf[$a]['id']."' data-direct='removeAvail' class='btn btn-simple btn-danger btn-icon deleteAvail'><i class='fa fa-times'></i></a>",
			 		"name" => $result_osf[$a]['name'],
			 		"title" => $result_osf[$a]['title'],
			 		"factor" => $result_osf[$a]['factor'],
			 		"code" => $result_osf[$a]['code'],
				);
			}
			foreach ($result_rsf as $b => $value) {
			$return['tbldataRequired'][$b] = array(
				 "btn" => "<a class='btn btn-simple btn-warning btn-icon btnUpdateRequire' data-code='".$result_rsf[$b]['id']."' data-form='updateRequire' data-name='#budgetNReq' data-codeavail='#codeReq' data-categ='#codeCategReq' data-factor='#factorReqUpdate' data-id='#theidReq' data-addcateg='#expenTypeReq'><i class='fa fa-edit'></i></a>".
				  "<a  data-code='".$result_rsf[$b]['id']."' data-direct='removeReq' class='btn btn-simple btn-danger btn-icon deleteReq'><i class='fa fa-times'></i></a>",
				 "name" => $result_rsf[$b]['name'],
				 "title" => $result_rsf[$b]['title'],
				 "factor" => $result_rsf[$b]['factor'],
				 "code" => $result_rsf[$b]['code'],
				);
			
			}
		}

        $this->Helper->sessionEndedHook('Session');
        $viewData['table'] = $this->load->view("forms/StableFunding",$return,TRUE);
        $viewData['content'] = $this->load->view("forms/stableform",$ydata,TRUE);
		$this->Helper->setTitle('User Configuration'); //--> Set afunc to the Template
		$this->Helper->setView('UserConfig.form',$viewData,FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}



	public function tblRespond(){
		$ret = $this->UserConfigCollection->getExpendsSaob($_POST);

	$viewData = array();
		$ret = $this->UserConfigCollection->getExpendsSaob($_POST);

		$arrsSTD_osf = json_decode($ret['OSF']);
		$arrs_osf = json_decode( json_encode($arrsSTD_osf), true);
		$result_osf = $arrs_osf['ResponseResult'];

		$arrsSTD_rsf = json_decode($ret['RSF']);
		$arrs_rsf = json_decode( json_encode($arrsSTD_rsf), true);
		$result_rsf = $arrs_rsf['ResponseResult'];
		//var_dump($result_osf); die();















		$ydata['ydata'] = "yform";
		$return = array();
		// var_dump($result[0]['name']); die();
		if($result_osf == "NO DATA FOUND" || $result_rsf == "NO DATA FOUND"){
			$return[] = array(
			 "code" => '',
			 "name" => 'no data',
			 "amount" => '',
			 "btn" => ''
			);
		}else{	
			foreach ($result_osf as $a => $value) {
			//if($a > 0 && $a < 45){
				$return['tbldataAvail'][$a] = array(
			 		"btn" => "<a class='btn btn-simple btn-warning btn-icon btnUpdateAvail' data-code='".$result_osf[$a]['id']."' data-form='updateAvail' data-name='#budgetNAvail' data-codeavail='#codeAvail' data-categ='#codeCategAvail' data-factor='#factorAvailUpdate' data-id='#theid' data-addcateg='#expenTypeAvail'><i class='fa fa-edit'></i></a>".
			  				 "<a  data-code='".$result_osf[$a]['id']."' data-direct='removeAvail' class='btn btn-simple btn-danger btn-icon deleteAvail'><i class='fa fa-times'></i></a>",
			 		"name" => $result_osf[$a]['name'],
			 		"title" => $result_osf[$a]['title'],
			 		"factor" => $result_osf[$a]['factor'],
			 		"code" => $result_osf[$a]['code'],
				);
			}
			foreach ($result_rsf as $b => $value) {
			$return['tbldataRequired'][$b] = array(
				 "btn" => "<a class='btn btn-simple btn-warning btn-icon btnUpdateRequire' data-code='".$result_rsf[$b]['id']."' data-form='updateRequire' data-name='#budgetNReq' data-codeavail='#codeReq' data-categ='#codeCategReq' data-factor='#factorReqUpdate' data-id='#theidReq' data-addcateg='#expenTypeReq'><i class='fa fa-edit'></i></a>".
				  "<a  data-code='".$result_rsf[$b]['id']."' data-direct='removeReq' class='btn btn-simple btn-danger btn-icon deleteReq'><i class='fa fa-times'></i></a>",
				 "name" => $result_rsf[$b]['name'],
				 "title" => $result_rsf[$b]['title'],
				 "factor" => $result_rsf[$b]['factor'],
				 "code" => $result_rsf[$b]['code'],
				);
			
			}
		}




	$result = array();
	 $viewData['table'] = $this->load->view("forms/StableFunding",$return,TRUE);
		echo json_encode($viewData);
	//var_dump($result);
	}

	public function getOptionAdd(){
		$ret = $this->UserConfigCollection->getOptionData($_POST);
		echo $ret;
		
			}	//getOptionSaobUpdate	
	public function getOptionSaobUpdate(){
		$ret = $this->UserConfigCollection->getOptionsSaobUpdate($_POST);
		echo $ret;
		
			}

	public function getExpendSaob(){

		// var_dump($_POST['date']); die();
		// $ret = $this->UserConfigCollection->getExpendsSaob($_POST);
		// echo $ret;
		
			}
	public function getExpendFar(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getExpendsFar($_POST);
		echo $ret;
		
			}
	public function getUpdateAvail(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getUpdatesAvail($_POST);
		echo $ret;
		
			}
	public function getUpdateFAR(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getUpdatesFAR($_POST);
		echo $ret;
		
			}

    public function addExpendAvail(){
    	//var_dump("hoy"); die();
		$ret = $this->UserConfigCollection->addExpendsAvail($_POST);
		echo $ret;
	}


	public function addNewExpendFar(){
		$ret = $this->UserConfigCollection->addNewExpendsFar($_POST);
		echo $ret;
	}

	public function UpdateExpendAvail(){
		$ret = $this->UserConfigCollection->UpdateExpendsAvail($_POST);
		echo $ret;
	}



	public function UpdateExpendReq(){
		$ret = $this->UserConfigCollection->UpdateExpendsReq($_POST);
		echo $ret;
	}

	public function removeAvail(){
		$ret = $this->UserConfigCollection->removeAvailExpends($_POST);
		echo $ret;
	}

	public function removeReq(){
		$ret = $this->UserConfigCollection->removeReqExpends($_POST);
		echo $ret;
	}
}
?>