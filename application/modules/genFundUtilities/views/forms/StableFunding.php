<?php                  //        var_dump($tbldata) ; die();

?>             
                  <div class="card">
                     <div class="header">
                        <h2>
                           Item List
                        </h2>
                     </div>
                     <div class="body" style="width:95%; margin:auto;">
                        <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" data-form="addavail" data-addcateg="#expenTypeAvail" href=".tbls1" id="btnsaob">Available Stable Funding</a></li>
                           <li><a data-toggle="tab" data-form="addreq" data-addcateg="#expenTypeReq" href=".tbls2" id="btnfar">Required Stable Funding</a></li>
                        </ul>
                        <div class="tab-content">
                           <div id="tbl1" class="tbls1 tab-pane fade in active">
                              <div class="table-responsive">
                                 <table id="updateexpendAvailtbl" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                                    <thead>
                                       <tr>
                                          <th></th>
                                          <th>Item Name</th>
                                          <th>Category</th>
                                          <th>Factor</th>
                                          <th>Code</th>
                                       </tr>
                                    </thead>
                                     <tbody>
                                       <?php 
                                          foreach ($tbldataAvail as $index => $value) {   
                                          //echo $tbldata[$index]['name'];                 
                                    ?>
                                    <tr>
                                       <td><?php echo $tbldataAvail[$index]['btn']; ?></td>
                                       <td><?php echo $tbldataAvail[$index]['name']; ?></td>
                                       <td><?php echo $tbldataAvail[$index]['title']; ?></td>
                                       <td><?php echo $tbldataAvail[$index]['factor']; ?></td>
                                       <td><?php echo $tbldataAvail[$index]['code']; ?></td>
                                   </tr>
                                   <?php
                                    }
                                   ?>
                                    </tbody>
                                    <tfoot></tfoot>
                                 </table>
                              </div>
                           </div>
                                          <div id="tbl2" class="tbls2 tab-pane fade">
                              <div class="table-responsive">
                                 <table id="updateexpendReqtbl" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                                    <thead>
                                       <tr>
                                          <th></th>
                                          <th>Item Name</th>
                                          <th>Category</th>
                                          <th>Factor</th>
                                          <th>Code</th>
                                       </tr>
                                    </thead>
                                   <tbody>
                                       <?php 
                                          foreach ($tbldataRequired as $index => $value) {   
                                          //echo $tbldata[$index]['name'];                 
                                    ?>
                                    <tr>
                                       <td><?php echo $tbldataRequired[$index]['btn']; ?></td>
                                       <td><?php echo $tbldataRequired[$index]['name']; ?></td>
                                       <td><?php echo $tbldataRequired[$index]['title']; ?></td>
                                       <td><?php echo $tbldataRequired[$index]['factor']; ?></td>
                                       <td><?php echo $tbldataRequired[$index]['code']; ?></td>
                                   </tr>
                                   <?php
                                    }
                                   ?>
                                    </tbody>
                                    <tfoot></tfoot>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
          
            <script type="text/javascript">
               $(document).ready(function() {



            $('#updateexpendAvailtbl').DataTable({
             dom: 'Bfrtip',
               "headerCallback": function( thead, data, start, end, display ) {
                    $(thead).find('th').css('background-color', 'rgb(244,149,66)');
                    $(thead).find('th').css('color', 'white');
                    },
             fixedHeader: {
                 header: true,
                 footer: true
             },
             responsive: true,
             buttons: [],
             "order": [],
             destroy: true
         });

         $('#updateexpendReqtbl').DataTable({
             dom: 'Bfrtip',
               "headerCallback": function( thead, data, start, end, display ) {
                    $(thead).find('th').css('background-color', 'rgb(244,149,66)');
                    $(thead).find('th').css('color', 'white');
                    },
             fixedHeader: {
                 header: true,
                 footer: true
             },
             responsive: true,
             buttons: [],
             "order": [],
             destroy: true
         });
           
         } );
   
 
            </script>