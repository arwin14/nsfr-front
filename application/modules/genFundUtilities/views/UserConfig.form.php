






<div class="row clearfix" style="position:relative; top:0px;">

   <div class="col-md-12">
      <div class="card">
         <div class="header ">
            <legend>
               <div class="row">
                  <div class="col-md-6">
                     NSF MODIFICATION
                  </div>
                  <div class="col-md-6">
                     <div class="pull-right" style="padding-bottom:10px; ">
                        <button class="btn btn-social btn-round btn-google" data-toggle="tooltip" data-placement="right" title="Tooltip on right" id="btnmod">
                           <i class="fa fa-cog"> </i>
                        </button>
                     </div>
                  </div>
               </div>
            </legend>
            <div class="collapse" id="dateSearch">
               <div class="row clearfix">
                  <div class="col-md-4">
                     <div class="form-group form-float">
                        <div class="form-line">
                           <input type="text" id="FindDate" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="container-fluid">
         <div class="content">
           <div class="row clearfix" style="width:98%; margin:auto;">
               <div class="collapse row" id="demo" >
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                     </div>
                  </div>
               </div>
               <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                  <?php echo $content; ?>
               </div>
               <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" id="tblContainer">
                 <?php echo $table; ?>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- end card -->
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/FormModUtil/budget.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>