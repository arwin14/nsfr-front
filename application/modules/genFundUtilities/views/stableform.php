
                  <div class="updateAvailStable collapse">
                     <div class="card" id='dateSearch collapse'>
                        <div class="header">
                           <h5>
                              Update budget Code(Avail)
                           </h5>
                        </div>
                        <div class="body" style="width:90%; margin:auto;">
                         <form id="updateexpendAvail" enctype="multipart/form-data" accept-charset="utf-8">
                          <!--  <div class="row" id="AvailErrorMessageUpdate">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                       <div class="body" style="background:rgb(251,70,55);">
                                          <p style="color:white;">Please filled the form properly.(You've inputed mismatched code from type of expend code you have selected)</p>
                                       </div>
                                    </div>
                                 </div>
                              </div> -->
                              <input type="hidden" name="theid" id="theid">
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='budgetNAvail' name="budgetNAvail" placeholder="Budget Name">
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='codeAvail' name="codeAvail" placeholder="Budget Code">
                                    </div>
                                 </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='factorAvailUpdate' name="factorAvailUpdate" placeholder="Factor">
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class='form-line' id="categories">
                                        <select class="codes form-control" id='codeCategAvail' name="codeCategAvail" >
                                       </select>
                                    </div>
                                 </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <button type="button" class="btn btn-lg btn-success btn-block waves-effect" id="submitUpdateAvail" name="submitUpdateAvail">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>SUBMIT</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnCancel" name="btnCancel" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CANCEL</strong>
                                 </button>
                              </div>
                           </div>
                        </form>
                        </div>
                     </div>
                  </div>
                  <div class="updateReqStable collapse">
                     <div class="card" id='dateSearch collapse'>
                        <div class="header">
                           <h2>
                              Update budget Code(Req2)
                           </h2>
                        </div>
                        <div class="body">
                           <form id="updateexpendReq" enctype="multipart/form-data" accept-charset="utf-8">
                              <input type="hidden" name="theidReq" id="theidReq">
                            <!-- <div class="row" id="ReqErrorMessageUpdate">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                       <div class="body" style="background:rgb(251,70,55);">
                                          <p style="color:white;">Please filled the form properly.(You've inputed mismatched code from type of expend code you have selected)</p>
                                       </div>
                                    </div>
                                 </div>
                              </div> -->
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <div class="input-group input-group-md">
                                    <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='budgetNReq' name="budgetNReq" placeholder="Budget Name">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="input-group input-group-md">
                                    <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='codeReq' name="codeReq" placeholder="Budget Code">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row clearfix">
                              <div class="col-md-12">
                                 <div class="form-group form-float">
                                    <div class='form-line' id="categories">
                                       <select class="codes form-control" id='codeCategReq' name="codeCategReq" >
                                           <option value="1">PERSONNEL SERVICES</option>
                                          <option value="2">MAINTENANCE AND OTHER OPERATING EXPENSES</option>
                                          <option value="3">CAPITAL OUTLAY</option>
                                          <option value="4">FINANCIAL EXPENSE</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                            <div class="row clearfix">
                              <div class="col-md-6">
                                 <button type="button" class="btn btn-lg btn-success btn-block waves-effect" id="submitUpdateReq" name="submitUpdateReq">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>SUBMIT</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnCancel" name="btnCancel" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CANCEL</strong>
                                 </button>
                              </div>
                           </div>
                        </form>
                        </div>
                     </div>
                  </div>
                  <div>

                     <div class="card collapse in" id='addAvail'>
                        <form id="addexpendAvail" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="header">
                           <h2>
                              Add budget Code(Avail)
                           </h2>
                        </div>
                        <div class="body">
                           <div class="row clearfix">
                              <!-- <div class="row" id="AvailErrorMessage">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                       <div class="body" style="background:rgb(251,70,55);">
                                          <p style="color:white;">Please filled the form properly.(You've inputed mismatched code from type of expend code you have selected)</p>
                                       </div>
                                    </div>
                                 </div>
                              </div> -->
                              <div class="col-md-6">
                                 <div class="input-group input-group-md">
                                    <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='expenNameAvail' name="expenNameAvail" placeholder="Budget Name" required>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="input-group input-group-md">
                                    <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='expenCodeAvail' name="expenCodeAvail"  maxlength="10" placeholder="Budget Code" required>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <div class="input-group input-group-md">
                                    <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='factorAvail' name="factorAvail" placeholder="Factor">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group form-float">
                                    <div class='form-line' ] name="expenTypeAvail">
                                       <select class="codes form-control" id="expenTypeAvail" name="expenTypeAvail" required>
                                         
                                       </select>
                                    </div>
                                 </div>
                              </div>   
                           </div>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <button type="submit" id="btnAddAvail" name="btnAddAvail" class="btn btn-lg btn-warning btn-block waves-effect">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>ADD</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnClear" name="btnClear" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CLEAR</strong>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </form>
                     </div>




                     <div class="card collapse" id='addReq'>
                        <form id="addexpendReq" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="header">
                           <h2>
                              Add budget Code(Req)
                           </h2>
                        </div>
                       <div class="body">
                           <div class="row clearfix">
                              <!-- <div class="row" id="ReqErrorMessage">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                       <div class="bodyReq" style="background:rgb(251,70,55);">
                                          <p style="color:white;">Please filled the form properly.(You've inputed mismatched code from type of expend code you have selected)</p>
                                       </div>
                                    </div>
                                 </div>
                              </div> -->
                              <div class="col-md-6">
                                 <div class="input-group input-group-md">
                                    <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='expenNameReq' name="expenNameReq" placeholder="Budget Name" required>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="input-group input-group-md">
                                    <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='expenCodeReq' name="expenCodeReq"  maxlength="10" placeholder="Budget Code" required>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row clearfix">
                              <div class="col-md-12">
                                 <div class="form-group form-float">
                                    <div class='form-line' ] name="expenTypeReq">
                                       <select class="codes form-control" id="expenTypeReq" name="expenTypeReq" required>
                                          <option value="" selected>------SELECT CODE------</option>
                                          <option value="1">PERSONNEL SERVICES</option>
                                          <option value="2">MAINTENANCE AND OTHER OPERATING EXPENSES</option>
                                          <option value="4">CAPITAL OUTLAY</option>
                                          <option value="3">FINANCIAL EXPENSE</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>   
                           </div>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <button type="submit" class="btn btn-lg btn-success btn-block waves-effect" id="btnAddReq" name="btnAddReq">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>ADD</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnClearReq" name="btnClearReq" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CLEAR</strong>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </form>
                     </div>

                  </div>