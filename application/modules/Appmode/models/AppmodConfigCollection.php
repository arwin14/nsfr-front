  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class AppmodConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


//=======================================================
       public function getFormstype1(){
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllFORMS/type/1';
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        return $result;
    }

           public function getFormstype2(){
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllFORMS/type/2';
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        return $result;
    }
//=======================================================
       public function updateDatas($datajson){
         $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['backend_URL']."NSFRA/API/v1/updateFORMS");
          //var_dump($ret); die();
         echo json_encode($ret);
        // $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllFORMS/type/1';
        // $ret = Helper::serviceGet($url);
        // $result = json_decode($ret);
        // return $result;
    }
//=======================================================


//========================================================
    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>