
<div class="container-fluid" style="font-size:8pt;">

<div class="row clearfix">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
         <div class="header bg-brown">
            <h2 style="font-size: 2.2rem">
               NSFR
               <!-- <small>Agricultural Credit Policy Council</small>  -->
            </h2>
         </div>
         <div class="body">
            <div class="row clearfix" style="width:98%; margin:auto;">
               <?php                    
                  echo $content;
               ?>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/FormModUtil/budgetMode.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>