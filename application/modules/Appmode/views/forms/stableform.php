
                  <div class="updateAvailStable collapse">
                     <div class="card" id='dateSearch collapse'>
                        <div class="header">
                           <h5>
                              Update Item Code(Avail)
                           </h5>
                        </div>
                        <div class="body" style="width:90%; margin:auto;">
                         <form id="updateexpendAvail" data-form="#updateexpendAvail" data-table="#updateexpendAvailtbl" data-direct = "UpdateExpendAvail" enctype="multipart/form-data" accept-charset="utf-8">
                           <input type="hidden" name="theid" id="theid">
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='budgetNAvail' name="budgetNAvail" placeholder="Budget Name" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='codeAvail' name="codeAvail" placeholder="Budget Code" required>
                                    </div>
                                 </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='factorAvailUpdate' name="factorAvailUpdate" placeholder="Factor" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class='form-line' id="categories">
                                        <select class="codes form-control" id='codeCategAvail' name="codeCategAvail" required>
                                       </select>
                                    </div>
                                 </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <button type="submit" class="btn btn-lg btn-success btn-block waves-effect" id="submitUpdateAvail" name="submitUpdateAvail">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>SUBMIT</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnCancel" name="btnCancel" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CANCEL</strong>
                                 </button>
                              </div>
                           </div><br>
                        </form>
                        </div>
                     </div>
                  </div>


                  <div class="updateReqStable collapse">
                <div class="card" id='dateSearch collapse'>
                        <div class="header">
                           <h5>
                              Update Item Code(Item)
                           </h5>
                        </div>
                        <div class="body" style="width:90%; margin:auto;">
                         <form id="updateexpendReq" data-form="#updateexpendReq" data-table="#updateexpendReqtbl" data-direct = "UpdateExpendReq" enctype="multipart/form-data" accept-charset="utf-8">
                              <input type="hidden" name="theid" id="theidReq">
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='budgetNReq' name="budgetNReq" placeholder="Budget Name" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='codeReq' name="codeReq" placeholder="Budget Code" required>
                                    </div>
                                 </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='factorReqUpdate' name="factorReqUpdate" placeholder="Factor" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class='form-line' id="categories">
                                        <select class="codes form-control" id='codeCategReq' name="codeCategReq" >
                                       </select>
                                    </div>
                                 </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <button type="submit" class="btn btn-lg btn-success btn-block waves-effect" id="submitUpdateReq" name="submitUpdateReq" >
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>SUBMIT</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnCancel" name="btnCancel" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CANCEL</strong>
                                 </button>
                              </div>
                           </div><br>
                        </form>
                        </div>
                     </div>
                  </div>
                  <div>

                     <div class="card collapse in" id='addAvail'>
                        <form id="addexpendAvail" data-form="#addexpendAvail" data-direct = "addExpendAvail" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="header">
                           <h5>
                              Add Item Code(ASF)
                           </h5>
                        </div>
                        <div class="body">
                           <div class="row clearfix" style="width:98%; margin:auto;">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='expenNameAvail' name="expenNameAvail" placeholder="Budget Name" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='expenCodeAvail' name="expenCodeAvail"  maxlength="10" placeholder="Budget Code" required>
                                    </div>
                              </div>
                           </div><br>
                           <div class="row clearfix" style="width:98%; margin:auto;">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='factorAvail' name="factorAvail" placeholder="Factor">
                                    </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group form-float">
                                    <div class='form-line'>
                                       <select class="codes form-control" id="expenTypeAvail" name="expenTypeAvail" required>
                                         
                                       </select>
                                    </div>
                                 </div>
                              </div>   
                           </div><br>
                           <div class="row clearfix" style="width:98%; margin:auto;">
                              <div class="col-md-6">
                                 <button type="submit" id="btnAddAvail" name="btnAddAvail" class="btn btn-lg btn-warning btn-block waves-effect">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>ADD</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnClear" name="btnClear" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CLEAR</strong>
                                 </button>
                              </div>
                           </div><br>
                        </div>
                     </form>
                     </div>




                     <div class="card collapse" id='addReq'>
                         <form id="addexpendReq" data-form="#addexpendReq" data-direct = "addExpendReq" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="header">
                           <h5>
                              Add Item Code(RSF)
                           </h5>
                        </div>
                        <div class="body">
                           <div class="row clearfix" style="width:98%; margin:auto;">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='expenNameReq' name="expenNameReq" placeholder="Budget Name" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='expenCodeReq' name="expenCodeReq"  maxlength="10" placeholder="Budget Code" required>
                                    </div>
                              </div>
                           </div><br>
                           <div class="row clearfix" style="width:98%; margin:auto;">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='factorReq' name="factorReq" placeholder="Factor">
                                    </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group form-float">
                                    <div class='form-line'>
                                       <select class="codes form-control" id="expenTypeReq" name="expenTypeReq" required>
                                         
                                       </select>
                                    </div>
                                 </div>
                              </div>   
                           </div><br>
                           <div class="row clearfix" style="width:98%; margin:auto;">
                              <div class="col-md-6">
                                 <button type="submit" id="btnAddReq" name="btnAddReq" class="btn btn-lg btn-warning btn-block waves-effect">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>ADD</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnClear" name="btnClear" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CLEAR</strong>
                                 </button>
                              </div>
                           </div><br>
                        </div>
                     </form>
                     </form>
                     </div>

                  </div>