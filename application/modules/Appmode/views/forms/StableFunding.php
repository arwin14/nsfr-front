<?php   
 $attach1 = $tbldata['type1']->ResponseResult;
$attach2 = $tbldata['type2']->ResponseResult;
 //var_dump($attach1);
?>             
                  <div class="card">
                     <div class="header">
                        <h2>
                           Reference Data
                        </h2>
                     </div>
                     <div class="body" style="width:95%; margin:auto;">
                        <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" data-form="addavail" data-addcateg="#expenTypeAvail" href=".tbls1" id="btnsaob">Attachment 2</a></li>
                           <li><a data-toggle="tab" data-form="addreq" data-addcateg="#expenTypeReq" href=".tbls2" id="btnfar">Attachment 3</a></li>
                        </ul>
                        <div class="tab-content">
                           <div id="tbl1" class="tbls1 tab-pane fade in active">
                              <div class="table-responsive">
                                 <table id="tblattach1" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                                    <thead>
                                       <tr>
                                          <th>Name</th>
                                          <th>Description</th>
                                          <th>Status</th>
                                          <th>id</th>
                                          <th>type</th>
                                       </tr>
                                    </thead>
                                     <tbody>
                                       <?php 
                                         foreach ($attach1 as $index => $value) {   
                                          //echo $tbldata[$index]['name'];                 
                                        ?>
                                    <tr>
                                        <td><?php echo $attach1[$index]->description; ?></td>
                                        <td><?php echo $attach1[$index]->name; ?></td>
                                        <td><?php echo $attach1[$index]->status; ?></td>
                                        <td><?php echo $attach1[$index]->id; ?></td>
                                        <td><?php echo $attach1[$index]->type; ?></td>
                                   </tr>
                                   <?php
                                     }
                                   ?>
                                    </tbody>
                                    <tfoot></tfoot>
                                 </table>
                              </div>
                           </div>
                                  <div id="tbl2" class="tbls2 tab-pane fade">
                              <div class="table-responsive">
                                 <table id="tblattach2" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                                    <thead>
                                       <tr>
                                          <th>Name</th>
                                          <th>Description</th>
                                          <th>Status</th>
                                          <th>id</th>
                                          <th>type</th>
                                       </tr>
                                    </thead>
                                  <tbody>
                                       <?php 
                                         foreach ($attach2 as $index => $value) {   
                                          //echo $tbldata[$index]['name'];                 
                                        ?>
                                    <tr>
                                        <td><?php echo $attach2[$index]->description; ?></td>
                                        <td><?php echo $attach2[$index]->name; ?></td>
                                        <td><?php echo $attach2[$index]->status; ?></td>
                                        <td><?php echo $attach2[$index]->id; ?></td>
                                        <td><?php echo $attach2[$index]->type; ?></td>
                                   </tr>
                                   <?php
                                     }
                                   ?>
                                    </tbody>
                                    <tfoot></tfoot>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
          <?php
          echo base_url();
          ?>
       