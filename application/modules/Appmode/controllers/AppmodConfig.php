<?php

class AppmodConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('AppmodConfigCollection');
	}

	public function index() {
		//var_dump("expression");
		$viewData = array();
        $this->Helper->sessionEndedHook('Session');
        $data['tbldata'] = $this->formData();

      // var_dump($data['tbldata']['type1']); die();

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$viewData['content'] = $this->load->view("forms/StableFunding", $data, TRUE);
		$this->Helper->setView('AppmodConfig.form', $viewData,FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}


	public function udpateData(){
		$data = array(
			"id" => $_POST['Id'], 
			"name" => $_POST['name'],
			"description" => $_POST['description'],
			"type" => $_POST['type'],
			);

		$datajson = json_encode($data);
		//var_dump($datajson); die();
		$ret = $this->AppmodConfigCollection->updateDatas($datajson);


			// {  "id":1,
			//    "name": "tin ceo",
			//    "description": "987654321",
			//    "type": 3
			// }

			
	}


	private function formData(){
        $ret['type1'] = $this->AppmodConfigCollection->getFormstype1();
        $ret['type2'] = $this->AppmodConfigCollection->getFormstype2();
        return $ret;
		
	}

}
?>