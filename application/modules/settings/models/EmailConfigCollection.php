  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class EmailConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


//=======================================================
       public function getData(){
        //http://127.0.0.1:8080/NSFRAccountingAPI/Email/API/v1/getAllData
        $url = $GLOBALS['userlevel'].'Email/API/v1/getAllData';
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        return $result;
    }

//=======================================================
       public function getSpecificData($data){
        //var_dump($data);
        $url = $GLOBALS['userlevel'].'Email/API/v1/getDataById/id/'.$data['baseid'];
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        //var_dump($result);
        return $result;
    }
//=========================================================
    public function updateEmailContent($datajson){
     //var_dump($datajson);
      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['userlevel'].'Email/API/v1/update');
      $result = json_encode($ret);
      var_dump($result);
      return $result;

    }
  
//========================================================
    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>