
<div class="row clearfix" style="position:relative; top:0px;">

   <div class="col-md-12">
      <div class="card">
         <div class="header ">
            <legend>
               <div class="row">
                  <div class="col-md-6">
                     EXTRACTION PATH
                  </div>
                  <!--   <div class="col-md-6" style="position:fixed;">
                     CALCULATION OF NET STABLE FUNDING RATIO
                     </div> -->
                  <div class="col-md-6">
                     <div class="pull-right" style="padding-bottom:10px; ">
                        <button class="btn btn-social btn-round btn-google" id="btnmod">
                                            <i class="fa fa-cog"> </i>
                        </button>
                     </div>
                  </div>
               </div>
            </legend>
            <div class="collapse" id="dateSearch">
               <div class="row clearfix">
                  <div class="col-md-4">
                     <div class="form-group form-float">
                        <div class="form-line">
                           <input type="text" id="FindDate" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="container-fluid">
            
  
        <!--  <input type="hidden" name="addreason" id="addreason" value="<?php //echo ModuleRels::SIMULATION_ADDREASON; ?>" style=""> --> 
         <div class="content">
            <div class="table-responsive">
               <?php                    
                  echo $content;
               ?>
            </div>
         </div>
      </div>
   </div>
   <!-- end card -->
</div>













<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/settings/budget.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>