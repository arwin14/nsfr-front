



<form id="updatepath">
<div class="form-group">
         <label>Choose File Path</label>
         <a class="file-path" id="selectdirect" data-txtbox="Fileinput" data-label="Input Path">
            <div class="input-group">
               <span class="input-group-btn">
                     <span class="btn btn-warning btn-sm btn-fill">File Inputed</span>
               </span>
               <input name="Fileinput" id="Fileinput" class="form-control" type="text" value="<?php //echo $default_path; ?>" readonly>
            </div>
         </a>
      </div>
      <div class="form-group">
         <label>Choose File Path</label>
         <a class="file-path" id="selectdirect" data-txtbox="Fileoutput" data-label="Output Path">
            <div class="input-group">
               <span class="input-group-btn">
                     <span class="btn btn-warning btn-sm btn-fill">File Outputed</span>
               </span>
               <input name="Fileoutput" id="Fileoutput" class="form-control" type="text" value="<?php //echo $default_path; ?>" readonly>
            </div>
         </a>
      </div>

       <div class="form-group">
         <label>Time Schedule</label>
            <div class="input-group">
               <span class="input-group-btn">
                     <span class="btn btn-warning btn-sm btn-fill"><i class="pe-7s-angle-right-circle" style="font-size:14pt;"></i></span>
               </span>
               <input type="text" name="timepath" id="timepath" class="form-control datetimepicker" value="<?php //echo date('Y-m-d'); ?>">
            </div>
      </div>
 <div class="text-right" style="width:100%">
  <button class="apply-path btn btn-md btn-primary btn-fill" id="btnUpdatePath">Update</button>
  <!-- <button class="close-image btn btn-md btn-danger btn-fill">Close</button> -->
</div>
</form>