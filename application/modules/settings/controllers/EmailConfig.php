<?php

class EmailConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('EmailConfigCollection');
	}
	public function index() {
		$viewData = array();
        $this->Helper->sessionEndedHook('Session');
  		//$data['tbldata'] = $this->formData()->ResponseResult;
		$viewData['content'] = $this->emaillist();

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('EmailConfig.form', $viewData,FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}



public function emaillistinit(){
		$data['tbldata'] = $this->formData()->ResponseResult;
		$viewData = $this->load->view("forms/tblemaillist", $data, TRUE);
		return $viewData;
}

public function emaillist(){
		$data['tbldata'] = $this->formData()->ResponseResult;
		$viewData = $this->load->view("forms/tblemaillist", $data, TRUE);
		return $viewData;
}


public function submitchangesdata(){
		var_dump($_POST); die();
}


public function emailmodification(){
		$contents = $this->contentlist($_POST);
		$result['form'] = $this->load->view('forms/emailform.php', $contents, TRUE);
		echo json_encode($result);	
}

public function conntentinit(){
		$contents['form'] = $this->contentlist($_POST);
		echo json_encode($contents['form']['body']);	
}

public function contentlist() {
		$ret = new EmailConfigCollection();
		$ret = $ret->getSpecificData($_POST);
		$data['content'] = $ret->ResponseResult;
		$contents['body'] = $this->load->view('formsContent/emailcontent.php', $data, TRUE);
		$contents['baseid'] = $ret->ResponseResult->id;
		return $contents;
}


public function Updatedata() {
	//var_dump($_POST); die();
	$arrs = explode(' ',$_POST['emailBody']);
	$arrs =	preg_split("/\\r\\n|\\r|\\n/", $_POST['emailBody']);
	$ret = "";
	foreach ($arrs as $value) {
			$ret =$ret.$value.'\\n';
	}
	$id = $_POST['id'];	
	$requestdata = array(
						'id'=>(string)$id,
						'email_subject'=>$_POST['email_subject'],
						'email_body'=>$ret,
						'send_to_userlevel_id'=>$_POST['send_to_userlevel_id']
					    );	
	$jsonlineoftext = json_encode($requestdata);

	$ret = $this->EmailConfigCollection->updateEmailContent($jsonlineoftext);
	echo $ret;
}


private function formData(){
        $ret = $this->EmailConfigCollection->getData();
        //var_dump($ret);
        //$ret['type2'] = $this->SettingsConfigCollection->getFormstype2();
        return $ret;
		
	}



}
?>