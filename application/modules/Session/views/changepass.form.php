<div class="login-box">
        <div class="logo">
            <a href="<?php echo base_url(); ?>">
                <img class="p-b-20" src="<?php echo base_url(); ?>assets/custom/images/singlelgalogo.png" width=50 alt="" />
                <span class="font-50"><b>EDRMS</b></span>
            </a>
            <small>EPS - Cloud-based Parking Ticketing System</small>
        </div>
        <?php if(isset($serverMessage)): ?>
        <div class="alert <?php echo $serverCode == 0 ? 'alert-info' : 'alert-danger';?>">
            <a href="#" aria-hidden="true" data-dismiss="alert" class="close" style=""><i class="font-15 material-icons">close</i></a>
            <strong><?php echo $serverCode == 0 ? 'Great!' : 'Oh snap!'; ?></strong> <?php echo $serverMessage; ?>
        </div>
        <?php endif; ?>
        <div class="card">
            <div class="body">
                <form id="frmChangePass" action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ?>/cpass" method="POST" role="form" data-parsley-validate>
                    <fieldset>
                        <div class="msg">
                            Enter your new password
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input id="password" type="password" class="required form-control" name="oldpass" placeholder="Old Password" required autofocus />
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input id="password" type="password" class="required form-control" name="newpass" placeholder="New Password" required autofocus />
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input id="password2" type="password" class="required form-control" name="newpass2" placeholder="Verify New Password" required autofocus />
                            </div>
                        </div>
                        <div class="pull-right">
                            <img src="<?php echo base_url(); ?>assets/img/ajax-loader-trans.gif" alt="" class="pull-right loader" style="display:none;" />
                            <button type="submit" class="btn btn-success btn-fill" id="btnUpdate">Change</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default btn-fill" id="bCancel">Cancel</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <center style="margin-top:-20px;">
            <small>EDRMS &copy; 2017 <a style="color:#FFFFFF;" href="http://www.mobilemoney.ph" target="_blank">Telcom Live Content, Inc.</a></small>
        </center>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/modules/js/session/changepass.js"></script>