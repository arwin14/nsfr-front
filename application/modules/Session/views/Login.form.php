<form role="form" id="frmLogin">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                <form method="#" action="#">

                <!--   if you want to have the card without animation please remove the ".card-hidden" class   -->
                    <div class="card card-hidden">
                        <div class="header text-center">Welcome!</div>

                        <div class="content">
                            <div id="usernamevalidpane">
                                <p class="text-center" id="putusernamehere">Please sign on</p>
                            </div>
                            <div class="form-group usernamegroup">
                                <label>Username</label>
                                 <input type="text" class="form-control" name="username" id="username" placeholder="Username" required autofocus>
                            </div>
                            <div class="form-group passwordgroup" hidden>
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="footer text-center">
                            <div class="loader" hidden>
                                <br>
                                <img  src="<?php echo base_url(); ?>assets/thirdparty/img/loaderTest.gif" style="width: 40px;" >
                            </div>
                                <br>
                            <div class="usernamegroup">
                                <button type="button" class="btn btn-fill btn-warning btn-wd" id="CHeckEmailBtn">Next</button>
                            </div>
                            <div class="passwordgroup" hidden>
                                <button type="button" class="btn btn-fill btn-warning btn-wd" id="loginBtn">Login</button>
                            </div>
                            

                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</form>


<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/Session/login.js"></script>