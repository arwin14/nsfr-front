<?php



class LoginCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


    public function checkemailModel($username){
      /*var_dump($username);die();*/
      $url =  $GLOBALS['login_url']."API/v1/checkUser/username/".$username;     
      //var_dump($url);die();

      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);  

      //var_dump($ret); die();
      return $ret;
    }

    public function login($username, $password){
      $data = array(
        'username' => $username,
        'password' => $password
      );
      $ret = Helper::serviceCall($data, $GLOBALS['login_url'] ."API/v1/logIn");
      

      if (isset($ret->ResponseResult->firstname)) {
        $useracct = $ret->ResponseResult;
        $sessUser = new UserAccount();
        $sessUser->modules($ret->ResponseResult->roles);
        $sessUser->put($useracct);
        $this->ModelResponse("0", "Log in success");
        return true;
      } else {
        $this->ModelResponse("1", $ret->ResponseResult);
      }
      return false;
    }

    public function checkSessionState() {
      $data = json_encode(array("SessionId"=>UserAccount::getSessionId()));
      $ret = parent::serviceCall("CHKSESS",$data);
      if($ret != null) {
        if($ret->Code == 0) {
          $this->ModelResponse($ret->Code, $ret->Message);
          return true;
        } else {
          $this->ModelResponse($ret->Code, $ret->Message);
        }     
      }
      return false;
    }


    public function CheckingSession() {
      //  var_dump(Helper::get('userid'));
      // $result = json_encode(Helper::get('status'));
      //  return $result;\

        $url = $GLOBALS['login_url'] . "API/v1/checkSession/userId/".Helper::get('userid');
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $ret  = curl_exec($ch);
       //$result = json_encode($ret);
      //var_dump($ret); die();

      curl_close($ch);
      return $ret;

    }

    public function logout(){
     //var_dump(Helper::get('userid')); die();
      $url = $GLOBALS['login_url'] . "API/v1/expire/userId/".Helper::get('userid');
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $ret  = curl_exec($ch);
      curl_close($ch);
      //var_dump($ret); die();
       if($ret->ResponseResult == "Expiring session Successful"){
          return true;
       }
       return false;
    }

  }