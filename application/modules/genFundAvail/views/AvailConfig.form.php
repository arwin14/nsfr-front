
<div class="col-md-12">
<div class="card">
   <div class="header">
      <legend>
         <div class="row">
            <div class="col-md-8">
               <b>PART II.</b> CALCULATION OF AVAILABLE STABLE FUNDING
            </div>
            <div class="col-md-4">
              
               <button type="button" id="btnDateSearch" class="btn btn-round btn-wd btn-warning pull-right" style="margin-bottom: 10px;" data-toggle="collapse" data-target="#dateSearch">
               <span class="btn-label">
               <i class="pe-7s-date"></i>
               </span>
               Search
               </button>
            </div>
         </div>
      </legend>
      <div class="collapse" id="dateSearch">
         <div class="row clearfix">
            <div class="col-md-4">
               <div class="form-group form-float">
                  <div class="form-line">
                     <!-- <input type="text" id="dateSearch" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>"> -->
                     <input type="text" id="FindDate" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="content">
      <form method="get" action="/" class="form-horizontal">
         <fieldset>
            <div class="table-responsive" >
               <table id="tlbmonthlyData" class="table  table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                  <thead>
                     <tr style="background:rgb(244,149,66) ;">
                        <th class="no-sort" style="color:white;">Item</th>
                        <th style="color:white;">Nature of Item</th>
                        <th style="color:white;">Amount</th>
                     </tr>
                  </thead>
               </table>
              </div>
         </fieldset>
      </form>
    </div>
   </div>
   <!-- end card -->
</div>
<!-- end col-md-12 -->
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/genEquip/genEquip.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>