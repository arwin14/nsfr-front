 <?php

class AvailConfig extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
        // $this->load->model('ModuleRels');
        $this->load->model('AvailConfigCollection');
    }
    
    public function index()
    {
        // var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');
        $this->Helper->setTitle('User Configuration');
        $this->Helper->setView('AvailConfig.form', '', FALSE);
        $this->Helper->setTemplate('templates/mastertemplate');
    }
    
    
    
    
    public function getDailyData()
    {
        $ret = $this->AvailConfigCollection->getDailyDataColl($_POST);
        
        
        $arrsSTD = json_decode($ret);
        $arrs    = json_decode(json_encode($arrsSTD), true);
        $result  = $arrs['ResponseResult'];
        $return  = array();
        //var_dump(count($result)); die();
        if ($result == "NO DATA FOUND") {
            $return = "";
        } else {
            for ($key = 0; $key < count($result); $key++) {
                $return[] = array(
                    "code" => $result[$key]['code'],
                    "name" => $result[$key]['name'],
                    "amount" => $result[$key]['amount'],
                    "btn" => "<a href='#' class='btn btn-simple btn-info btn-icon like'><i class='fa fa-eye'></i></a>" . "<a href='#' class='btn btn-simple btn-warning btn-icon edit'><i class='fa fa-edit'></i></a>" . "<a href='#' class='btn btn-simple btn-danger btn-icon remove'><i class='fa fa-times'></i></a>"
                );
            }
            

            
        }

        $result = json_encode($return);
        echo $result;
        
    }
    
    
    
    
    //==========================================================================================================
    
    public function exportExcel()
    {
        ob_start();
        $mydate = $_POST['excelDate'];
        $out    = $this->TRExportExcel($mydate);
        ob_end_clean();
        header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=BIR_Sales_Summary" . date("Y-m-d H-i-s") . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $out;
        die();
        exit;
    }
    
    
    
    // public function TRExportExcel($mydate)
    // {
    //     $thedate = $mydate;
      
    //     $title = '<table style="font-weight:bold; font-size: 12px;">' 
	   //      		. '<tr style="text-align:center;">' 
    //                     . '<td colspan="5" align="center">MUFG</td>'
    //                  . '</tr>'  
    //                  . '<tr style="text-align:center;">'
    //                     . '<td colspan="5" align="center">Name of Bank</td>'
    //                 .   '</tr>'
    //                 . '<tr style="text-align:center;">'
    //                     . '<td colspan="5" align="center"><b>BASEL III NSFR REPORT (Solo)</b></td>'
    //                 .   '</tr>'
    //                 . '<tr style="text-align:center;">' 
    //                     . '<td colspan="5" align="center">As of (Monthly-end)</td>' 
    //                 . '</tr>' 
    //                 . '<tr style="text-align:center;">' 
    //                     . '<td colspan="5" align="left" style="font-size:10pt;"><b>PART II.  CALCULATION OF AVAILABLE STABLE FUNDING(In Absolute Amount)</b></td>'
    //                 . '</tr>' 
    //                  . '<tr style="text-align:center;">' 
    //                 . '</tr>' 
    //             . '</table>';

        

    //     $tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
				// 		<thead>
				// 			<tr>
				// 				<th class="no-sort">Item</th>
				// 				<th>Nature of Item</th>
				// 				<th>Amount</th>
    //                             <th>Factor</th>
    //                             <th>Weighted Amount (a x b)</th>  
				// 			</tr>
				// 		</thead>
				// 		<tbody>';
        
    //     $tabledata = '';
    //     $data      = '';
        
        
    //     $url     = $GLOBALS['backend_URL'] . "NSFRA/API/v1/getDataCAP/type/2/date/" . $thedate;
    //     $ret     = Helper::serviceGet($url);
    //     $arrsSTD = json_decode($ret);
    //     $arrs    = json_decode(json_encode($arrsSTD), true);
    //     $result  = $arrs['ResponseResult'];
    //     foreach ($result as $key => $value) {
    //             $cstat = ($result[$key]['factor'] != "")? "":"gray";
    //         	$data = $data . '<tr>' 
    //                           . '<td > ' . $result[$key]['code'] . '</td>' 
    //                           . '<td width="98%"> ' . $result[$key]['name'] . '</td>' 
    //                           . '<td> ' . $result[$key]['amount'] . '</td>' 
    //                           . '<td style="background:"'.$cstat.'";"> ' . $result[$key]['factor'] * 100 . '%</td>' 
    //                           . '<td> ' . $result[$key]['weighted'] . '</td>' 
    //                           . '</tr>';
    //     }
        
    //     $tableend = '</tbody></table>';
        
        
    //     //die();
    //     return $title . $tablehead . $data . $tableend;
        
        
        
        
        
    // }
    
}
?>	