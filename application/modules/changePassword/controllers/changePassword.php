<?php

class changePassword extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('changePasswordCollection');
		$this->load->module('session');
		Helper::sessionEndedHook('session');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');
        /*Session::checksession();*/
		$this->Helper->setTitle('User Level Configuration'); //--> Set title to the Template
		$this->Helper->setView('changePassword.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}

	public function submitChange(){
		$ret = $this->changePasswordCollection->submitChange($_POST);
		echo json_encode(array("data"=>$ret));
	}
}

?>