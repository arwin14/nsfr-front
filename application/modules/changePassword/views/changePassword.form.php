<style type="text/css">

	div.form-group{
		margin-bottom: -.5em;
	}

	.displaynone{
		display: none;
	}

</style>

<div class = "container-fluid">
	<div class = "row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
                <div class="header bg-green">
                    <h2>
                        Change Password<small> Please make sure all required fields are filled out correctly..</small>
                    </h2>
                </div>	
                <div class = "body">
                	<form id = "changePasswordForm">
	                	<center>
	                		<div class = "form-group" style="width: 30%;">
	                			<input type="hidden" id="sessuserid" value="<?php echo (isset($_SESSION["userid"])) ? $_SESSION["userid"] : " "; ?>">
	                			<label style="font-size: 12px;">Old Password:</label>
	                			<div class = "form-line">
	                				<input type="password" class="form-control" name="old_password" id = "old_password" placeholder="Please type your Old Password.." style="text-align: center;">
	                				<!-- <label class="form-label" style="font-size: 1.25rem">Old Password:</label> -->
	                			</div>
	                		</div>
	                		<br>
	                		<div class = "form-group" style="width: 30%;">
	                			<label style="font-size: 12px;">New Password:</label>
	                			<div class = "form-line">
	                				<input type="password" class="form-control" name="new_password" id="new_password" placeholder="Please type your New Password.." style="text-align: center;">
	                				<!-- <label class="form-label" style="font-size: 1.25rem">Old Password:</label> -->
	                			</div>
	                		</div>
	                		<br>
	                		<div class = "form-group" style="width: 30%;">
	                			<input type="submit" class="btn btn-success" name="" style="width: 100%;">
	                		</div>
	                	</center>
	                </form>
                </div>
            </div>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/changePassword/changePassword.js"></script> 
