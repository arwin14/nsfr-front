<?php
class changePasswordCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }

   public function submitChange($postdata){
        $data = array(
            'userId' => $postdata['sessuserid'],
            'oldPassword' => $postdata['old_password'],
            'newPassword' => $postdata['new_password']
        );

                                          
        $ret = Helper::serviceCall($data, $GLOBALS['login_url']."API/v1/changePassword");

        return $ret;
   }

}

?>