<?php



class LoginCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }

    public function login($username, $password){
      $data = array(
        'username' => $username,
        'password' => $password
      );
      $ret = Helper::serviceCall($data, $GLOBALS['login_url'] ."API/v1/logIn");
     //   var_dump($ret->ResponseResult); die();
      if($ret->ResponseResult == "INVALID USERNAME"){
        $this->ModelResponse("01", $ret->ResponseResult);
      }
      elseif ($ret->ResponseResult == "ALREADY LOGIN") {
        $this->ModelResponse("01", $ret->ResponseResult);
          //$alldata = json_decode(Helper::serviceGet($GLOBALS['login_url'] ."API/v1/getAllData"), true);
          // foreach ($alldata["ResponseResult"] as $key => $value) {
          //   $url = $GLOBALS['login_url'] ."API/v1/expire/userId/" . $alldata["ResponseResult"][$key]["id"];
          //   $ch = curl_init();      
          //   curl_setopt($ch, CURLOPT_URL, $url);         
          //   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
          //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
          //   curl_exec($ch);
          // }
      }
      else{
        //var_dump($ret->ResponseResult->roles); die();
        $useracct = $ret->ResponseResult;
        $sessUser = new UserAccount();
        $sessUser->modules($ret->ResponseResult->roles);
        $sessUser->put($useracct);
        $this->ModelResponse($ret->Code, $ret->Message, $useracct);
        return true;
      }
      return false;
    }

    public function checkSessionState() {
      $data = json_encode(array("SessionId"=>UserAccount::getSessionId()));
      $ret = parent::serviceCall("CHKSESS",$data);
      if($ret != null) {
        if($ret->Code == 0) {
          $this->ModelResponse($ret->Code, $ret->Message);
          return true;
        } else {
          $this->ModelResponse($ret->Code, $ret->Message);
        }     
      }
      return false;
    }


    public function CheckingSession() {
      //  var_dump(Helper::get('userid'));
      // $result = json_encode(Helper::get('status'));
      //  return $result;\

        $url = $GLOBALS['login_url'] . "API/v1/checkSession/userId/".Helper::get('userid');
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $ret  = curl_exec($ch);
       //$result = json_encode($ret);
      //var_dump($ret); die();

      curl_close($ch);
      return $ret;

    }

    public function logout(){
     //var_dump(Helper::get('userid')); die();
      $url = $GLOBALS['login_url'] . "API/v1/expire/userId/".Helper::get('userid');
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $ret  = curl_exec($ch);
      curl_close($ch);
      //var_dump($ret); die();
       if($ret->ResponseResult == "Expiring session Successful"){
          return true;
       }
       return false;
    }

  }