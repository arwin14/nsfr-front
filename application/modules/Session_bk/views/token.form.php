<div class="container" style="padding:1px 16px;height:100%;padding-top:130px;margin-bottom:25%;">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<div class="text-center">
							<img src="<?php echo base_url(); ?>assets/local/images/NSC.png" width="90" alt="" style="margin-top:;" />
							<p style="font-weight:bold;font-size:1.5em">
								National Security Council<br />
								Collaboration Information and Handling System
							</p>
						</div>
					</div>
					<div class="panel-body">
						<form role="form" id="frmLogin" action="<?php echo base_url(); ?>session/verifytoken" method="POST">
							<fieldset>
								<div class="form-group input-group">
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
									<input class="form-control" id="username" placeholder="USERNAME" name="username" type="text" value="<?php echo $username; ?>" style="" readonly>
									<!-- <input class="form-control" id="username" placeholder="USERNAME" name="username" type="text" style="text-transform: uppercase;" autofocus> -->
								</div>
								<div class="form-group input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input class="form-control" id="token" placeholder="TOKEN" name="token" type="text" style="text-transform: uppercase;"> <!--onkeypress="capLock(event)"-->
								</div>
								<div class="form-group text-justify" style="font-size:12px;">
									<em>Note: Unauthorized access is illegal and is protected by law.</em>
								</div>
								<div class="row">
									<div class="col-md-6">
										<a href="<?php echo base_url(); ?>session/logout" class="btn btn-lg btn-danger btn-block">Cancel</a>
									</div>
									<div class="col-md-6">
										<input type="submit" id="submitToken" class="btn btn-lg btn-primary btn-block" value="Submit" />
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer">
						<input type="hidden" name="serverCode" id="serverCode" value="<?php echo isset($serverCode) ? $serverCode : ""; ?>" />
						<div class="text-center">
						<span style="color:red;" id="serverMessage"><?php echo isset($serverMessage) ? $serverMessage : ""; ?></span>
						</div>
						<div class="text-right">
							<img src="<?php echo base_url(); ?>assets/local/images/squares-loader.gif" alt="" id="loader" style="display:none;" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/local/module/js/login/session.js"></script>