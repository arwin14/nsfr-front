 <form role="form" id="frmLogin" action="<?php echo base_url(); ?>Session/Validate" method="POST">
<div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form method="#" action="#">

                        <!--   if you want to have the card without animation please remove the ".card-hidden" class   -->
                            <div class="card card-hidden">
                                <div class="header text-center">Login</div>
                                <div class="content">
                                    <div class="form-group">
                                        <label>Email address</label>
                                         <input type="text" class="form-control" name="username" id="username" placeholder="Username" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" data-toggle="checkbox" value="">
                                            Subscribe to newsletter
                                              <span style="color:red;" id="serverMessage"><?php echo isset($serverMessage) ? $serverMessage : ""; ?></span>
                                              <span id="capsLock" style="color:blue;display:none;font-size:11px;font-style:italic;">Caps Lock is ON.</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-warning btn-wd">Login</button>

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
</form>