    <?php

  class Session extends MX_Controller {

  	public function __construct() {
      parent::__construct();
      $this->load->model('Helper');
      $this->load->model('LoginCollection');
    }
    
    public function index() {
      //var_dump(expression)
      if(Helper::get('userlevel') == "ADMIN") {

        Helper::sessionStartedHook('genFundBAF/UserConfig');
      } else if(Helper::get('userlevel') == "FINANCE") {
        Helper::sessionStartedHook('genFundBAF/UserConfig');
        //$this->Helper->setTemplate('templates/mastertemplate_ppe');
      }else if(Helper::get('userlevel') == "BUDGET") {
        Helper::sessionStartedHook('DashboardBudget/DashboardBudget');
        //$this->Helper->setTemplate('templates/mastertemplate_ppe');
      }
      
      /*Helper::sessionStartedHook('home');*/
      $this->Helper->setTitle('Home');
      $this->Helper->setView('Login.form','',FALSE);
      $this->Helper->setTemplate('templates/login_template');
      //$this->Helper->sessionStartedHook('UserMngmt/UserLevelConfig');
    }

    public function Validate() {
      // }
    
      $ret = new LoginCollection();
      $user = isset($_POST['username']) ? $_POST['username'] : "";
      $passw = isset($_POST['password']) ? $_POST['password'] : "";
      //$token =  isset($_POST['ip']) ? base64_encode($user . ':' . $_POST['ip']) : "";

      if($ret->login($user,$passw)) {
        Helper::sessionStart();
      } else {  
        $response['serverCode'] = $ret->getCode();
        $response['serverMessage'] = $ret->getMessage();
        $this->Helper->setTitle('Home');
        $this->Helper->setView('Login.form',$response,FALSE);
        $this->Helper->setTemplate('templates/login_template');
        
      }



              /*$user = isset($_POST['username']) ? strtoupper($_POST['username']) : "";
              $passw = isset($_POST['password']) ? $_POST['password'] : "";
              $res = new LoginCollection();
              $ret = $res->login($user,$passw);
              
                if(isset($ret->ResponseResult->logging_state)) {
                    $useraccount = array(
                      "firstname"=>$ret->ResponseResult->firstname,
                      "gender"=>$ret->ResponseResult->gender,
                      "userlevel"=>$ret->ResponseResult->userlevel, 
                      "logging_state"=>$ret->ResponseResult->logging_state, 
                      "middlename"=>$ret->ResponseResult->middlename, 
                      "id"=>$ret->ResponseResult->id, 
                      "userid"=>$ret->ResponseResult->userid, 
                      "email"=>$ret->ResponseResult->email,
                      "lastname" => $ret->ResponseResult->lastname,
                      "username" => $ret->ResponseResult->username
                    );
                    $retmodules = $this->LoginCollection->modules($ret->ResponseResult->userlevel);
                    $modules = array("modules"=>$retmodules);
                    $this->Helper->createSession($modules);
                    $this->Helper->createSession($useraccount);
                    redirect();
                } else {
                    $response['serverCode'] = "00";
                    $response['serverMessage'] = $ret->ResponseResult;
                    
                }*/
              
      }

      public static function checksession() {
        $ret = new LoginCollection();   
        
        if(!$ret->checkSessionState()) {
          Helper::sessionTerminate();
        }
      }
      
      /*public function getsessiondata() {
        $param = isset($_REQUEST['param']) ? $_REQUEST['param'] : "username";
        echo Helper::get($param);
      }
      
      public function sendmail() {
        $ret = new LoginCollection();   
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : "";
        
        if($ret->fPass($email)) {
          $ret = new ModelResponse($ret->getCode(), $ret->getMessage());
        } else {
          $ret = new ModelResponse($ret->getCode(), $ret->getMessage());
        }
        
        echo $ret;
      }
      
      public function fcpass() {
        $ret = new LoginCollection();
        $linkid = isset($_REQUEST['linkid']) ? $_REQUEST['linkid'] : "";
        $newpass = isset($_REQUEST['password']) ? $_REQUEST['password'] : "";
        $newpass2 = isset($_REQUEST['password2']) ? $_REQUEST['password2'] : "";
        
        if($ret->fCPass($linkid,$newpass,$newpass2)) {
          $ret =  new ModelResponse($ret->getCode(), $ret->getMessage());
        } else {
          $ret =  new ModelResponse($ret->getCode(), $ret->getMessage());
        }
        
        echo $ret;
      }*/


      /*public function verifytoken(){
          $user = isset($_POST['username']) ? $_POST['username'] : "";
          $code = isset($_POST['token']) ? strtoupper($_POST['token']) : "";
          $res = new LoginCollection();
          $ret = $res->token($user,$code);
          // var_dump($ret);die();
          if($ret->status==0) {
              $useraccount = array("userid"=>"4","username"=>$user,"userlevel"=>"1", "firstname"=>"ADMIN", "middlename"=>"", "lastname"=>"ADMIN");
              $this->Helper->createSession($useraccount);
              redirect();
          } else {
              $response['serverCode'] = "00";
              $data["username"] = $user;
              $response['serverMessage'] = "The system does not recognize your account. Please try again.";
              $this->Helper->setView('token.form',$data,FALSE);
              $this->Helper->setTemplate('templates/public_template');
          }
      }*/

      public function checkSessioning(){
       $ret = $this->LoginCollection->CheckingSession($_POST);
       echo $ret;
    
      }


      // public function checkSession() {
      //     $ret = $this->LoginCollection->CheckingSession();
      //     session_destroy();
      //     redirect();
      // } 

      public function Logout() {
          $ret = $this->LoginCollection->Logout();
          session_destroy();
          redirect();

      }	

  }