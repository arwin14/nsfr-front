<?php

class ForgotPassReset extends MX_Controller {
	
	public function __construct() {
		parent::__construct();		
		$this->load->model('Helper');
		$this->load->model('LoginCollection');
		$this->load->model('Home/Collections');
	}
	
	public function index() {
		$ret = new Collections();
		$get = $this->input->get();
		if($ret->hasRows($get['lid'])) {
			Helper::setTitle('Reset Password | LGA - EDRMS');
			Helper::setView('reset.pass','',FALSE);
			Helper::setTemplate('templates/blank_template');
		} else {
			show_404();
		}
	}

	public function change() {
		$ret = new LoginCollection();
		$post = $this->input->post();
		$get = $this->input->get();
		if($this->input->server('REQUEST_METHOD') == 'POST' && $get != null) {			
			if($ret->fCPass($get['lid'],$post['password'],$post['password2'])) {
				$response['serverMessage'] = $ret->getMessage();
				$response['serverCode'] = $ret->getCode();
				Helper::setTitle('Sign In | LGA - EDRMS');
				Helper::setView('login',$response,FALSE);
				Helper::setTemplate('templates/login_template');
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}
}
?>
