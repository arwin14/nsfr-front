<style type="text/css">
   .displaynone{
   display: none;
   }
   .no-sort::after { display: none!important; }
   .no-sort { 
   pointer-events: none!important; cursor: default!important; 
   }
   #tlbmonthlyData tbody td {
   white-space: nowrap;
   }
   div.form-group{
   margin-bottom: -.5em;
   }
   .displaynone{
   display: none;
   }
   .btn:focus,
   .btn:active,
   .btn:active:focus {
   box-shadow: none;
   outline: none;
   }
   .btn-modal {
   position: absolute;
   top: 50%;
   left: 50%;
   margin-top: -20px;
   margin-left: -100px;
   width: 200px;
   }
   .btn-primary:active,
   .btn-primary:hover:active,
   .btn-primary:focus:active,
   .btn-primary:active:active {
   border-bottom: 1px solid #36a940;
   }
   .btn-default:active,
   .btn-default:hover:active,
   .btn-default:focus:active,
   .btn-default:active:active {
   border-bottom: 1px solid #a2aab8;
   }
   .btn-secondary,
   .btn-secondary:hover,
   .btn-secondary:focus,
   .btn-secondary:active {
   color: #cc7272;
   background: transparent;
   border: 0;
   }
   h1:first-child,
   h2:first-child,
   h3:first-child {
   margin-top: 0;
   }
   .form-label{
   font-size:8pt;
   }
   .number{
   background:rgba(0,0,0,0);
   }
   .loaders {
   border: 6px solid #f3f3f3;
   border-radius: 50%;
   border-top: 6px solid #3498db;
   width: 50px;
   height: 50px;
   -webkit-animation: spin 2s linear infinite; /* Safari */
   animation: spin 2s linear infinite;
   }
   .loader{
   width: 70px;
   height: 70px;
   margin: 40px auto;
   }
   .loader p{
   font-size: 16px;
   color: #777;
   }
   .loader .loader-inner{
   display: inline-block;
   width: 15px;
   border-radius: 15px;
   background: #74d2ba;
   }
   .loader .loader-inner:nth-last-child(1){
   -webkit-animation: loading 1.5s 1s infinite;
   animation: loading 1.5s 1s infinite;
   }
   .loader .loader-inner:nth-last-child(2){
   -webkit-animation: loading 1.5s .5s infinite;
   animation: loading 1.5s .5s infinite;
   }
   .loader .loader-inner:nth-last-child(3){
   -webkit-animation: loading 1.5s 0s infinite;
   animation: loading 1.5s 0s infinite;
   }
   @-webkit-keyframes loading{
   0%{
   height: 15px;
   }
   50%{
   height: 35px;
   }
   100%{
   height: 15px;
   }
   }
   @keyframes loading{
   0%{
   height: 15px;
   }
   50%{
   height: 35px;
   }
   100%{
   height: 15px;
   }
   /* Safari */
   @-webkit-keyframes spin {
   0% { -webkit-transform: rotate(0deg); }
   100% { -webkit-transform: rotate(360deg); }
   }
   @keyframes spin {
   0% { transform: rotate(0deg); }
   100% { transform: rotate(360deg); }
   }
</style>
<div clas
<div class="col-md-12">
<div class="card">
   <div class="header">
      <legend>
         <div class="row">
            <div class="col-md-8" >
            <b> PART I. </b>  CALCULATION OF NET STABLE FUNDING RATIO
            </div>
            <div class="col-md-4">
          <!--      <button type="button" id="exportExcelButton" class="btn btn-round btn-wd btn-success" style="margin-bottom: 10px;" data-holder="">
               <input type="hidden" name="exportExceltxt" id="exportExceltxt" value>
               <span class="btn-label">
               <i class="pe-7s-print"></i>
               </span>
               Export Excel
               </button> -->
               <button type="button" id="btnDateSearch" class="btn btn-round btn-wd btn-warning pull-right" style="margin-bottom: 10px;" data-toggle="collapse" data-target="#dateSearch">
               <span class="btn-label">
               <i class="pe-7s-date"></i>
               </span>
               Search
               </button>
            </div>
         </div>
      </legend>
      <div class="collapse" id="dateSearch">
         <div class="row clearfix">
            <div class="col-md-4">
               <div class="form-group form-float">
                  <div class="form-line">
                     <!-- <input type="text" id="dateSearch" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>"> -->
                     <input type="text" id="FindDate" class="form-control datetimepicker" value="<?php echo date('Y-m-d'); ?>">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="content">
      <form method="get" action="/" class="form-horizontal">
         <fieldset>
            <div class="table-responsive">
               <table id="tblsummary" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                  <thead>
                     <tr style="background:rgb(244,149,66) ;">
                        <th class="no-sort" style="color:white;">Item</th>
                        <th style="color:white;">Nature of Item</th>
                        <th style="color:white;">Reference</th>
                        <th style="color:white;">Account Code</th>
                        <th style="color:white;">Weighted Amount</th>
                     </tr>
                  </thead>
               </table>
         </fieldset>
      </form>
      </div>
   </div>
   <!-- end card -->
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/genBAF306B/baf.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>