<?php

				
class UserConfig extends MX_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}





		public function getBaf(){
		$ret = $this->UserConfigCollection->getBafs($_POST);
		$arrsSTD = json_decode($ret);
		$arrs = json_decode( json_encode($arrsSTD), true);
		$result = $arrs['ResponseResult'];
		//var_dump($result);
		$return = array();

		$Ntotal = 0;
		$Atotal = 0;
		$Btotal = 0;
		$Ctotal = 0;
		$Ntotal = 0;
		$weight = 0;
		$acc = "";



		$child = $arrs["ResponseResult"];
							$prev = 0;
							$count = 0;
							
							foreach ($result as $a => $value) {
								//var_dump($result[$a]['weighted']);
							if($a < 6){
                            		$Atotal += $result[$a]['weighted'];
                    		}else if($a < 12 && $a > 5){
                             		$Btotal += $result[$a]['weighted'];
                    		}else if($a == 12){
                            if($Btotal == 0){
                                $Ctotal = 0;
                            }else{
                            $Ctotal = $Atotal/$Btotal;
                            }
                   			}	

                   			if($a == 5){
                             $weight = $Atotal;
                             $acc = $result[$a]['accountcode'];
                    		}else if($a == 11){
                        		$weight = $Btotal;
                        		$acc = $result[$a]['accountcode'];
                       
                    		}else if($a == 12){
                        		$weight = $Ctotal;
                         	if($Ctotal > 1){
                                $acc = "NSFR COMPLIANT";
                        }else{
                                $acc = "NSFR NON-COMPLIANT";
                        }

                    }else{
                        $weight = $result[$a]['weighted'];
                        $acc = $result[$a]['accountcode'];
                    }
                    $return[] = array(
					"code" => $result[$a]['itemno'],
					"name" => $result[$a]['title'],
					"amount" => $result[$a]['reference'],
					"acc" => $acc,
					"weight" =>  number_format($weight, 2)
				);
                }
		 $resultme = json_encode($return);
		 //var_dump($resultme);
		echo $resultme;
			}

		public function getBafOption(){
		$ret = $this->UserConfigCollection->getBafsOption($_POST);
		echo $ret;
		
			}




	public function exportExcel(){
	    ob_start();
	    $mydate = $_POST['excelDate'];
		$out = $this->TRExportExcel($mydate);
		ob_end_clean();
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment; filename=Agricultural_Credit_Policy_Council". date("Y-m-d H-i-s") . ".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $out;
		die();
		exit;
		//$gg = $_POST['dateMonth'];
		//var_dump($gg);
	}



	// public function TRExportExcel($mydate){
	// 	$thedate = $mydate;


	// 	$title = '<table style="font-weight:bold; font-size: 12px;">' 
	// 						. '<tr style="text-align:center;">' 
	// 						. '<td colspan="3" align="center">MUFG</td>'
	// 						. '</tr>'  
	// 						. '<tr style="text-align:center;">'
	// 						. '<td colspan="3" align="center">Name of Bank</td>'
	// 						.	'</tr>'
	// 						. '<tr style="text-align:center;">'
	// 						. '<td colspan="3" align="center"><b>BASEL III NSFR REPORT (Solo)</b></td>'
	// 						.	'</tr>'
	// 						. '<tr style="text-align:center;">' 
	// 						. '<td colspan="3" align="center">As of (Monthly-end)</td>' 
	// 						. '</tr>' 
	// 					. '</table>';



	// 			$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
	// 						 <thead>
 //                                 <tr>' .
	// 							'<th>ITEM</th>' .
	// 							'<th>NATURE OF ITEM</th>' .
	// 							'<th>REFERENCE</th>' .
	// 							'<th>ACCOUNT CODE</th>' .
	// 							'<th>WEIGHTED AMOUNT</th>' .
	// 						'</tr>'.
 //                             ' </thead> 
	// 						<tbody>';

	// 				$tabledata = '';
	// 				$data = '';
					
	// 						// $json_string =  file_get_contents( $GLOBALS['backend_URL']."ORS/API/v1/getDataBAF306B/date/".$thedate);
	// 						// $arrs = json_decode($json_string, true);
							
	// 						//$theDate = $_POST['date'];
 //      						$url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataByDayAndYearASF/date/".$thedate;
 //      						$ret = Helper::serviceGet($url);
 //      						$arrsSTD = json_decode($ret);
 //      						$arrs = json_decode( json_encode($arrsSTD), true);
 //      						// $arrs = json_decode( json_encode($ret), true);
 //      						// var_dump($thedate);
 //      						// var_dump($arrs); die();
 //      						//$arrs = serialize($strarrs);

 //      						//var_dump($arrs); die();
	//      					$Ntotal = 0;
	//      					$Atotal = 0;
	//      					$Btotal = 0;
	//      					$Ctotal = 0;
	//      					$Ntotal = 0;
	//      					$weight = 0;
	//      					$acc = "";
	// 						$child = $arrs["ResponseResult"];
	// 						//	var_dump($child); die();
	// 						$prev = 0;
	// 						$count = 0;
							
	// 						foreach ($child as $a => $value) {

	// 						if($a < 6){
 //                            		$Atotal += $child[$a]['weighted'];
 //                    		}else if($a < 12 && $a > 5){
 //                             		$Btotal += $child[$a]['weighted'];
 //                    		}else if($a == 12){
 //                            if($Btotal == 0){
 //                                $Ctotal = 0;
 //                            }else{
 //                            $Ctotal = $Atotal/$Btotal.'%';
 //                            }
 //                   			}	

 //                   			if($a == 5){
 //                             $weight = $Atotal;
 //                             $acc = $child[$a]['accountcode'];
 //                    		}else if($a == 11){
 //                        		$weight = $Btotal;
 //                        		$acc = $child[i]['accountcode'];
                       
 //                    		}else if($a == 12){
 //                        		$weight = $Ctotal;
 //                         	if($Ctotal > 1){
 //                                $acc = "NSFR COMPLIANT";
 //                        }else{
 //                                $acc = "NSFR NON-COMPLIANT";
 //                        }

 //                    }else{
 //                        $weight = $child[$a]['weighted'];
 //                        $acc = $child[$a]['accountcode'];
 //                    }
								
	// 									 		//echo '<pre>' .$count.' = '.$childnum . '</pre>';
	// 								 		$data = $data . '<tr>'.
	// 											'<td>'.$child[$a]['itemno'].'</td>' .
	// 											'<td>'.$child[$a]['title'].'</td>' .
	// 											'<td>'.$child[$a]['reference'].'</td>' .
	// 											'<td>'.$acc.'</td>' .
	// 											'<td>'.$weight.'</td>' .
	// 											'</tr>';
	// 				//			var_dump($data); 
									
	// 				}

	// 				$tableend = '</tbody></table>';
					
	// 			//		die();
	// 				return  $title . $tablehead . $data . $tableend;




		
	// }


	
}
?>