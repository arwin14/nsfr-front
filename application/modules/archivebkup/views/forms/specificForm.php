<?php 
  //var_dump($form);
?>
<div class="container-fluid">
   <form id="reasonForm">
      <input type="hidden" value="<?php echo $form['id'] ; ?>" name="id">
      <input type="hidden" value="<?php echo $form['refid'] ; ?>" name="refid">
      <input type="hidden" value="<?php echo $form['nsfr'] ; ?>" name="nsfr">
      <input type="hidden" value="<?php echo $form['asf'] ; ?>" name="asf">
      <input type="hidden" value="<?php echo $form['rsf'] ; ?>" name="rsf">
      <input type="hidden" value="<?php echo $form['measurement_date'] ; ?>" name="measurement_date">
      <input type="hidden" value="<?php echo $form['dateCreated'] ; ?>" name="dateCreated">
      <input type="hidden" value="<?php echo $form['dateUpdated'] ; ?>" name="dateUpdated">
      <input type="hidden" value="<?php echo $form['noteStatus'] ; ?>" name="noteStatus">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="content">

              <div class="row">
                <div class="col-md-6">
                <h5><label></label> </h5>
                
                </div>
                
                <div class="col-md-1">
                </div>
                <div class="col-md-5">
                <div class="pull-right">
                </div>
                <div class="pull-right">
                  <button type="submit" id="updatebtn" class="savebtnBudget btn btn-round btn-wd btn-primary" style="margin-bottom: 10px;">Save Changes</button>
                </div>
                </div>
                </div>
                  <div class="">
                     <div class="row" style="background:;">
                        <div class="col-md-9">
                           <h4 class="title">Provide Reason</h4>
                           <p class="category">(OPTIONAL)</p>
                        </div>
                        <div class="col-md-3">
                           <label class="form-label" style="font-size: 1.25rem">Duration:</label><br>
                           <div class="form-group form-float">
                              <div class="form-line">
                                 <input type="text" id="duration" name="duration" value="<?php echo $form['duration'] ; ?>" class="form-control ">
                              </div>
                           </div>
                           <br>
                        </div>
                     </div>
                     <div id="content" style="background: ;">

                        <div class="form-group" style="width:100%;">
                           <label for="comment">Comment:</label>
                           <textarea class="form-control" rows="5" name="reason" id="reason" style="width:100%;"><?php echo $form['reason'] ; ?></textarea>
                        </div>
                        <div class="form-group" style="width:100%;">
                           <label for="comment">Action:</label>
                           <textarea class="form-control" rows="5" name="action" id="action" style="width:100%;"><?php echo $form['action'] ; ?></textarea>
                        </div>
                 
                     </div>
                  </div>
                  <br>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>