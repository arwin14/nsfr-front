  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class appFormConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


    public function getFormsData(){
            //var_dump($_POST); die();
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllFORMS/type/'.$_POST['attachmentType'];
    //  var_dump($url); die();
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }


    //JOMER ADD
    public function getPartTwoSheet($date,$basis){
        
        $data = 'type/'.'1'
                .'/date/'.$date;

        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataCAPtype/".$data;
        //var_dump($url);die();
        $ret = Helper::serviceGet($url);
        //var_dump($ret);die();

        $ret = json_decode($ret);
        if($ret != null) {
            if($ret->StatusCode == 100) {
                $this->ModelResponse($ret->StatusCode, 'Data Received', $ret->ResponseResult);
                return true;
            } else {
                $this->ModelResponse($ret->StatusCode, $ret->ResponseResult);
            }
        }
        
        return false;
    }

    public function getPartThreeSheet($date,$basis){
        
        $data = 'type/'.'2'
                .'/date/'.$date;

        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataCAPtype/".$data;
        $ret = Helper::serviceGet($url);
        //var_dump($ret);die();
        $ret = json_decode($ret);
        if($ret != null) {
            if($ret->StatusCode == 100) {
                $this->ModelResponse($ret->StatusCode, 'Data Received', $ret->ResponseResult);
                return true;
            } else {
                $this->ModelResponse($ret->StatusCode, $ret->ResponseResult);
            }
        }
        
        return false;
    }

    //END JOMER ADD




    public function getdownfallData(){
      $date = $_POST['year']."-".$_POST['datespec'];
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getDataShortfall/basis/'.$_POST['dateBasis'].'/date/'.$date;
      //var_dump($url); die();
        $ret = Helper::serviceGet($url);
        $result = json_decode($ret);
        return $result;
    }

    public function updateformData($datajson){
      //var_dump($datajson); die();
      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['backend_URL']."NSFRA/API/v1/updateFORMS");
      echo json_encode($ret);

    }



}
?>