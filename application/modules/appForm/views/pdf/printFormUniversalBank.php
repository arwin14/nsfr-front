<?php
    
?>

<div class="modal fade" id="print_preview_modal" tabindex="-1" role="dialog" aria-labelledby="print_preview_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Print Preview</h4>
            </div>
            <div class = "table-responsive">
                <div id = "content">
                    <style type="text/css" media="print">
                        @media print {
                            @page { 
                                size: legal portrait;
                                width:816px;
                                height:1344px;
                                margin: 10mm 10mm 10mm 10mm;
                            }

                            #foot label{
                                color: black;
                                font-family: arial;
                                font-size: 10pt;
                            }

                            .break{
                                page-break-before: always;
                            }

                            span{
                                font-family: calibri;
                                font-size: 11pt;
                            }

                            #table-pageii{
                                font-family: calibri;
                                font-size: 11pt;
                            }

                            h5{
                                font-family: calibri;
                                font-size: 11pt;
                            }
                        </style>
                    <center>
                     <br><br><br><br>
                        <h3 style="font-family: calibri; margin: 0px;">FOR UNIVERSAL BANK/COMMERCIAL BAMKS AND THEIR SUBSIDIARY BANKS AND QUASI-BANKS</h3>
                    </center>
                    <div class = "container-fluid">
                        <h5 style="font-family: calibri; margin: 0px;">FORM A: INDIVIDUAL GOAL WORKSHEET</h5>
                        <h5 style="font-family: calibri; margin: 0px; font-weight: normal;">PART I</h5>
                        <br>




                        <div style=" border-style: solid; border-width: thin;"><br>
                        <table style="width: 90%; margin:auto;">
                            <thead style="font-family: calibri;">
                                <tr>
                                    <td style="font-size: 10pt; width:70%;"> <input type="text" value="<?php echo $data['BankName']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                     <td style="font-size: 10pt;"><input type="text" value="<?php echo $data['Code']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                  
                                </tr>

                                 <tr>
                                    <td style="font-size: 10pt; width:70%;" align="center">(Name of Bank/Quasi-bank)</td>
                                     <td style="font-size: 10pt; width:70%;" align="center">(Code)</td>
                                  
                                </tr>
                                <tr>
                                    <td colspan="2" style="font-size: 10pt; width:70%;" align="center"> <input type="text" value="<?php echo $data['Address']; ?>"  style=" width:80%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                </tr>
                                 <tr>
                                   <td colspan="2" style="font-size: 10pt; width:70%;" align="center">(Name of Bank/Quasi-bank)</td>
                                </tr>
                            </thead>
                        </table><hr size="width:80%; margin:auto;">
                     </div>




                     <center>
                        <h3>BASEL || NET STABLE FUNDING RATIO REPORT<h3><br><br>
                        <div style="width:60%; margin:auto;">
                           <input type="text" value="<?php echo $data['Basis']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>
                        </div>
                              Indicate it for Solo Basis or Consolidated
                     </center><br>


                      <div style=" border-style: solid; border-width: thin; width:70%; margin:auto;"><br>
                           <center>
                              As of <input type="text" value="<?php echo $data['MonthEnd']; ?>"  style=" width:60%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                              <?php echo '('.$data['datebasis'].')'; ?>
                           </center><br><br>
                      </div><br><br><br><br>


                    </div>  
                </div>
            </div>
            <div class = "modal-footer">
                <div class = "container-fluid">  
                    <button type="submit" class="btn btn-success" id = "print_preview">Print Preview</button>
                </div>
            </div>
        </div>
    </div>
</div>