<?php echo "---------".$data['MonthEndDate'][0]; ?>
<div class="modal fade" id="print_preview_modal" tabindex="-1" role="dialog" aria-labelledby="print_preview_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Print Preview</h4>
            </div>
            <div class = "table-responsive">
                <div id = "content">
                    <style type="text/css" media="print">
                        @media print {
                            @page { 
                                size: legal portrait;
                                width:816px;
                                height:1344px;
                                margin: 10mm 10mm 10mm 10mm;
                            }

                            #foot label{
                                color: black;
                                font-family: arial;
                                font-size: 10pt;
                            }

                            .break{
                                page-break-before: always;
                            }

                            span{
                                font-family: calibri;
                                font-size: 11pt;
                            }

                            #table-pageii{
                                font-family: calibri;
                                font-size: 11pt;
                            }
                            #table2, #table2 th, #table2 th, #table2 td{
                                border: 1px solid black;
                            }
                            h5{
                                font-family: calibri;
                                font-size: 11pt;
                            }
                        </style>
                    <center>
                     <br><br><br><br>

                        <h4 style="font-family: calibri; margin: 0px;">Appendix 74f/Q-44f</h4>
                        <h4 style="font-family: calibri; margin: 0px;">(Appendix to Subsec. X176.5/4176Q.5)</h4>
                    </center>

                    <center>
                     <br>
                        <h3 style="font-family: calibri; margin: 0px;">(FORMAT)</h3>
                        <h3 style="font-family: calibri; margin: 0px;">SWORN CERTIFICATION OF COMPLIANCE WITH</h3>
                        <h3 style="font-family: calibri; margin: 0px;">THE NET STABLE FUNDING RATIO (NSFR) REQUIREMENTS</h3>
                    </center>

                    <center>
                     <br>
                       <input type="text" value="<?php echo $data['certification'][0]; ?>" style=" width:35%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>
                    </center>

                    <center>
                     <br>
                        <h3 style="font-family: calibri; margin: 0px;">CERTIFICATION</h3>
                    </center>

                    <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Pursuant to Subsection X176.5/4176Q.5 of the manual of Regulations for Banks/Manual of Regulations for Non-Bank Financial Institutions, we hereby certify that the Bank/QB has fully complied with the minimum NSFR requirement on all calendar days of the <?php echo $data['datebasis'][0]; ?> ended <input type="text" value="<?php echo $data['MonthEndDate'][0]; ?>" style=" width:100px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>
                        </div>
                    </center>

                     <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We further certify to the best of our knowledge that above statement is true and correct.
                        </div>
                    </center>

                    <center>
                        <br>
                        <table style="width:100%;">
                            <tbody>
                                <tr>
                                    <td align="center"> <input type="text" value="<?php echo $data['CEOSignfirst']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                      <td align="center"> <input type="text" value="<?php echo $data['OtherSign1']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                      <td align="center"> <input type="text" value="<?php echo $data['OtherSign2']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                </tr>
                                <tr>
                                    <td align="center"><?php echo $data['Signatories1'][0]; ?></td>
                                    <td align="center"> <?php echo $data['Signatories2'][0]; ?></td>
                                    <td align="center"> <?php echo $data['Signatories3'][0]; ?></td>
                                </tr>
                                <tr>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['CEOTin']; ?>" style="border: 0px; text-align: center;"></td>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['OtherTin1']; ?>" style="border: 0px; text-align: center;"></td>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['OtherTin2']; ?>"  style="border: 0px; text-align: center;"></td>
                                </tr>
                            
                            </tbody>
                        </table>
                    </center>

                    <center>
                        <br>
                        <table style="width:80%;">
                            <tbody>
                                <tr>
                                    <td align="center"> <input type="text" value="<?php echo $data['OfficerSign']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                    <td align="center"> <input type="text" value="<?php echo $data['ComplianceSign']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                </tr>
                                <tr>
                                    <td align="center" style="width:50%;"><?php echo $data['Signatories4'][0]; ?></td>
                                    <td align="center" colspan="2"><?php echo $data['Signatories5'][0]; ?></td>
                                </tr>
                                <tr>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['OfficerTin']; ?>" style="border: 0px; text-align: center;"></td>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['ComplianceTin']; ?>" style="border: 0px; text-align: center;"></td>
                                </tr>
                            </tbody>
                        </table>
                    </center>


                    <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp SUBSCRIBED AND SWORN TO before me this <input type="text" value="<?php echo $data['SubDay']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly> &nbsp day<input type="text" value="<?php echo $data['SubMonth']; ?>" style=" width:200px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly> 20 <input type="text" value="<?php echo $data['SubYear']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>, at <input type="text" value="<?php echo $data['SubPlace']; ?>" style=" width:300px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>, at Philippines affiant/s exhibiting their government-issued identification cards as follows:
                        </div>
                    </center>


                    <center>
                        <br>
                        <table style="width:100%; border-collapse: collapse;" id="table2">
                            <thead>
                                <tr>
                                    <th>NAME</th>
                                    <th>GOVERNMENT-ISSUED ID</th>
                                    <th>DATE OF ISSUE</th>
                                    <th>PLACE OF ISSUE</th>
                                </tr>
                            </thead>
                          <tbody>
                                 <tr>
                                    <td><input type="text" value="<?php echo $data['Name1']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['GovId1']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['DateIssue1']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['PlaceIssue1']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                 </tr>
                                 <tr>
                                    <td><input type="text" value="<?php echo $data['Name2']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['GovId2']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['DateIssue2']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['PlaceIssue2']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                 </tr>
                                 <tr>
                                    <td><input type="text" value="<?php echo $data['Name3']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['GovId3']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['DateIssue3']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['PlaceIssue3']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                 </tr>
                              </tbody>
                        </table>
                    </center>

                     <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Witness my hand and notarial seal on the date and place above-written.
                        </div>
                    </center>

                    <center>
                        <br>
                        <table style="width:100%; background:red;">
                            <tbody>
                                <tr>
                                    <td align="center" style="width:25%;" >&nbsp</td>
                                    <td align="center" style="width:30%;">&nbsp</td>
                                    <td align="center"><input type="text" value="<?php echo $data['Notary']; ?>" style=" width:300px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                </tr> 
                                <tr>
                                    <td align="center" style="width:25%;">&nbsp</td>
                                    <td align="center" style="width:25%;">&nbsp</td>
                                    <td align="center">NOTARY PUBLIC</td>
                                </tr>
                            </tbody>
                        </table>
                    </center>

                    <div align="left">
                        Doc. No.<input type="text" value="<?php echo $data['DocNo']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                        Doc. No.<input type="text" value="<?php echo $data['PageNo']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                        Doc. No.<input type="text" value="<?php echo $data['BookNo']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                        Doc. No.<input type="text" value="<?php echo $data['SeriesNo']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                    </div>

                    <div class = "break">
                        fdgfd
                    </div>
                  

                    <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            (ln cose of occurrences of non-complionce during the <?php echo $data['complianceDateBasis']; ?>, the certificotion should read os follows:)
                        </div>
                    </center>
                    <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Pursuant to Subsection X176.5/4176Q.5 of the manual of Regulations for Banks/Manual of Regulations for Non-Bank Financial Institutions, we hereby certify that the Bank/QB has fully complied with the minimum NSFR requirement on all calendar days of the <?php echo $data['datebasis'][0]; ?> ended <input type="text" value="<?php echo $data['datebasisCase']; ?>" style=" width:100px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>&nbsp 20<input type="text" value="<?php echo $data['MonthEndYear']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>
                        </div>
                    </center>


                     <center>
                        <br>
                        <table style="width:45%; border-collapse: collapse; margin:auto;" id="table2">
                            <thead>
                                <tr>
                                    <th>Date(Day)</th>
                                    <th>NSFR Compliance (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach ($data['dateCompliance'] as $k3 => $v3) { 
                            ?>
                                <tr>
                                    <td align="center"><?php echo $data['dateCompliance'][$k3]; ?></td>
                                    <td align="center"><?php echo $data['dateCompliance'][$k3]; ?></td>
                                </tr>
                               <?php
                           }
                               ?>
                            </tbody>
                        </table>
                    </center>

                    <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                           A shortfall notice containing the minimum information required under subsection xt76.7/4I76Q.7 of the MORB/MORNBFI had been submitted to the BSP on <?php echo $data['BSPdate']; ?> through the appropriate supervisory department.
                        </div>
                    </center>

                     <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We further certify to the best of our knowledge that above statement is true and correct.
                        </div>
                    </center>

                   <center>
                        <br>
                        <table style="width:100%;">
                            <tbody>
                                <tr>
                                    <td align="center"> <input type="text" value="<?php echo $data['CEOSignDup']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                      <td align="center"> <input type="text" value="<?php echo $data['OtherSign1Dup']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                      <td align="center"> <input type="text" value="<?php echo $data['OtherSign2Dup']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                </tr>
                                <tr>
                                      <td align="center"><?php echo $data['Signatories1'][1]; ?></td>
                                    <td align="center"> <?php echo $data['Signatories2'][1]; ?></td>
                                    <td align="center"> <?php echo $data['Signatories3'][1]; ?></td>
                                </tr>
                                <tr>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['CEOTinDup']; ?>" style="border: 0px; text-align: center;"></td>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['OtherTin1Dup']; ?>" style="border: 0px; text-align: center;"></td>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['OtherTin2Dup']; ?>"  style="border: 0px; text-align: center;"></td>
                                </tr>
                            
                            </tbody>
                        </table>
                    </center>

                    <center>
                        <br>
                        <table style="width:80%;">
                            <tbody>
                                <tr>
                                    <td align="center"> <input type="text" value="<?php echo $data['OfficerSignDup']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                    <td align="center"> <input type="text" value="<?php echo $data['ComplianceSignDup']; ?>" style=" width:90%; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                </tr>
                                <tr>
                                    <td align="center" style="width:50%;"><?php echo $data['Signatories4'][1]; ?></td>
                                    <td align="center" colspan="2"><?php echo $data['Signatories5'][1]; ?></td>
                                </tr>
                                <tr>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['OfficerTinDup']; ?>" style="border: 0px; text-align: center;"></td>
                                    <td align="left">TIN:<input type = "text" value="<?php echo $data['ComplianceTinDup']; ?>" style="border: 0px; text-align: center;"></td>
                                </tr>
                            </tbody>
                        </table>
                    </center>


                   <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp SUBSCRIBED AND SWORN TO before me this <input type="text" value="<?php echo $data['SubDayDup']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly> &nbsp day<input type="text" value="<?php echo $data['SubMonthDup']; ?>" style=" width:200px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly> 20 <input type="text" value="<?php echo $data['SubYearDup']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>, at <input type="text" value="<?php echo $data['SubPlaceDup']; ?>" style=" width:300px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly>, at Philippines affiant/s exhibiting their government-issued identification cards as follows:
                        </div>
                    </center>


                    <center>
                        <br>
                        <table style="width:100%; border-collapse: collapse;" id="table2">
                            <thead>
                                <tr>
                                    <th>NAME</th>
                                    <th>GOVERNMENT-ISSUED ID</th>
                                    <th>DATE OF ISSUE</th>
                                    <th>PLACE OF ISSUE</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <tr>
                                    <td><input type="text" value="<?php echo $data['Name1Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['GovId1Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['DateIssue1Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['PlaceIssue1Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                 </tr>
                                 <tr>
                                    <td><input type="text" value="<?php echo $data['Name2Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['GovId2Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['DateIssue2Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['PlaceIssue2Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                 </tr>
                                 <tr>
                                    <td><input type="text" value="<?php echo $data['Name3Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['GovId3Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['DateIssue3Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                    <td><input type="text" value="<?php echo $data['PlaceIssue3Dup']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" ></td>
                                 </tr>
                              </tbody>
                        </table>
                    </center>

                     <center>
                        <br>
                        <div style="text-align: justify; text-justify: inter-word;">
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Witness my hand and notarial seal on the date and place above-written.
                        </div>
                    </center>

                    <center>
                        <br>
                        <table style="width:100%; background:red;">
                            <tbody>
                                <tr>
                                    <td align="center" style="width:25%;" >&nbsp</td>
                                    <td align="center" style="width:30%;">&nbsp</td>
                                    <td align="center"><input type="text" value="<?php echo $data['NotaryDup']; ?>" style=" width:300px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly></td>
                                </tr> 
                                <tr>
                                    <td align="center" style="width:25%;">&nbsp</td>
                                    <td align="center" style="width:25%;">&nbsp</td>
                                    <td align="center">NOTARY PUBLIC</td>
                                </tr>
                            </tbody>
                        </table>
                    </center>

                    <div align="left">
                        Doc. No.<input type="text" value="<?php echo $data['DocNoDup']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                        Doc. No.<input type="text" value="<?php echo $data['PageNoDup']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                        Doc. No.<input type="text" value="<?php echo $data['BookNoDup']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                        Doc. No.<input type="text" value="<?php echo $data['SeriesNoDup']; ?>" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;" readonly><br>
                    </div>










                    <div class = "container-fluid">
                        <h5 style="font-family: calibri; margin: 0px;">FORM A: INDIVIDUAL GOAL WORKSHEET</h5>
                        <h5 style="font-family: calibri; margin: 0px; font-weight: normal;">PART I</h5>
                        <br>
                    </div>  
                </div>
            </div>
            <div class = "modal-footer">
                <div class = "container-fluid">  
                    <button type="submit" class="btn btn-success" id = "print_preview">Print Preview</button>
                </div>
            </div>
        </div>
    </div>
</div>