   <?php
   $downfall = $downfalldata->ResponseResult;
   $respectiveDate  = $datebasis['date'][3]."-".$datebasis['date'][2] ;
   $monthRespective = $datebasis['date'][2];
   //$year = substr($datebasis['date'][3], 2,4);
   $month = explode("-",$monthRespective);
   $monthIndex =  $month[0] -1;
   $monthString = ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'];
   $monthDisplay =  $monthString[$monthIndex]." ".date("t", strtotime($respectiveDate));
   //echo  $year;
   ?>

    <form id="secondAttache" data-previewtype="printCertification" data-form="#secondAttache" data-id = "3">
                <div class="row">  
                  <div class="col-md-8" >
                  </div>
                  <div class="col-md-4">
                     <button type="submit" class="btn btn-round btn-wd btn-success" id = "printpreviewCERTIFICATION" data-previewtype="printCertification" data-form="#secondAttache" data-id = "3" style="margin-bottom: 10px;">
                     <span class="btn-label">
                     <i class="pe-7s-print"></i>
                     </span>
                     View as PDF
                     </button>
                     <button type="button" id="attachement1Update" class="btn btn-round btn-wd btn-warning" style="margin-bottom: 10px;" data-id="2" data-form="#secondAttache">
                     <span class="btn-label">
                     <i class="pe-7s-paperclip"></i>
                     </span>
                     Update
                     </button>
                  </div>
               </div>

                <div class="container-fluid" style=" border-style: solid; border-width: thin;">
                     <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-md-3">
                           <h4><i><u>Attachment 3</u></i></h4>
                        </div>
                     </div><br>

                     <center>
                        <h6>
                        Appendix 74ilq-44f<br>
                        <i>(Appendix to Subsec. x176.5/4776Q.5)</i>
                        <br><br><br>
                        
                           (FORMAT)<br>
                           SWORN CERTIFICATION OF COMPTIANCE WITH<br>
                           THE NET STABLE FUNDING RATIO (NSFR) REQUIREMENTS<br><br>

                           <input type="text" value="<?php echo $setdata[0]['name']; ?>" name="certification[]" style=" width:30%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);">
                           
                           <br><br>
                           <p style="font-size:12pt;"><b>CERTIFICATION</b></p>
                        </h5>
                     </center><br>

                      <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;"><b>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Pursuant to Subsection X176.5/4176Q.5 of the manual of Regulations for Banks/Manual of Regulations for Non-Bank Financial Institutions, we hereby certify that the Bank/QB has fully complied with the minimum NSFR requirement on all calendar days of the  <input type = "text" value="<?php echo  $datebasis['date'][0]; ?>" name="datebasis[]" style=" border: 0px; width:50px;"> ended <input type="text" value="<?php echo $monthDisplay.', '.$datebasis['date'][3]; ?>" name="MonthEndDate[]" style=" width:100px; border: 0px; border-bottom: 1px solid black; text-align: center;" ><br>

                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We further certify to the best of our knowledge that above statement is true and correct.

                         </b>
                        </div><br><br>

                        <div class="row" style="width:85%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;">
                           <div class="col-md-4">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[3]['name']; ?>" name="CEOSignfirst" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                  <b>
                                    <input type = "text" value="<?php echo  $setdata[45]['name']; ?>" name="Signatories1[]" style="  background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                              TIN:<input type = "text" value="<?php echo $setdata[8]['name']; ?>" name="CEOTin" style="border: 0px; text-align: center; background:rgb(255,224,119);">

                           </div>
                           <div class="col-md-8">
                            <div class="row">
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[4]['name']; ?>" name="OtherSign1" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                  <b>
                                    <input type = "text" value="<?php echo $setdata[41]['name']; ?>" name="Signatories2[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                           </div>
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[5]['name']; ?>" name="OtherSign2" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                  <b>
                                    <input type = "text" value="<?php echo  $setdata[42]['name']; ?>" name="Signatories3[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                           </div>
                           
                           <div class="col-md-6">
                                 TIN:<input type = "text" value="<?php echo $setdata[9]['name']; ?>" name="OtherTin1" style="border: 0px; text-align: center; background:rgb(255,224,119);">
                           </div>
                           <div class="col-md-6">
                                 TIN:<input type = "text" value="<?php echo $setdata[10]['name']; ?>" name="OtherTin2" style="border: 0px; text-align: center; background:rgb(255,224,119);">
                           </div>
                         </div>
                         </div>
                        </div><br><br>

                        <div class="row" style="width:65%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;">
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[6]['name']; ?>" name="OfficerSign" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                 <b>
                                    <input type = "text" value="<?php echo  $setdata[43]['name']; ?>" name="Signatories4[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                              TIN:<input type = "text" value="<?php echo $setdata[11]['name']; ?>" name="OfficerTin" style="border: 0px; text-align: center; background:rgb(255,224,119);">

                           </div>
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[7]['name']; ?>" name="ComplianceSign" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                    <b>
                                    <input type = "text" value="<?php echo  $setdata[44]['name']; ?>" name="Signatories5[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                                TIN:<input type = "text" value="<?php echo $setdata[12]['name']; ?>" name="ComplianceTin" style="border: 0px; text-align: center; background:rgb(255,224,119);">
                           </div>
                        </div><br><br>

       


                        <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;"><b>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp SUBSCRIBED AND SWORN TO before me this <input type="text" value="<?php //echo $setdata[13]['name']; ?>" name="SubDay" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;">day of <input type="text" value="<?php //echo $setdata[14]['name']; ?>" name="SubMonth" style=" width:100px; border: 0px; border-bottom: 1px solid black; text-align: center;" > 20<input type="text" value="<?php //echo substr($setdata[15]['name'],2,4); ?>" name="SubYear" style=" width:30px; border: 0px; border-bottom: 1px solid black; text-align: center;" >, at <input type="text" value="<?php //echo $setdata[16]['name']; ?>" name="SubPlace" style=" width:150px; border: 0px; border-bottom: 1px solid black; text-align: center; " >, Philippines affiant/s exhibiting their government-issued identification cards as follows:
                            <br>

                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We further certify to the best of our knowledge that above statement is true and correct.

                         </b>
                        </div><br><br>
                        <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;" id="table2">
                           <table id="govtbl" style="width:100%;">
                              <thead>
                                 <tr>
                                    <th>NAME</th>
                                    <th>GOVERNMENT-ISSUED ID</th> 
                                    <th>DATE OF ISSUE</th>
                                    <th>PLACE OF ISSUE</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td><input type="text" value="<?php echo $setdata[17]['name']; ?>" name="Name1" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[20]['name']; ?>" name="GovId1" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[23]['name']; ?>" name="DateIssue1" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[26]['name']; ?>" name="PlaceIssue1" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                 </tr>
                                  <tr>
                                    <td><input type="text" value="<?php echo $setdata[18]['name']; ?>" name="Name2" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[21]['name']; ?>" name="GovId2" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[24]['name']; ?>" name="DateIssue2" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[27]['name']; ?>" name="PlaceIssue2" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" >
                                      </td>
                                 </tr>
                                 <tr>
                                    <td><input type="text" value="<?php echo $setdata[19]['name']; ?>" name="Name3" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[22]['name']; ?>" name="GovId3" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[25]['name']; ?>" name="DateIssue3" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[28]['name']; ?>" name="PlaceIssue3" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" >
                                      </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div><br>
                         <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;"><b>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We further certify to the best of our knowledge that above statement is true and correct.
                         </b>
                        </div><br><br>
                        <div class="row" style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;">
                           <div class="col-md-4"></div>
                           <div class="col-md-4"></div>
                           <div class="col-md-4">
                              <input type="text" value="<?php echo $setdata[29]['name']; ?>" name="Notary" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" >
                              <center>
                                 NOTARY PUBLIC
                              </center>
                           </div>

                        <div class="col-md-2">
                              Doc. No.<input type="text" value="<?php echo $setdata[30]['name']; ?>" name="DocNo" style=" width:40%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                              Page. No.<input type="text" value="<?php echo $setdata[31]['name']; ?>" name="PageNo" style=" width:30%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                              Book. No.<input type="text" value="<?php echo $setdata[32]['name']; ?>" name="BookNo" style=" width:30%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                              Series of 20<input type="text" value="<?php echo substr($setdata[33]['name'],2,4); ?>" name="SeriesNo" style=" width:15%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                           
                           </div>


                  </div><br><br>
                  <hr><hr>
                   <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;">
                     <center><b><i>
                            (ln case of occurrences of non-compliance during the <input type = "text" value="<?php echo  $datebasis['date'][0]; ?>" name="complianceDateBasis" style=" border: 0px; width:50px;">, the certification should read os follows:)
                         </i></b>
                         </center>
                        </div><br><br><br><br>
                    <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;"><b>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPursuant to Subsection X176.5 /47LGQ.5 of the Manual of Regulations for Banks (MORB)/Manual of Regulations for Non-Bank Financial Institutions (MORNBFI), we hereby certify that the Bank/QB have fully complied with the minimum NSFR requirement on all calendar days of the <input type = "text" value="<?php echo  $datebasis['date'][0]; ?>" name="datebasis[]" style=" border: 0px; width:50px;"> ended <input type="text" value="<?php echo $monthDisplay; ?>" name="datebasisCase" style=" width:100px; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" >&nbsp 20<input type="text" value="<?php echo trim($datebasis['date'][3], '20'); ?>" name="MonthEndYear" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" >, except on (example):

                         </b>
                        </div><br><br>
                     <div style="width:50%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;" id="table2">
                              <table style="width:100%;" id="tbl2s">
                              <thead>
                                 <tr>
                                    <th>Dates (Day)</th>
                                    <th>NSFR Compliance (%)</th> 
                                    
                                 </tr>
                              </thead>
                               <tbody>
                                <?php foreach ($downfall as $k3 => $v3) { 
                                  if($downfall == "NO DATA FOUND"){
                                ?>
                                     <tr>
                                    <td><input type="text" value="No Data Found" name="dateCompliance[]" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="No Data Found" name="NSFRCompliance[]" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                 </tr>
                                <?php
                                  }else{
                                ?>
                                  <tr>
                                    <td><input type="text" value="<?php echo $downfall[$k3]->name; ?>" name="dateCompliance[]" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo round($downfall[$k3]->count,2).'%'; ?>" name="NSFRCompliance[]" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td align="center">
                                      <!-- <input type="button" value="remove" class="delete"> -->
                                      <!-- <button class="btn btn-social btn-round btn-google delete">
                                            <i class="fa fa-google-plus"> </i>
                                        </button> -->
                                        <button class="btn btn-social btn-simple btn-google delete">
                                           <i class="fa fa-minus-circle"></i>
                                        </button>
                                    </td>
                                 </tr>

                                <?php
                              }
                                  }
                                ?>
                              </tbody>
                           </table>                      
                        </div><br><br>

                        <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;"><b>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspA shortfall notice containing the minimum information required under subsection xt76.7/4I76Q.7 of the MORB/MORNBFI had been submitted to the BSP on  <input type = "text"  value="<?php echo $monthDisplay.', '.$datebasis['date'][3]; ?>" name="BSPdate" style="background:rgb(255,224,119); border: 0px; width:90px;"> through the appropriate supervisory department.<br>

                           &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspWe further certify to the best of our knowledge that above statement is true and correct.

                         </b>
                        </div><br><br>
                      <div class="row" style="width:85%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;">
                           <div class="col-md-4">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[3]['name']; ?>" name="CEOSignDup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                    <b>
                                    <input type = "text" value="<?php echo  $setdata[45]['name']; ?>" name="Signatories1[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                              TIN:<input type = "text" value="<?php echo $setdata[8]['name']; ?>" name="CEOTinDup" style="border: 0px; text-align: center; background:rgb(255,224,119);">

                           </div>
                           <div class="col-md-8">
                            <div class="row">
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[4]['name']; ?>" name="OtherSign1Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                   <b>
                                    <input type = "text" value="<?php echo  $setdata[41]['name']; ?>" name="Signatories2[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                           </div>
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[5]['name']; ?>" name="OtherSign2Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                <b>
                                    <input type = "text" value="<?php echo  $setdata[42]['name']; ?>" name="Signatories3[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                           </div>


                           <div class="col-md-6">
                                 TIN:<input type = "text" value="<?php echo $setdata[9]['name']; ?>" name="OtherTin1Dup" style="border: 0px; text-align: center; background:rgb(255,224,119);">
                           </div>
                           <div class="col-md-6">
                                 TIN:<input type = "text" value="<?php echo $setdata[10]['name']; ?>" name="OtherTin2Dup" style="border: 0px; text-align: center; background:rgb(255,224,119);">
                           </div>
                         </div>
                         </div>
                        </div><br><br>

                        <div class="row" style="width:65%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;">
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[6]['name']; ?>" name="OfficerSignDup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                   <b>
                                    <input type = "text" value="<?php echo  $setdata[43]['name']; ?>" name="Signatories4[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                              TIN:<input type = "text" value="<?php echo $setdata[11]['name']; ?>" name="OfficerTinDup" style="border: 0px; text-align: center; background:rgb(255,224,119);">
                           </div>
                           <div class="col-md-6">
                              <center>
                                 <input type = "text" value="<?php echo $setdata[7]['name']; ?>" name="ComplianceSignDup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                                   <b>
                                    <input type = "text" value="<?php echo  $setdata[44]['name']; ?>" name="Signatories5[]" style=" background:rgb(255,224,119); border: 0px; width:100%; text-align: center;">
                                   </b>
                              </center>
                              
                              TIN:<input type = "text" value="<?php echo $setdata[12]['name']; ?>" name="ComplianceTinDup" style="border: 0px; text-align: center; background:rgb(255,224,119);">
                           </div>
                        </div><br><br>

                         <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;"><b>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp SUBSCRIBED AND SWORN TO before me this <input type="text" value="<?php echo $setdata[13]['name']; ?>" name="SubDayDup" style=" width:50px; border: 0px; border-bottom: 1px solid black; text-align: center;">day of <input type="text" value="<?php //echo $setdata[14]['name']; ?>" name="SubMonthDup" style=" width:100px; border: 0px; border-bottom: 1px solid black; text-align: center; " > 20<input type="text" value="<?php //echo substr($setdata[15]['name'],2,4); ?>" name="SubYearDup" style=" width:30px; border: 0px; border-bottom: 1px solid black; text-align: center; " >, at <input type="text" value="<?php //echo $setdata[16]['name']; ?>" name="SubPlaceDup" style=" width:150px; border: 0px; border-bottom: 1px solid black; text-align: center; " >, Philippines affiant/s exhibiting their government-issued identification cards as follows:
                            <br>

                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We further certify to the best of our knowledge that above statement is true and correct.

                         </b>
                        </div><br><br>

                        <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;" id="table2">
                          <table id="govtbl" style="width:100%;">
                              <thead>
                                 <tr>
                                    <th>NAME</th>
                                    <th>GOVERNMENT-ISSUED ID</th> 
                                    <th>DATE OF ISSUE</th>
                                    <th>PLACE OF ISSUE</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td><input type="text" value="<?php echo $setdata[17]['name']; ?>" name="Name1Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[20]['name']; ?>" name="GovId1Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[23]['name']; ?>" name="DateIssue1Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[26]['name']; ?>" name="PlaceIssue1Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                 </tr>
                                  <tr>
                                    <td><input type="text" value="<?php echo $setdata[18]['name']; ?>" name="Name2Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[21]['name']; ?>" name="GovId2Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[24]['name']; ?>" name="DateIssue2Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[27]['name']; ?>" name="PlaceIssue2Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" >
                                      </td>
                                 </tr>
                                 <tr>
                                    <td><input type="text" value="<?php echo $setdata[19]['name']; ?>" name="Name3Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[22]['name']; ?>" name="GovId3Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[25]['name']; ?>" name="DateIssue3Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" ></td>
                                    <td><input type="text" value="<?php echo $setdata[28]['name']; ?>" name="PlaceIssue3Dup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" >
                                      </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div><br>
                         <div style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;"><b>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We further certify to the best of our knowledge that above statement is true and correct.
                         </b>
                        </div><br><br>
                       <div class="row" style="width:80%; margin:auto; text-align: justify; text-justify: inter-word; font-size:10pt;">
                           <div class="col-md-4"></div>
                           <div class="col-md-4"></div>
                           <div class="col-md-4">
                              <input type="text" value="<?php echo $setdata[29]['name']; ?>" name="NotaryDup" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center;" >
                              <center>
                                 NOTARY PUBLIC
                              </center>
                           </div>

                        <div class="col-md-2">
                              Doc. No.<input type="text" value="<?php echo $setdata[30]['name']; ?>" name="DocNoDup" style=" width:40%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                              Page. No.<input type="text" value="<?php echo $setdata[31]['name']; ?>" name="PageNoDup" style=" width:30%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                              Book. No.<input type="text" value="<?php echo $setdata[32]['name']; ?>" name="BookNoDup" style=" width:30%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                              Series of 20<input type="text" value="<?php echo $setdata[33]['name']; ?>" name="SeriesNoDup" style=" width:15%; border: 0px; border-bottom: 1px solid black; text-align: center; " ><br>
                           
                           </div>


                  </div><br><br>


         <input type="hidden" value="<?php echo $setdata[0]['id'].'-'.$setdata[0]['description']; ?>" name="certification[]" style=" width:30%; border: 0px; border-bottom: 1px solid black; text-align: center;">

            <input type="hidden" value="<?php echo $setdata[1]['id'].'-'.$setdata[1]['description']; ?>" name="MonthEndDate[]" style=" width:30%; border: 0px; border-bottom: 1px solid black; text-align: center;">