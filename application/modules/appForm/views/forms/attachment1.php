         <?php
               $downfall = $downfalldata->ResponseResult;
               $respectiveDate  = $datebasis['date'][3]."-".$datebasis['date'][2] ;
               $monthRespective = $datebasis['date'][2];
               $month = explode("-",$monthRespective);
               $monthIndex =  $month[0] -1;
               $monthDisplay = date("Y-m-t", strtotime($respectiveDate));
               $basis = ($datebasis['date'][0] == 'month')? 'Solo':'Consolidated'
         ?>
              <form id="firstAttache" data-previewtype="printFormUniversalBank" data-form="#firstAttache" data-id = "3">
               <div class="row">
                  
                  <div class="col-md-8" >
                  </div>
                  <div class="col-md-4">
                     <button type="submit" class="btn btn-round btn-wd btn-success"  id = "printpreviewUNIVERSALBANK" style="margin-bottom: 10px;">
                     <span class="btn-label">
                     <i class="pe-7s-print"></i>
                     </span>
                     View as PDF
                     </button>
                     <button type="button" id="attachement1Update" class="btn btn-round btn-wd btn-warning" style="margin-bottom: 10px;" data-id="1" data-form="#firstAttache">
                     <span class="btn-label">
                     <i class="pe-7s-paperclip"></i>
                     </span>
                     Update
                     </button>
                  </div>
               </div>
               <div class="container-fluid" style=" border-style: solid; border-width: thin;">
                  <div class="row">
                     <div class="col-md-9"></div>
                     <div class="col-md-3">
                        <h4><i><u>Attachment 2</u></i></h4>
                     </div>
                  </div>
                  <div class="row"  style="width:80%; margin:auto;">
                     <div class="col-md-3" style="font-size:8pt;"><b>15 banking days after<br>end of reference month</b></div>
                     <div class="col-md-3" style="font-size:8pt;"><b>15 banking days after<br>end of reference month</b></div>
                  </div>
                  <br><br><br><br>
                  <center>
                     <h6>FOR UNIVERSAL BANKS/COMMERCIAL BANKS AND THEIR SUBSIDIARY BANKS AND OUASI-BANKS</h6>
                  </center>
                  <br>
                  <div style=" border-style: solid; border-width: thin; width:80%; margin:auto;">
                     <br>
                     <div style="width:90%; margin:auto;">
                        <div class="row">
                           <div class="col-md-8" style="font-size:8pt;">
                              <input type="text" name="BankName" id="Bankname" class="attachement1" value="<?php echo $setdata[0]['name']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);" required>
                              <center><b>(Name of BanUQuasi-Bank)</b></center>
                           </div>
                           <div class="col-md-4" style="font-size:8pt;">
                              <input type="text" name="Code" id="Code" class="attachement1" value="<?php echo $setdata[1]['name']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);">
                              <center><b>(Code)</b></center>
                           </div>
                        </div>
                        <br>
                        <div class="row">
                           <div class="col-md-12" style="font-size:8pt;">
                              <input type="text" name="Address" id="Address" class="attachement1" value="<?php echo $setdata[2]['name']; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);">
                              <center><b>Address</b></center>
                           </div>
                        </div>
                        <br>
                     </div>
                  </div>
                  <br><br><br>
                  <center>
                     <h6>BASEL III NET STABLE FUNDING RATIO REPORT</h6>
                  </center>
                  <br>
                  <div style="width:40%; margin:auto; font-size:8pt;">
                     <input type="text"  name="Basis" id="Basis" class="attachement1" value="<?php echo $basis; ?>" style=" width:100%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);">
                     <center>
                        <b>(lndicate if for Solo Basis or Consolidated Basis)</b>
                     </center>
                  </div>
                  <br><br>
                  <div style=" border-style: solid; border-width: thin; width:60%; margin:auto; font-size:8pt;">
                     <br>
                     <div style="width:60%; margin:auto;">
                        <center>
                           <b>As of</b><input type="text" name="MonthEnd" id="MonthEnd" class="attachement1" value="<?php echo $monthDisplay; ?>" style=" width:80%; border: 0px; border-bottom: 1px solid black; text-align: center; background:rgb(255,224,119);"><br>
                           <input type = "text" value="<?php echo  $datebasis['date'][0].'-end'; ?>" name="datebasis" style=" border: 0px; width:55px;">
                        </center>
                        <br><br>
                     </div>
                  </div>
                  <br><br><br>
               </div>
            </form>