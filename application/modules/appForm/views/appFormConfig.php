<style type="text/css">
   #table2, #table2 th, #table2 th, #table2 td{
   border: 1px solid black;
   }
</style>
<div class="col-md-12">
   <div class="card">
      <div class="header">
         <legend>
            <div class="row">
               <div class="col-md-8" >
                  <b> Search For Attachments </b> 
               </div>
            </div>
         </legend>
      </div>
      <div class="content">
         <div class="container-fluid" style="background:; width:60%;">
            <form id="attachFilter">
                <input type="hidden" name="basisDisplay" id="basisDisplay">
            <div class="row">
               <div class="col-md-11">
                  <div class="form-group">
                     <div class="form-group">
                        <select name="attachmentType" class="selectpicker" data-title="Attachment Type" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
                           <option value="3">NSFR Attachment</option>
                           <option value="1">Attachment 2</option>
                           <option value="2">Attachment 3</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="col-md-1">
                  <button type="submit" class="btn btn-success btn-fill btn-wd">Search</button>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                     <select name="dateBasis" id="dateBasis" class="form-control" required>
                        <option value="" selected disabled hidden>Date Basis</option>
                        <option value="0">Solo</option>
                        <option value="1">Consolidated</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <!--     <select name="datespec" id="datespec1" class="selectpicker datespec"  data-style="btn-default btn-block" data-menu-style="dropdown-blue">
                        </select> -->
                     <select class="form-control" id="datespec" name="datespec" required>
                        <option value="" selected disabled hidden>Specify Date</option>
                     </select>
                  </div>
               </div>
                  <div class="col-md-3">
                  <div class="form-group">
                     <!--     <select name="datespec" id="datespec1" class="selectpicker datespec"  data-style="btn-default btn-block" data-menu-style="dropdown-blue">
                        </select> -->
                     <select class="form-control" id="year" name="year" required>
                        <option value="" selected disabled hidden>Specify Year</option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
       </form>
      </div>
   </div>
</div>



<div class="col-md-12" id="attachResult" style="display:none;">
   <div class="card">
      <div class="header">
         <legend>
            <div class="row">
               <div class="col-md-8" >
                  <b> ATTACHMENT FORM </b> 
               </div>
            </div>
         </legend>
      </div>
      <div class="content">
         <div class="body" style="width:95%; margin:auto;">
            
               <div id="tbl1" class="tbls1 tab-pane fade in active">
                     <div id="attachmentContent"> 
               <!--    <form id="firstAttache" data-previewtype="printFormUniversalBank" data-form="#firstAttache" data-id = "3">
                  </form> -->
                     </div>
               </div>
         </div>
      </div>
   </div>
</div>







<div id="printcontent"></div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/appForm/appForm.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>