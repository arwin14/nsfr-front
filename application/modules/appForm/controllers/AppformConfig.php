<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xls;


class AppformConfig extends MX_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('appFormConfigCollection');
	}

	public function index() {
		$viewData = array();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration');
		$this->Helper->setView('appFormConfig',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}

	//JOMER ADD
	public function makeDecimal($amount){
		return number_format((float)$amount, 2, '.', '');
	}


	function getIndexByCode($array, $data) {
	    foreach($array as $row) {
	       if($row['code'] == $data){
	       	  return $row;
	       } 
	    }
	    return NULL;
	}

	function getAmountByCode($array, $data){
		foreach($array as $row) {
	       if(trim($row['code']) == $data){
	       	//if($row['code'] == $data){
	       	  return $this->makeDecimal($row['amount']);
	       } 
	    }
	    return "None";
	    var_dump("NONE at ". $data);die();
	}

	public function generateWorksheet(){
		$ret = new appFormConfigCollection();
		$partTwoRet = new appFormConfigCollection();
		$partThreeRet = new appFormConfigCollection();
		$retrieveError = 0;

		$receivedData =	explode("--", $this->uri->segment(4));

		$basis = $receivedData[1];
		$date = $receivedData[0];

		$wews = $partTwoRet->getPartTwoSheet($date,$basis);
		//var_dump($wews); die();
		//var_dump($basis);die();
		if(isset($basis) && isset($date)) {

			
			$basisWord = '';
			if ($basis == 0) {
				$basisWord = 'Solo';
			} else {
				$basisWord = 'Consolidated';
			}


			if($partTwoRet->getPartTwoSheet($date,$basis)) {
				
			} else {
				$retrieveError = 1;
			}

			if($partThreeRet->getPartThreeSheet($date,$basis)) {
				
			} else {
				$retrieveError = 1;
			}


			if ($retrieveError != 1) { //fillupWorkSheet

				$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("assets/local/files/nsfrattachment/Basis NSFR Report_mod.xls"); //set the template
				$Excel_writer = new Xls($spreadsheet);

				/*$spreadsheet->getProperties()
				    ->setTitle('PHP Download Example')
				    ->setSubject('A PHPExcel example')
				    ->setDescription('A simple example for PhpSpreadsheet. This class replaces the PHPExcel class')
				    ->setCreator('php-download.com')
				    ->setLastModifiedBy('php-download.com');*/

				//Fill Cover
				$d=strtotime($date);
				$appearedDate = date("m/d/Y", $d);

				//var_dump($appearedDate);die();

				$spreadsheet->setActiveSheetIndex(0)
				    ->setCellValue('C20', $basisWord.' Basis')
				    ->setCellValue('E24', $appearedDate);

				$partTwoData = json_decode($partTwoRet, true);
				$partTwoData = $partTwoData["Data"];

				$partThreeData = json_decode($partThreeRet, true);
				$partThreeData = $partThreeData["Data"];
				//Fill up Part II
				$spreadsheet->setActiveSheetIndex(2)

				    ->setCellValue('C6', $this->getAmountByCode($partTwoData,'1.a'))	
				    ->setCellValue('C7', $this->getAmountByCode($partTwoData,'1.b'))

				    ->setCellValue('C12', $this->getAmountByCode($partTwoData,'2.a.1.i'))	
				    ->setCellValue('C13', $this->getAmountByCode($partTwoData,'2.a.1.ii'))

				    ->setCellValue('C15', $this->getAmountByCode($partTwoData,'2.a.2.i'))	
				    ->setCellValue('C16', $this->getAmountByCode($partTwoData,'2.a.2.ii'))

				    ->setCellValue('C18', $this->getAmountByCode($partTwoData,'2.b.1'))	
				    ->setCellValue('C19', $this->getAmountByCode($partTwoData,'2.b.2'))

				    ->setCellValue('C24', $this->getAmountByCode($partTwoData,'3.a.1.i'))	
				    
				    ->setCellValue('C28', $this->getAmountByCode($partTwoData,'3.a.1.ii'))	
				    ->setCellValue('C29', $this->getAmountByCode($partTwoData,'3.a.1.iii'))

				    ->setCellValue('C31', $this->getAmountByCode($partTwoData,'3.a.2.i'))
				    
				    ->setCellValue('C35', $this->getAmountByCode($partTwoData,'3.a.2.ii'))
				    ->setCellValue('C36', $this->getAmountByCode($partTwoData,'3.a.2.iii'))

				    ->setCellValue('C39', $this->getAmountByCode($partTwoData,'3.a.3.i'))
				    ->setCellValue('C40', $this->getAmountByCode($partTwoData,'3.a.3.ii'))
				    ->setCellValue('C41', $this->getAmountByCode($partTwoData,'3.a.3.iii'))
				    ->setCellValue('C42', $this->getAmountByCode($partTwoData,'3.b'))

				    ->setCellValue('C46', $this->getAmountByCode($partTwoData,'4.a.1'))
				    ->setCellValue('C47', $this->getAmountByCode($partTwoData,'4.a.2'))
				    ->setCellValue('C48', $this->getAmountByCode($partTwoData,'4.a.3'))
				    ->setCellValue('C49', $this->getAmountByCode($partTwoData,'4.a.4'))
				    ->setCellValue('C50', $this->getAmountByCode($partTwoData,'4.a.5'))
				    ->setCellValue('C51', $this->getAmountByCode($partTwoData,'4.a.6'))

				    ->setCellValue('C53', $this->getAmountByCode($partTwoData,'4.b.1'))
				    ->setCellValue('C54', $this->getAmountByCode($partTwoData,'4.b.2'))
				    ->setCellValue('C55', $this->getAmountByCode($partTwoData,'4.b.3'))
				    ->setCellValue('C56', $this->getAmountByCode($partTwoData,'4.b.4'))
				    ->setCellValue('C57', $this->getAmountByCode($partTwoData,'4.b.5'))
				    ->setCellValue('C58', $this->getAmountByCode($partTwoData,'4.b.6'))

				    ->setCellValue('C60', $this->getAmountByCode($partTwoData,'4.c.1'))
				    ->setCellValue('C61', $this->getAmountByCode($partTwoData,'4.c.2'))
				    ->setCellValue('C62', $this->getAmountByCode($partTwoData,'4.c.3'))
				    ->setCellValue('C63', $this->getAmountByCode($partTwoData,'4.c.4'))
				    ->setCellValue('C64', $this->getAmountByCode($partTwoData,'4.c.5'))
				    ->setCellValue('C65', $this->getAmountByCode($partTwoData,'4.c.6'))

				    ->setCellValue('C69', $this->getAmountByCode($partTwoData,'5.a.1'))
				    ->setCellValue('C70', $this->getAmountByCode($partTwoData,'5.a.2'))
				    ->setCellValue('C71', $this->getAmountByCode($partTwoData,'5.a.3'))

				    ->setCellValue('C73', $this->getAmountByCode($partTwoData,'5.b.1'))
				    ->setCellValue('C74', $this->getAmountByCode($partTwoData,'5.b.2'))
				    ->setCellValue('C75', $this->getAmountByCode($partTwoData,'5.b.3'))
				    ->setCellValue('C76', $this->getAmountByCode($partTwoData,'5.c'))
				    ->setCellValue('C77', $this->getAmountByCode($partTwoData,'5.d'))
				    ->setCellValue('C78', $this->getAmountByCode($partTwoData,'5.e'))
				    ->setCellValue('C79', $this->getAmountByCode($partTwoData,'5.f'));



				$partThreeData = json_decode($partThreeRet, true);
				$partThreeData = $partThreeData["Data"];
				//Fill up Part III
				$spreadsheet->setActiveSheetIndex(3)
				    ->setCellValue('C6', $this->getAmountByCode($partThreeData,'1.a.i'))
					->setCellValue('C7', $this->getAmountByCode($partThreeData,'1.b.i'))
					->setCellValue('C8', $this->getAmountByCode($partThreeData,'1.c.i'))
									
									
					->setCellValue('C11', $this->getAmountByCode($partThreeData,'1.1.i'))
									
					->setCellValue('C13', $this->getAmountByCode($partThreeData,'1.1.ii.a'))
					->setCellValue('C14', $this->getAmountByCode($partThreeData,'1.1.ii.b'))
					->setCellValue('C15', $this->getAmountByCode($partThreeData,'1.1.ii.c'))
									
									
					->setCellValue('C18', $this->getAmountByCode($partThreeData,'1.2.i'))
									
					->setCellValue('C20', $this->getAmountByCode($partThreeData,'1.2.ii.a'))
					->setCellValue('C21', $this->getAmountByCode($partThreeData,'1.2.ii.b'))
					->setCellValue('C22', $this->getAmountByCode($partThreeData,'1.2.ii.c'))
									
					->setCellValue('C24', $this->getAmountByCode($partThreeData,'1.3.i'))
									
					->setCellValue('C26', $this->getAmountByCode($partThreeData,'1.3.ii.a'))
					->setCellValue('C27', $this->getAmountByCode($partThreeData,'1.3.ii.b'))
					->setCellValue('C28', $this->getAmountByCode($partThreeData,'1.3.ii.c'))
									
									
					->setCellValue('C31', $this->getAmountByCode($partThreeData,'1.4.i'))
									
					->setCellValue('C33', $this->getAmountByCode($partThreeData,'1.4.ii.a'))
					->setCellValue('C34', $this->getAmountByCode($partThreeData,'1.4.ii.b'))
					->setCellValue('C35', $this->getAmountByCode($partThreeData,'1.4.ii.c'))
									
					->setCellValue('C37', $this->getAmountByCode($partThreeData,'1.5.i'))
									
					->setCellValue('C39', $this->getAmountByCode($partThreeData,'1.7.ii.a'))
					->setCellValue('C40', $this->getAmountByCode($partThreeData,'1.7.ii.b'))
					->setCellValue('C41', $this->getAmountByCode($partThreeData,'1.7.ii.c'))
									
					->setCellValue('C43', $this->getAmountByCode($partThreeData,'1.6.i'))
									
					->setCellValue('C45', $this->getAmountByCode($partThreeData,'1.6.ii.a'))
					->setCellValue('C46', $this->getAmountByCode($partThreeData,'1.6.ii.b'))
					->setCellValue('C47', $this->getAmountByCode($partThreeData,'1.6.ii.c'))
									
									
					->setCellValue('C50', $this->getAmountByCode($partThreeData,'1.7.i'))
									
					->setCellValue('C52', $this->getAmountByCode($partThreeData,'1.7.ii.a'))
					->setCellValue('C53', $this->getAmountByCode($partThreeData,'1.7.ii.b'))
					->setCellValue('C54', $this->getAmountByCode($partThreeData,'1.7.ii.c'))
									
					->setCellValue('C56', $this->getAmountByCode($partThreeData,'1.8.i'))
									
					->setCellValue('C58', $this->getAmountByCode($partThreeData,'1.8.ii.a'))
					->setCellValue('C59', $this->getAmountByCode($partThreeData,'1.8.ii.b'))
					->setCellValue('C60', $this->getAmountByCode($partThreeData,'1.8.ii.c'))
									
					->setCellValue('C62', $this->getAmountByCode($partThreeData,'1.9.i'))
									
					->setCellValue('C64', $this->getAmountByCode($partThreeData,'1.9.ii.a'))
					->setCellValue('C65', $this->getAmountByCode($partThreeData,'1.9.ii.b'))
					->setCellValue('C66', $this->getAmountByCode($partThreeData,'1.9.ii.c'))
									
					->setCellValue('C68', $this->getAmountByCode($partThreeData,'1.10.i'))
									
					->setCellValue('C70', $this->getAmountByCode($partThreeData,'1.10.ii.a'))
					->setCellValue('C71', $this->getAmountByCode($partThreeData,'1.10.ii.b'))
					->setCellValue('C72', $this->getAmountByCode($partThreeData,'1.10.ii.c'))
									
									
					->setCellValue('C75', $this->getAmountByCode($partThreeData,'1.11.i'))
									
					->setCellValue('C77', $this->getAmountByCode($partThreeData,'1.11.ii.a'))
					->setCellValue('C78', $this->getAmountByCode($partThreeData,'1.11.ii.b'))
					->setCellValue('C79', $this->getAmountByCode($partThreeData,'1.11.ii.c'))
									
					->setCellValue('C81', $this->getAmountByCode($partThreeData,'1.12.i'))
									
					->setCellValue('C83', $this->getAmountByCode($partThreeData,'1.12.ii.a'))
					->setCellValue('C84', $this->getAmountByCode($partThreeData,'1.12.ii.b'))
					->setCellValue('C85', $this->getAmountByCode($partThreeData,'1.12.ii.c'))
					->setCellValue('C86', $this->getAmountByCode($partThreeData,'1.d.6'))
									
									
					->setCellValue('C89', $this->getAmountByCode($partThreeData,'2.a'))
									
					->setCellValue('C91', $this->getAmountByCode($partThreeData,'2.b.i'))
					->setCellValue('C92', $this->getAmountByCode($partThreeData,'2.b.ii'))
					->setCellValue('C93', $this->getAmountByCode($partThreeData,'2.b.iii'))

					->setCellValue('C98', $this->getAmountByCode($partThreeData,'3.a.1.a'))
					->setCellValue('C99', $this->getAmountByCode($partThreeData,'3.a.1.b'))
					->setCellValue('C100', $this->getAmountByCode($partThreeData,'3.a.1.c'))
									
					->setCellValue('C102', $this->getAmountByCode($partThreeData,'3.a.2.a'))
					->setCellValue('C103', $this->getAmountByCode($partThreeData,'3.a.2.b'))
					->setCellValue('C104', $this->getAmountByCode($partThreeData,'3.a.2.c'))
					->setCellValue('C105', $this->getAmountByCode($partThreeData,'3.a.2.d'))
									
					->setCellValue('C107', $this->getAmountByCode($partThreeData,'3.a.3.a'))
					->setCellValue('C108', $this->getAmountByCode($partThreeData,'3.a.3.b'))
					->setCellValue('C109', $this->getAmountByCode($partThreeData,'3.a.3.c'))
					->setCellValue('C110', $this->getAmountByCode($partThreeData,'3.a.3.d'))
									
					->setCellValue('C112', $this->getAmountByCode($partThreeData,'3.a.4.a'))
					->setCellValue('C113', $this->getAmountByCode($partThreeData,'3.a.4.b'))
					->setCellValue('C114', $this->getAmountByCode($partThreeData,'3.a.4.c'))
					->setCellValue('C115', $this->getAmountByCode($partThreeData,'3.a.4.d'))
									
					->setCellValue('C117', $this->getAmountByCode($partThreeData,'3.a.5.a'))
					->setCellValue('C118', $this->getAmountByCode($partThreeData,'3.a.5.b'))
					->setCellValue('C119', $this->getAmountByCode($partThreeData,'3.a.5.c'))
					->setCellValue('C120', $this->getAmountByCode($partThreeData,'3.a.5.d'))
									
					->setCellValue('C122', $this->getAmountByCode($partThreeData,'3.a.6.a'))
									
					->setCellValue('C124', $this->getAmountByCode($partThreeData,'3.a.6.a.i'))
					->setCellValue('C125', $this->getAmountByCode($partThreeData,'3.a.6.a.ii'))
									
					->setCellValue('C127', $this->getAmountByCode($partThreeData,'3.a.7.a'))
					->setCellValue('C128', $this->getAmountByCode($partThreeData,'3.a.7.b'))
									
					->setCellValue('C130', $this->getAmountByCode($partThreeData,'3.b.1'))
					->setCellValue('C131', $this->getAmountByCode($partThreeData,'3.b.2'))
					->setCellValue('C132', $this->getAmountByCode($partThreeData,'3.b.3'))
					->setCellValue('C133', $this->getAmountByCode($partThreeData,'3.b.4'))
									
									
					->setCellValue('C136', $this->getAmountByCode($partThreeData,'4.a'))
					->setCellValue('C137', $this->getAmountByCode($partThreeData,'4.b'))
					->setCellValue('C138', $this->getAmountByCode($partThreeData,'4.c'))
					->setCellValue('C139', $this->getAmountByCode($partThreeData,'4.d'))
					->setCellValue('C140', $this->getAmountByCode($partThreeData,'4.e'))
					->setCellValue('C141', $this->getAmountByCode($partThreeData,'4.f'))
									
									
									
					->setCellValue('C145', $this->getAmountByCode($partThreeData,'5.a.i'))
					->setCellValue('C146', $this->getAmountByCode($partThreeData,'5.a.ii'))
					->setCellValue('C147', $this->getAmountByCode($partThreeData,'5.a.iii'))
					->setCellValue('C148', $this->getAmountByCode($partThreeData,'5.a.vi'))
									
					->setCellValue('C150', $this->getAmountByCode($partThreeData,'5.b.i'))
					->setCellValue('C151', $this->getAmountByCode($partThreeData,'5.b.ii'))
					->setCellValue('C152', $this->getAmountByCode($partThreeData,'5.b.iii'))
									
					->setCellValue('C154', $this->getAmountByCode($partThreeData,'5.c.1'))
					->setCellValue('C155', $this->getAmountByCode($partThreeData,'5.c.2'))
					->setCellValue('C156', $this->getAmountByCode($partThreeData,'5.c.3'))
					->setCellValue('C157', $this->getAmountByCode($partThreeData,'5.c.4'))
					->setCellValue('C158', $this->getAmountByCode($partThreeData,'5.c.5'))
					->setCellValue('C159', $this->getAmountByCode($partThreeData,'5.c.6'))
					->setCellValue('C160', $this->getAmountByCode($partThreeData,'5.c.7'))
					->setCellValue('C161', $this->getAmountByCode($partThreeData,'5.c.8'))
					->setCellValue('C162', $this->getAmountByCode($partThreeData,'5.c.9'))
					->setCellValue('C163', $this->getAmountByCode($partThreeData,'5.c.10'))
									
					->setCellValue('C165', $this->getAmountByCode($partThreeData,'5.d.1'))
					->setCellValue('C166', $this->getAmountByCode($partThreeData,'5.d.2'))
					->setCellValue('C167', $this->getAmountByCode($partThreeData,'5.d.3'))
					->setCellValue('C168', $this->getAmountByCode($partThreeData,'5.d.4'))
					->setCellValue('C169', $this->getAmountByCode($partThreeData,'5.d.5'))				
					->setCellValue('C171', $this->getAmountByCode($partThreeData,'5.e.1'))
					->setCellValue('C172', $this->getAmountByCode($partThreeData,'5.e.2'))
					->setCellValue('C173', $this->getAmountByCode($partThreeData,'5.e.3'));

				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$spreadsheet->setActiveSheetIndex(0);

		        
		        //$writer = IOFactory::createWriter($spreadsheet, "Xlsx");
		        $writer = new Xls($spreadsheet);

		        $filename = 'NSFR_Report.xls';
 
		        header('Content-Type: application/vnd.ms-excel');
		        header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		        header('Cache-Control: max-age=0');
		        
		        ob_end_clean();
				ob_start();

		        $writer->save('php://output');
		        $writer->save('assets/local/files/nsfrattachment_output/'.$filename);




				$ret = new ModelResponse("100", "Successfully retrieve data.");
			} else {
				$ret = new ModelResponse("-20", "Failed to retrieve data.");
			}

			echo $ret;
		} else {
			show_404();
		}
	}



	//END JOMER ADD




public function printGen(){

	$return = array();
	$return['data'] = $_POST;
	$result['form'] = $this->load->view('pdf/'.$_POST['PDFtype'].'.php', $return, TRUE);
	echo json_encode($result);
}


public function formGen(){
	$ret = $this->appFormConfigCollection->getFormsData($_POST);
	$arrsSTD = json_decode($ret);	
	$arrs = json_decode(json_encode($arrsSTD), true);
	$baseLink = "getdownfallData";
	$downfallret = $this->appFormConfigCollection->$baseLink($_POST);
	$datasend = array();
	$datasend['setdata'] = $arrs['ResponseResult'];
	//var_export($arrs['ResponseResult']); die();
	$datasend['downfalldata'] = $downfallret;
	$datasend['datebasis']['date'] = array($_POST['basisDisplay'], $_POST['dateBasis'],$_POST['datespec'],$_POST['year']);

	$pageDirect = ($_POST['attachmentType'] == "2")? $this->validatePage($_POST['attachmentType'], $downfallret) : 'forms/attachment'.$_POST['attachmentType'].'.php';


	$return['form'] = $this->load->view($pageDirect, $datasend, TRUE);
	echo json_encode($return);
}



private function validatePage($data, $downfallret){
		$returnReq = ($downfallret->ResponseResult == "NO DATA FOUND")? 'defaultPage/falseBack.php' : 'forms/attachment'.$data.'.php';
		return $returnReq;
}



public function updateForm(){
	$data = array();
	foreach ($_POST as $k3 => $v3) {
		$countStat = count($_POST[$k3]);
		var_dump($countStat);

			if($countStat == 1){

			}else{
						$str = $_POST[$k3][1];
						$description = explode("-",$str, 2);
						$id = explode("-",$str, -1);
						$size = count($_POST) - 1;		
						$data[] = array(
								"id" => $str[0]
								,"name" => $_POST[$k3][0]
								,"description" => $description[1]
								,"type" => $_POST['id']
							);
		}
	}
			
var_dump(json_encode($data));
			die();

		var_dump(json_encode($_POST['certification'])); die();
		// $data = ["type" => $_POST['id']];
		// foreach ($_POST as $k3 => $v3) {
		// 	 $data += [ $k3 => $_POST[$k3] ];
		// }
	$datajson = json_encode($data);
	$ret = $this->appFormConfigCollection->updateformData($datajson);
	echo $ret;
	//var_dump($datajson); die();
}



}

?>