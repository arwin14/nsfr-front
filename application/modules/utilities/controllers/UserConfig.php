<?php

class UserConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set afunc to the Template
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}




	public function getOption(){
		$ret = $this->UserConfigCollection->getOptionData($_POST);
		echo $ret;
		
			}	//getOptionSaobUpdate	
	public function getOptionSaobUpdate(){
		$ret = $this->UserConfigCollection->getOptionsSaobUpdate($_POST);
		echo $ret;
		
			}

	public function getExpendSaob(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getExpendsSaob($_POST);
		echo $ret;
		
			}
	public function getExpendFar(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getExpendsFar($_POST);
		echo $ret;
		
			}
	public function getUpdateSAOB(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getUpdatesSAOB($_POST);
		echo $ret;
		
			}
	public function getUpdateFAR(){

		// var_dump($_POST['date']); die();
		$ret = $this->UserConfigCollection->getUpdatesFAR($_POST);
		echo $ret;
		
			}

    public function addNewExpend(){
		$ret = $this->UserConfigCollection->addNewExpends($_POST);
		echo $ret;
	}


	public function addNewExpendFar(){
		$ret = $this->UserConfigCollection->addNewExpendsFar($_POST);
		echo $ret;
	}

	public function UpdateExpendSaob(){
		$ret = $this->UserConfigCollection->UpdateExpendsSaob($_POST);
		echo $ret;
	}

		public function UpdateExpendFar(){
		$ret = $this->UserConfigCollection->UpdateExpendsFar($_POST);
		echo $ret;
	}

	public function removeFarExpend(){
		$ret = $this->UserConfigCollection->removeFarExpends($_POST);
		echo $ret;
	}

	public function removeSaobExpend(){
		$ret = $this->UserConfigCollection->removeSaobExpends($_POST);
		echo $ret;
	}
}
?>