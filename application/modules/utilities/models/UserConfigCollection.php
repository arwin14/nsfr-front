  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


//=======================================================
       public function getOptionData(){
       // http://127.0.0.1:8080/NSFRAccountingAPI/BUDGETService/NSFRA/API/v1/getAllExpenditureType/type
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllExpenditureType/type';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;
    }
//=======================================================
       public function getExpendsSaob(){

        $url = $GLOBALS['backend_URL'].'ORS/API/v1/getAllExpenditures/expenditures/';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;
    }
//=======================================================
       public function getOptionsSaobUpdate(){

        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllExpenditureType/type';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;
    }
//=======================================================
       public function getExpendsFar(){
       $url = $GLOBALS['backend_URL'].'FAR/API/v1/getAllExpenditures/expenditures/';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }
//=======================================================
       public function getUpdatesSAOB(){
        $id = $_POST['id'];
        //http://127.0.0.1:8080/NSFRAccountingAPI/BUDGETService/NSFRA/API/v1/selectEXP/id/1
       $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/selectEXP/id/'.$id;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }
//=======================================================
       public function getUpdatesFAR(){
        $id = $_POST['id'];
       $url = $GLOBALS['backend_URL'].'FAR/API/v1/selectEXP/id/'.$id;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        return $ret;
    }
//========================================================
  public function addNewExpends(){

      $arr =
        array(
            "code" => $_POST['expenCodeSaob'],
            "name" => $_POST['expenNameSaob'],
            "factor" => $_POST['factorSAOB'],
            "expend_type" => $_POST['expenTypeSaob']
            );
      $result = json_encode($arr);
      //var_dump($result);die();


      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."NSFRA/API/v1/insertEXP");
        echo json_encode($ret);
          // die();
          //return $ret;
      
     }
//========================================================
  public function addNewExpendsFar(){

      $arr =
        array(
            "code" => $_POST['expenCodeFar'],
            "name" => $_POST['expenNameFar'],
            "expend_type" => $_POST['expenTypeFar']
            );
      $result = json_encode($arr);
      //var_dump($result);die();


      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."FAR/API/v1/insertEXP");
      echo json_encode($ret);
        // die();
        //return $ret;
      
     }
//========================================================
  public function UpdateExpendsSaob(){

      $arr =
        array(
            "id" => $_POST['theid'],
            "code" => $_POST['codeSAOB'],
            "name" => $_POST['budgetNSAOB'],
            "factor" => $_POST['factorSAOBUpdate'],
            "expend_type" => $_POST['codeCategSAOB'],
            "status" => 'ACTIVE'
            );
      $result = json_encode($arr);
      //http://127.0.0.1:8080/ACPCAccountingAPI/BUDGETService/ORS/API/v1/updateEXP

      //var_dump($result); die();
      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."ORS/API/v1/updateEXP");
      echo json_encode($ret);
        // die();
        //return $ret;
      
     }
//========================================================
  public function UpdateExpendsFar(){

      $arr =
        array(
            "id" => $_POST['theidFar'],
            "code" => $_POST['codeFAR'],
            "name" => $_POST['budgetNFAR'],
            "expend_type" => $_POST['codeCategFAR']
            );
      $result = json_encode($arr);
//http://127.0.0.1:8080/ACPCAccountingAPI/BUDGETService/ORS/API/v1/updateEXP

      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."FAR/API/v1/updateEXP");
     // var_dump($ret); die();
      echo json_encode($ret);
        // die();
        //return $ret;
      
     }

//========================================================
     public function removeFarExpends(){
      $code = $_POST['code'];
       $url = $GLOBALS['backend_URL']."FAR/API/v1/deactivateEXP/id/".$code; 

      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   
      //var_dump($ret); die();    
      return $ret;


    }

//========================================================
     public function removeSAOBExpends(){
      $code = $_POST['code'];
       $url = $GLOBALS['backend_URL']."NSFRA/API/v1/deactivateEXP/id/".$code; 

      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   
      var_dump($ret); die();    
      return $ret;


    }

//========================================================
    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>