  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';
//$baseURL =  'http://localhost:48082/ACPCAccountingAPI/BUDGETService/';

class UserConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


//===========================================================
      public function addBudgets(){
        //var_dump($GLOBALS['backend_URL']."far/API/v1/insert"); die();
   $post = $this->input->post();
      $post_data = array();

      foreach ($post as $k => $v) {
        $post_data[$k] = $this->input->post($k,true);

      }


      $farContentList = array();

      foreach($post_data['theCode'] as $k3 => $v3){
        $farContentList[] = 
          array(
          'expendCode'=>$_POST['theCode'][$k3]
          ,'tax' =>$_POST['taxSpec'][$k3]

          ,"ocdAF" => array(
            'fundCategory' => "I.A.1"
             ,'ps'=>($_POST['1ps'][$k3][0] == '0' && $_POST['1ps'][$k3] != 0)? substr($_POST['1ps'][$k3],1) : $_POST['1ps'][$k3]
            ,'mooe'=>($_POST['1mooe'][$k3][0] == '0' && $_POST['1mooe'][$k3] != 0)? substr($_POST['1mooe'][$k3],1) : $_POST['1mooe'][$k3] 
            ,'co'=>($_POST['1co'][$k3][0] == '0' && $_POST['1co'][$k3] != 0)? substr($_POST['1co'][$k3],1) : $_POST['1co'][$k3]
            ,'fe'=>($_POST['1fe'][$k3][0] == '0' && $_POST['1fe'][$k3] != 0)? substr($_POST['1fe'][$k3],1) : $_POST['1fe'][$k3] 
            ,'tax2' => ($_POST['taxAdmin'][$k3][0] == '0' && $_POST['taxAdmin'][$k3] != 0)? substr($_POST['taxAdmin'][$k3],1) : $_POST['taxAdmin'][$k3] 
          )
          ,"ocdAM" => array(
            "fundCategory" => "II.A.1"
             ,"ps" => ($_POST['2ps'][$k3][0] == '0' && $_POST['2ps'][$k3] != 0)? substr($_POST['2ps'][$k3],1) : $_POST['2ps'][$k3]
            ,"mooe" => ($_POST['2mooe'][$k3][0] == '0' && $_POST['2mooe'][$k3] != 0)? substr($_POST['2mooe'][$k3],1) : $_POST['2mooe'][$k3] 
            ,"co" => ($_POST['2co'][$k3][0] == '0' && $_POST['2co'][$k3] != 0)? substr($_POST['2co'][$k3],1) : $_POST['2co'][$k3]
            ,"fe" => 0       
            ,'tax2' =>($_POST['taxAmcfp'][$k3][0] == '0' && $_POST['taxAmcfp'][$k3] != 0)? substr($_POST['taxAmcfp'][$k3],1) : $_POST['taxAmcfp'][$k3]
          )           
            
          ,"ocdOS" => array(
            "fundCategory" => "II.A.2"
             ,"ps" => ($_POST['3ps'][$k3][0] == '0' && $_POST['3ps'][$k3] != 0)? substr($_POST['3ps'][$k3],1) : $_POST['3ps'][$k3]
            ,"mooe" =>($_POST['3mooe'][$k3][0] == '0' && $_POST['3mooe'][$k3] != 0)? substr($_POST['3mooe'][$k3],1) : $_POST['3mooe'][$k3]
            ,"co" => ($_POST['3co'][$k3][0] == '0' && $_POST['3co'][$k3] != 0)? substr($_POST['3co'][$k3],1) : $_POST['3co'][$k3]
            ,"fe" => 0  
            ,'tax2' =>($_POST['taxOperation'][$k3][0] == '0' && $_POST['taxOperation'][$k3] != 0)? substr($_POST['taxOperation'][$k3],1) : $_POST['taxOperation'][$k3]      
          )
        );
      }

      $arr =
        array(
            "refid2" => $_POST['refB'].''.$_POST['refid2'],
          "fundConsumer" => $_POST['empl'],
            "fundDate" => $_POST['DateCur'],
            "createdBy" => $_SESSION['userid'],//============================================
            "farContentList" => $farContentList
            );
      $result = json_encode($arr);
       //var_dump($result);die();


        //var_dump($GLOBALS['backend_URL']."FAR/API/v1/insert");die();
      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."FAR/API/v1/insert");
      echo json_encode($ret);
        // die();
        //return $ret;
      
     }
// //========================================================

      public function updateBudgets(){
    $post = $this->input->post();
      $post_data = array();

      foreach ($post as $k => $v) {
        $post_data[$k] = $this->input->post($k,true);

      }


      $farContentList = array();

      foreach($post_data['theCodeUpdate'] as $k3 => $v3){
          // $handler = $_POST['statusUpdate'][$k3];
          // var_dump($handler); die();
        //if(){
        //var_dump($_POST['statusUpdate'][$k3]);
       if($_POST['statusUpdate'][$k3] == 1){
       $handler = array(
          "expendId" => $_POST['expendidUpdate'][$k3]//================   
          ,"refid" => $_POST['refidUpdate']
          ,"expendCode" => $_POST['theCodeUpdate'][$k3]
          ,"obligations" => $_POST['obligationsUpdate'][$k3]//=========================
          ,"tax" => '0'//=============================
         ,"disbursements" => $_POST['disbursementsUpdate'][$k3]//==================
          ,"ocdAF" => array(
            'id' => $_POST['afid'][$k3]
            ,'fundCategory' => "I.A.1"
            ,'ps'=>($_POST['1psUpdate'][$k3][0] == '0' && $_POST['1psUpdate'][$k3] != 0)? substr($_POST['1psUpdate'][$k3],1) : $_POST['1psUpdate'][$k3]
            ,'mooe'=>($_POST['1mooeUpdate'][$k3][0] == '0' && $_POST['1mooeUpdate'][$k3] != 0)? substr($_POST['1mooeUpdate'][$k3],1) : $_POST['1mooeUpdate'][$k3]
            ,'co'=>($_POST['1coUpdate'][$k3][0] == '0' && $_POST['1coUpdate'][$k3] != 0)? substr($_POST['1coUpdate'][$k3],1) : $_POST['1coUpdate'][$k3]
            ,'fe'=>($_POST['1feUpdate'][$k3][0] == '0' && $_POST['1feUpdate'][$k3] != 0)? substr($_POST['1feUpdate'][$k3],1) : $_POST['1feUpdate'][$k3]
             ,'tax2'=>($_POST['taxAdminUpdate'][$k3][0] == '0' && $_POST['taxAdminUpdate'][$k3] != 0)? substr($_POST['taxAdminUpdate'][$k3],1) : $_POST['taxAdminUpdate'][$k3] 
          )

          ,"ocdAM" => array(
            'id' => $_POST['amid'][$k3]
            ,"fundCategory" => "II.A.1"
            ,"ps" => ($_POST['2psUpdate'][$k3][0] == '0' && $_POST['2psUpdate'][$k3] != 0)? substr($_POST['2psUpdate'][$k3],1) : $_POST['2psUpdate'][$k3]
            ,"mooe" => ($_POST['2mooeUpdate'][$k3][0] == '0' && $_POST['2mooeUpdate'][$k3] != 0)? substr($_POST['2mooeUpdate'][$k3],1) : $_POST['2mooeUpdate'][$k3]
            ,"co" => ($_POST['2coUpdate'][$k3][0] == '0' && $_POST['2coUpdate'][$k3] != 0)? substr($_POST['2coUpdate'][$k3],1) : $_POST['2coUpdate'][$k3]
            ,"fe" => 0   
            ,'tax2'=>($_POST['taxAmcfpUpdate'][$k3][0] == '0' && $_POST['taxAmcfpUpdate'][$k3] != 0)? substr($_POST['taxAmcfpUpdate'][$k3],1) : $_POST['taxAmcfpUpdate'][$k3]    
          )           
            
          ,"ocdOS" => array(
            'id' => $_POST['osid'][$k3]
            ,"fundCategory" => "II.A.2"
            ,"ps" => ($_POST['3psUpdate'][$k3][0] == '0' && $_POST['3psUpdate'][$k3] != 0)? substr($_POST['3psUpdate'][$k3],1) : $_POST['3psUpdate'][$k3]
            ,"mooe" => ($_POST['3mooeUpdate'][$k3][0] == '0' && $_POST['3mooeUpdate'][$k3] != 0)? substr($_POST['3mooeUpdate'][$k3],1) : $_POST['3mooeUpdate'][$k3]
            ,"co" => ($_POST['3coUpdate'][$k3][0] == '0' && $_POST['3coUpdate'][$k3] != 0)? substr($_POST['3coUpdate'][$k3],1) : $_POST['3coUpdate'][$k3]
            ,"fe" => 0 
            ,'tax2'=>($_POST['taxOperationUpdate'][$k3][0] == '0' && $_POST['taxOperationUpdate'][$k3] != 0)? substr($_POST['taxOperationUpdate'][$k3],1) : $_POST['taxOperationUpdate'][$k3]        
          )
      ,"totalPS" => $_POST['totalPSUpdate'][$k3]//=======
      ,"totalMOOE" => $_POST['totalMOOEUpdate'][$k3]//=========
      ,"totalCO" => $_POST['totalCOUpdate'][$k3]//======
      ,"totalFE" => $_POST['totalFEUpdate'][$k3]//======
      ,"farcStatus"=> $_POST['statusUpdateString'][$k3]
        );
    }else{
      $handler = array( 
          "refid" => $_POST['refidUpdate']
          ,"expendCode" => $_POST['theCodeUpdate'][$k3]
          ,"tax" =>"0"//=============================
          ,"ocdAF" => array('fundCategory' => "I.A.1"
           ,'ps'=>($_POST['1psUpdate'][$k3][0] == '0' && $_POST['1psUpdate'][$k3] != 0)? substr($_POST['1psUpdate'][$k3],1) : $_POST['1psUpdate'][$k3]
            ,'mooe'=>($_POST['1mooeUpdate'][$k3][0] == '0' && $_POST['1mooeUpdate'][$k3] != 0)? substr($_POST['1mooeUpdate'][$k3],1) : $_POST['1mooeUpdate'][$k3]
            ,'co'=>($_POST['1coUpdate'][$k3][0] == '0' && $_POST['1coUpdate'][$k3] != 0)? substr($_POST['1coUpdate'][$k3],1) : $_POST['1coUpdate'][$k3]
            ,'fe'=>($_POST['1feUpdate'][$k3][0] == '0' && $_POST['1feUpdate'][$k3] != 0)? substr($_POST['1feUpdate'][$k3],1) : $_POST['1feUpdate'][$k3]
             ,'tax2'=>($_POST['taxAdminUpdate'][$k3][0] == '0' && $_POST['taxAdminUpdate'][$k3] != 0)? substr($_POST['taxAdminUpdate'][$k3],1) : $_POST['taxAdminUpdate'][$k3] 
          )

          ,"ocdAM" => array(
            "fundCategory" => "II.A.1"
             ,"ps" => ($_POST['2psUpdate'][$k3][0] == '0' && $_POST['2psUpdate'][$k3] != 0)? substr($_POST['2psUpdate'][$k3],1) : $_POST['2psUpdate'][$k3]
            ,"mooe" => ($_POST['2mooeUpdate'][$k3][0] == '0' && $_POST['2mooeUpdate'][$k3] != 0)? substr($_POST['2mooeUpdate'][$k3],1) : $_POST['2mooeUpdate'][$k3]
            ,"co" => ($_POST['2coUpdate'][$k3][0] == '0' && $_POST['2coUpdate'][$k3] != 0)? substr($_POST['2coUpdate'][$k3],1) : $_POST['2coUpdate'][$k3]
            ,"fe" => 0   
            ,'tax2'=>($_POST['taxAmcfpUpdate'][$k3][0] == '0' && $_POST['taxAmcfpUpdate'][$k3] != 0)? substr($_POST['taxAmcfpUpdate'][$k3],1) : $_POST['taxAmcfpUpdate'][$k3]        
          )           
            
          ,"ocdOS" => array(
      "fundCategory" => "II.A.2"
           ,"ps" => ($_POST['3psUpdate'][$k3][0] == '0' && $_POST['3psUpdate'][$k3] != 0)? substr($_POST['3psUpdate'][$k3],1) : $_POST['3psUpdate'][$k3]
            ,"mooe" => ($_POST['3mooeUpdate'][$k3][0] == '0' && $_POST['3mooeUpdate'][$k3] != 0)? substr($_POST['3mooeUpdate'][$k3],1) : $_POST['3mooeUpdate'][$k3]
            ,"co" => ($_POST['3coUpdate'][$k3][0] == '0' && $_POST['3coUpdate'][$k3] != 0)? substr($_POST['3coUpdate'][$k3],1) : $_POST['3coUpdate'][$k3]
            ,"fe" => 0 
            ,'tax2'=>($_POST['taxOperationUpdate'][$k3][0] == '0' && $_POST['taxOperationUpdate'][$k3] != 0)? substr($_POST['taxOperationUpdate'][$k3],1) : $_POST['taxOperationUpdate'][$k3]                  
          )

      ,"farcStatus"=> $_POST['statusUpdateString'][$k3]
        );
    }



        $farContentList[] = $handler;
          
      }
      $arr =
        array(
          "refid" => $_POST['refidUpdate'],
          "refid2" => $_POST['refBUpdate'].''.$_POST['refid2Update'],
            "fundConsumer" => $_POST['emplUpdate'],
            "fundDate" => $_POST['DateCurUpdate'],
            "updatedBy" => $_SESSION['userid'],//============================================
            "farContentList" => $farContentList,
            "farStatus" => "ACTIVE"
            );
        //var_dump($arr);die();
      $result = json_encode($arr);
     // var_dump($result);die();


      $ret = Helper::serviceCallBudget($result, $GLOBALS['backend_URL']."FAR/API/v1/update");
      //var_dump($ret);die();
      echo json_encode($ret);
        // die();
        //return $ret;
      
      }




//===========================================================

   public function getDataUpdate(){
       $refId = $_POST['refId'];
       //var_dump($refid); die();
      $url = $GLOBALS['backend_URL']."FAR/API/v1/getSpecData/refid/".$refId;

       $ret = Helper::serviceGet($url);
     //var_dump($ret); die();
       
        return $ret;

    }
    //===========================================================




   public function getOptionsData(){
       $url = $GLOBALS['backend_URL']."FAR/API/v1/getAllExpenditures/expenditures/";
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
       //var_dump($result); //die();
        //var_dump($ret); die();
        return $ret;



    }
    //===========================================================

   public function getOptionsDataAlters(){
       $date = '2018-04-01';
       $url = $GLOBALS['backend_URL']."FAR/API/v1/getAllExpenditures/expenditures/";
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
    
        return $ret;



    }
    //===========================================================

   public function getMonthlyDataColl(){
      $theDate = $_POST['date'];
      //var_dump($theDate); die();
        //$url = "http://localhost:48082/ACPCAccountingAPI/BUDGETService/far/API/v1/getDataByMonthAndYear/date/".$theDate;
        $url = $GLOBALS['backend_URL']."FAR/API/v1/getDataByMonthAndYear/date/".$theDate;
        $ret = Helper::serviceGet($url);
        //\spe($ret); die();
        return $ret;



    }

// //========================================================


     public function removeBudgets(){
      $refId = $_POST['refId'];
       $url = $GLOBALS['backend_URL']."FAR/API/v1/deactivateFAR/refid/".$refId;     
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);       
      return $ret;


    }

  


//========================================================

    public function activateuser($id){
        $data = array(
            "status"=>"ACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }
    
    public function deactivateuser($id){
        $data = array(
            "status"=>"INACTIVE"
            );
        $this->db->where("userid",$id);
        return $this->db->update("webusers",$data);
    }

}

?>