<?php

class UserConfig extends MX_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('UserConfigCollection');
	}

	public function index() {
		// var_dump("test");die();
        $this->Helper->sessionEndedHook('Session');
		$this->Helper->setTitle('User Configuration'); 
		$this->Helper->setView('UserConfig.form','',FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}

	

	public function getUpdate(){
		$ret = $this->UserConfigCollection->getDataUpdate($_POST);
		echo $ret;
			}

	public function getOptionData(){
		$ret = $this->UserConfigCollection->getOptionsDataAlters($_POST);
		echo $ret;
		
			}
	public function getOptionDataAlter(){
		$ret = $this->UserConfigCollection->getOptionsDataAlters($_POST);
		echo $ret;
		
			}
	public function getMonthlyData(){
		$ret = $this->UserConfigCollection->getMonthlyDataColl($_POST);
		echo $ret;
		
			}


	public function removeBudget(){

		$ret = $this->UserConfigCollection->removeBudgets($_POST);
		echo $ret;
			}


	public function addBudget(){
		$ret = $this->UserConfigCollection->addBudgets($_POST);
		echo $ret;
	}

	public function updateBudget(){
		
		$ret = $this->UserConfigCollection->updateBudgets($_POST);
		echo $ret;
	}

//==========================================================================================================

	public function exportExcel(){
	    ob_start();
	    $mydate = $_POST['excelDate'];
		$out = $this->TRExportExcel($mydate);
		ob_end_clean();
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment; filename=BIR_Sales_Summary". date("Y-m-d H-i-s") . ".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $out;
		die();
		exit;
	}



	public function TRExportExcel($mydate){
		$thedate = $mydate;

			$topheader = 	'<table style="font-weight:bold;">' .
							'<tr style="text-align:left;">' .
								'<td colspan="22"><h3>REGISTRY OF ALLOTMENTS, OBLIGATIONS AND DISBURSEMENTS (RAOD)</h3></td>' .
							'</tr>' .
							'<tr style="text-align:left;">' .
								'<td colspan="22">(PERSONEL SERVICES & MOOE)</td>' .
							'</tr>' .
							'<tr style="text-align:left;">' .
								'<td colspan="22">(Allotment Class)</td>' .
							'</tr>'.
							'<tr style="text-align:left;">' .
								'<td colspan="22">Name of Agency</td>' .
							'</tr>'.
							'<tr>' .
								'<td><h2 style="color:blue;">'.$thedate.'</h2></td>' .

							'</tr>'.	
							'</table>';

				$title = 	'<table style="font-weight:bold; font-size: 25px;">' .
							'<tr style="text-align:center;">' .
								'<td colspan="20">SAOB Monthly Report</td>' .
							'</tr>' .	
							'</table>';



				$tablehead = '<table cellpadding="0" cellspacing="0" border="1" width="100%" style="border-color:black;">
							  <thead>
                           <tr>
                                <th colspan="7" class="no-sort"><h3>(SAOB)<label id="dateHead"><?php echo $datehead; ?></label></h3></th>     
                                <th colspan="3"  style="text-align: center;">ADMIN & FINANCE</th>
                                <th></th>
                                <th colspan="2" style="text-align: center;">AMCFP-FRM</th> 
                                <th></th>
                                <th colspan="2" style="text-align: center;">OPERATIONS-RESEARCH</th> 
                                <th></th>
                                 <th colspan="4" style="text-align: center;">TOTAL</th> 
                            </tr>
                            <tr>
                                <th colspan="7" class="no-sort"></th>     
                                <th colspan="4" style="text-align: center;">1.A.1</th>
                                <th colspan="3" style="text-align: center;">11.A.1</th> 
                                <th colspan="3" style="text-align: center;">11.A.2</th>
                                <th colspan="4" style="text-align: center;">Total</th>
                            </tr>
                            <tr>
                       
                                <th class="no-sort"></th>
                                <th class="no-sort"></th>
                                <th class="no-sort">REMARKS ON Ca</th>
                                <th class="no-sort">CODE</th>
                                <th class="no-sort">OBLIGATIONS</th>
                                <th class="no-sort">TAX</th>
                                <th class="no-sort">DISBURSEMENTS</th>
                                <th class="no-sort">PS</th>
                                <th class="no-sort">MOOE</th>  
                                <th class="no-sort">CO</th>
                                <th class="no-sort">FE</th>
                                <th class="no-sort">PS</th>
                                <th class="no-sort">MOOE</th>
                                <th class="no-sort">CO</th>
                                <th class="no-sort">PS</th>
                                <th class="no-sort">MOOE</th>
                                <th class="no-sort">CO</th>
                                <th class="no-sort">PS</th>
                                <th class="no-sort">CO</th>
                                <th class="no-sort">FE</th>
                                <th class="no-sort">MOOE</th>
                            </tr>
                    </thead>
							<tbody>';

					$tabledata = '';
					$data = '';
					
							
							$url = $GLOBALS['backend_URL']."FAR/API/v1/getDataByMonthAndYear/date/".$thedate;
      						$ret = Helper::serviceGet($url);
      						$arrsSTD = json_decode($ret);
      						$arrs = json_decode( json_encode($arrsSTD), true);
						
							// $json_string =  file_get_contents( $GLOBALS['backend_URL']."far/API/v1/getDataByMonthAndYear/date/".$thedate);
							// $arrs = json_decode($json_string, true);
							

							function sortById($a, $b) {
								return str_replace('-', '', $b['refid2']) - str_replace('-', '', $a['refid2']);
							}

							usort($arrs['ResponseResult'], 'sortById');


							$data = '';
							$data1 = '';
						    //ksort($arrs['ResponseResult'][0]['refid']);
							$obligationTotal = 0;
							$taxTotal = 0;
							$disbursementsTotal = 0;
							$ocdpsTotal = 0;
							$ocdmooeTotal = 0;
							$ocdcoTotal = 0;
							$ocdfeTotal = 0;

							$ocdampsTotal = 0;
							$ocdammooeTotal = 0;
							$ocdamcoTotal = 0;

							$ocdospsTotal = 0;
							$ocdosmooeTotal = 0;
							$ocdoscoTotal = 0;

							$totalpstotal = 0;
							$totalmooetotal = 0;
							$totalcototal = 0;
							$totalfetotal = 0;

						  // var_dump($arrs); die();
						foreach ($arrs['ResponseResult'] as $key => $value) {	
							$farNum = count( $value['farContentList']);
							$farContentList = $value['farContentList'];;
							if($farNum > 1){
								foreach ($farContentList as $key1 => $value1) {
										
										 if($key1 == 0){	
											$data = $data . '<tr>'.
											'<td>'. $value['refid2'] .'</td>' .
											'<td>'. $value['fundConsumer'].'</td>' .
											'<td></td>'.
											'<td>'. $farContentList[0]['expendCode'].'</td>' .
											'<td>'. $farContentList[0]['obligations'].'</td>'.
											'<td>'. $farContentList[$key1]['tax'].'</td>'.
											'<td>'. $farContentList[0]['disbursements'].'</td>'.
											'<td>'. $farContentList[0]['ocdAF']['ps'].'</td>'.
											'<td>'. $farContentList[0]['ocdAF']['mooe'].'</td>'.
											'<td>'. $farContentList[0]['ocdAF']['co'].'</td>'.
											'<td>'. $farContentList[0]['ocdAF']['fe'].'</td>'.
											'<td>'. $farContentList[0]['ocdAM']['ps'].'</td>'.
											'<td>'. $farContentList[0]['ocdAM']['mooe'].'</td>'.
											'<td>'. $farContentList[0]['ocdAM']['co'].'</td>'.
											'<td>'. $farContentList[0]['ocdOS']['ps'].'</td>'.
											'<td>'. $farContentList[0]['ocdOS']['mooe'].'</td>'.
											'<td>'. $farContentList[0]['ocdOS']['co'].'</td>'.
											'<td>'. $farContentList[0]['totalPS'].'</td>'.
											'<td>'. $farContentList[0]['totalCO'].'</td>'.
											'<td>'. $farContentList[0]['totalFE'].'</td>'.
											'<td>'. $farContentList[0]['totalMOOE'].'</td>'.
											'</tr>';

											$obligationTotal = $obligationTotal + $value['farContentList'][0]['obligations'];
											$taxTotal = $taxTotal + $value['farContentList'][$key1]['tax'];
											$disbursementsTotal = $disbursementsTotal + $value['farContentList'][0]['disbursements'];
											$ocdpsTotal = $ocdpsTotal + $value['farContentList'][0]['ocdAF']['ps'];
											$ocdmooeTotal = $ocdmooeTotal + $value['farContentList'][0]['ocdAF']['mooe'];
											$ocdcoTotal = $ocdcoTotal + $value['farContentList'][0]['ocdAF']['co'];
											$ocdfeTotal = $ocdfeTotal + $value['farContentList'][0]['ocdAF']['fe'];

											$ocdampsTotal = $ocdampsTotal + $value['farContentList'][0]['ocdAM']['ps'];
											$ocdammooeTotal = $ocdammooeTotal + $value['farContentList'][0]['ocdAM']['mooe'];
											$ocdamcoTotal = $ocdamcoTotal + $value['farContentList'][0]['ocdAM']['co'];


											$ocdospsTotal = $ocdospsTotal + $value['farContentList'][0]['ocdOS']['ps'];
											$ocdosmooeTotal = $ocdosmooeTotal + $value['farContentList'][0]['ocdOS']['mooe'];
											$ocdoscoTotal = $ocdoscoTotal + $value['farContentList'][0]['ocdOS']['co'];

											$totalpstotal = $totalpstotal +  $value['farContentList'][0]['totalPS'];
											$totalcototal = $totalcototal + $value['farContentList'][0]['totalCO'];
											$totalfetotal = $totalfetotal + $value['farContentList'][0]['totalFE'];
											$totalmooetotal = $totalmooetotal + $value['farContentList'][0]['totalMOOE'];
										}else{
										$data = $data . '<tr>'.
											'<td ></td>' .
											'<td></td>' .
											'<td></td>' .
											'<td>'. $farContentList[$key1]['expendCode'].'</td>' .
											'<td>'. $farContentList[$key1]['obligations'].'</td>'.
											'<td>'. $farContentList[$key1]['tax'].'</td>'.
											'<td>'. $farContentList[$key1]['disbursements'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdAF']['ps'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdAF']['mooe'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdAF']['co'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdAF']['fe'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdAM']['ps'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdAM']['mooe'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdAM']['co'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdOS']['ps'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdOS']['mooe'].'</td>'.
											'<td>'. $farContentList[$key1]['ocdOS']['co'].'</td>'.
											'<td>'. $farContentList[$key1]['totalPS'].'</td>'.
											'<td>'. $farContentList[$key1]['totalCO'].'</td>'.
											'<td>'. $farContentList[$key1]['totalFE'].'</td>'.
											'<td>'. $farContentList[$key1]['totalMOOE'].'</td>'.
											'</tr>';
											$obligationTotal = $obligationTotal + $value['farContentList'][$key1]['obligations'];
											$taxTotal = $taxTotal + $value['farContentList'][$key1]['tax'];
											$disbursementsTotal = $disbursementsTotal + $value['farContentList'][$key1]['disbursements'];
											$ocdpsTotal = $ocdpsTotal + $value['farContentList'][$key1]['ocdAF']['ps'];
											$ocdmooeTotal = $ocdmooeTotal + $value['farContentList'][$key1]['ocdAF']['mooe'];
											$ocdcoTotal = $ocdcoTotal + $value['farContentList'][$key1]['ocdAF']['co'];
											$ocdfeTotal = $ocdfeTotal + $value['farContentList'][$key1]['ocdAF']['fe'];

											$ocdampsTotal = $ocdampsTotal + $value['farContentList'][$key1]['ocdAM']['ps'];
											$ocdammooeTotal = $ocdammooeTotal + $value['farContentList'][$key1]['ocdAM']['mooe'];
											$ocdamcoTotal = $ocdamcoTotal + $value['farContentList'][$key1]['ocdAM']['co'];

											$ocdospsTotal = $ocdospsTotal + $value['farContentList'][$key1]['ocdOS']['ps'];
											$ocdosmooeTotal = $ocdosmooeTotal + $value['farContentList'][$key1]['ocdOS']['mooe'];
											$ocdoscoTotal = $ocdoscoTotal + $value['farContentList'][$key1]['ocdOS']['co'];

											$totalpstotal = $totalpstotal +  $value['farContentList'][$key1]['totalPS'];
											$totalcototal = $totalcototal + $value['farContentList'][$key1]['totalCO'];
											$totalfetotal = $totalfetotal + $value['farContentList'][$key1]['totalFE'];
											$totalmooetotal = $totalmooetotal + $value['farContentList'][$key1]['totalMOOE'];
										}
									}
							}else{
							$data = $data . '<tr>'.
								'<td>'. $value['refid2'] .'</td>' .
								'<td>'. $value['fundConsumer'].'</td>' .
								'<td></td>' .
								'<td>'. $farContentList[0]['expendCode'].'</td>' .
								'<td>'. $farContentList[0]['obligations'].'</td>'.
								'<td>'. $farContentList[0]['tax'].'</td>'.
								'<td>'. $farContentList[0]['disbursements'].'</td>'.
								'<td>'. $farContentList[0]['ocdAF']['ps'].'</td>'.
								'<td>'. $farContentList[0]['ocdAF']['mooe'].'</td>'.
								'<td>'. $farContentList[0]['ocdAF']['co'].'</td>'.
								'<td>'. $farContentList[0]['ocdAF']['fe'].'</td>'.
								'<td>'. $farContentList[0]['ocdAM']['ps'].'</td>'.
								'<td>'. $farContentList[0]['ocdAM']['mooe'].'</td>'.
								'<td>'. $farContentList[0]['ocdAM']['co'].'</td>'.
								'<td>'. $farContentList[0]['ocdOS']['ps'].'</td>'.
								'<td>'. $farContentList[0]['ocdOS']['mooe'].'</td>'.
								'<td>'. $farContentList[0]['ocdOS']['co'].'</td>'.
								'<td>'. $farContentList[0]['totalPS'].'</td>'.
								'<td>'. $farContentList[0]['totalCO'].'</td>'.
								'<td>'. $farContentList[0]['totalFE'].'</td>'.
								'<td>'. $farContentList[0]['totalMOOE'].'</td>'.
								'</tr>';

								$obligationTotal = $obligationTotal + $value['farContentList'][0]['obligations'];
								$taxTotal = $taxTotal + $value['farContentList'][0]['tax'];
								$disbursementsTotal = $disbursementsTotal + $value['farContentList'][0]['disbursements'];
								$ocdpsTotal = $ocdpsTotal + $value['farContentList'][0]['ocdAF']['ps'];
								$ocdmooeTotal = $ocdmooeTotal + $value['farContentList'][0]['ocdAF']['mooe'];
								$ocdcoTotal = $ocdcoTotal + $value['farContentList'][0]['ocdAF']['co'];
								$ocdfeTotal = $ocdfeTotal + $value['farContentList'][0]['ocdAF']['fe'];

								$ocdampsTotal = $ocdampsTotal + $value['farContentList'][0]['ocdAM']['ps'];
								$ocdammooeTotal = $ocdammooeTotal + $value['farContentList'][0]['ocdAM']['mooe'];
								$ocdamcoTotal = $ocdamcoTotal + $value['farContentList'][0]['ocdAM']['co'];

								$ocdospsTotal = $ocdospsTotal + $value['farContentList'][0]['ocdOS']['ps'];
								$ocdosmooeTotal = $ocdosmooeTotal + $value['farContentList'][0]['ocdOS']['mooe'];
								$ocdoscoTotal = $ocdoscoTotal + $value['farContentList'][0]['ocdOS']['co'];

								$totalpstotal = $totalpstotal +  $value['farContentList'][0]['totalPS'];
								$totalcototal = $totalcototal + $value['farContentList'][0]['totalCO'];
								$totalfetotal = $totalfetotal + $value['farContentList'][0]['totalFE'];
								$totalmooetotal = $totalmooetotal + $value['farContentList'][0]['totalMOOE'];
						
						}
					// var_dump($farContentList); die();
					// echo '<pre>' . var_export($farContentList, true) . '</pre>'; 
					// die();
					}
					$tablefooter = '<tfoot>
                            <tr>
                       
                                <th class="no-sort"></th>
                                <th class="no-sort"></th>
                                <th class="no-sort"></th>
                                <th class="no-sort">total</th>
                                <th>'.$obligationTotal.'</th>
                                <th class="no-sort">'.$taxTotal.'</th>
                                <th class="no-sort">'.$disbursementsTotal.'</th>
                                <th class="no-sort">'.$ocdpsTotal.'</th>
                                <th class="no-sort">'.$ocdmooeTotal.'</th>  
                                <th class="no-sort">'.$ocdcoTotal.'</th>  
                                <th class="no-sort">'.$ocdfeTotal.'</th>  
								<th class="no-sort">'.$ocdampsTotal.'</th>
								<th class="no-sort">'.$ocdammooeTotal.'</th>  
								<th class="no-sort">'.$ocdamcoTotal.'</th>  
								<th class="no-sort">'.$ocdospsTotal.'</th>
								<th class="no-sort">'.$ocdosmooeTotal.'</th>  
								<th class="no-sort">'.$ocdoscoTotal.'</th> 
                                <th class="no-sort">'.$totalpstotal.'</th> 
                                <th class="no-sort">'.$totalcototal.'</th> 
                                <th class="no-sort">'.$totalfetotal.'</th> 
                                <th class="no-sort">'.$totalmooetotal.'</th> 
                            </tr>
                    <tfoot>';	
					$tableend = '</tbody></table>';

					
				//die();
					return $topheader .  $title . $tablehead . $data . $tablefooter . $tableend;




		
	}
	
}
?>