
                  <div class="updateAvailStable collapse">
                     <div class="card" id='dateSearch collapse'>
                        <div class="header">
                           <h5 id="">
                              Update Currency
                           </h5>
                        </div>
                        <div class="body" style="width:90%; margin:auto;">
                         <form id="UpdateCurrency" data-form="#UpdateCurrency" data-function="tblCurrency" data-direct="CurrencyConfig/updateCurrency" enctype="multipart/form-data" accept-charset="utf-8">
                           <input type="hidden" name="theid" id="theid">
                           <div class="row clearfix">
                              <div class="col-md-12">
                                    <div class="form-line">
                                       <input type="text" class="alp form-control" id='CurrencyNameUpdate' name="CurrencyNameUpdate" placeholder="Currency Name" readonly>
                                    </div>
                              </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='CurrencyCodeUpdate' name="CurrencyCodeUpdate" placeholder="Currency Code" readonly>
                                    </div>
                              </div>
                                <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='CurrencyRateUpdate' name="CurrencyRateUpdate" placeholder="Currency Rate" required>
                                    </div>
                              </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                 <button type="submit" class="btn btn-lg btn-success btn-block waves-effect" id="submitUpdateAvail" name="submitUpdateAvail">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>SUBMIT</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnCancel" name="btnCancel" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CANCEL</strong>
                                 </button>
                              </div>
                           </div><br>
                        </form>
                        </div>
                     </div>
                  </div>
                  <div>

                     <div class="card collapse in" id='addAvail'>
                        <form id="AddCurrency" data-form="#AddCurrency" data-function="tblCurrency" data-functiontwo="tblcurrencyChart" data-direct="CurrencyConfig/addCurrency" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="header">
                           <h5>
                              Add Currency
                           </h5>
                        </div>
                        <div class="body" style="width:90%; margin:auto;">
                           <div class="row clearfix">
                              <div class="col-md-12">
                              <div class = "form-line">
                           <select class="form-control" id = "currencyName" name="CurrencyName" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
                              <option value="" selected>Select your option</option>
                           </select>
                        </div>
      
                              </div>
                           </div><br>
                           <div class="row clearfix">
                              <div class="col-md-6">
                                    <div class="form-line">                                         
                                       <input type="text" class="num form-control" id='CurrencyCode' name="CurrencyCode" placeholder="Currency Code" readonly="">
                                    </div>
                              </div>
                                <div class="col-md-6">
                                    <div class="form-line">
                                       <input type="text" class="num form-control" id='CurrencyRate' name="CurrencyRate" placeholder="Currency Rate" required>
                                    </div>
                              </div>
                           </div><br>
                           <div class="row clearfix" style="width:98%; margin:auto;">
                              <div class="col-md-6">
                                 <button type="submit" id="btnAddAvail" name="btnAddAvail" class="btn btn-lg btn-warning btn-block waves-effect">
                                 <i class="fa fa-paper-plane fa-fw"></i>
                                 <strong>ADD</strong>
                                 </button>
                              </div>
                              <div class="col-md-6">
                                 <button id="btnClear" name="btnClear" type="button" class="btn btn-lg btn-danger btn-block waves-effect">
                                 <i class="fa fa-trash fa-fw"></i>
                                 <strong>CLEAR</strong>
                                 </button>
                              </div>
                           </div><br>
                        </div>
                     </form>
                     </div>

                  </div>
<script src="<?php echo base_url(); ?>assets/thirdparty/js/select2.js" type="text/javascript"></script>