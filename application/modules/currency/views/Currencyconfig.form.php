<div class="container-fluid" style="font-size:8pt;">
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="card">
      <div class="header bg-brown">
         <h2 style="font-size: 2.2rem">
            NSFR
            <!-- <small>Agricultural Credit Policy Council</small>  -->
         </h2>
      </div>
      <div class="body">
         <!-- <a  data-toggle="collapse" data-target="#demo">
            <i class="material-icons">info_outline</i>
            </a> -->
         <div class="row clearfix" style="width:98%; margin:auto;">
            <div class="collapse row" id="demo" >
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                     <div class="body" style="background:rgb(151,210,154);">
                        It allows you to modify/add personel services that automatically appear in the form.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <!-- <div class="col-md-12">
                  <table id="currencyTblChart" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;"></table>
               </div> -->
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
               <?php echo $content; ?>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" id="tblContainer">
               <div class="card">
                  <div class="header">
                     <h2>
                        Currency List
                     </h2>
                  </div>
                  <div class="body" style="width:95%; margin:auto;">
                     <!--     <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" data-form="addavail" data-addcateg="#expenTypeAvail" href=".tbls1" id="btnsaob">101 GAA</a></li>
                        <li><a data-toggle="tab" data-form="addreq" data-addcateg="#expenTypeReq" href=".tbls2" id="btnfar">SB</a></li>
                        </ul> -->
                     <div class="tab-content">
                        <div id="tbl1" class="tbls1 tab-pane fade in active">
                           <div class="table-responsive">
                              <table id="currencyTbl" class="table  table-hover dataTable js-exportable" style="font-size:8pt; width:100%;">
                                 <thead>
                                    <tr>
                                       <th></th>
                                       <th>Name</th>
                                       <th>Code</th>
                                       <th>Rate</th>
                                    </tr>
                                 </thead>
                                 <!--  <tbody>
                                    <?php 
                                       foreach ($tbldataAvail as $index => $value) {   
                                       //echo $tbldata[$index]['name'];                 
                                       ?>
                                    <tr>
                                    <td><?php echo $tbldataAvail[$index]['btn']; ?></td>
                                    <td><?php echo $tbldataAvail[$index]['name']; ?></td>
                                    <td><?php echo $tbldataAvail[$index]['title']; ?></td>
                                    <td><?php echo $tbldataAvail[$index]['factor']; ?></td>
                                    <td><?php echo $tbldataAvail[$index]['code']; ?></td>
                                    </tr>
                                    <?php
                                       }
                                       ?>
                                    </tbody> -->
                                 <tfoot></tfoot>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/FormModUtil/currency.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>