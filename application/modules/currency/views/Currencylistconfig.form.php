<div class="container-fluid" style="font-size:8pt;">
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="card">
      <div class="header bg-brown">
         <h2 style="font-size: 2.2rem">
            NSFR
            <label class="pull-right">Last Updated: <?php echo date("M d, Y"); ?> </label>
            <!-- <small>Agricultural Credit Policy Council</small>  -->
         </h2>
      </div>
      <div class="body">
         <!-- <a  data-toggle="collapse" data-target="#demo">
            <i class="material-icons">info_outline</i>
            </a> -->
         <div class="row clearfix" style="width:98%; margin:auto;">
            <div class="row">
               <div class="col-md-12">
                  <table id="currencyTblChart" class="table table-bordered table-striped table-hover dataTable js-exportable" style="font-size:8pt; width:100%;"></table>
               </div>
            </div>
           
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/FormModUtil/currencylist.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/scrolljs/jquery.doubleScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/alphanum/jquery.alphanum.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/thirdparty/mask/src/jquery.mask.js"></script>