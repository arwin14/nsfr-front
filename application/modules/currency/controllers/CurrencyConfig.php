<?php
class CurrencyConfig extends MX_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->model('Helper'); //--> Loads Helper Class (Contains various helper functions)
		// $this->load->model('ModuleRels');
		$this->load->model('CurrencyConfigCollection');
	}

	public function index() {
		$viewData = array();;
        $this->Helper->sessionEndedHook('Session');

		$this->Helper->setTitle('User Configuration'); //--> Set title to the Template
		$viewData['content'] = $this->load->view("forms/currencyform", "forms",TRUE);
		$this->Helper->setView('CurrencyConfig.form',$viewData,FALSE);
		$this->Helper->setTemplate('templates/mastertemplate');
	}






	public function getCurrency(){
		$ret = $this->CurrencyConfigCollection->getCurrencyData($_POST);
		$arrsSTD = json_decode($ret);
		$arrs = json_decode( json_encode($arrsSTD), true);
		$result = $arrs['ResponseResult'];
		$return = array();
		/*var_dump($result); die();*/
		if($result == "NO DATA FOUND"){
			$return[] = array(
			 "code" => '',
			 "name" => 'No Data Found',
			 "rate" => '',
			 "btn" => ''
			);
		}else{	
		foreach ($result as $key => $value) {
		$return[] = array(
			 "code" => $result[$key]['code'],
			 "name" => $result[$key]['name'],
			 "rate" => $result[$key]['rate'],
			 "btn" =>
			 		  "<a class='btn btn-simple btn-warning btn-icon editbtn' data-refid='".$result[$key]['id']."' data-direct='getspecCurrency'><i class='fa fa-edit'></i></a>".
			          "<a  class='btn btn-simple btn-danger btn-icon removebtn' data-refid='".$result[$key]['id']."' data-direct='CurrencyConfig/removeCurrency' data-functionname = 'tblCurrency'><i class='fa fa-times'></i></a>"
			);
		}
		}

		$result = json_encode($return);
		//var_dump($result); die();
		echo $result;
		
	}
public function addCurrency(){
	$ret = new CurrencyConfigCollection();
	$requestdata = array(
						'code'=>$_POST['CurrencyCode'],
						'name'=>$_POST['CurrencyName'],
						'rate'=>$_POST['CurrencyRate'],
					);

	$datajson = json_encode($requestdata);
	$ret = $this->CurrencyConfigCollection->addDataCurrency($datajson);
	echo $ret;
}

public function getCurrencyChart(){
				$data = [];
	$ret = $this->CurrencyConfigCollection->getCurrencyCharts($_POST);


		$arrsSTD = json_decode($ret);
		$arrs = json_decode( json_encode($arrsSTD), true);
		$result = $arrs['ResponseResult'];
		//$return = array();
		$rets = array();
		$totaldata = array();
		//$data1 = array();
		$merge = array();
			for($a = 0; $a < count($result); $a++){
				$conversionList = $result[$a]['currenciesConvertion'];
				${"data" . $a} = array();
				for($b=0; $b < count($conversionList); $b++){
						${"data" . $a} +=["" => $result[$a]['label'], $conversionList[$b]['code']=> round($conversionList[$b]['rate'],2 )];
						
				}
				${"datas" . $a} = ${"data" . $a};
				array_push($merge, ${"datas" . $a});
			}

		$datajson = json_encode($merge);
		echo $datajson;













  //die();
}

public function updateCurrency(){
	$ret = new CurrencyConfigCollection();
	$requestdata = array(
						'id'=>$_POST['theid'],
						'code'=>$_POST['CurrencyCodeUpdate'],
						'name'=>$_POST['CurrencyNameUpdate'],
						'rate'=>$_POST['CurrencyRateUpdate'],
						'status'=>"ACTIVE",
					);

	$datajson = json_encode($requestdata);
	$ret = $this->CurrencyConfigCollection->updateDataCurrency($datajson);
	echo $ret;
}

public function removeCurrency(){
	// $ret = new CurrencyConfigCollection();
	// var_dump($_POST); die();
	// $requestdata = array(
	// 					'id'=>$_POST['idbase']
	// 				);

	// $datajson = json_encode($requestdata);
	$ret = $this->CurrencyConfigCollection->removeDataCurrency($_POST);
	echo $ret;
}

public function getspecCurrency(){
	$ret = $this->CurrencyConfigCollection->getspecsCurrency($_POST);
	echo $ret;
}


}

?>