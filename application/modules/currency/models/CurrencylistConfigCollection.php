  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class CurrencylistConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


    public function getCurrencyData(){

        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllCurrency';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;
    }



    public function getCurrencyCharts(){
        // var_dump($_POST); die();
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllForEx';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
       // var_dump($result); die();
        return $ret;
    }




}
?>