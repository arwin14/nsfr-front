  <?php
include_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';

class CurrencyConfigCollection extends Helper {
 
    public function __construct()
    {
        $this->load->database();
        $this->load->model("UserAccount");
    }


    public function getCurrencyData(){

        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllCurrency';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
        //var_dump($result); die();
        return $ret;
    }

    public function addDataCurrency($datajson){
      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['backend_URL']."NSFRA/API/v1/insertFOREX");
      echo json_encode($ret);

    }

    public function updateDataCurrency($datajson){
      $ret = Helper::serviceCallBudget($datajson,  $GLOBALS['backend_URL']."NSFRA/API/v1/updateFOREX");
      echo json_encode($ret);

    }

    public function getspecsCurrency(){
        // var_dump($_POST); die();
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/selectFOREX/id/'.$_POST['refid'];
        $ret = Helper::serviceGet($url);
        //var_dump($ret); die();
        $result = json_encode($ret);
        return $ret;
    }

    public function getCurrencyCharts(){
        // var_dump($_POST); die();
        $url = $GLOBALS['backend_URL'].'NSFRA/API/v1/getAllForEx';
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
       // var_dump($result); die();
        return $ret;
    }


    public function removeDataCurrency(){
      $refId = $_POST['idbase'];
      $url =  $GLOBALS['backend_URL']."NSFRA/API/v1/deactivateFOREX/id/".$refId;     
     //var_dump($url);
      $ch = curl_init();      
      curl_setopt($ch, CURLOPT_URL, $url);    
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $ret  = curl_exec($ch);   
      //var_dump($ret); die();
      return $ret;


    }


}
?>