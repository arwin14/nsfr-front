<?php

  class forgotPassword extends MX_Controller {

    public function __construct() {
      parent::__construct();
      $this->load->model('Helper');
      $this->load->model('forgotPasswordCollection');
    }
    
    public function index() {
      
      /*Helper::sessionStartedHook('home');*/
      $this->Helper->setTitle('Home');
      $this->Helper->setView('forgotPassword.form','',FALSE);
      $this->Helper->setTemplate('templates/logintemplate');
    }

    public function forgotPass(){
        $ret = $this->forgotPasswordCollection->forgotPass($_POST);
        echo json_encode(array("data"=>$ret));
    }

  }
?>