<?php

  class forgotPasswordCollection extends Helper {

  	public function __construct() {
  		$this->load->database();
      $this->load->model("UserAccount");
  	}
    
    public function forgotPass($posdata){
        $data = array(
            'email' => $posdata['email'],
            'link' => 'http://localhost/UserManagement/forgotChangePassword'
        );
        $ret = Helper::serviceCall($data, "http://180.150.134.136:48082/ACPCAccountingAPI/User/API/v1/forgotPassword");
        return $ret;
    }

  }
?>