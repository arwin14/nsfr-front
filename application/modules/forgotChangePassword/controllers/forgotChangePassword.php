<?php

  class forgotChangePassword extends MX_Controller {

    public function __construct() {
      parent::__construct();
      $this->load->model('Helper');
      $this->load->model('forgotChangePasswordCollection');
    }
    
    public function index() {
      
      /*Helper::sessionStartedHook('home');*/
      $this->Helper->setTitle('Home');
      $this->Helper->setView('forgotChangePassword.form','',FALSE);
      $this->Helper->setTemplate('templates/logintemplate');
    }

    public function submitChangePassword(){
        $ret = $this->forgotChangePasswordCollection->submitChangePassword($_POST);
        echo json_encode(array("data"=>$ret));
    }

  }
?>