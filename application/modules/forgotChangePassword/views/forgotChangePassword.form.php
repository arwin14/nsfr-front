<div class = "login-box">
    <div class="logo">
        <a href="javascript:void(0);" style="font-size:1.5em; color: black;" >Agricultural Credit and Policy Council </a>
        <small style="color: black;">User Management</small>
    </div>
    <div class = "card">
        <div class = "body">
            <form id="formChangeForgot">
                <div class="msg">Enter your New Password:</div>
                <div class="input-group">
                    <div class="form-group">
                        <label>New Password:</label>
                        <input type="password" class="form-control" name="newPass" id="newPass" placeholder="New Password">
                        <input type="hidden" name="" value="">
                    </div>
                    <br>
                    <div class="form-group">
                        <label>Re-type Password:</label>
                        <input type="password" class="form-control" name="reTypePass" id="reTypePass" placeholder="Re-Type Password">
                        <input type="hidden" name="" value="">
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-xs-8 p-t-5">
                        <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Remember Me</label>
                    </div> -->
                    <div class="col-xs-8 p-t-5">
                        <center>
                            <p id = "passwordLabel"></p>
                        </center>
                    </div>
                    <div class="col-xs-12">
                        <input type="submit" id="submitForgot" class="btn btn-block bg-pink waves-effect" value="Submit" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
  



    <script src="<?php echo base_url(); ?>assets/local/module/js/forgotChangePassword/forgotChangePassword.js"></script>