<?php

  class forgotChangePasswordCollection extends Helper {

  	public function __construct() {
  		$this->load->database();
      $this->load->model("UserAccount");
  	}
    
    public function submitChangePassword($postdata){
        $data = array(
        	'userId' => $postdata['id'],
            'newPassword' => $postdata['newPass']
        );
        $ret = Helper::serviceCall($data, "http://180.150.134.136:48082/ACPCAccountingAPI/User/API/v1/changePassword2");
        return $ret;
    }
  }
?>