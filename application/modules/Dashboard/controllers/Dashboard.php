<?php

class Dashboard extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Helper");
		$this->load->model('DashboardCollection');
	}

	public function index(){
		$this->Helper->sessionEndedHook("Session");
		$this->Helper->setView("Dashboard.form","",FALSE);
		$this->Helper->setTemplate("templates/mastertemplate");
	}

	public function solo(){
		$this->Helper->sessionEndedHook("Session");
		$this->Helper->setTitle('Solo - Dashboard');
		$this->Helper->setView("Dashboard.form","",FALSE);
		$this->Helper->setTemplate("templates/mastertemplate");
	}

	public function consolidated(){
		$this->Helper->sessionEndedHook("Session");
		$this->Helper->setTitle('Consolidated - Dashboard');
		$this->Helper->setView("Dashboard.form","",FALSE);
		$this->Helper->setTemplate("templates/mastertemplate");
	}

	public function getDashboardData(){
		//var_dump($_POST);die();
		$ret = new DashboardCollection();

		if(!empty($_POST)) {

			$month = $_POST['datespec'];
			$year = $_POST['year'];
			$basis = $_POST['basisspec'];

			$date = $year.'-'.$month;


			if($ret->getDashboardDataModel($date,$basis)) {
					$ret = new ModelResponse($ret->getCode(), $ret->getMessage(), $ret->getData());
					//var_dump($ret);
			} else {
				$ret = new ModelResponse($ret->getCode(), $ret->getMessage());
			}

				echo $ret;
		} else {
					show_404();
		}


		
	}

}


?>