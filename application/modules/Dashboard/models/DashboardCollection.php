<?php

class DashboardCollection extends Helper {
 
    public function __construct(){

        $this->load->database();
        $this->load->model("UserAccount");

    }

    public function getDashboardDataModel($date,$basis){
        
        $data = 'basis/'.$basis
                .'/date/'.$date;

        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getDataNSFR/".$data;
        //var_dump($url);die();
        $ret = Helper::serviceGet($url);

        //var_dump($ret);die();
        $ret = json_decode($ret);
        if($ret != null) {
            if($ret->StatusCode == 100) {
                $this->ModelResponse($ret->StatusCode, 'Data Received', $ret->ResponseResult);
                    return true;
            } else {
                $this->ModelResponse($ret->StatusCode, $ret->ResponseResult);
            }
        }
        
        return false;
    }

    public function getSpecificsforTotal(){
       // $url = $GLOBALS['backend_URL']."ORSF/API/v1/getDataBAF306B/date/2018-04-10";
        $refid = $_POST['refId'];

        //var_dump($refid);
        $url = $GLOBALS['backend_URL']."NSFRA/API/v1/getSpecDataNSFR/refid/".$refid;
        $ret = Helper::serviceGet($url);
        $result = json_encode($ret);
     //   var_dump($ret); die();
        return $ret;



    }

}

?>
