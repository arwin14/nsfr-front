

<style>
  #nsfrchart .ct-bar {
  /* Colour of your bars */
  stroke: #bc42f4;
}

 #asfchart .ct-bar{
  /* Colour of your bars */
  stroke:  green;
}

 #rsfchart .ct-bar {
  /* Colour of your bars */
  stroke: red;
}

.chart-container {
  width: 100%;
  overflow-x: scroll;
}
</style>

<?php 

    if ($this->uri->segment(3) == "Solo" || $this->uri->segment(3) == "solo") {
        $basis = "0";
        $pagetitle = "Solo";
        $pagecategory = 'Month of <span id="monthlabel">September</span> <span id="yearlabel">2018</span>';
        $selecttext = 'Select Month';
        $chartcontainer = '';
    } else {
        $basis = "1";
        $pagetitle = "Consolidated";
        $pagecategory = '<span id="monthlabel">First Quarter</span> of <span id="yearlabel">2018</span>';
        $selecttext = 'Select Quarter';
        $chartcontainer = 'chart-container';
    }

?>

<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="header">
                <h4 class="title"><?php echo $pagetitle ?> - Dashboard</h4>
                <p class="category" id="dashdescription">
                    <?php echo $pagecategory ?>

                    <a href="#" id="show_date_filter" title='Select Month'>
                        <i class="fa fa-filter"></i>
                    </a>
                </p>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card" id="FilterContainer" style="display: none;">
                            <div class="header">
                                <h5 class="title"><?php echo $selecttext ?></h5>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form id="filterform">
                                            <input type="text" name="basisspec" id="basisspec" value="<?php echo $basis ?>" hidden>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <select class="form-control" id="datespec" name="datespec" required>
                                                            <option value="" selected disabled hidden>Specify Date</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <select class="form-control" id="year" name="year" required>
                                                            <option value="" selected disabled hidden>Specify Year</option>
                                                        </select>
                                                    </div>
                                               </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="appenderrordiv" hidden>
                                            <center>
                                                <label><h5 id="appenderrortext" class="text-danger">No Data Available</h5></label>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="pull-right">
                                            <button class="btn btn-success" id="submitfilter">Submit</button>
                                            <button class="btn btn-warning" id="closeit"> Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card ">
                            <div class="header">
                                <h5 class="title">Net Stable Funding Ratio, %</h5>
                            </div>
                            <div class="content">
                                <div class="<?php echo $chartcontainer ?>">
                                    <div id="nsfrchart" class="ct-chart"></div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card ">
                            <div class="header">
                                <h5 class="title">Available Stable Funding</h5>
                            </div>
                            <div class="content">
                                <div class="<?php echo $chartcontainer ?>">
                                    <div id="asfchart" class="ct-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card ">
                            <div class="header">
                                <h5 class="title">Required Stable Funding</h5>
                            </div>
                            <div class="content">
                                <div class="<?php echo $chartcontainer ?>">
                                    <div id="rsfchart" class="ct-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/local/module/js/dashboard/solodashboard.js"></script>