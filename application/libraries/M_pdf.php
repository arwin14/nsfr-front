<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class m_pdf {
    
    function m_pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
         
        if ($params == NULL)
        {
            $param = '"en-GB-x","A4","","",10,10,10,10,6,3';          		
        }
         
        //return new mPDF($param);
        return new mPDF();
    }

    function load_v2($lang, $format)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';

        return new mPDF($lang, $format);
    }

    function reportlist($format)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';

        return new mPDF("en-GB-x",$format,"","",10,10,10,10,6,3);
    }

    function local_load($lang, $format)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';

        return new mPDF($lang, $format,"","",0,0,0,0,0,0);
    }

    function official_receipt($lang, $format)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';

        return new mPDF($lang, $format,0,'',10,10,5,5);
    }
}