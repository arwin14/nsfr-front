$(document).ready(function () {
	
	var baseurl = "http://localhost/UserManagement/";
	var sessuserid = $("#sessuserid").val();

	$("#changePasswordForm").validate({
		rules: {
			 old_password: {required: true},
			 new_password: {required: true}
		},
		messages:{
			old_password: { required: "Please fill out this field." },
			new_password: { required: "Please fill out this field." }
		},
		submitHandler: function(form) {
			var formData = new FormData($("#changePasswordForm")[0]);
			$.ajax({
                url: window.location +"/submitChange",
                type: "POST",
                data: {
                	sessuserid: sessuserid,
                	old_password: $("#old_password").val(),
                	new_password: $("#new_password").val()
                },
                dataType: "json",
                success: function(json){
                	console.log(json);
                    if(json.data.ResponseResult == "Change Password Success|Please check your email. Thankyou"){
                    	//swal("Success!", "Changed Password Successfully", "success");
                    	swal({
				            title: "Success!",
				            text: "Changed Password Successfully!",
				            type: "success"
				        }, function() {
				            window.location = window.location;
				        });
                    }
                    else{
                    	swal("Invalid Password!", "Please Input Correct Password!", "warning");
                    }
                }
            });
		}
	});

});


