//Added by Jomer 1 of 2
var editeddetailsjson = [];

var selecteddatadetails = [];

function reformatdetails(data) {


    for (var i = 0; i < data.length; i++) {
        if (typeof data[i].title != 'undefined') {
            item = {};
            item["expendId"] = data[i].fundid;
            item["expendCode"] = data[i].code;
            item["amount"] = data[i].amount;
            item["nsfrStatus"] = data[i].status;

            selecteddatadetails.push(item);
        } else {

        }
    }
}
var openRows = new Array();



function modulesValidate(data) {
    let res
    $.ajax({
        type: "POST",
        url: "AllotmentConfig/getModule",
        data: {
            module: data
        },
        dataType: "json",
        async: false,
        success: function(result) {
            res = result

        },
        error: function(result) {
            alert("error");
            retdata = "error";
        }
    });
    return res
}



var openRows = new Array();
function validateChanges(id, baseid, data) {
    let res
    $.ajax({
        type: "POST",
        url: "AllotmentConfig/getSpecific",
        data: {
            refId: baseid
        },
        dataType: "json",
        async: false,
        success: function(result) {

            returnvalue = result.ResponseResult

            for (var i = 1; i < returnvalue.length; i++) {
                if (id == returnvalue[i].fundid) {
                    (data == returnvalue[i].amount) ? res = "1": res = "0"
                } else {

                }

            }

        },
        error: function(result) {
            alert("error");
            retdata = "error";
        }
    });
    return res

}



function submitvalidate(data, refid) {
    let myvalidatejson = [];
    let myvalidateCurrentjson = [];
    $('input[name^="totalreponse"]').each(function() {
        expendId = $(this).data('fundid')
        item = {};
        item["amount"] = $(this).val();
        item["expendId"] = expendId.toString();
        myvalidatejson.push(item);
    });
    $.ajax({

        type: "POST",
        url: "AllotmentConfig/getSpecificValidate",
        data: {
            refId: refid
        },
        dataType: "json",
        async: false,
        success: function(result) {
            returnvalue = result.ResponseResult

            for (var i = 1; i < returnvalue.length; i++) {
                currentitem = {};
                currentitem['amount'] = returnvalue[i].amount
                currentitem['expendId'] = returnvalue[i].fundid
                myvalidateCurrentjson.push(currentitem);
            }

        },
        error: function(result) {
            alert("error");
            retdata = "error";
        }
    });
    myvalidatejson = myvalidatejson.sort(function SortByID(x, y) {
        return x.expendId - y.expendId;
    });

    statusRes = JSON.stringify(myvalidatejson) != JSON.stringify(myvalidateCurrentjson) ? '1' : '0';
    return statusRes

}




function formatopen(d) {
    data = d;
    var retdata = "";
    $.ajax({
        type: "POST",
        url: "AllotmentConfig/tablecollapseview",
        dataType: "json",
        async: false,
        success: function(result) {
            retdata = result;

        },
        error: function(result) {
            alert("error");
            retdata = "error";
        }
    });

    return retdata.form;
}

function getSet(data, specific) {
    var group = [];
    for (var i = 0; i < data.length; i++) {
        if (typeof data[i][specific] != 'undefined') {
            group.push(data[i][specific]);
        } else {

        }
    }

    group = $.unique(group);

    return group
}

function getalldatabytitle(data, title) {
    var group = [];

    for (var i = 0; i < data.length; i++) {
        if (typeof data[i].title != 'undefined') {
            if (data[i].title == title) {
                group.push(data[i]);
            }
        } else {

        }
    }
    return group;
}

function getalldata(data) {
    var group = [];
    for (var i = 0; i < data.length; i++) {
        if (typeof data[i].title != 'undefined') {
            //   if (data[i].title == title) {
            group.push(data[i]);
            // }
        } else {

        }
    }
    return group;
}

function getalldataOrig(data) {
    var group = [];
    for (var i = 0; i < data.length; i++) {
        if (typeof data[i].title != 'undefined') {
            //   if (data[i].title == title) {
            let item = {};
            item["expendCode"] = data[i].code;
            item["amount"] = data[i].amount;

            group.push(item);
            // }
        } else {

        }
    }
    return group;
}



function totaldetails(d) {
    summarytotal = []
    $.ajax({
        type: "POST",
        url: "AllotmentConfig/getSpecificforTotal",
        data: {
            refId: d
        },
        dataType: "json",
        async: false,
        success: function(breakdowndetails) {
            summarytotal.push(breakdowndetails.ResponseResult[0]);
        }
    })
    return summarytotal;
}


function SolutionTotal(nsfrAmount, asfAmount, rsfAmount) {
    computedAmount = {}
    rawNSFRtotal = nsfrAmount * 100
    computedAmount['nsfr'] = rawNSFRtotal.toFixed(2)
    let rawasf = parseFloat(asfAmount)
    computedAmount['asf'] = rawasf.toFixed(2)

    let rawrsf = parseFloat(rsfAmount)
    computedAmount['rsf'] = rawrsf.toFixed(2)


    return computedAmount
}

function getOrigdata(datadate) {
    let response;
    $.ajax({
        type: "POST",
        url: "AllotmentConfig/getOrigTotal",
        data: {
            date: datadate
        },
        dataType: "json",
        async: false,
        success: function(breakdowndetails) {
            response = breakdowndetails.ResponseResult
        }
    })
    return response
}

function getSubStatus(datadate){
    let response;
    $.ajax({
        type: "POST",
        url: "AllotmentConfig/getExpAllotStat",
        data: {
            date: datadate
        },
        dataType: "json",
        async: false,
        success: function(respo) {
            //console.log(breakdowndetails);
            response = respo
        }
    })
    //console.log(response)
    return response
}

function setOrigdata(datadate, datatypes) {
    let response;
    data = getOrigdata(datadate)
    switch (datatypes) {
        case "total":
            response = SolutionTotal(data[0].NSFR, data[0].ASF, data[0].RSF)
            break;
        case "content":
            datame = getalldataOrig(data)
            $.ajax({
                type: "POST",
                url: "AllotmentConfig/addSimulation",
                data: {
                    data: datame,
                    date: datadate
                },
                dataType: "json",
                async: false,
                success: function(breakdowndetails) {
                    if (breakdowndetails.ResponseResult == "Insert Successful") {
                        swal("Success!", "Added Successfully", "success");
                        tblAllocate(datadate);
                    } else {
                        swal("Error!", breakdowndetails.ResponseResult, "error");
                    }

                }
            })

            break;

    }

    return response;
}


function complianceStat(simulateds, originals){

            comstatus = (simulateds > 100) ? 'Compliant' : 'Non-compliant'
            comstatusOrig = (originals > 100) ? 'Compliant' : 'Non-compliant'

            response = {}
            response['comstatus'] = comstatus
            response['comstatusOrig'] = comstatusOrig



            if (simulateds > 100) {
                $("#nsfr").css("color", "black")
            } else {
                $("#nsfr").css("color", "red")
            }
            if (originals > 100) {
                $("#Orignsfr").css("color", "black")
            } else {
                $("#Orignsfr").css("color", "red")
            }

            return response
}


function populaterowdetails(d) {
    editeddetailsjson = []
    
    $.ajax({
        type: "POST",
        url: "AllotmentConfig/getSpecific",
        data: {
            refId: d.refference
        },
        dataType: "json",
        async: false,
        success: function(breakdowndetails) {
            datanew = breakdowndetails.ResponseResult

            summaryData = datanew[0];
            dropdownStatus = (d.Status == 'REJECTED') ? "disabled" : ""
            reformatdetails(datanew);

            titles = getSet(datanew, "title");
            let simulateStat = (modulesValidate($('#statModule').val()) == '1') ? '<select class="form-control" id = "simulationStat" name="simulationStat" data-default="'+d.Status+'" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" style="width:100%;" ' + dropdownStatus + '><option value="PENDING" data-refid="' + d.refference + '"  data-funddate="' + d.date + '">Pending</option><option value="APPROVED" data-refid="' + d.refference + '" data-funddate="' + d.date + '">Approved</option><option value="REJECTED" data-refid="' + d.refference + '" data-funddate="' + d.date + '">Rejected</option></select>' : ''
            let updateStat = ((modulesValidate($('#UpdateSimulationModule').val()) == '1') && (d.SimulationStat != "Original")) ? '<button type="button" id="btnsavechanges-' + d.refference + '" class="savebtnBudget btn btn-round btn-wd btn-primary" data-refid="' + d.refference + '" data-mystat="' + d.SimulationStat + '" data-funddate="' + d.date + '" data-nsFraStatus="' + d.nsfraStatus + '" style="margin-bottom: 10px;">Save Changes</button>' : ''
            //let simulateStat =  (modulesValidate($('#addreason').val()) == '1' && rawNSFRtotal)? '<select class="form-control" id = "simulationStat" name="simulationStat" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" style="width:100%;" '+dropdownStatus+'><option value="PENDING" data-refid="'+d.refference+'"  data-funddate="'+d.date+'">Pending</option><option value="APPROVED" data-refid="'+d.refference+'" data-funddate="'+d.date+'">Approved</option><option value="REJECTED" data-refid="'+d.refference+'" data-funddate="'+d.date+'">Rejected</option></select>' : ''
            collapsebody = '';
            collapseDataCategASF = ''
            collapseDataCategRSF = ''
            output = '';
            panelheadingASFCateg = '';
            panelheadingRSFCateg = '';
            //-===== new -=====
            
            panelheadingASFCateg = '' +
                '<div class="panel-heading">' +
                '<h4 class="panel-title">' +
                '<a data-target="#asfData" data-toggle="collapse" class="collapsed" aria-expanded="false">' +
                '<b>Available Stable Funding</b>' +
                '<b class="caret"></b>' +
                '</a>' +
                '</h4>' +
                '</div>' +
                '<hr>' +
                '';
            panelheadingRSFCateg = '' +
                '<div class="panel-heading">' +
                '<h4 class="panel-title">' +
                '<a data-target="#rsfData" data-toggle="collapse" class="collapsed" aria-expanded="false">' +
                '<b>Required Stable Funding</b>' +
                '<b class="caret"></b>' +
                '</a>' +
                '</h4>' +
                '</div>' +
                '<hr>' +
                '';


            for (var i = 0; i < titles.length; i++) {
                var groupsettitle = getalldatabytitle(datanew, titles[i]);
                titlewithoutspace = titles[i].replace(/\s/g, '');
                titlewithoutspace = titlewithoutspace.replace(/\(|\)/g, '');
                tablebodyASF = '';
                tablebodyRSF = '';
                panelheadingASF = '';
                panelheadingRSF = '';
                panelbodyASF = '';
                panelbodyRSF = '';
                scriptbodyASF = '';
                scriptbodyRSF = '';
                buildit = '';



                //-==== new ====-
                builditcollapse = '';
                let basekey;
                for (var z = 0; z < groupsettitle.length; z++) {
                    basekey = groupsettitle[z].code;
                    var res = basekey.split(".");

                 if (groupsettitle[z].fund_type == 1) {
                        tablebodyASF = tablebodyASF +
                            '<tr>' +
                            '<td class="editko" style="width:5%;"><input type="textbox" name="simulatedtxt[]"  data-baseid="' + d.refference + '" data-fundid="' + groupsettitle[z].fundid + '" data-baseorigamount="' + groupsettitle[z].amount + '" data-factor="' + groupsettitle[z].factor + '" data-basetitle="' + i + '" id="' + groupsettitle[z].fundid + '" class="simulateInput" placeholder="A + B = C" style=" text-align:right; width:100%;">' +
                            '<input type="hidden" name="expendid[]" id="expendid" value="' + groupsettitle[z].fundid + '">' +
                            '<input type="hidden" name="expendCode[]" id="expendCode" value="' + groupsettitle[z].code + '">' +
                            '<input type="hidden" name="nsfrStatus[]" id="nsfrStatus" value="ACTIVE">' +
                            '</td>' +
                            '<td>' +
                            groupsettitle[z].amount +
                            '</td>' +
                            '<td>' +
                            '<input type="text" class="valueInputed" id="adjustedAmount' + groupsettitle[z].fundid + '" data-fundid ="' + groupsettitle[z].fundid + '" name="totalreponse[]" value="0" readonly>' +
                            '</td>' +
                            '<td>' +
                            groupsettitle[z].code +
                            '</td>' +
                            '<td>' +
                            groupsettitle[z].name +
                            '</td>' +
                            '<td>' +
                            groupsettitle[z].status +
                            '</td>' +
                            '</tr>' +
                            ''
                    } else {
                        tablebodyRSF = tablebodyRSF +
                            '<tr>' +
                            '<td class="editko" style="width:5%;"><input type="textbox" name="simulatedtxt[]"  data-baseid="' + d.refference + '" data-fundid="' + groupsettitle[z].fundid + '" data-baseorigamount="' + groupsettitle[z].amount + '" data-factor="' + groupsettitle[z].factor+  '" data-basetitle="' + i + '" id="' + groupsettitle[z].fundid + '" class="simulateInput" placeholder="A + B = C" style=" text-align:right; width:100%;">' +
                            '<input type="hidden" name="expendid[]" id="expendid" value="' + groupsettitle[z].fundid + '">' +
                            '<input type="hidden" name="expendCode[]" id="expendCode" value="' + groupsettitle[z].code + '">' +
                            '<input type="hidden" name="nsfrStatus[]" id="nsfrStatus" value="ACTIVE">' +
                            '</td>' +
                            '<td>' +
                            groupsettitle[z].amount +
                            '</td>' +
                            '<td>' +
                            '<input type="text" class="valueInputed" id="adjustedAmount' + groupsettitle[z].fundid + '" data-fundid ="' + groupsettitle[z].fundid + '" name="totalreponse[]" value="0" readonly>' +
                            '</td>' +
                            '<td>' +
                            groupsettitle[z].code +
                            '</td>' +
                            '<td style="width:60%;">' +
                            groupsettitle[z].name +
                            '</td>' +
                            '<td>' +
                            groupsettitle[z].status +
                            '</td>' +
                            '</tr>' +
                            ''
                    }
                }

                if (i <= 4) {
                    panelheadingASF = '' +
                        '<div class="panel-heading">' +
                        '<h4 class="panel-title">' +
                        '<a data-target=".' + titlewithoutspace + '-' + d.refference + '" data-toggle="collapse" class="collapsed" aria-expanded="false">' +
                        titles[i] +
                        '<b class="caret"></b>' +
                        '</a>' +
                        '</h4>' +
                        '</div>' +
                        '' +
                        '';

                    panelbodyASF = '' +
                        '<div id="' + titlewithoutspace + '-' + d.refference + '" class="' + titlewithoutspace + '-' + d.refference + ' panel-collapse collapse" aria-expanded="false" style="height: 0px;">' +
                        '<div class="panel-body">' +
                        '<div class="table-responsive">' +
                        '<table id="tbl' + titlewithoutspace + '-' + d.refference + '" class="tableclass-' + d.refference + ' table table-bordered table-striped dataTable js-exportable" style=" width:100%;">' +
                        '<thead>' +
                        '<tr>' +
                        '<th style="width:10%">Adjustment Amount (a)</th>' +
                        '<th style="width:10%;">Original Amount (b)</th>' +
                        '<th style="width:10%;">Adjusted Amount (c)</th>' +
                        '<th>Code</th>' +
                        '<th style="width:70%">Item Name</th>' +
                        '<th>Status</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        tablebodyASF +
                        '</tbody>' +
                        '</table>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '' +
                        ''

                    scriptbodyASF = '<script type="text/javascript">' +
                        '$(document).ready(function () {' +
                        '$("#tbl' + titlewithoutspace + '-' + d.refference + '").DataTable().destroy();' +
                        '$("#tbl' + titlewithoutspace + '-' + d.refference + '").DataTable({' +
                        '"sDom": \'t\',' +
                        '"paging": false,' +
                        '"columnDefs": [' +
                        '{' +
                        '"visible": false,' +
                        '"searchable": false' +
                        '}' +
                        ']' +
                        '})' +
                        '});' +
                        '</script>'
                    builditdataASF = '<div class="panel panel-default" style="display:;">' + panelheadingASF + panelbodyASF + '</div>';
                    collapseDataCategASF = collapseDataCategASF + builditdataASF + scriptbodyASF;
                } else {


                    panelheadingRSF = '' +
                        '<div class="panel-heading">' +
                        '<h4 class="panel-title">' +
                        '<a data-target=".' + titlewithoutspace + '-' + d.refference + '" data-toggle="collapse" class="collapsed" aria-expanded="false">' +
                        titles[i] +
                        '<b class="caret"></b>' +
                        '</a>' +
                        '</h4>' +
                        '</div>' +
                        '' +
                        '';

                    panelbodyRSF = '' +
                        '<div id="' + titlewithoutspace + '-' + d.refference + '" class="' + titlewithoutspace + '-' + d.refference + ' panel-collapse collapse" aria-expanded="false" style="height: 0px;">' +
                        '<div class="panel-body">' +
                        '<div class="table-responsive">' +
                        '<table id="tbl' + titlewithoutspace + '-' + d.refference + '" class="tableclass-' + d.refference + ' table table-bordered table-striped dataTable js-exportable" style=" width:100%;">' +
                        '<thead>' +
                        '<tr>' +
                        '<th style="width:10%">Adjustment Amount (a)</th>' +
                        '<th style="width:10%;">Original Amount (b)</th>' +
                        '<th style="width:10%;">Adjusted Amount (c)</th>' +
                        '<th>Code</th>' +
                        '<th style="width:70%">Item Name</th>' +
                        '<th>Status</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        tablebodyRSF +
                        '</tbody>' +
                        '</table>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '' +
                        ''

                    scriptbodyRSF = '<script type="text/javascript">' +
                        '$(document).ready(function () {' +
                        '$("#tbl' + titlewithoutspace + '-' + d.refference + '").DataTable().destroy();' +
                        '$("#tbl' + titlewithoutspace + '-' + d.refference + '").DataTable({' +
                        '"sDom": \'t\',' +
                        '"paging": false,' +
                        '"columnDefs": [' +
                        '{' +
                        '"visible": false,' +
                        '"searchable": false' +
                        '}' +
                        ']' +
                        '})' +
                        '});' +
                        '</script>'
                    builditdataRSF = '<div class="panel panel-default" style="display:;">' + panelheadingRSF + panelbodyRSF + '</div>';
                    collapseDataCategRSF = collapseDataCategRSF + builditdataRSF + scriptbodyRSF;
                }

                //alert("-------"+modulesValidate($('#statModule').val()));

                // solution = '<div class="panel panel-default">'+wew+'</div>=========================================================================================';

            }
            dataCateg =
                collapsebody = panelheadingASFCateg + '<div class="panel-collapse collapse" id="asfData" style="margin-left: 20px;margin-right: 20px;">' + collapseDataCategASF + '</div>' +
                panelheadingRSFCateg + '<div class="panel-collapse collapse" id="rsfData" style="margin-left: 20px;margin-right: 20px;">' + collapseDataCategRSF + '</div>';
            savebtn = '' +
                '<div class="row">' +
                '<div class="col-md-6">' +
                '<h5><label>' + d.refference + ' Data Details</label> </h5>' +
                '' +
                '</div>'

                +
                '<div class="col-md-1">' +
                '</div>' +
                '<div class="col-md-5">' +
                '<div class="pull-right">' +
                simulateStat +
                '</div>' +
                '<div class="pull-right">' +
                updateStat +
                '</div>' +
                '</div>' +
                '</div>'


            output = savebtn

                +
                '<div class="panel-group-' + d.refference + '" id="accordion-' + d.refference + '" style="margin-left: 20px;margin-right: 20px;">' +
                collapsebody +
                '</div>'


            $("#append-data").append(output);
            $('select option[value="' + d.Status + '"]').attr("selected", true);

            
            $("#simulationStat").val(d.Status)
        },
        error: function(result) {
            alert("error");
            retdata = "error";
        }


    });




    $(".simulateInput").keyup(function(e) {

        $("#mysimulation").collapse('show');
        $("#nsfr").removeClass("animated bounce");
        let my = $(this)
        let result = my.attr('class').split(" ");
        let origsimulateAmount = my.data('baseorigamount');
        let origfactor = my.data('factor');
        let ret = result[1];
        let a = (my.val() ==  "") ?  0 : my.val();
        let title = my.data('basetitle');
        let txtlength = $("input[name='simulatedtxt[]']").length;
        let dataall = getalldata(datanew);
        console.log(origfactor)
        var baseid = my.data('baseid');
        var fundid = my.data('fundid');
        var id = my.attr("id");


        adjustedAmount =  (parseFloat(origsimulateAmount) + parseFloat(a) < 0 || parseFloat(origsimulateAmount) + parseFloat(a) == parseFloat(origsimulateAmount))? "0" : (parseFloat(origsimulateAmount) + parseFloat(a)) * origfactor    
        $('#adjustedAmount'+fundid).val(adjustedAmount);



        changesStat = validateChanges(id, baseid, a);
        (changesStat > 0) ? $("#" + id).removeClass("simulateInput mycssvalid"): $("#" + id).addClass("simulateInput mycssvalid")
        ASFtotal = parseFloat(0)
        NSFRtotal = parseFloat(0)
        RSFtotal = parseFloat(0)
        for (var z = 0; z < dataall.length; z++) {
            if (dataall[z].fund_type == "2") {
                RSFtotal += parseFloat($('#adjustedAmount' + dataall[z].fundid).val())
            } else {
                ASFtotal += parseFloat($('#adjustedAmount' + dataall[z].fundid).val())
            }
        }

        $("#nsfr").addClass("animated flash");
        $("#asf").addClass("animated flash");
        $("#rsf").addClass("animated flash");
        rawNSFRtotal = (ASFtotal / RSFtotal) * 100
        NSFRtotal = (RSFtotal == 0) ? 0 : rawNSFRtotal.toFixed(2);

        comstatus = (NSFRtotal > 100) ? 'Compliant' : 'Non-compliant'
        if (NSFRtotal > 100) {
            $("#nsfr").css("color", "black")
        } else {
            $("#nsfr").css("color", "red")
        }
        $("#nsfr").text(NSFRtotal + "%  " + comstatus)
        $("#asf").text(toaddcomma(ASFtotal.toFixed(2)))
        $("#rsf").text(toaddcomma(RSFtotal.toFixed(2)))
    });



    $(".simulateInput").alphanum({
        allow: '1234567890-.', // Specify characters to allow
        disallow: 'qwertyuiopasdfghjklzxcvbnm' // Specify characters to disallow
    });
}


function myCallbackFunction(updatedCell, updatedRow, oldValue) {
    item["expendId"] = updatedRowdata[0];
    updatedRowdata = updatedRow.data();
    var datajson = [];

    if (updatedCell.data() != oldValue) {

        item = {};
        item["expendId"] = updatedRowdata[0];
        item["expendCode"] = updatedRowdata[1];
        item["amount"] = updatedRowdata[2];
        item["nsfrStatus"] = updatedRowdata[3];

        editeddetailsjson.push(item);
    }
}



function closeOpenedRows(table, selectedRow) {
    $.each(openRows, function(index, openRow) {
        // not the selected row!
        if ($.data(selectedRow) !== $.data(openRow)) {
            var rowToCollapse = table.row(openRow);
            rowToCollapse.child.hide();
            openRow.removeClass('shown');
            // replace icon to expand
            $(openRow).find('td.details-control').html('<div class="toberemove"><span class="glyphicon glyphicon-plus"></span></div>');
            // remove from list
            var index = $.inArray(selectedRow, openRows);
            openRows.splice(index, 1);
        }
    });
}





function appendcsvdata() {
    $("#errorpane").hide();
    $("#showdatapane").hide();
    form = $("#csvformcatcher");
    data = new FormData(form[0]);

    $.ajax({
        url: "AllotmentConfig/readCSVfile",
        data: data,
        type: 'POST',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function(data) {

            if (data.Code == 0) {
                result = data.CSVData;
                fieldresult = "";
                for (var i = 0; i < result.length; i++) {
                    fieldresult = fieldresult +
                        '<div class="row">' +
                        '<div class="col-md-6">' +
                        '<div class="form-group">' +
                        '<label>Code</label>' +
                        '<input type="text" id="codefieldid-' + i + '" class="form-control codefield" name="codefield[]" value="' + result[i][0] + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<div class="form-group">' +
                        '<label>Amount</label>' +
                        '<input type="text" id="amountfieldid-' + i + '" class="form-control amountfield" name="amountfield[]"  value="' + result[i][1] + '">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '' +
                        '';
                }
                $("#csvdatafield").empty();
                $("#csvdatafield").append(fieldresult);
                $("#showdatapane").show();
                $("#btnsubmitaddcsv").attr('disabled', false);
            } else {
                $("#errorpane").show();
                $("#btnsubmitaddcsv").attr('disabled', true);
            }
        },
        error: function(result) {
            alert("error");
        }
    });
}

//end == jomer


function tblAllocate(date) {
     $('#tblAllocate').DataTable().clear();
        $('#tblAllocate').DataTable().destroy();
    $("#loader").show();
    $.post(window.location + '/getExpAllot/', {
        'date': date
    }).done(function(res) {
        let data = JSON.parse(res);
       
        status = "false"
        $("#loader").hide();
        $('#tblAllocate').DataTable({
            data: data.allocate,
            //dataOrig : data.original,
            "columnDefs": [{
                "visible": false,
                "searchable": false
            }],
            columns: [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                    data: 'refference'
                },
                {
                    data: 'date'
                },
                {
                    data: 'SimulationStat'
                },
                {
                    data: 'btn'
                }

            ],
            "aaSorting": [[ 1, "asc" ]],
        });

    });
}




function addAproval(data, mssg) {
    $.confirm({
        title: '<label class="text-warning">Confirm</label>',
        content: 'Are you sure you want to '+mssg+' this?',
        type: 'orange',
        buttons: {
            confirm: function() {
                $.ajax({
                    type: "POST",
                    url: "AllotmentConfig/addApproval",
                    data: data,
                    dataType: "json",
                    success: function(result) {
                        if(result.ResponseResult == "Insert Successful"){
                            $.alert(mssg)
                            tblAllocate(data.fund_date);
                            $('#displayModal').modal('hide')
                        }else{
                             $.alert("error")
                        }
                    },
                    error: function(result) {
                        alert("error");
                        $("#simulationStat").val(data.default)
                    }


                });

            },
            cancel: function() {
                $("#simulationStat").val(data.default)
                //$.alert('Canceled!');
            }

        }
    });

}




$(document).ready(function() {



    // $(document).on("change", "#simulationStat", function () {
    //    let my = $(this);
    //    var selected = my.find('option:selected');
    //    let refid = selected.data('refid');
    //    let rawdate = selected.data('funddate');
    //    let date = rawdate.split(" ")
    //    let status = my.val();
    //    $.ajax({
    //             type: "POST",
    //             url: "AllotmentConfig/ChangeStatus",
    //             data: {
    //               Refid: refid,
    //               Status: status
    //             },
    //        dataType: "json",
    //        async:false,
    //        success: function (result) {
    //            mssg = JSON.parse(result)
    //            swal("Success!", mssg.ResponseResult);
    //            tblAllocate(date[0]);
    //        },
    //        error: function (result) {
    //           alert("error");
    //           retdata = "error";
    //        }
    //    });

    // })




    $(document).on("change", "#simulationStat", function() {
        let my = $(this)
        let selected = my.find('option:selected');
        let refid = selected.data('refid');
        let mydata = totaldetails(refid);
        let datas = mydata[0];
        let selectedStatus = $('#simulationStat').val()

        rawNSFRtotal = (mydata[0].NSFR * 100).toFixed(2);

        if (rawNSFRtotal < 100 && selectedStatus == "APPROVED") {
            $("#displayModal").modal('show');
            datas["status"] = "Approved";
            $.ajax({
                type: "POST",
                url: "AllotmentConfig/submitApproval",
                data: datas,
                dataType: "json",
                success: function(result) {
                    $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                    $('#displayModal .modal-title').html('<i class="text-info">Add Data</i>');
                    $('#displayModal .modal-body').html(result.form);


                    $('#displayModal').modal('show');
                },
                error: function(result) {
                    alert("error");
                }
            });
        } else if (rawNSFRtotal > 100 && selectedStatus == "APPROVED") {
            datas["reason"] = "";
            datas["action"] = "";
            datas["duration"] = "";
            datas["default"] = my.data('default');
            datas["status"] = "APPROVED";
            addAproval(datas, 'APPROVED');
        } else {

            datas["reason"] = "";
            datas["action"] = "";
            datas["duration"] = "";
             datas["default"] = my.data('default');
            datas["status"] = selectedStatus;
            addAproval(datas, selectedStatus);

            //console.log(selectedStatus)
        }

    });




    $(document).on("submit", "#reasonForm", function(e) {
        e.preventDefault();
        let my = $(this);
        let refid = my.data('id');
        let mydata = totaldetails(refid);
        let datas = mydata[0];
        datas["reason"] = $('#reason').val();
        datas["action"] = $('#action').val();
        datas["duration"] = $('#duration').val();
        datas["remarks"] = $('#remarks').val();
        datas["status"] = "Approved";


        addAproval(datas, datas["status"]);

        //var formData = new FormData($("#attachFilter")[0]);


    })




    // $(document).on("click", "#btnimportcsv", function() {
    //     $("#errorpane").hide();
    //     $("#showdatapane").hide();
    //     $.ajax({
    //         type: "POST",
    //         url: "AllotmentConfig/addImportCSVModal",
    //         dataType: "json",
    //         success: function(result) {
    //             $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
    //             $('#displayModal .modal-title').html('<i class="text-info">Add Data by CSV</i>');
    //             $('#displayModal .modal-body').html(result.form);
    //             dateGen();
    //             Dropzone.autoDiscover = false;
    //             var myDropzone = new Dropzone("#my-dropzone", {
    //                 url: "AllotmentConfig/upload",
    //                 autoProcessQueue: false,
    //                 params: {
    //                     date: $("#dateCreatedCSV").val()
    //                 },
    //                 acceptedFiles: ".csv",
    //                 addRemoveLinks: true,
    //                 removedfile: function(file) {
    //                     var name = file.name;
    //                     var previewElement;
    //                     return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
    //                 },
    //                 init: function() {
    //                     var me = this;
    //                     var submitButton = document.querySelector("#importCSV")
    //                     myDropzone = this;
    //                     submitButton.addEventListener("click", function() {
    //                         myDropzone.processQueue();
    //                     });
    //                 },
    //                 success: function(file, response) {
    //                     // myDropzone.removeAllFiles();
    //                     mssg = JSON.parse(response)
    //                     if (mssg.ResponseResult == "Insert Successful") {
    //                         swal("Success!", "Added Successfully", "success");
    //                         $(".modal").modal("hide");
    //                         $('#displayModal').modal('hide')
    //                         tblAllocate($("#dateCreatedCSV").val());
    //                     } else {

    //                     }

    //                 }
    //             });


    //             $('#displayModal').modal('show');


    //         },
    //         error: function(result) {
    //             alert("error");
    //         }
    //     });
    // });

    $(document).on("click", "#btnimportcsv", function () {
        $("#errorpane").hide();
        $("#showdatapane").hide();
        $.ajax({
            type: "POST",
            url: "AllotmentConfig/addImportCSVModal",
            dataType: "json",
            success: function (result) {
                $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                $('#displayModal .modal-title').html('<i class="text-info">Add Data by CSV</i>');
                $('#displayModal .modal-body').html(result.form);
                dateGen();

                $('#displayModal').modal('show');  


            },
            error: function (result) {
               alert("error");
            }
        }); 
    });



    //     $(document).on('click', '#clear-dropzone', function (e) {
    //     e.preventDefault();
    //     myDropzone.removeAllFiles();
    // })
    $(document).on('click', '.browse', function() {
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });

    $(document).on('change', '.file', function() {
        var names = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            names.push($(this).get(0).files[i].name);
        }
        $(this).parent().find('.form-control').val(names);
        if (names === "") {

        } else {
            appendcsvdata();
        }

    });

    $(document).on("click", "#btnsubmitaddcsv", function() {
        //alert( $("#dateCreatedCSV").val() );
        form = $("#csvdataform");
        data = new FormData(form[0]);
        data.append("funddate", $("#dateCreatedCSV").val());

        $.ajax({
            url: "AllotmentConfig/submitCSVData",
            data: data,
            type: 'POST',
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.ResponseResult == "Insert Successful") {
                    swal("Success!", "Added Successfully", "success");
                    $(".modal").modal("hide");
                    $('#displayModal').modal('hide')
                    tblAllocate($("#dateCreatedCSV").val());
                    $("#FindDate").val($("#dateCreatedCSV").val());
                } else {

                }
            },
            error: function(result) {
                alert("error");
            }
        });
    });




    $(document).on("click", ".remove", function() {
        let me = $(this);
        let urlkey = me.data("direct")
        let id = me.data("id")
        let date = me.data("date")
        let functName = me.data("functionname")
        //alert(urlkey+' --- '+id+' ---- '+date+' ---- '+functName);

        $.confirm({
            title: '<label class="text-warning">Confirm</label>',
            content: 'Are you sure you want to remove it from the list?',
            type: 'orange',
            buttons: {
                yes: {
                    btnClass: 'btn-blue',
                    action: function() {
                        var resultCode;
                        fields.deletesimulation(id, functName, urlkey, date)

                    }

                },
                cancel: function() {}
            }
        });
    });


    $('#AllotmentMssgModal').hide();
    $(document).on("click", "#btnsubmitadd", function() {
        //alert($('#AllotmentDate').val());
        let date = $('#AllotmentDate').val()
        $empty = $('#addrecordform').find("input").filter(function() {
            return this.value === "";
        });
        let notempty = $("input[name='amount[]']").length;
        if ($empty.length == notempty) {
            $.alert('Must Filled atleast one Value');
        } else {
            $.confirm({
                title: 'Confirm!',
                content: 'Are You Sure?',
                buttons: {
                    confirm: function() {
                        var formData = new FormData($("#addrecordform")[0]);
                        var url = "addAllotment";
                        fields.adding(formData, url, date)
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    }

                }
            });
        }
    });


    $(document).on("click", "#addSimulate", function() {
        let date = $('#FindDate').val()
        allowedSub = getSubStatus(date);
        if(allowedSub == 0){
        $.confirm({
            title: '<label class="text-warning">Confirm</label>',
            content: 'Are you sure you want to add another simulation item?',
            type: 'orange',
            buttons: {
                confirm: function() {
                    setOrigdata(date, 'content');

                    // $.alert('Confirmed!');
                },
                cancel: function() {
                   // $.alert('Canceled!');
                }

            }
        });

    }else{
            $.alert('No Original Data Found!');
    }

    })



    $(document).on("click", ".savebtnBudget", function() {

        let refid = $(this).data("refid");
        let funddate = $(this).data("funddate");
        let nsFraStatus = $(this).data("nsfrastatus");
        let changesStatus = $(this).data("mystat");
        let updateby = $("#currentuseridbudget").val();




        form = $('.nsfrForm').serializeArray();
        form.push({
                name: 'refid',
                value: refid

            }, {
                name: 'funddate',
                value: funddate
            }, {
                name: 'changesStatus',
                value: changesStatus
            }, {
                name: 'updateBy',
                value: updateby
            }, {
                name: 'nsfraStatus',
                value: nsFraStatus
            }, {
                name: 'nsfraStatus',
                value: nsFraStatus
            }

        );
        let dateinit = funddate.split(" ");
        let submitStat = submitvalidate($('.nsfrForm').serializeArray(), refid);
        if (submitStat == 0) {
            $.alert("You Don't have any Changes");
        } else {
            $.confirm({
                title: '<label class="text-warning">Confirm</label>',
                content: 'Are you sure you want to save the changes?',
                type: 'orange',
                buttons: {
                    yes: {
                        btnClass: 'btn-blue',
                        action: function() {
                            var resultCode;
                            $.confirm({
                                content: function() {
                                    var self = this;
                                    return $.ajax({
                                        url: "AllotmentConfig/submitchangesdata",
                                        data: form,
                                        type: 'POST',
                                        dataType: 'json',
                                        async: 'false'
                                    }).done(function(result) {
                                        resultCode = result.ResponseResult;
                                        if (resultCode == 'Update Successful') {
                                            tblAllocate(dateinit[0])
                                            $('#mysimulation').collapse('hide');
                                            // alert(funddate);
                                            self.setTitle('<label class="text-success">Success</label>');
                                        } else {
                                            self.setTitle('<label class="text-danger">Failed</label>');
                                        }
                                        self.setContent(resultCode);
                                    }).fail(function() {
                                        self.setTitle('<label class="text-danger">Failed</label>');
                                        self.setContent('Url Loading Failed');
                                    });
                                },
                                buttons: {
                                    ok: {
                                        action: function() {
                                            /*location.reload();*/
                                        }
                                    }
                                }
                            });
                        }

                    },
                    cancel: function() {}
                }
            });
        }
    });



    $('.modal').on('hidden.bs.modal', function(e) {
        if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    });

    function dateGen() {
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    }
    dateGen();

    function StartPage() {
        let date = $("#FindDate").val();
        $('#exportExcelButton').attr("data-holder", date);
        tblAllocate(date);
    }
    StartPage();

    $("#btnDateSearch").click(function() {
        tblAllocate($("#FindDate").val());
        $('#exportExcelButton').attr("data-holder", $("#FindDate").val());
    });
    //===================================================================================================================================



    $('#tblAllocate').on('click', 'td.details-control', function() {
        $("#nsfr").removeClass("animated flash");
        $("#asf").removeClass("animated flash");
        $("#rsf").removeClass("animated flash");

        table = $('#tblAllocate').DataTable();


        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            $('#mysimulation').collapse('hide');
            row.child.hide();
            tr.removeClass('shown');
        } else {
            closeOpenedRows(table, tr);
            $(this).html('<div class="toberemove"><span class="glyphicon glyphicon-minus"></span></div>');
            // Open this row
            origtotal = setOrigdata(row.data().date, 'total');
            row.child(formatopen(row.data())).show();
            populaterowdetails(row.data());

            let totalsummary = totaldetails(row.data().refference);

            let GetTotalSolution = SolutionTotal(totalsummary[0].NSFR, totalsummary[0].ASF, totalsummary[0].RSF)
            
            complianceStat(GetTotalSolution.nsfr, origtotal.nsfr)

             labelstatus = complianceStat(GetTotalSolution.nsfr, origtotal.nsfr)

            tr.addClass('shown');
            $('#mysimulation').collapse('show');
            $("#nsfr").text(GetTotalSolution.nsfr + "%  " + labelstatus.comstatus);
            $("#Orignsfr").text(origtotal.nsfr + "%  " + labelstatus.comstatusOrig);
            $("#asf").text(toaddcomma(GetTotalSolution.asf));
            $("#Origasf").text(toaddcomma(origtotal.asf));
            $("#rsf").text(toaddcomma(GetTotalSolution.rsf));
            $("#Origrsf").text(toaddcomma(origtotal.rsf));

            openRows.push(tr);



            $(".toberemove").empty();
        }

        function addCommasInternal(string) {
            let result = string.split(".");
            let ret = result[0] + "." + result[1].substr(0, 2);
            return ret;

        }
    });



    //=====================================================

    function UpdateForm(refid, exptype) {
        $("#formGenUpdate").empty();
        // var refid = $(this).attr('value');
        // var exptype = $(this).data("exptype")
        $("#formloaderUpdate").show();
        $("#more_informationUpdate").hide();
        $.post(window.location + '/getSpecific', {
            'refId': refid
        }).done(function(result) {
            $("#exptype").text(exptype);
            $("#refid").val(refid);
            //alert(window.location + '/getSpecific')

            res = JSON.parse(result);
            rets = res.ResponseResult;
            child = res.ResponseResult;

            $.each(child, function(i, v) {
                if (i == 0) {
                    // alert('wew');

                } else {
                    // alert('now');
                    $("#formGenUpdate").append(
                        '<div class="col-md-3">' +
                        '<br>' +
                        '<div class="form-group form-float">' +
                        '<div class="focused form-line" focused>' +
                        '<input type="text" class="number form-control" id="allotmentupdateTxt" name="allotmentupdateTxt[]" value="' + child[i].amount + '">' +
                        '<input type="hidden" class="number form-control" id="expendcode" name="expendcode[]" value="' + child[i].code + '">' +
                        '<input type="hidden" class="number form-control" id="fundid" name="fundid[]" value="' + child[i].fundid + '">' +
                        '<label class="form-label" style="font-size: 1.3rem">' + child[i].code + '</label>' +
                        '</div>' +
                        '</div>' +
                        '</div>'

                    );


                }


                $("#formloaderUpdate").hide();
                $("#more_informationUpdate").show();
            });

        });
    }




    $(document).on("click", "#adding", function() {
        $("#displayModal").modal('show');

        $.ajax({
            type: "POST",
            url: "AllotmentConfig/addModal",
            dataType: "json",
            success: function(result) {
                $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                $('#displayModal .modal-title').html('<i class="text-info">Add Data</i>');
                $('#displayModal .modal-body').html(result.form);
                getoptionSAOB();
                dateGen();
                bootAdd();
                $('#displayModal').modal('show');


            },
            error: function(result) {
                alert("error");
            }
        });
    });


    $(document).on('click', '.btnnavigation', function() {});
    $(document).on("click", ".editbtn", function() {
        let refid = $(this).data("refid")
        let type = $(this).data("type")
        $.ajax({
            type: "POST",
            url: "AllotmentConfig/UpdateModal",
            dataType: "json",
            success: function(result) {
                $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                $('#displayModal .modal-title').html('<i class="text-info">Update Data</i>');
                $('#displayModal .modal-body').html(result.form);
                UpdateForm(refid, type);
                $('#displayModal').modal('show');
            },
            error: function(result) {
                alert("error");
            }
        });
    });


    $(document).on("click", "#btnclear", function() {})

    //=====================my validation======================================

    function getoptionSAOB() {
        $.get(window.location + '/getOptionAdd').done(function(ret) {
            let result = JSON.parse(ret);
            let response = result.ResponseResult;
            $.each(response, function(x, y) {
                expendName = response[x].title;
                expendcode = response[x].id;
                $('#expendtypeAddOption').append(
                    '<option value = "' + expendcode + '"' + status + '>' + expendName + '</option>'
                );
            });
        });
    }


    //===================================================================================================================================
    function bootAdd() {
        $("#AllotmentMssgModal").hide();
        $("#formloader").hide();
        $(document).on("click", "#generate_form", function() {
            expendtype = $('#expendtypeAddOption').val();
            if (expendtype == '--') {
                $("#btnsubmitadd").attr("disabled", true);
                $("#btnclear").attr("disabled", true);
                $("#formGen").empty();
                $("#formloader").hide();
                $("#AllotmentMssgModal").show();
            } else {
                $("#btnsubmitadd").removeAttr("disabled");
                $("#btnclear").removeAttr("disabled");
                $("#formGen").empty();
                $("#formloader").show();
                $('#AllotmentMssgModal').hide();
                $.post(window.location + '/getExpend/', {
                    'id': expendtype
                }).done(function(res) {
                    let result = JSON.parse(res);
                    $("#formloader").hide();
                    let response = result.ResponseResult;
                    $.each(response, function(i, v) {
                        let label = response[i].name;
                        let code = response[i].code;
                        $("#formGen").append(
                            '<div class="col-md-3">' +
                            '<br>' +
                            '<div class="form-group form-float">' +
                            '<div class="focused form-line" focused>' +
                            '<input type="text" class="number form-control" id="allotmentTxt" name="allotmentTxt[]" placeholder ="' + label + '">' +
                            '<input type="hidden" class="number form-control" id="expendCode" name="expendCode[]" value="' + code + '" >' +
                            '</div>' +
                            '</div>' +
                            '</div>'

                        );
                        numberOnly();

                    });
                });
            }

        });
    }

    //======================================================================================================================================================

    $(document).on("click", "#addForm", function() {
        resetModal();


    });
    //===================================================================================================================================

    $(document).on("click", "#btnclear", function() {
        resetModal();


    });
    //===================================================================================================================================




    //$('#AllotmentMssgModal').hide();
    $(document).on("click", "#btnsubmitUpdate", function() {
        $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function() {
                    var formData = new FormData($("#updaterecordform")[0]);
                    var url = "updateAllotment";
                    fields.Updating(formData, url)

                    // $.alert('Confirmed!');
                },
                cancel: function() {
                    $.alert('Canceled!');
                }

            }
        });
    });

    // //=============================================================================
    function deleteConfirm(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, I changed my mind.",
            closeOnConfirm: false,
            closeOnCancel: false,
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajaxSetup({
                    async: false
                });
                $.post(window.location + '/removeBudget', {
                    'refId': id
                });

                swal("Deleted!", "record has been deleted.", "success");
                //swal("Success!", "added successfully", "success");
                var dateval = $("#modalDate").val()
                var arr = dateval.split("-");
                var monthval = arr[1];
                var yearval = arr[0];
                let dateRaw = new Date(yearval, monthval, 1);
                var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(), dateRaw.getDate());
                let dd = dateneed.getDate();
                let mm = dateneed.getMonth();
                let yyyy = dateneed.getFullYear();
                let sendPostData = new Object();
                if (dd < 10) dd = '0' + dd
                if (mm < 10) mm = '0' + mm
                defDate = yyyy + '-' + mm + '-' + dd;
                //alert(defDate)
                monthyFunction(defDate);
            } else {
                swal("Cancelled", "Record has not been deleted.", "error");
            }

        });
    }

    $(document).on("click", ".deleteReq", function() {
        var refId = $(this).attr("value");
        deleteConfirm(refId);
        // alert(refId);
    });

    //========================================================================

    //$(document).on("click",".updateReq",function(){


    function numberOnly() {
        $(".number").on("keypress keyup blur", function(event) {

            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        //}
    }


    //======================================================================

    $("#property_number").alphanum({
        allow: '1234567890-qwertyuiopasdfghjklzxcvbnm', // Specify characters to allow
        disallow: '' // Specify characters to disallow
    });


    $("#btnupdate").hide();
    $("#disabler").click(function() {
        $(".handler").toggleClass("nopointerevent");
        $("#disabler").fadeOut("fast");
        $("#btnupdate").fadeIn("fast");
    });

    //========================= my date Generator  ============================= 

    //=======================================================================================================================================
    function resetModal() {
        $("#modify_date_container").collapse("hide");
        $("#formGen").empty();
        $("#btnsubmitadd").attr("disabled", true);
        $("#btnclear").attr("disabled", true);
        $('#searchMssg').hide();
        $('#searchMssgModal').hide();
        $('#AllotmentMssgModal').hide();
        //$('#budget_classificationModalhandle option[value="--"]').attr('selected','selected');
        $('select[name="budget_listModal"]').val('--');
        //})
    }


    //===========================================================================================================================================

    function addCommas(string) {
        return string.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        })
    }
});


function toaddcomma(number) {
    var data = number;
    data = data.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return data;

}