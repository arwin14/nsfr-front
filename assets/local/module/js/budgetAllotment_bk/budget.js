//Added by Jomer 1 of 2

function StartPage(){
        alert('gg');
        let date = $("#FindDate").val();
        $('#exportExcelButton').attr("data-holder",date);
        tblAllocate(date);
    }
    StartPage();


function appendcsvdata(){
    $("#errorpane").hide();
    $("#showdatapane").hide();
    form = $("#csvformcatcher");
    data = new FormData(form[0]);

    $.ajax({
        url: "UserConfig/readCSVfile",
        data: data,
        type:'POST',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (data) {
            /*console.log(data);*/

            if (data.Code == 0) {
                result = data.CSVData;
                fieldresult = "";
                for (var i = 0; i < result.length; i++) {
                    fieldresult = fieldresult
                        + '<div class="row">'
                        + '<div class="col-md-6">'
                        + '<div class="form-group">'
                        + '<label>Code</label>'
                        + '<input type="text" id="codefieldid-'+i+'" class="form-control codefield" name="codefield[]" value="'+result[i][0]+'">'
                        + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                        + '<div class="form-group">'
                        + '<label>Amount</label>'
                        + '<input type="text" id="amountfieldid-'+i+'" class="form-control amountfield" name="amountfield[]"  value="'+result[i][1]+'">'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + ''
                        + '';
                }


                $("#csvdatafield").empty();
                $("#csvdatafield").append(fieldresult);
                $("#showdatapane").show();

                

                $("#btnsubmitaddcsv").attr('disabled', false);
            } else {
                $("#errorpane").show();
                $("#btnsubmitaddcsv").attr('disabled', true);
            }

            

        },
        error: function (result) {
           alert("error");
        }
    }); 


}
var table;


    function tblAllocate(date) {
        $.post(window.location + '/getExpAllot/', {
            'date': date
        }).done(function(res) {
            let data = JSON.parse(res);
            console.log(data);
            $('#tblAllocate').DataTable().clear();
            $('#tblAllocate').DataTable().destroy();

              var table = $('#tblAllocate').DataTable({
                    data: data,
                    columns: [
                        {
                                "className": 'details-control',
                                "orderable": false,
                                "data": null,
                                "defaultContent": '',
                                "render": function () {
                                return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                                },
                                width:"15px",
                               
                        },
                        {
                            data: 'refference'
                        },
                        {
                            data: 'Eexpendtype'
                        },
                        {
                            data: 'date',
                        },
                        {
                            data: 'btn'
                        }

                    ]
                });
        });
    }




$(document).ready(function () {
    //Added by Jomer 2 of 2

    $(document).on("click", "#generate_form", function () {
            expendtype = $('#expendtypeAddOption').val();
            if (expendtype == '--'){
                //alert(expendtype);
                $("#btnsubmitadd").attr("disabled", true);
                $("#btnclear").attr("disabled", true);
                $("#formGen").empty();
                $("#formloader").hide();
                $("#AllotmentMssgModal").show();
            } else {
                $("#btnsubmitadd").removeAttr("disabled");
                $("#btnclear").removeAttr("disabled");
                $("#formGen").empty();
                $("#formloader").show();
                $('#AllotmentMssgModal').hide();
                $.post(window.location + '/getExpend/',{'id':expendtype}).done(function (res) {
                    let result = JSON.parse(res);
                    $("#formloader").hide();
                    let response = result.ResponseResult;
                    $.each(response, function (i, v) {
                        let label = response[i].name;
                        let code = response[i].code;
                        $("#formGen").append(
                            '<div class="col-md-3">' +
                            '<br>' +
                            '<div class="form-group form-float">' +
                            '<div class="focused form-line" focused>' +
                            '<input type="text" class="number form-control" id="allotmentTxt" name="allotmentTxt[]" placeholder ="' + label + '">' +
                            '<input type="hidden" class="number form-control" id="expendCode" name="expendCode[]" value="' + code + '" >' +
                            '</div>' +
                            '</div>' +
                            '</div>'

                        );
                        numberOnly();

                    });
                });
            }

        });

    // function bootAdd(){
    //     $("#AllotmentMssgModal").hide();
    //     $("#formloader").hide();
    //     $(document).on("click", "#generate_form", function () {
    //         expendtype = $('#expendtypeAddOption').val();
    //         if (expendtype == '--'){
    //             //alert(expendtype);
    //             $("#btnsubmitadd").attr("disabled", true);
    //             $("#btnclear").attr("disabled", true);
    //             $("#formGen").empty();
    //             $("#formloader").hide();
    //             $("#AllotmentMssgModal").show();
    //         } else {
    //             $("#btnsubmitadd").removeAttr("disabled");
    //             $("#btnclear").removeAttr("disabled");
    //             $("#formGen").empty();
    //             $("#formloader").show();
    //             $('#AllotmentMssgModal').hide();
    //             $.post(window.location + '/getExpend/',{'id':expendtype}).done(function (res) {
    //                 let result = JSON.parse(res);
    //                 $("#formloader").hide();
    //                 let response = result.ResponseResult;
    //                 $.each(response, function (i, v) {
    //                     let label = response[i].name;
    //                     let code = response[i].code;
    //                     $("#formGen").append(
    //                         '<div class="col-md-3">' +
    //                         '<br>' +
    //                         '<div class="form-group form-float">' +
    //                         '<div class="focused form-line" focused>' +
    //                         '<input type="text" class="number form-control" id="allotmentTxt" name="allotmentTxt[]" placeholder ="' + label + '">' +
    //                         '<input type="hidden" class="number form-control" id="expendCode" name="expendCode[]" value="' + code + '" >' +
    //                         '</div>' +
    //                         '</div>' +
    //                         '</div>'

    //                     );
    //                     numberOnly();

    //                 });
    //             });
    //         }

    //     });
    // }




    
    $(document).on("click", "#btnimportcsv", function () {
        $("#errorpane").hide();
        $("#showdatapane").hide();
        $.ajax({
            type: "POST",
            url: "UserConfig/addImportCSVModal",
            dataType: "json",
            success: function (result) {
                $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                $('#displayModal .modal-title').html('<i class="text-info">Add Data by CSV</i>');
                $('#displayModal .modal-body').html(result.form);
                dateGen();

                $('#displayModal').modal('show');  

                
            },
            error: function (result) {
               alert("error");
            }
        }); 
    });

    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });

    $(document).on('change', '.file', function(){
        var names = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            names.push($(this).get(0).files[i].name);
        }
        $(this).parent().find('.form-control').val(names);
        if (names === "") {

        } else{
            appendcsvdata();
        }
        
    });


    $(document).on("click", "#btnsubmitaddcsv", function () {
        form = $("#csvdataform");
        data = new FormData(form[0]);
        data.append("funddate", $("#dateCreatedCSV").val());

        $.ajax({
            url: "UserConfig/submitCSVData",
            data: data,
            type:'POST',
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);

                if (data.ResponseResult == "Insert Successful") {
                    
                } else {
                    
                }
            },
            error: function (result) {
               alert("error");
            }
        }); 
    });

    //end == jomer






    //alert("2342s");
    // var baseURL = 'http://localhost:48082/ACPCAccountingAPI/BUDGETService/';

    $('.modal').on('hidden.bs.modal', function (e) {
        if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    });

    function dateGen(){
    $('.datetimepicker').datetimepicker({
                format: 'YYYY-MM-DD',
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                }
             });
    }
    dateGen();
    
    $( "#btnDateSearch" ).click(function() {
        tblAllocate($("#FindDate").val());
        $('#exportExcelButton').attr("data-holder",$("#FindDate").val());
    });
    //===================================================================================================================================
   


    $(document).on("click", "#adding", function () {
      //  alert("sdf");
        $("#displayModal").modal('show');

        $.ajax({
                    type: "POST",
                    url: "UserConfig/addModal",
                    dataType: "json",
                    success: function (result) {
                        console.log(result.form);
                        $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                        $('#displayModal .modal-title').html('<i class="text-info">Add Data</i>');
                        $('#displayModal .modal-body').html(result.form);
                        getoptionSAOB();
                        dateGen();
                        // bootAdd();
                        $("#AllotmentMssgModal").hide();
                        $("#formloader").hide();
                        $('#displayModal').modal('show');  

                        
                    },
                    error: function (result) {
                       alert("error");
                    }
                }); 
    });


    $(document).on("click", ".editbtn", function () {
        //$("#displayModal").modal('show');
        let refid = $(this).data( "refid" )
        let type = $(this).data( "type" )
        alert(refid+" - "+type);
        $.ajax({
                    type: "POST",
                    url: "UserConfig/UpdateModal",
                    dataType: "json",
                    success: function (result) {
                        console.log(result.form);
                        $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                        $('#displayModal .modal-title').html('<i class="text-info">Update Data</i>');
                        $('#displayModal .modal-body').html(result.form);
                        UpdateForm(refid, type);
                        $('#displayModal').modal('show');  

                        
                    },
                    error: function (result) {
                       alert("error");
                    }
                });    
    });


    $(document).on("click", "#btnclear", function () {
        alert($("#budget_classificationModal").val());
    })

    //=====================my validation======================================

    function getoptionSAOB(){
       $.get(window.location + '/getOptionAdd').done(function(ret) {
       let result = JSON.parse(ret);
                 let response = result.ResponseResult;
                 $.each(response, function (x, y) {
                    console.log( response[x].title);
                    expendName = response[x].title;
                    expendcode = response[x].id;
                    $('#expendtypeAddOption').append(
                    '<option value = "'+expendcode+'"'+status+'>'+expendName+'</option>'
                );
        });    
     });
    }


    //===================================================================================================================================
    


    //======================================================================================================================================================

    $(document).on("click", "#addForm", function () {
        resetModal();


    });
    //===================================================================================================================================

    $(document).on("click", "#btnclear", function () {
        resetModal();


    });
    //===================================================================================================================================

    $('#AllotmentMssgModal').hide();
    $(document).on("click", "#btnsubmitadd", function () {
        $empty = $('#addrecordform').find("input").filter(function () {
            return this.value === "";
        });
        let notempty = $("input[name='allotmentTxt[]']").length;
        if ($empty.length == notempty) {
            $('#AllotmentMssgModal').show();
        } else {
            $.confirm({
                title: 'Confirm!',
                content: 'Are You Sure?',
                buttons: {
                    confirm: function () {
                          var formData = new FormData($("#addrecordform")[0]);
                          var url = "addAllotment";
                          fields.adding(formData, url)
                    
                       // $.alert('Confirmed!');
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    }
                   
                }
            });
        }
    });


    //$('#AllotmentMssgModal').hide();
    $(document).on("click", "#btnsubmitUpdate", function () {
        $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {
                      var formData = new FormData($("#updaterecordform")[0]);
                      var url = "updateAllotment";
                      fields.Updating(formData, url)
                
                   // $.alert('Confirmed!');
                },
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });
    });


    //========================================================================

    //     $('#AllotmentMssgModal').hide();
    // $(document).on("click", "#btnsubmitaupdate", function () {
    //         swal({
    //             title: "Are you sure?",
    //             text: "You will directly send the data in report",
    //             type: "warning",
    //             showCancelButton: true,
    //             confirmButtonColor: "#DD6B55",
    //             confirmButtonText: "Yes,   Submit it!",
    //             cancelButtonText: "No, I changed my mind.",
    //             closeOnConfirm: false,
    //             closeOnCancel: false
    //         }, function (isConfirm) {
    //             if (isConfirm) {

    //                 var date
    //                 var formData = new FormData($("#updaterecordform")[0]);
    //                 $.ajax({
    //                     url: "UserConfig/updateAllotment",
    //                     type: "POST",
    //                     data: formData,
    //                     cache: false,
    //                     contentType: false,
    //                     processData: false,
    //                     dataType: "json",
    //                     async: false,
    //                     success: function (json) {

    //                         message = json.ResponseResult
    //                         //                 console.log(message);
    //                         //alert(JSON.stringify(json));
    //                         if (message != 'Update Successful') {
    //                             swal(message, "error", "error");
    //                         } else {
    //                             swal("Success!", "Update successfully", "success");
    //                             $(".modal").modal("hide");
    //                            monthyFunction(triggerDate);  


    //                         }
    //                     }
    //                 });


    //             } else {
    //                 swal("Cancelled", "Record has not been Record.", "error");
    //             }

    //             // finalConfirm();
    //             //alert('hello');
    //         });


    


    // });
    // //=============================================================================
    function deleteConfirm(id) {
        //alert(id);
        //  console.log(id);
        //alert(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, I changed my mind.",
            closeOnConfirm: false,
            closeOnCancel: false,
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajaxSetup({async: false});
                $.post( window.location + '/removeBudget', {
                    'refId': id
                });
                
                swal("Deleted!", "record has been deleted.", "success");
                //swal("Success!", "added successfully", "success");
                    var dateval = $("#modalDate").val()
                    var arr = dateval.split("-");
                    var monthval = arr[1];
                    var yearval = arr[0];
                    let dateRaw = new Date(yearval,monthval,1);
                    var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                    let dd = dateneed.getDate();
                    let mm = dateneed.getMonth();
                    let yyyy = dateneed.getFullYear();
                    let sendPostData = new Object();        
                    if (dd < 10) dd = '0' + dd
                    if (mm < 10) mm = '0' + mm
                    defDate = yyyy + '-' + mm + '-' + dd;
                    //alert(defDate)
                    monthyFunction(defDate); 
            } else {
                swal("Cancelled", "Record has not been deleted.", "error");
            }

        });
    }

    $(document).on("click", ".deleteReq", function () {
        var refId = $(this).attr("value");
        deleteConfirm(refId);
       // alert(refId);
    });

    //========================================================================

    //$(document).on("click",".updateReq",function(){
    function UpdateForm(refid, exptype){
        $("#formGenUpdate").empty();
        // var refid = $(this).attr('value');
        // var exptype = $(this).data("exptype")
        $("#formloaderUpdate").show();
        $("#more_informationUpdate").hide();
        $.post( window.location + '/getSpecific',{ 'refId': refid }).done(function (result) {
            $("#exptype").text(exptype);
            $("#refid").val(refid);
            //alert(window.location + '/getSpecific')

            res = JSON.parse(result);
            rets = res.ResponseResult;

            console.log(res);
            child = res.ResponseResult;

                    $.each(child, function (i, v) {
                        if(i == 0){
                           // alert('wew');

                        }else{
                           // alert('now');
                        $("#formGenUpdate").append(
                            '<div class="col-md-3">' +
                            '<br>' +
                            '<div class="form-group form-float">' +
                            '<div class="focused form-line" focused>' +
                            '<input type="text" class="number form-control" id="allotmentupdateTxt" name="allotmentupdateTxt[]" value="'+child[i].amount+'">' +
                            '<input type="hidden" class="number form-control" id="expendcode" name="expendcode[]" value="'+child[i].code+'">' +
                            '<input type="hidden" class="number form-control" id="fundid" name="fundid[]" value="' + child[i].fundid + '">' +
                            '<label class="form-label" style="font-size: 1.3rem">' + child[i].code + '</label>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
          
                            
                        }


            $(".number").alphanum({
            allow: '1234567890-.', // Specify characters to allow
            disallow: 'qwertyuiopasdfghjklzxcvbnm' // Specify characters to disallow
            });

            // $(".number").on("keypress keyup blur",function (event) {
            // $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            // if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            // event.preventDefault();
            // }
            // });


          //     var child = res.ResponseResult;
          //       $("#tbodyexpend").empty();
              $("#formloaderUpdate").hide();
              $("#more_informationUpdate").show();   
            }); 

        });
    }

    function numberOnly(){
        $(".number").on("keypress keyup blur", function (event) {

                            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                                event.preventDefault();
                            }
                        });
    //}
    }
                    

    //======================================================================

    $("#property_number").alphanum({
        allow: '1234567890-qwertyuiopasdfghjklzxcvbnm', // Specify characters to allow
        disallow: '' // Specify characters to disallow
    });
    

    $("#btnupdate").hide();
    $("#disabler").click(function () {
        $(".handler").toggleClass("nopointerevent");
        $("#disabler").fadeOut("fast");
        $("#btnupdate").fadeIn("fast");
    });

    //========================= my date Generator  ============================= 

    //=======================================================================================================================================
    function resetModal() {
        $("#modify_date_container").collapse("hide");
        $("#formGen").empty();
        $("#btnsubmitadd").attr("disabled", true);
        $("#btnclear").attr("disabled", true);
        $('#searchMssg').hide();
        $('#searchMssgModal').hide();
        $('#AllotmentMssgModal').hide();
        //$('#budget_classificationModalhandle option[value="--"]').attr('selected','selected');
        $('select[name="budget_listModal"]').val('--');
        //})
    }


    //===========================================================================================================================================

    function addCommas(string) {
        return string.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        })
    }
});