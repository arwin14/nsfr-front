$(document).ready(function(){
 
 //var baseURL = "http://180.150.134.136:48082/ACPCAccountingAPI/BUDGETService";


$('.modal').on('hidden.bs.modal', function (e) {
    if($('.modal').hasClass('in')) {
    $('body').addClass('modal-open');
    }    
});

//=====================validation======================================

$(".number").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
        }
});
$("#property_number").alphanum({
    allow :'1234567890-qwertyuiopasdfghjklzxcvbnm', // Specify characters to allow
    disallow : ''  // Specify characters to disallow
});
$("#btnupdate").hide();
$("#disabler").click(function(){
    $(".handler").toggleClass("nopointerevent");
    $("#disabler").fadeOut("fast");
    $("#btnupdate").fadeIn("fast");
});

//=========================  date Generator  =============================
//var default date= 
//=======================================================================
            $('#MonthYear').bootstrapMaterialDatePicker({
                format: 'MM/DD/YYYY',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });
            $('#searchDate').bootstrapMaterialDatePicker({
                format: 'MM/DD/YYYY',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });
             $('#modalDate').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });

            

//=================================================================================================================================

            $("#dateMod").click(function(){

                   let thedate = $('#modalDate').val();
//console.log(thedate);
                    var arr = thedate.split("-");
                    var monthval = arr[1];
                    var yearval = arr[0];

                    let dateRaw = new Date(yearval,monthval,1);
                    var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                    let dd = dateneed.getDate();
                    let mm = dateneed.getMonth();
                    let yyyy = dateneed.getFullYear();
                    let sendPostData = new Object();
                    if (dd < 10) dd = '0' + dd
                    if (mm < 10) mm = '0' + mm
                    defDate = yyyy + '-' + mm + '-' + dd;
                    defDateSearch = mm + '/' + dd + '/' + yyyy;
                   // alert(yearval);

                $('#dateHead').text(defDate);
                $('#searchDate').val(defDateSearch);
                $('#DateCur').val(defDate);
                 $("#exportExcelButton").attr({
                "data-holder" : defDate
                });
                $("#exportExceltxt").attr({
                "value" : defDate
                });
                monthyFunction(defDate);
            });

//=================================================================================================================================
     
$("#btnupdate").click(function(){
$(".handler").toggleClass("nopointerevent");
$("#disabler").show();
$("#btnupdate").hide();
});



//===================================================================================================
  
$("#addForm").click(function(){
    $('#errorMssg').hide();
    $("#modify_date_container").collapse("hide");
    let dateRaw = new Date();
    var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
    let dd = dateneed.getDate();
    let mm = dateneed.getMonth() + 1;
    let yyyy = dateneed.getFullYear();
    let sendPostData = new Object();
    if (dd < 10) dd = '0' + dd
    if (mm < 10) mm = '0' + mm
    defDate = yyyy + '-' + mm + '-' + dd;

    
    $('#modalDate').val(defDate);
    $('#btnreset').trigger('click');
});


//=======================================================================================================

  
$("#submitDate").click(function(){
    $('#errorMssg').hide();



    var dateval = $("#searchDate").val()
    var arr = dateval.split("/");
    var monthval = arr[0];
    var yearval = arr[2];
    let dateRaw = new Date(yearval,monthval,1);
    var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
    let dd = dateneed.getDate();
    let mm = dateneed.getMonth();
    let yyyy = dateneed.getFullYear();
    let sendPostData = new Object();
    if (dd < 10) dd = '0' + dd
    if (mm < 10) mm = '0' + mm
    defDate = yyyy + '-' + mm + '-' + dd;

  
    $('#modalDate').val(defDate);
     monthyFunction(defDate);
     $('#dateHead').text(defDate);
        $("#exportExcelButton").attr({
        "data-holder" : defDate
        });
        $("#exportExceltxt").attr({
        "value" : defDate
        });
        $('#DateCurUpdate').val(defDate);

    $('#btnreset').trigger('click');

});


//=======================================================================================================
     $(document).on("click", "#btnaddBudget", function () {
                 swal({
            title: "Are you sure?",
            text: "You will directly send the data in report",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes,   Submit it!",
            cancelButtonText: "No, I changed my mind.",
            closeOnConfirm: false,
            closeOnCancel: false
            }, function (isConfirm) 
                {
                    if (isConfirm) 
                    {
                        var formData = new FormData($("#addBudget")[0]);
                        var theDate = $('#modalDate').val();
                          $.ajax({      
                            url: "UserConfig/addBudget",
                            type: "POST",
                            data: formData,
                            cache : false,
                            contentType : false,
                            processData : false,
                            dataType: "json",
                            async: false,
                            success: function(json){
                               
                         message = json.ResponseResult
                               // console.log(message);
                                //alert(JSON.stringify(json));
                                if(message  != 'Insert Successful'){
                                    swal(message, "error", "error");
                                }else{
                                    swal("Success!", "added successfully", "success");
                                    var dateval = $("#DateCur").val()
                                    var arr = dateval.split("-");
                                    var monthval = arr[1];
                                    var yearval = arr[0];
                                    let dateRaw = new Date(yearval,monthval,1);
                                    var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                                    let dd = dateneed.getDate();
                                    let mm = dateneed.getMonth();
                                    let yyyy = dateneed.getFullYear();
                                    let sendPostData = new Object();        
                                    if (dd < 10) dd = '0' + dd
                                    if (mm < 10) mm = '0' + mm
                                    defDate = yyyy + '-' + mm + '-' + dd;
                                    //alert(defDate)
                                     $('#btnreset').trigger('click');
                                     $(".modal").modal("hide");
                                    monthyFunction(defDate); 
                                }
                            }
                        });
                    } 
                    else 
                    {
                        swal("Cancelled", "Record has not been Record.", "error");
                    }
            });
        });
//===============================================================================================================
        

$("#person").click(function(){
     $(".empll").attr("placeholder", "Employee Name");

});

//========================================================================================================
$("#program").click(function(){
     $(".empll").attr("placeholder", "Program Name");

});
//========================================================================================================
    function autoTrigger(){
         $('#dateMod').trigger('click');
    }
    autoTrigger();

//========================================================================================================
 function monthyFunction(params){
       // $('#btnDateSearch').attr("disabled", true);
       // $('#addForm').attr("disabled", true);
       // $('.deleteReq').attr("disabled", true);
       // $('.updateReq').attr("disabled", true);


       //  $('#submitDate').attr("disabled", true);
       //  $('#searchDate').attr("disabled", true);
        $('#exportExcelButton').hide();

           // alert(params);
   
    theDate = params;
   //alert("---"+theDate);

         $("#loaders").show();
         //console.log(window.location+ '/getMonthlyData/');
          $.post(window.location+ '/getMonthlyData/',{'date':theDate}).done(function(res){
        let result = JSON.parse(res);
        let resResult = result.ResponseResult;


        $('#tlbmonthlyData').DataTable().clear();
        $('#tlbmonthlyData').DataTable().destroy();

         if(resResult == "NO DATA FOUND"){
            
                 $("#loaders").hide();

                 $('#tlbmonthlyData').DataTable({
                "scrollY": 300,
                "scrollX": true,
                dom: 'Bfrtip',
                fixedHeader: {
                    header: true,
                    footer: false
                },
                responsive: true,
                buttons: [

                ]
            });
            }else{
                    var prevConsumer;
                    var childIndex;
                        sortingData = result.ResponseResult.sort(function(x, y) {
                        left = x.refid2.replace(/-/g, "")
                        right = y.refid2.replace(/-/g, "")
                            return right.replace("-", "") - left.replace("-", "");
                    })
                    console.log(sortingData)


                      $.each(sortingData, function (i, v) {
                   

                         var child = result.ResponseResult[i].farContentList;
                         var FarNum = child.length;
                         child = result.ResponseResult[i];
                        if(FarNum > 1){
                         $.each(result.ResponseResult[i].farContentList, function (a, b) {
                                if(a == 0){
                                    $('#tlbmonthlyData').find('tbody').append(
                                    '<tr>'+
                                        '<td>'+
                                        '<a  class="deleteReq btn btn-danger btn-circle" value="'+child.refid+'"><i class="glyphicon glyphicon-trash"></i></a>'+               
                                        '<a class="updateReq btn btn-success btn-circle" data-toggle="modal" data-target="#myModalUpdate" value='+child.refid+'><i class="glyphicon glyphicon-th-list"></i></a>'+
                                        '</td>'+   
                                        '<td>'+addCommas(child.refid2)+'</td>'+
                                        '<td>'+addCommas(child.fundConsumer)+'</td>'+
                                        '<td></td>'+
                                        '<td>'+addCommas(child.farContentList[0].expendCode)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].obligations)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].tax)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].disbursements)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdAF.ps)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdAF.mooe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdAF.co)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdAF.fe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdAM.ps)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdAM.mooe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdAM.co)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdOS.ps)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdOS.mooe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].ocdOS.co)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].totalPS)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].totalCO)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].totalFE)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[0].totalMOOE)+'</td>'+
                                    '</tr>'      );
                                }else{

                                    $('#tlbmonthlyData').find('tbody').append(
                                     '<tr>'+
                                        '<td>'+
                                        '<a  class="deleteReq btn btn-danger btn-circle" style=" display:none;" value="'+child.refid+'"><i class="glyphicon glyphicon-trash" ></i></a>'+               
                                        '<a class="updateReq btn btn-success btn-circle" data-toggle="modal" data-target="#myModalUpdate" style=" display:none;"><i class="glyphicon glyphicon-th-list"></i></a>'+
                                        '</td>'+   
                                        '<td style="color:transparent;">'+addCommas(child.refid2)+'</td>'+
                                        '<td style="color:transparent;">'+addCommas(child.fundConsumer)+'</td>'+
                                        '<td style="color:transparent;"></td>'+
                                        '<td>'+addCommas(child.farContentList[a].expendCode)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].obligations)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].tax)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].disbursements)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdAF.ps)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdAF.mooe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdAF.co)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdAF.fe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdAM.ps)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdAM.mooe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdAM.co)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdOS.ps)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdOS.mooe)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].ocdOS.co)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].totalPS)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].totalCO)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].totalFE)+'</td>'+
                                        '<td>'+addCommas(child.farContentList[a].totalMOOE)+'</td>'+
                                    '</tr>'      );
                                }
                         });
                    
                     }else{
                        $('#tlbmonthlyData').find('tbody').append(
                            '<tr>'+
                                '<td>'+
                                '<a  class="deleteReq btn btn-danger btn-circle"  value="'+child.refid+'"><i class="glyphicon glyphicon-trash"></i></a>'+               
                                '<a class="updateReq btn btn-success btn-circle" data-toggle="modal" data-target="#myModalUpdate" value='+child.refid+'><i class="glyphicon glyphicon-th-list"></i></a>'+
                                '</td>'+   
                                '<td>'+addCommas(child.refid2)+'</td>'+
                                '<td>'+addCommas(child.fundConsumer)+'</td>'+
                                '<td></td>'+
                                '<td>'+addCommas(child.farContentList[0].expendCode)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].obligations)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].tax)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].disbursements)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdAF.ps)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdAF.mooe)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdAF.co)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdAF.fe)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdAM.ps)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdAM.mooe)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdAM.co)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdOS.ps)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdOS.mooe)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].ocdOS.co)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].totalPS)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].totalCO)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].totalFE)+'</td>'+
                                '<td>'+addCommas(child.farContentList[0].totalMOOE)+'</td>'+
                               
                            '</tr>'      );
                        }
                    });
                        

                     $('#tlbmonthlyData').DataTable( {
                       
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;
                        

                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
                        var intVal2 = function ( ii ) {
                            return typeof ii === 'string' ?
                                ii.replace(/[\$,]/g, '')*1 :
                                typeof ii === 'number' ?
                                    ii : 0;
                        };
                        var intVal3 = function ( iii ) {
                            return typeof iii === 'string' ?
                                iii.replace(/[\$,]/g, '')*1 :
                                typeof iii === 'number' ?
                                    iii : 0;
                        };
                        var intVal4 = function ( iiii ) {
                            return typeof iiii === 'string' ?
                                iiii.replace(/[\$,]/g, '')*1 :
                                typeof iiii === 'number' ?
                                    iiii : 0;
                        };
                        var intVal5 = function ( iiiii ) {
                            return typeof iiiii === 'string' ?
                                iiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiii === 'number' ?
                                    iiiii : 0;
                        };
                         var intVal6 = function ( iiiiii ) {
                            return typeof iiiiii === 'string' ?
                                iiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiii === 'number' ?
                                    iiiiii : 0;
                        }; 
                         var intVal7 = function ( iiiiiii ) {
                            return typeof iiiiiii === 'string' ?
                                iiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiii === 'number' ?
                                    iiiiiii : 0;
                        };  
                         var intVal8 = function ( iiiiiiii ) {
                            return typeof iiiiiiii === 'string' ?
                                iiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiii === 'number' ?
                                    iiiiiiii : 0;
                        };    
                         var intVal9 = function ( iiiiiiiii ) {
                            return typeof iiiiiiiii === 'string' ?
                                iiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiii === 'number' ?
                                    iiiiiiiii : 0;
                        }; 
                         var intVal10 = function ( iiiiiiiiii ) {
                            return typeof iiiiiiiiii === 'string' ?
                                iiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiii === 'number' ?
                                    iiiiiiiiii : 0;
                        };    
                         var intVal11 = function ( iiiiiiiiiii ) {
                            return typeof iiiiiiiiiii === 'string' ?
                                iiiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiiii === 'number' ?
                                    iiiiiiiiiii : 0;
                        };    
                         var intVal12 = function ( iiiiiiiiiiii ) {
                            return typeof iiiiiiiiiiii === 'string' ?
                                iiiiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiiiii === 'number' ?
                                    iiiiiiiiiiii : 0;
                        };    
                         var intVal13 = function ( iiiiiiiiiiiii ) {
                            return typeof iiiiiiiiiiiii === 'string' ?
                                iiiiiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiiiiii === 'number' ?
                                    iiiiiiiiiiiii : 0;
                        };    
                         var intVal14 = function ( iiiiiiiiiiiiii ) {
                            return typeof iiiiiiiiiiiiii === 'string' ?
                                iiiiiiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiiiiiii === 'number' ?
                                    iiiiiiiiiiiiii : 0;
                        }; 
                         var intVal15 = function ( iiiiiiiiiiiiiii ) {
                            return typeof iiiiiiiiiiiiiii === 'string' ?
                                iiiiiiiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiiiiiiii === 'number' ?
                                    iiiiiiiiiiiiiii : 0;
                        };    
                         var intVal16 = function ( iiiiiiiiiiiiiiii ) {
                            return typeof iiiiiiiiiiiiiiii === 'string' ?
                                iiiiiiiiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiiiiiiiii === 'number' ?
                                    iiiiiiiiiiiiiiii : 0;
                        };    
                         var intVal17 = function ( iiiiiiiiiiiiiiiii ) {
                            return typeof iiiiiiiiiiiiiiiii === 'string' ?
                                iiiiiiiiiiiiiiiii.replace(/[\$,]/g, '')*1 :
                                typeof iiiiiiiiiiiiiiiii === 'number' ?
                                    iiiiiiiiiiiiiiiii : 0;
                        };  

                        // Total over all pages
                        total = api
                            .column( 5 )
                            .data()
                            .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                        total2 = api
                            .column( 6 )
                            .data()
                            .reduce( function (aa, bb) {
                            return intVal2(aa) + intVal2(bb);
                        }, 0 );
                        total3 = api
                            .column( 7 )
                            .data()
                            .reduce( function (aaa, bbb) {
                            return intVal3(aaa) + intVal3(bbb);
                        }, 0 );
                        total4 = api
                            .column( 8 )
                            .data()
                            .reduce( function (aaaa, bbbb) {
                            return intVal4(aaaa) + intVal4(bbbb);
                        }, 0 );
                        total5 = api
                            .column( 9 )
                            .data()
                            .reduce( function (aaaaa, bbbbb) {
                            return intVal5(aaaaa) + intVal5(bbbbb);
                        }, 0 );
                        total6 = api
                            .column( 10 )
                            .data()
                            .reduce( function (aaaaaa, bbbbbb) {
                            return intVal6(aaaaaa) + intVal6(bbbbbb);
                        }, 0 );
                        total7 = api
                            .column( 11 )
                            .data()
                            .reduce( function (aaaaaaa, bbbbbbb) {
                            return intVal7(aaaaaaa) + intVal7(bbbbbbb);
                        }, 0 );
                        total8 = api
                            .column( 12 )
                            .data()
                            .reduce( function (aaaaaaaa, bbbbbbbb) {
                            return intVal8(aaaaaaaa) + intVal8(bbbbbbbb);
                        }, 0 );
                        total9 = api
                            .column( 13 )
                            .data()
                            .reduce( function (aaaaaaaaa, bbbbbbbbb) {
                            return intVal9(aaaaaaaaa) + intVal9(bbbbbbbbb);
                        }, 0 );
                        total10 = api
                            .column( 14 )
                            .data()
                            .reduce( function (aaaaaaaaaa, bbbbbbbbbb) {
                            return intVal10(aaaaaaaaaa) + intVal10(bbbbbbbbbb);
                        }, 0 );
                        total11 = api
                            .column( 15 )
                            .data()
                            .reduce( function (aaaaaaaaaaa, bbbbbbbbbbb) {
                            return intVal11(aaaaaaaaaaa) + intVal11(bbbbbbbbbbb);
                        }, 0 );
                        total12 = api
                            .column( 16 )
                            .data()
                            .reduce( function (aaaaaaaaaaaa, bbbbbbbbbbbb) {
                            return intVal12(aaaaaaaaaaaa) + intVal12(bbbbbbbbbbbb);
                        }, 0 );
                        total13 = api
                            .column( 17 )
                            .data()
                            .reduce( function (aaaaaaaaaaaaa, bbbbbbbbbbbbb) {
                            return intVal13(aaaaaaaaaaaaa) + intVal13(bbbbbbbbbbbbb);
                        }, 0 );
                        total14 = api
                            .column( 18 )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaa, bbbbbbbbbbbbbb) {
                            return intVal14(aaaaaaaaaaaaaa) + intVal14(bbbbbbbbbbbbbb);
                        }, 0 );
                        total15 = api
                            .column( 19 )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaaa, bbbbbbbbbbbbbbb) {
                            return intVal15(aaaaaaaaaaaaaaa) + intVal15(bbbbbbbbbbbbbbb);
                        }, 0 );
                        total16 = api
                            .column( 20 )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaaaa, bbbbbbbbbbbbbbbb) {
                            return intVal16(aaaaaaaaaaaaaaaa) + intVal16(bbbbbbbbbbbbbbbb);
                        }, 0 );
                        total17 = api
                            .column( 21 )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaaaaa, bbbbbbbbbbbbbbbbb) {
                            return intVal17(aaaaaaaaaaaaaaaaa) + intVal17(bbbbbbbbbbbbbbbbb);
                        }, 0 );
                        // Total over this page
                        pageTotal = api
                            .column( 5, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
                       pageTotal2 = api
                            .column( 6, { page: 'current'} )
                            .data()
                            .reduce( function (aa, bb) {
                                return intVal2(aa) + intVal2(bb);
                            }, 0 );
                       pageTotal3 = api
                            .column( 7, { page: 'current'} )
                            .data()
                            .reduce( function (aaa, bbb) {
                                return intVal3(aaa) + intVal3(bbb);
                            }, 0 );
                       pageTotal4 = api
                            .column( 8, { page: 'current'} )
                            .data()
                            .reduce( function (aaaa, bbbb) {
                                return intVal4(aaaa) + intVal4(bbbb);
                            }, 0 );
                       pageTotal5 = api
                            .column( 9, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaa, bbbbb) {
                                return intVal5(aaaaa) + intVal5(bbbbb);
                            }, 0 );
                       pageTotal6 = api
                            .column( 10, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaa, bbbbbb) {
                                return intVal6(aaaaaa) + intVal6(bbbbbb);
                            }, 0 );
                       pageTotal7 = api
                            .column( 11, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaa, bbbbbbb) {
                                return intVal7(aaaaaaa) + intVal7(bbbbbbb);
                            }, 0 );
                       pageTotal8 = api
                            .column( 12, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaa, bbbbbbbb) {
                                return intVal8(aaaaaaaa) + intVal7(bbbbbbbb);
                            }, 0 );
                       pageTotal9 = api
                            .column( 13, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaa, bbbbbbbbb) {
                                return intVal9(aaaaaaaaa) + intVal7(bbbbbbbbb);
                            }, 0 );
                       pageTotal10 = api
                            .column( 14, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaa, bbbbbbbbbb) {
                                return intVal10(aaaaaaaaaa) + intVal10(bbbbbbbbbb);
                            }, 0 );
                       pageTotal11 = api
                            .column( 15, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaaa, bbbbbbbbbbb) {
                                return intVal11(aaaaaaaaaaa) + intVal11(bbbbbbbbbbb);
                            }, 0 );
                       pageTotal12 = api
                            .column( 16, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaaaa, bbbbbbbbbbbb) {
                                return intVal12(aaaaaaaaaaaa) + intVal12(bbbbbbbbbbbb);
                            }, 0 );
                       pageTotal13 = api
                            .column( 17, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaaaaa, bbbbbbbbbbbbb) {
                                return intVal13(aaaaaaaaaaaaa) + intVal13(bbbbbbbbbbbbb);
                            }, 0 );
                       pageTotal14 = api
                            .column( 18, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaa, bbbbbbbbbbbbbb) {
                                return intVal14(aaaaaaaaaaaaaa) + intVal14(bbbbbbbbbbbbbb);
                            }, 0 );
                       pageTotal15 = api
                            .column( 19, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaaa, bbbbbbbbbbbbbbb) {
                                return intVal15(aaaaaaaaaaaaaaa) + intVal15(bbbbbbbbbbbbbbb);
                            }, 0 );
                       pageTotal16 = api
                            .column( 20, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaaaa, bbbbbbbbbbbbbbbb) {
                                return intVal16(aaaaaaaaaaaaaaaa) + intVal16(bbbbbbbbbbbbbbbb);
                            }, 0 );
                       pageTotal17 = api
                            .column( 21, { page: 'current'} )
                            .data()
                            .reduce( function (aaaaaaaaaaaaaaaaa, bbbbbbbbbbbbbbbbb) {
                                return intVal17(aaaaaaaaaaaaaaaaa) + intVal17(bbbbbbbbbbbbbbbbb);
                            }, 0 );

                        // Update footer
                        $( api.column( 5 ).footer() ).html(
                            'P'+addCommas(pageTotal) +' ( P'+ addCommas(total) +' total)'
                        );
                         $( api.column( 6 ).footer() ).html(
                            'P'+addCommas(pageTotal2) +' ( P'+ addCommas(total2) +' total)'
                        );
                         $( api.column( 7 ).footer() ).html(
                            'P'+addCommas(pageTotal3) +' ( P'+ addCommas(total3) +' total)'
                        );
                         $( api.column( 8 ).footer() ).html(
                            'P'+addCommas(pageTotal4) +' ( P'+ addCommas(total4) +' total)'
                        ); 
                         $( api.column( 9 ).footer() ).html(
                            'P'+addCommas(pageTotal5) +' ( P'+ addCommas(total5) +' total)'
                        );
                         $( api.column( 10 ).footer() ).html(
                            'P'+addCommas(pageTotal6) +' ( P'+ addCommas(total6) +' total)'
                        );    
                         $( api.column( 11 ).footer() ).html(
                            'P'+addCommas(pageTotal7) +' ( P'+ addCommas(total7) +' total)'
                        ); 
                         $( api.column( 12 ).footer() ).html(
                            'P'+addCommas(pageTotal8) +' ( P'+ addCommas(total8)+' total)'
                        );
                         $( api.column( 13 ).footer() ).html(
                            'P'+addCommas(pageTotal9) +' ( P'+ addCommas(total9)+' total)'
                        ); 
                         $( api.column( 14 ).footer() ).html(
                            'P'+addCommas(pageTotal10) +' ( P'+ addCommas(total10)+' total)'
                        );  
                         $( api.column( 15 ).footer() ).html(
                            'P'+addCommas(pageTotal11) +' ( P'+ addCommas(total11)+' total)'
                        ); 
                         $( api.column( 16 ).footer() ).html(
                            'P'+addCommas(pageTotal12) +' ( P'+ addCommas(total12)+' total)'
                        ); 
                         $( api.column( 17 ).footer() ).html(
                            'P'+addCommas(pageTotal13) +' ( P'+ addCommas(total13)+' total)'
                        );   
                         $( api.column( 18 ).footer() ).html(
                            'P'+addCommas(pageTotal14) +' ( P'+ addCommas(total14)+' total)'
                        ); 
                         $( api.column( 19 ).footer() ).html(
                            'P'+addCommas(pageTotal15) +' ( P'+ addCommas(total15)+' total)'
                        );
                         $( api.column( 20 ).footer() ).html(
                            'P'+addCommas(pageTotal16) +' ( P'+ addCommas(total16)+' total)'
                        );
                         $( api.column( 21 ).footer() ).html(
                            'P'+addCommas(pageTotal17) +' ( P'+ addCommas(total17)+' total)'
                        );           
                    },



                      "scrollY": 300,
                      "scrollX": true,    
                        dom: 'Bfrtip',
                        fixedHeader: {
                            header: true,
                            footer: false
                        },
                        responsive: true,
                        buttons: [
                      
                        ] 
                });

            }
             $("#loaders").hide();
             $("#addForm").removeAttr('disabled');
             $("#btnDateSearch").removeAttr('disabled');
             $(".deleteReq").removeAttr('disabled');
             $(".updateReq").removeAttr('disabled');



            $('#submitDate').removeAttr('disabled');
            $('#searchDate').removeAttr('disabled');
            $('#exportExcelButton').show();
    });
}



//==========================================================================================================
function start(value, count){
       $.get(window.location + '/getOptionData').done(function(ret) {
      console.log(value);
       let result = JSON.parse(ret); 
                 let response = result.ResponseResult;
                 $.each(response, function (x, y) {
                    expendName = response[x].name;
                    expendcode = response[x].code;
                  //  console.log(expendcode);

                    if(value == expendcode){
                    status = 'selected';
                    }else{
                    status='';
                    }
                    $('#expenditureTable').find('#theCodeUpdate'+count).append(
                    '<option value = "'+expendcode+'"'+status+'>'+expendName+'</option>'
                );
        });    
     });
    }

    //==========================================================================================================
    function selectStart(){
       $.get(window.location + '/getOptionData').done(function(ret) {
           let result = JSON.parse(ret); 
              $('#descriptTable').find('#theCode0').empty( );
                 let response = result.ResponseResult;
                 let optiondata = '';

                $.each(response, function (a, b) {
                    expendName = response[a].name;
                    expendcode = response[a].code;

                   //console.log(expendName+" == "+expendcode);
                    if(a == 0){
                         optiondata = '<option value = "">Select Code</option>'+
                                      '<option value = "'+expendcode+'">'+expendName+'</option>'
                    }else{
                         optiondata =  '<option value = "'+expendcode+'">'+expendName+'</option>'
                    }
                     $('#descriptTable').find('#theCode0').append(
                         optiondata
                 );

            }); 
       



     });
    }
     selectStart();
     //==========================================================================================================
    function selectTagAlter(count){
        //console.log("thecode"+count);
       $.get(window.location + '/getOptionDataAlter').done(function(ret) {
            let result = JSON.parse(ret); 
              $('#descriptTable').find('#theCode'+count).empty( );
                 let response = result.ResponseResult;
                 let optiondata = ''; 
                $.each(response, function (a, b) {
                    expendName = response[a].name;
                    expendcode = response[a].code;

                    if(a == 0){
                     optiondata =  '<option value = "">Select Code</option>'+
                                    '<option value = "'+expendcode+'">'+expendName+'</option>'
                    }else{
                     optiondata =  '<option value = "'+expendcode+'">'+expendName+'</option>'
                    }
                    //console.log(optiondata);
                     $('#descriptTable').find('#theCode'+count).append(
                     optiondata
                 );


        });    
    });
    }
    //==========================================================================================================
     $('#alterBudgetUpdate').on("click", function(params){
                tbody_length = $('#expenditureTable tbody').find('tr').length
                i = tbody_length + 1;
            //    console.log(i);

            $('#expenditureTable > tbody').append(
                        '<tr id="handlerStat'+i+'" ><td>'+
                               '<div class="rowtoClear"><div class="row clearfix"><div class="row clearfix"><div class="col-md-12"><div class="form-group form-float"><div class="form-line" id="categories">'
                                + '<input type="hidden" value="0" id="afidUpdate" name="afid[]"><input type="hidden" value="0" id="amidUpdate" name="amid[]"><input type="hidden" value="0" id="osidUpdate" name="osid[]">'                            
                                + '<select class="codesAddUpdate form-control" number-countUpdate='+i+' id="theCodeUpdate'+i+'" name="theCodeUpdate[]" style="background:rgba(0,0,0,0);">'
                                + '<option value="0">Select Code</option></select>'
                                +'<input type="hidden" value="0" id="statusUpdate'+i+'" name="statusUpdate[]"><input type="hidden" value="ACTIVE" id="statusUpdateString'+i+'" class="statusGroup" name="statusUpdateString[]">'
                                + '</div></div></div></div>'
                                +'<p><b style="padding-left:10px;">1.A.1 Admin & Finance</b><div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxAdminUpdate" name="taxAdminUpdate[]" value="0" style="background:rgba(0,0,0,0);" ></div></div></p>'
                                + '<div id="pshandler'+i+'" class="nopointerevent"> <div class="col-md-3"> <div class="form-group"> <div class="form-line" id="1psFormUpdate'+i+'"> <input type="text" class="numberInternal form-control" value="0" id="1psUpdate'+i+'" name="1psUpdate[]" style="background:rgba(0,0,0,0);"> <label>PS</label> </div> </div> </div> </div> <div id="mooehandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1mooeFormUpdate'+i+'"> <input type="text" class="numberInternal form-control" value="0" id="1mooeUpdate'+i+'" name="1mooeUpdate[]" style="background:rgba(0,0,0,0);"> <label>MOOE</label> </div></div> </div></div>'
                                + '<div id="cohandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1coFormUpdate'+i+'"> <input type="text" name="1coUpdate[]" id="1coUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>CO</label> </div> </div> </div> </div> <div id="fehandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1feFormUpdate'+i+'"> <input type="text" name="1feUpdate[]" id="1feUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>FE</label> </div> </div> </div> </div> </div>'
                                + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.11 AMCFP-FRM</b><div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxAmcfpUpdate" name="taxAmcfpUpdate[]"  value="0" placeholder="Tax Charges" style="background:rgba(0,0,0,0);" ></div></div> </p> <div id="2pshandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2psFormUpdate'+i+'"> <input type="text" name="2psUpdate[]" id="2psUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>PS</label> </div> </div> </div> </div> <div id="2mooehandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2mooeFormUpdate'+i+'"> <input type="text" name="2mooeUpdate[]" id="2mooeUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div> <div id="2cohandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2coFormUpdate'+i+'"> <input type="text" name="2coUpdate[]" id="2coUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div> </div> </div> </div>'
                                + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.2 Operation-Reasearch</b> <div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxOperationUpdate" name="taxOperationUpdate[]" value="0" style="background:rgba(0,0,0,0);" ></div></div>        </p> <div id="3pshandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3psFormUpdate'+i+'"> <input type="text" name="3psUpdate[]" id="3psUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="number form-control" > <label>PS</label> </div> </div> </div></div><div id="3mooehandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3mooeFormUpdate'+i+'"> <input type="text" name="3mooeUpdate[]" id="3mooeUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div>'
                                + '<div class="col-md-4"> <div id="3cohandler'+i+'" class="nopointerevent"> <div class="form-group form-float"> <div class="form-line" id="3coFormUpdate'+i+'"> <input type="text" name="3coUpdate[]" id="3coUpdate'+i+'" value="0" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div> </div> </div></div></div>'        
                                + '</td><td><button href="#" data-toggle="tooltip" data-count='+i+' data-value="statusUpdateString'+i+'" data-status="0" data-placement="left" title="Remove Description" id="removelast" class="removeDescripUpdate btn-basic btn-circle"><i class="material-icons">remove</i></button></td></tr>'
                              );
            value = '';
            internalValidation();
            UpdateTypeValidation();
            start(value, i);

    });
    //==========================================================================================================
    $('#removelast').on("click", function(){
                $('#emplUpdate tr:last').remove();
    });
    //==========================================================================================================
    $(document).on("click",".updateReq",function(){
         var budgetId = $(this).attr('value');
          //$.post(window.location+ '/getMonthlyData/',{'date':theDate}).done(function(res){
        //console.log(budgetId);

          $.post( window.location + '/getUpdate/',{'refId': budgetId }).done(function (result) {
            res = JSON.parse(result);
                  console.log(res);
                var child = res.ResponseResult;
                $("#tbodyexpend").empty();
                $.each(child, function (i, v) {
                        console.log(i);
                        if(i < 1){
                            let refbase =  child[0].refid2;
                                $('#emplUpdate').val(child[0].fund_consumer);
                                $('#taxUpdate').val(child[0].tax);
                                $('#refBUpdate').val(refbase.substr(0,8));
                                $('#refid2Update').val(refbase.substr(8,13));
                                $('#refidUpdate').val(child[0].refid);
                        }else{
                                if(i<2){
                                // $('#expenditureTable').find('tbody').append(
                                //     '<tr><td>'+
                                //    '<div class="rowtoClear"><div class="row clearfix"><div class="row clearfix"><div class="col-md-6"><div class="form-group form-float"><div class="form-line" id="categories">'
                                //     + '<input type="hidden" value="'+child[i].afid+'" id="afidUpdate'+i+'" name="afid[]"><input type="hidden" value="'+child[i].amid+'" id="amidUpdate" name="amid[]"><input type="hidden" value="'+child[i].osid+'" id="osidUpdate" name="osid[]">'                            
                                //     + '<select class="codesAddUpdate form-control" number-countUpdate='+i+' id="theCodeUpdate'+i+'" name="theCodeUpdate[]" style="background:rgba(0,0,0,0);">'
                                //     +'</select>'
                                //      +'<input type="hidden" value="1" id="statusUpdate'+i+'" name="statusUpdate[]"> <input type="hidden" value="ACTIVE" id="statusUpdateString'+i+'" class="statusGroup" name="statusUpdateString[]">'
                                //     +'<input type="hidden" value="'+child[i].expendid+'" id="expendidUpdate" name="expendidUpdate[]"><input type="hidden" value="'+child[i].obligations+'" id="obligationsUpdate" name="obligationsUpdate[]"> <input type="hidden" value="'+child[i].disbursments+'" id="disbursementsUpdate" name="disbursementsUpdate[]">'
                                //     +'<input type="hidden" value="'+child[i].totalPS+'" id="totalPSUpdate" name="totalPSUpdate[]"><input type="hidden" value="'+child[i].totalMOOE+'" id="totalMOOEUpdate" name="totalMOOEUpdate[]"><input type="hidden" value="'+child[i].totalCO+'" id="totalCOUpdate" name="totalCOUpdate[]"><input type="hidden" value="'+child[i].totalMOOE+'" id="totalFEUpdate" name="totalFEUpdate[]">'
                                //     + '</div></div></div><div class="col-md-6"> <div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i> </span> <div class="form-line"><input type="text" class="form-control" id="taxSpecUpdate'+i+'" name="taxSpecUpdate[]" value="'+child[i].tax+'" style="background:rgba(0,0,0,0);"></div></div></div></div>'
                                //     +'<p> <b style="padding-left:10px;">1.A.1 Admin & Finance</b></p>'
                                //     + '<div id="pshandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group"> <div class="form-line" id="1psFormUpdate'+i+'"> <input type="text" class="numberInternal form-control" value="'+child[i].afps+'" id="1psUpdate'+i+'" name="1psUpdate[]" style="background:rgba(0,0,0,0);"> <label>PS</label> </div> </div> </div></div> <div id="mooehandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1mooeFormUpdate'+i+'"> <input type="text" name="1mooeUpdate[]" id="1mooeUpdate'+i+'" value="'+child[i].afmooe+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>MOOE</label> </div> </div> </div></div>'
                                //     + '<div id="cohandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1coFormUpdate'+i+'"> <input type="text" name="1coUpdate[]" id="1coUpdate'+i+'" value="'+child[i].afco+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>CO</label> </div> </div> </div> </div> <div id="fehandler'+i+'" class="nopointerevent"> <div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1feFormUpdate'+i+'"> <input type="text" name="1feUpdate[]" id="1feUpdate'+i+'" value="'+child[i].affe+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>FE</label> </div> </div> </div> </div> </div>'
                                //     + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.11 AMCFP-FRM</b> </p> <div id="2pshandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2psFormUpdate'+i+'"> <input type="text" name="2psUpdate[]" id="2psUpdate'+i+'" value="'+child[i].amps+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>PS</label> </div> </div> </div></div> <div id="2mooehandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2mooeFormUpdate'+i+'"> <input type="text" name="2mooeUpdate[]" id="2mooeUpdate'+i+'" value="'+child[i].ammooe+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div>  <div id="2cohandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2coFormUpdate'+i+'"> <input type="text" name="2coUpdate[]" id="2coUpdate'+i+'" value="'+child[i].amco+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div> </div> </div> </div>'
                                //     + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.2 Operation-Reasearch</b> </p> <div id="3pshandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3psFormUpdate'+i+'"> <input type="text" name="3psUpdate[]" id="3psUpdate'+i+'" value="'+child[i].osps+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>PS</label> </div> </div> </div> </div> <div id="3mooehandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3mooeFormUpdate'+i+'"> <input type="text" name="3mooeUpdate[]" id="3mooeUpdate'+i+'" value="'+child[i].osmooe+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div>'
                                //     + '<div id="3cohandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3coFormUpdate'+i+'"> <input type="text" name="3coUpdate[]" id="3coUpdate'+i+'" value="'+child[i].osco+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div></div></div></div> </div>'        
                                //     +'</td><td></td></tr>'
                                // );
                                    $('#expenditureTable').find('tbody').append(
                                    '<tr><td>'+
                                   '<div class="rowtoClear"><div class="row clearfix"><div class="row clearfix"><div class="col-md-12"><div class="form-group form-float"><div class="form-line" id="categories">'
                                    + '<input type="hidden" value="'+child[i].afid+'" id="afidUpdate'+i+'" name="afid[]"><input type="hidden" value="'+child[i].amid+'" id="amidUpdate" name="amid[]"><input type="hidden" value="'+child[i].osid+'" id="osidUpdate" name="osid[]">'                            
                                    + '<select class="codesAddUpdate form-control" number-countUpdate='+i+' id="theCodeUpdate'+i+'" name="theCodeUpdate[]" style="background:rgba(0,0,0,0);">'
                                    +'</select>'
                                     +'<input type="hidden" value="1" id="statusUpdate'+i+'" name="statusUpdate[]"> <input type="hidden" value="ACTIVE" id="statusUpdateString'+i+'" class="statusGroup" name="statusUpdateString[]">'
                                    +'<input type="hidden" value="'+child[i].expendid+'" id="expendidUpdate" name="expendidUpdate[]"><input type="hidden" value="'+child[i].obligations+'" id="obligationsUpdate" name="obligationsUpdate[]"> <input type="hidden" value="'+child[i].disbursments+'" id="disbursementsUpdate" name="disbursementsUpdate[]">'
                                    +'<input type="hidden" value="'+child[i].totalPS+'" id="totalPSUpdate" name="totalPSUpdate[]"><input type="hidden" value="'+child[i].totalMOOE+'" id="totalMOOEUpdate" name="totalMOOEUpdate[]"><input type="hidden" value="'+child[i].totalCO+'" id="totalCOUpdate" name="totalCOUpdate[]"><input type="hidden" value="'+child[i].totalMOOE+'" id="totalFEUpdate" name="totalFEUpdate[]">'
                                    + '</div></div></div></div>'
                                    +'<p> <b style="padding-left:10px;">1.A.1 Admin & Finance</b><div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxAdminUpdate" name="taxAdminUpdate[]" value="'+child[i].aftax2+'" style="background:rgba(0,0,0,0);" ></div></div></p>'
                                    + '<div id="pshandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group"> <div class="form-line" id="1psFormUpdate'+i+'"> <input type="text" class="numberInternal form-control" value="'+child[i].afps+'" id="1psUpdate'+i+'" name="1psUpdate[]" style="background:rgba(0,0,0,0);"> <label>PS</label> </div> </div> </div></div> <div id="mooehandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1mooeFormUpdate'+i+'"> <input type="text" name="1mooeUpdate[]" id="1mooeUpdate'+i+'" value="'+child[i].afmooe+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>MOOE</label> </div> </div> </div></div>'
                                    + '<div id="cohandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1coFormUpdate'+i+'"> <input type="text" name="1coUpdate[]" id="1coUpdate'+i+'" value="'+child[i].afco+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>CO</label> </div> </div> </div> </div> <div id="fehandler'+i+'" class="nopointerevent"> <div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1feFormUpdate'+i+'"> <input type="text" name="1feUpdate[]" id="1feUpdate'+i+'" value="'+child[i].affe+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>FE</label> </div> </div> </div> </div> </div>'
                                    + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.11 AMCFP-FRM</b><div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="F" name="taxAmcfpUpdate[]"  value="'+child[i].amtax2+'" placeholder="Tax Charges" style="background:rgba(0,0,0,0);" ></div></div></p> <div id="2pshandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2psFormUpdate'+i+'"> <input type="text" name="2psUpdate[]" id="2psUpdate'+i+'" value="'+child[i].amps+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>PS</label> </div> </div> </div></div> <div id="2mooehandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2mooeFormUpdate'+i+'"> <input type="text" name="2mooeUpdate[]" id="2mooeUpdate'+i+'" value="'+child[i].ammooe+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div>  <div id="2cohandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2coFormUpdate'+i+'"> <input type="text" name="2coUpdate[]" id="2coUpdate'+i+'" value="'+child[i].amco+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div> </div> </div> </div>'
                                    + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.2 Operation-Reasearch</b> <div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxOperationUpdate" name="taxOperationUpdate[]" value="'+child[i].ostax2+'" style="background:rgba(0,0,0,0);" ></div></div></p> <div id="3pshandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3psFormUpdate'+i+'"> <input type="text" name="3psUpdate[]" id="3psUpdate'+i+'" value="'+child[i].osps+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>PS</label> </div> </div> </div> </div> <div id="3mooehandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3mooeFormUpdate'+i+'"> <input type="text" name="3mooeUpdate[]" id="3mooeUpdate'+i+'" value="'+child[i].osmooe+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div>'
                                    + '<div id="3cohandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3coFormUpdate'+i+'"> <input type="text" name="3coUpdate[]" id="3coUpdate'+i+'" value="'+child[i].osco+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div></div></div></div> </div>'        
                                    +'</td><td></td></tr>'
                                );
                                }else{
                                     $('#expenditureTable').find('tbody').append(
                                    '<tr id="handlerStat'+i+'" ><td>'+
                                   '<div class="rowtoClear"><div class="row clearfix"><div class="row clearfix"><div class="col-md-12"><div class="form-group form-float"><div class="form-line" id="categories">'
                                    + '<input type="hidden" value="'+child[i].afid+'" id="afidUpdate" name="afid[]"><input type="hidden" value="'+child[i].amid+'" id="amidUpdate" name="amid[]"><input type="hidden" value="'+child[i].osid+'" id="osidUpdate" name="osid[]">'                            
                                    + '<select class="codesAddUpdate form-control" number-countUpdate='+i+' id="theCodeUpdate'+i+'" name="theCodeUpdate[]" style="background:rgba(0,0,0,0);">'
                                    +'</select>'
                                    +'<input type="hidden" value="1" id="statusUpdate'+i+'" name="statusUpdate[]"><input type="hidden" value="ACTIVE" id="statusUpdateString'+i+'" class="statusGroup" name="statusUpdateString[]">'
                                    +'<input type="hidden" value="'+child[i].expendid+'" id="expendidUpdate" name="expendidUpdate[]"><input type="hidden" value="'+child[i].obligations+'" id="obligationsUpdate" name="obligationsUpdate[]"> <input type="hidden" value="'+child[i].disbursments+'" id="disbursementsUpdate" name="disbursementsUpdate[]">'
                                    +'<input type="hidden" value="'+child[i].totalPS+'" id="totalPSUpdate" name="totalPSUpdate[]"><input type="hidden" value="'+child[i].totalMOOE+'" id="totalMOOEUpdate" name="totalMOOEUpdate[]"><input type="hidden" value="'+child[i].totalCO+'" id="totalCOUpdate" name="totalCOUpdate[]"><input type="hidden" value="'+child[i].totalMOOE+'" id="totalFEUpdate" name="totalFEUpdate[]">'
                                    + '</div></div></div><input type="hidden" class="form-control" id="taxSpecUpdate'+i+'" name="taxSpecUpdate[]" value="0" style="background:rgba(0,0,0,0);" ></div>'
                                    +'<p> <b style="padding-left:10px;">1.A.1 Admin & Finance</b><div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxAdminUpdate" name="taxAdminUpdate[]" value="'+child[i].aftax2+'" style="background:rgba(0,0,0,0);" ></div></div></p>'
                                    + '<div id="pshandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group"> <div class="form-line" id="1psFormUpdate'+i+'"> <input type="text" class="numberInternal form-control" value="'+child[i].afps+'" id="1psUpdate'+i+'" name="1psUpdate[]" style="background:rgba(0,0,0,0);"> <label>PS</label> </div> </div> </div></div> <div id="mooehandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1mooeFormUpdate'+i+'"> <input type="text" name="1mooeUpdate[]" id="1mooeUpdate'+i+'" value="'+child[i].afmooe+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>MOOE</label> </div> </div> </div></div>'
                                    + '<div id="cohandler'+i+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1coFormUpdate'+i+'"> <input type="text" name="1coUpdate[]" id="1coUpdate'+i+'" value="'+child[i].afco+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>CO</label> </div> </div> </div> </div> <div id="fehandler'+i+'" class="nopointerevent"> <div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1feFormUpdate'+i+'"> <input type="text" name="1feUpdate[]" id="1feUpdate'+i+'" value="'+child[i].affe+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>FE</label> </div> </div> </div> </div> </div>'
                                    + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.11 AMCFP-FRM</b><div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxAmcfpUpdate" name="taxAmcfpUpdate[]"  value="'+child[i].amtax2+'" placeholder="Tax Charges" style="background:rgba(0,0,0,0);" ></div></div> </p> <div id="2pshandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2psFormUpdate'+i+'"> <input type="text" name="2psUpdate[]" id="2psUpdate'+i+'" value="'+child[i].amps+'" style="background:rgba(0,0,0,0);" class="numberInternal form-control" > <label>PS</label> </div> </div> </div></div> <div id="2mooehandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2mooeFormUpdate'+i+'"> <input type="text" name="2mooeUpdate[]" id="2mooeUpdate'+i+'" value="'+child[i].ammooe+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div>  <div id="2cohandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2coFormUpdate'+i+'"> <input type="text" name="2coUpdate[]" id="2coUpdate'+i+'" value="'+child[i].amco+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div> </div> </div> </div>'
                                    + '<div class="row clearfix"> <p> <b style="padding-left:10px;">11.A.2 Operation-Reasearch</b> <div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxOperationUpdate" name="taxOperationUpdate[]" value="'+child[i].ostax2+'" style="background:rgba(0,0,0,0);" ></div></div></p> <div id="3pshandler'+i+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3psFormUpdate'+i+'"> <input type="text" name="3psUpdate[]" id="3psUpdate'+i+'" value="'+child[i].osps+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>PS</label> </div> </div> </div> </div> <div id="3mooehandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3mooeFormUpdate'+i+'"> <input type="text" name="3mooeUpdate[]" id="3mooeUpdate'+i+'" value="'+child[i].osmooe+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>MOOE</label> </div> </div> </div> </div>'
                                    + '<div id="3cohandler'+i+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3coFormUpdate'+i+'"> <input type="text" name="3coUpdate[]" id="3coUpdate'+i+'" value="'+child[i].osco+'" style="background:rgba(0,0,0,0);" class="number form-control" > <label>CO</label> </div> </div></div></div></div> </div>'        
                                    +'</td><td>'
                                    +'<button href="#" data-toggle="tooltip"  data-count='+i+' data-value="statusUpdateString'+i+'" data-status="1" data-placement="left" title="Remove Description" id="removelast" class="removeDescripUpdate btn-basic btn-circle"><i class="material-icons">remove</i></button>'
                                    +'</td></tr>'
                                );
                                }
                        start(child[i].expend_code, i);
                        internalValidation();
                        UpdateTypeValidation(child[i].afps, child[i].afmooe, child[i].afco, child[i].affe, i); 
                        }
                });
            
        });

    });
    //=======================================================================================================


             
         $(document).on("click", "#btnUpdateBudget", function () {
            swal({
                title: "Are you sure?",
                text: "It will save changes in your Data",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes,   Submit it!",
                cancelButtonText: "No, I changed my mind.",
                closeOnConfirm: false,
                closeOnCancel: false
                }, function (isConfirm) 
                    {
                        if (isConfirm) 
                        {
                            var formData = new FormData($("#updatebudget")[0]);
                            var theDate = $('#modalDate').val();
                             //console.log(theDate);
                              $.ajax({      
                                url: "UserConfig/updatebudget",
                                type: "POST",
                                data: formData,
                                cache : false,
                                contentType : false,
                                processData : false,
                                dataType: "json",
                                async: false,
                                success: function(json){
                                    message = json.ResponseResult
                 //                   console.log(message);
                                    if(message  != 'Update Successful'){
                                        swal(message, "error", "error");
                                    }else{
                                        swal("Success!", "update successfully", "success");
                                        var dateval = $("#modalDate").val()
                                        var arr = dateval.split("-");
                                        var monthval = arr[1];
                                        var yearval = arr[0];
                                        let dateRaw = new Date(yearval,monthval,1);
                                        var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                                        let dd = dateneed.getDate();
                                        let mm = dateneed.getMonth();
                                        let yyyy = dateneed.getFullYear();
                                        let sendPostData = new Object();        
                                        if (dd < 10) dd = '0' + dd
                                        if (mm < 10) mm = '0' + mm
                                        defDate = yyyy + '-' + mm + '-' + dd;
                                         // $('#autoTrigger').trigger('click');
                                         $("#myModalUpdate").modal("hide");
                                        // alert(defDate);
                                        monthyFunction(defDate); 
                      
                                      
                                    }
                                }
                            });
                        } 
                        else 
                        {
                            swal("Cancelled", "  has not been Record.", "error");
                        }
                });
            });
    //==========================================================================================================
    function deleteConfirm(id) {
      //  console.log(id);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this record",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, I changed my mind.",
                closeOnConfirm: false,
                closeOnCancel: false,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajaxSetup({async: false});
                    $.post( window.location + '/removeBudget', {
                        'refId': id
                    });
                    swal("Deleted!", "record has been deleted.", "success");
                    //swal("Success!", "added successfully", "success");
                        var dateval = $("#modalDate").val()
                        var arr = dateval.split("-");
                        var monthval = arr[1];
                        var yearval = arr[0];
                        let dateRaw = new Date(yearval,monthval,1);
                        var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                        let dd = dateneed.getDate();
                        let mm = dateneed.getMonth();
                        let yyyy = dateneed.getFullYear();
                        let sendPostData = new Object();        
                        if (dd < 10) dd = '0' + dd
                        if (mm < 10) mm = '0' + mm
                        defDate = yyyy + '-' + mm + '-' + dd;
                        //alert(defDate)
                        monthyFunction(defDate); 
                } else {
                    swal("Cancelled", "Record has not been deleted.", "error");
                }

            });
        }

        $(document).on("click", ".deleteReq", function () {
            var refId = $(this).attr("value");
            deleteConfirm(refId);
           // alert(refId);
        });

    //==========================================================================================================

        $(document).on("click","#btnreset",function(){
                AddReset();
        });
    //==========================================================================================================

        $(document).on("click","#btnresetUpdate",function(){
            //console.log('gumagana sa una');
            UpdateReset();
        });


    //==========================================================================================================function AddReset(){
       function AddReset(){
        $(document).on("click","#btnreset",function(){
            $("#empl").val('');
            $("#tax").val(0);
            $(".taxClass").val(0);
            $("#cur").val(0);
            $("#refid").val(0);
            $("#1ps0").val(0);
            $("#1co0").val(0);
            $("#1mooe0").val(0);
            $("#1fe0").val(0);
            $("#2ps0").val(0);
            $("#2co0").val(0);
            $("#2mooe0").val(0);
            $("#3ps0").val(0);
            $("#3co0").val(0);
            $("#3mooe0").val(0);
             $( "#1psForm0" ).removeClass( "focused" );
             $( "#1coForm0" ).removeClass( "focused" );
             $( "#1mooeForm0" ).removeClass( "focused" );
             $( "#1feForm0" ).removeClass( "focused" );
             $( "#2psForm0" ).removeClass( "focused" );
             $( "#2coForm0" ).removeClass( "focused" );
             $( "#2mooeForm0" ).removeClass( "focused" );
             $( "#3psForm0" ).removeClass( "focused" );
             $( "#3mooeForm0" ).removeClass( "focused" );
             $( "#3coForm0" ).removeClass( "focused" );
        
        $(".removeDescrip").parent().parent().remove();  
        });
    }

    //==========================================================================================================

       function UpdateReset(){

            $("#emplUpdate").val('');
            $("#taxUpdate").val(0);
            //$("#DateCurUpdate").val(0);
            $("#refid2Update").val(0);
            $("#1psUpdate1").val(0);
            $("#1coUpdate1").val(0);
            $("#1mooeUpdate1").val(0);
            $("#1feUpdate1").val(0);
            $("#2psUpdate1").val(0);
            $("#2coUpdate1").val(0);
            $("#2mooeUpdate1").val(0);
            $("#3psUpdate1").val(0);
            $("#3coUpdate1").val(0);
            $("#3mooeUpdate1").val(0);
             $( "#1psFormUpdate1" ).removeClass( "focused" );
             $( "#1coFormUpdate1" ).removeClass( "focused" );
             $( "#1mooeFormUpdate1" ).removeClass( "focused" );
             $( "#1feFormUpdate1" ).removeClass( "focused" );
             $( "#2psFormUpdate1" ).removeClass( "focused" );
             $( "#2coFormUpdate1" ).removeClass( "focused" );
             $( "#2mooeFormUpdate1" ).removeClass( "focused" );
             $( "#3psFormUpdate1" ).removeClass( "focused" );
             $( "#3mooeFormUpdate1" ).removeClass( "focused" );
             $( "#3coFormUpdate1" ).removeClass( "focused" );
            $(".statusGroup").val('INACTIVE');
            $("#statusUpdateString1").val('ACTIVE');
        $(".removeDescripUpdate").parent().parent().hide();  
       
        
    }

    //==========================================================================================================
    $( "#successMssg" ).hide();
    $( "#errorMssg" ).hide();
      
    $(document).on("click","#alterBudget",function(e){

         //e.preventDefault();
           thedata =  $('#addBudget').find(".codesAdd").val();
            $empty = $('#addBudget').find(".codesAdd").filter(function() {
            return this.value === "";
            });

            if ($empty.length > 0) {
               // alert('error men ===='+thedata);
            }else{
               // alert('success men men ===='+thedata);
                  row_content = $('#descriptTable #descriptBody .firstrow').html();
      //      console.log(row_content);
            $('#descriptTable #descriptBody').append('<tr>'+row_content+'</tr>');
            last_row = $('#descriptTable #descriptBody tr:last');
      
            tbody_length = $('#descriptTable #descriptBody').find('tr').length
            addedIndex = tbody_length - 1;
           
            minus_button = '<button href="#" data-toggle="tooltip" data-placement="left" title="Remove Description" class="removeDescrip btn-basic btn-circle"><i class="material-icons">remove</i></button>';
            last_row.find(".action").html(minus_button);
          

            expendituresModule = '<div class="rowtoClear"><div class="row clearfix"><div class="col-md-12"><div class="form-group form-float"><div class="form-line" id="categories">'
                                    + '<select class="codesAdd form-control" number-count='+addedIndex+' id="theCode'+addedIndex+'" name="theCode[]" style="background:rgba(0,0,0,0);">'
                                    + '<option value="0" disabled>Select Code</option></select>'
                                    + '</div></div></div>'
                                    +'<input type="hidden" class="taxClass form-control" id="taxSpec" name="taxSpec[]" placeholder="Tax Charges" value="0" style="background:rgba(0,0,0,0);" required><b style="">1.A.1 Admin & Finance</b> <div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxAdmin'+addedIndex+'" name="taxAdmin[]" placeholder="Tax Charges" value="0" style="background:rgba(0,0,0,0);"></div></div>'
                                    + '<div id="pshandlerIn'+addedIndex+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group"> <div class="form-line" id="1psForm'+addedIndex+'"> <input type="text" class="numberInternal form-control" value="0" id="1ps'+addedIndex+'" name="1ps[]" > <label>PS</label> </div> </div> </div> </div> <div id="mooehandlerIn'+addedIndex+'" class="nopointerevent"> <div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1mooeForm'+addedIndex+'"> <input type="text" name="1mooe[]" id="1mooe'+addedIndex+'" class="numberInternal form-control" > <label>MOOE</label> </div> </div> </div> </div>'
                                    + '<div id="cohandlerIn'+addedIndex+'" class="nopointerevent"><div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1coForm'+addedIndex+'"> <input type="text" name="1co[]" id="1co'+addedIndex+'" class="numberInternal form-control" > <label>CO</label> </div> </div> </div> </div> <div id="fehandlerIn'+addedIndex+'" class="nopointerevent"> <div class="col-md-3"> <div class="form-group form-float"> <div class="form-line" id="1feForm'+addedIndex+'"> <input type="text" name="1fe[]" id="1fe'+addedIndex+'" class="numberInternal form-control" > <label>FE</label> </div> </div> </div> </div> </div>'
                                    + '<div class="row clearfix" class="nopointerevent"> <b style="0">11.A.11 AMCFP-FRM</b> <div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxAmcfp'+addedIndex+'" value="0" name="taxAmcfp[]" placeholder="Tax Charges" style="background:rgba(0,0,0,0);" ></div></div>  <div id="2pshandlerIn'+addedIndex+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2psForm'+addedIndex+'"> <input type="text" name="2ps[]" id="2ps'+addedIndex+'" class="numberInternal form-control" > <label>PS</label> </div> </div> </div></div> <div id="2mooehandlerIn'+addedIndex+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="2mooeForm'+addedIndex+'"> <input type="text" name="2mooe[]" id="2mooe'+addedIndex+'" class="numberInternal form-control" > <label>MOOE</label> </div> </div> </div> </div> <div class="col-md-4"> <div class="form-group form-float"><div id="2cohandlerIn'+addedIndex+'" class="nopointerevent"> <div class="form-line" id="2coForm'+addedIndex+'"> <input type="text" name="2co[]" id="2co'+addedIndex+'" class="numberInternal form-control" > <label>CO</label> </div> </div> </div> </div> </div>'
                                    + '<div class="row clearfix">  <b style="0">11.A.2 Operation-Reasearch</b> <div class="input-group input-group-md"><span class="input-group-addon"><i class="material-icons">credit_card</i></span><div class="form-line"><input type="text" class="taxClass form-control" id="taxOperation'+addedIndex+'" name="taxOperation[]" placeholder="Tax Charges" value="0" style="background:rgba(0,0,0,0);" ></div></div>  <div id="3pshandlerIn'+addedIndex+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3psForm'+addedIndex+'"> <input type="text" name="3ps[]" id="3ps'+addedIndex+'" class="numberInternal form-control" > <label>PS</label> </div> </div> </div> </div><div id="3mooehandlerIn'+addedIndex+'" class="nopointerevent"> <div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3mooeForm'+addedIndex+'"> <input type="text" name="3mooe[]" id="3mooe'+addedIndex+'" class="numberInternal form-control" > <label>MOOE</label> </div> </div> </div> </div>'
                                    + '<div id="3cohandlerIn'+addedIndex+'" class="nopointerevent"><div class="col-md-4"> <div class="form-group form-float"> <div class="form-line" id="3coForm'+addedIndex+'"> <input type="text" name="3co[]" id="3co'+addedIndex+'" class="number form-control" > <label>CO</label> </div> </div> </div> </div></div></div>';        

            last_row.find("#handler").html(expendituresModule);
            $(".numberInternal").css("background-color", "rgba(255,255,255,0)");
        
            $(".codesAdd").change(function(e){
                 let count = $(this).attr('number-count');
                 InsertTypeValidation(count);
                 internalValidation();
              });
            selectTagAlter(addedIndex);
            }
            $(this).tooltip('hide')

          
    });

    $(document).on("submit","#addBudget",function(e){
         e.preventDefault();
    });

     
    //===========================================================================================================

          
             $(".codesAdd").click(function(e){
                 let count = $(this).attr('number-count');
                 InsertTypeValidation(count);
              });



             function InsertTypeValidation(count){
            internalValidation();

            // $(".taxClass"+count).val(0);
             $( "#pshandlerIn"+count).addClass( "nopointerevent" );
             $( "#mooehandlerIn"+count).addClass( "nopointerevent" );
             $( "#cohandlerIn"+count).addClass( "nopointerevent" );
             $( "#fehandlerIn"+count).addClass( "nopointerevent" );
             $( "#2pshandlerIn"+count).addClass( "nopointerevent" );
             $( "#2mooehandlerIn"+count).addClass( "nopointerevent" );
             $( "#2cohandlerIn"+count).addClass( "nopointerevent" );
             $( "#3pshandlerIn"+count).addClass( "nopointerevent" );
             $( "#3mooehandlerIn"+count).addClass( "nopointerevent" );
             $( "#3cohandlerIn"+count).addClass( "nopointerevent" );

                $("#1ps"+count).val(0); 
                $("#1mooe"+count).val(0); 
                $("#1co"+count).val(0);
                $("#1fe"+count).val(0);
                $("#2ps"+count).val(0); 
                $("#2mooe"+count).val(0);
                $("#2co"+count).val(0);
                $("#3ps"+count).val(0); 
                $("#3co"+count).val(0); 
                $("#3mooe"+count).val(0);

            var selectedVal = $('#theCode'+count).val();

            $("#1ps"+count).val(0); 
            $("#1mooe"+count).val(0); 
            $("#1co"+count).val(0);
            $("#1fe"+count).val(0);
            $("#2ps"+count).val(0); 
            $("#2mooe"+count).val(0);
            $("#2co"+count).val(0);
            $("#3ps"+count).val(0); 
            $("#3co"+count).val(0); 
            $("#3mooe"+count).val(0); 

        if(selectedVal <= 5010499099 && selectedVal >= 5010101000){
                $("#1ps"+count).removeAttr("disabled");
                $("#1ps"+count).val("0");
                $("#2ps"+count).removeAttr("disabled"); 
                $("#2ps"+count).val("0");
                $("#3ps"+count).removeAttr("disabled");
                $("#3ps"+count).val("0");
                $("#1psForm"+count).addClass('focused');
                $("#2psForm"+count).addClass('focused');
                $("#3psForm"+count).addClass('focused');

                $( "#pshandlerIn"+count).removeClass( "nopointerevent" );
                $( "#2pshandlerIn"+count).removeClass( "nopointerevent" );
                $( "#3pshandlerIn"+count).removeClass( "nopointerevent" );

                 $( "#1mooeForm"+count).removeClass( "focused" );
                 $( "#1coForm"+count).removeClass( "focused" );
                 $( "#1feForm"+count).removeClass( "focused" );
                 $( "#2mooeForm"+count).removeClass( "focused" );
                 $( "#2coForm"+count).removeClass( "focused" );
                 $( "#3mooeForm"+count).removeClass( "focused" );
                 $( "#3coForm"+count).removeClass( "focused" );

            }else if(selectedVal <= 5029907000 && selectedVal >= 5020101000){
            //}else if(selectedVal <= 5029299000 && selectedVal >= 5020101000){

                $("#1mooe"+count).removeAttr("disabled");
                $("#1mooe"+count).val("0");
                $("#2mooe"+count).removeAttr("disabled");
                $("#2mooe"+count).val("0"); 
                $("#3mooe"+count).removeAttr("disabled");
                $("#3mooe"+count).val("0");
                $("#1mooeForm"+count).addClass('focused');
                $("#2mooeForm"+count).addClass('focused');
                $("#3mooeForm"+count).addClass('focused');

                $( "#mooehandlerIn"+count).removeClass( "nopointerevent" );
                $( "#2mooehandlerIn"+count).removeClass( "nopointerevent" );
                $( "#3mooehandlerIn"+count).removeClass( "nopointerevent" );

                 $( "#1psForm"+count).removeClass( "focused" );
                 $( "#1coForm"+count).removeClass( "focused" );
                 $( "#1feForm"+count).removeClass( "focused" );
                 $( "#2psForm"+count).removeClass( "focused" );
                 $( "#2coForm"+count).removeClass( "focused" );
                 $( "#3psForm"+count).removeClass( "focused" );
                 $( "#3coForm"+count).removeClass( "focused" );
            

            }else if(selectedVal > 5029299000 && selectedVal < 5060201000){
            //}else if(selectedVal > 5029299000 && selectedVal < 5060201000){
                $("#1fe"+count).removeAttr("disabled");
                $("#1fe"+count).val("0");
                $("#1feForm"+count).addClass('focused');

                $( "#fehandlerIn"+count).removeClass( "nopointerevent" );

                 $( "#1psForm"+count).removeClass( "focused" );
                 $( "#1mooeForm"+count).removeClass( "focused" );
                 $( "#1coForm"+count).removeClass( "focused" );
                 $( "#2psForm"+count).removeClass( "focused" );
                 $( "#2coForm"+count).removeClass( "focused" );
                 $( "#2mooeForm"+count).removeClass( "focused" );
                 $( "#3psForm"+count).removeClass( "focused" );
                 $( "#3mooeForm"+count).removeClass( "focused" );
                 $( "#3coForm"+count).removeClass( "focused" );

                //  $( "#3coForm" ).removeClass( "focused" );
            }else if(selectedVal >= 5060201000 && selectedVal <= 5060602000){
            //}else if(selectedVal >= 5060201000 && selectedVal <= 5060406000){
                /*alert('asd');*/       
                $("#1co"+count).removeAttr("disabled");
                $("#1co"+count).val("0");
                $("#2co"+count).removeAttr("disabled");
                $("#2co"+count).val("0"); 
                $("#3co"+count).removeAttr("disabled");
                $("#3co"+count).val("0");
                $("#1coForm"+count).addClass('focused');
                $("#2coForm"+count).addClass('focused');
                $("#3coForm"+count).addClass('focused');

                $( "#cohandlerIn"+count).removeClass( "nopointerevent" );
                $( "#2cohandlerIn"+count).removeClass( "nopointerevent" );
                $( "#3cohandlerIn"+count).removeClass( "nopointerevent" );

                 $( "#1psForm"+count).removeClass( "focused" );
                 $( "#1mooeForm"+count).removeClass( "focused" );
                 $( "#1feForm"+count).removeClass( "focused" );
                 $( "#2psForm"+count).removeClass( "focused" );
                 $( "#2mooeForm"+count).removeClass( "focused" );
                 $( "#3psForm"+count).removeClass( "focused" );
                 $( "#3mooeForm"+count).removeClass( "focused" );

            }else{

            }

            }















    //==========================================================================================================
    function UpdateTypeValidation(ps, mooe, co, fe, primary){ 
      
        if(ps > 0 ){
                 $( "#mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#cohandler"+primary).addClass( "nopointerevent" );
                 $( "#fehandler"+primary).addClass( "nopointerevent" );
                 $( "#2mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#2cohandler"+primary).addClass( "nopointerevent" );
                 $( "#3mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#3cohandler"+primary).addClass( "nopointerevent" );
            
        }if(mooe > 0){

                 $( "#pshandler"+primary).addClass( "nopointerevent" );
                 $( "#cohandler"+primary).addClass( "nopointerevent" );
                 $( "#fehandler"+primary).addClass( "nopointerevent" );
                 $( "#2pshandler"+primary).addClass( "nopointerevent" );
                 $( "#2cohandler"+primary).addClass( "nopointerevent" );
                 $( "#3pshandler"+primary).addClass( "nopointerevent" );
                 $( "#3cohandler"+primary).addClass( "nopointerevent" );

        }if(co > 0){
                 $( "#pshandler"+primary).addClass( "nopointerevent" );
                 $( "#mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#fehandler"+primary).addClass( "nopointerevent" );
                 $( "#2pshandler"+primary).addClass( "nopointerevent" );
                 $( "#2mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#3pshandler"+primary).addClass( "nopointerevent" );
                 $( "#3mooehandler"+primary).addClass( "nopointerevent" );

        }if(fe > 0){
                $( "#pshandler"+primary).addClass( "nopointerevent" );
                 $( "#mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#cohandler"+primary).addClass( "nopointerevent" );
                 $( "#2pshandler"+primary).addClass( "nopointerevent" );
                 $( "#2mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#2cohandler"+primary).addClass( "nopointerevent" );
                 $( "#3pshandler"+primary).addClass( "nopointerevent" );
                 $( "#3mooehandler"+primary).addClass( "nopointerevent" );
                 $( "#3cohandler"+primary).addClass( "nopointerevent" );
        }        


            $(".codesAddUpdate").click(function(e){
                 let count = $(this).attr('number-countupdate');
                $("#1psUpdate"+count).val(0); 
                $("#1mooeUpdate"+count).val(0); 
                $("#1coUpdate"+count).val(0);
                $("#1feUpdate"+count).val(0);
                $("#2psUpdate"+count).val(0); 
                $("#2mooeUpdate"+count).val(0);
                $("#2coUpdate"+count).val(0);
                $("#3psUpdate"+count).val(0); 
                $("#3coUpdate"+count).val(0); 
                $("#3mooeUpdate"+count).val(0);

                 $( "#pshandler"+count).addClass( "nopointerevent" );
                 $( "#mooehandler"+count).addClass( "nopointerevent" );
                 $( "#cohandler"+count).addClass( "nopointerevent" );
                 $( "#fehandler"+count).addClass( "nopointerevent" );
                 $( "#2pshandler"+count).addClass( "nopointerevent" );
                 $( "#2mooehandler"+count).addClass( "nopointerevent" );
                 $( "#2cohandler"+count).addClass( "nopointerevent" );
                 $( "#3pshandler"+count).addClass( "nopointerevent" );
                 $( "#3mooehandler"+count).addClass( "nopointerevent" );
                 $( "#3cohandler"+count).addClass( "nopointerevent" );



    //====================================================================================================================================================================
    //====================================================================================================================================================================
    //====================================================================================================================================================================



            var selectedVal = $('#theCodeUpdate'+count).val();


          if(selectedVal <= 5010499099 && selectedVal >= 5010101000){
                $("#1psUpdate"+count).removeAttr("disabled");
                $("#1psUpdate"+count).val("0");
                $("#2psUpdate"+count).removeAttr("disabled"); 
                $("#2psUpdate"+count).val("0");
                $("#3psUpdate"+count).removeAttr("disabled");
                $("#3psUpdate"+count).val("0");
                $("#1psFormUpdate"+count).addClass('focused');
                $("#2psFormUpdate"+count).addClass('focused');
                $("#3psFormUpdate"+count).addClass('focused');

                $( "#pshandler"+count).removeClass( "nopointerevent" );
                $( "#2pshandler"+count).removeClass( "nopointerevent" );
                $( "#3pshandler"+count).removeClass( "nopointerevent" );

                 $( "#1mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#1coFormUpdate"+count).removeClass( "focused" );
                 $( "#1feFormUpdate"+count).removeClass( "focused" );
                 $( "#2mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#2coFormUpdate"+count).removeClass( "focused" );
                 $( "#3mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#3coFormUpdate"+count).removeClass( "focused" );


            }else if(selectedVal <= 5029907000 && selectedVal >= 5020101000){

                $("#1mooeUpdate"+count).removeAttr("disabled");
                $("#1mooeUpdate"+count).val("0");
                $("#2mooeUpdate"+count).removeAttr("disabled");
                $("#2mooeUpdate"+count).val("0"); 
                $("#3mooeUpdate"+count).removeAttr("disabled");
                $("#3mooeUpdate"+count).val("0"); 
                $("#1mooeFormUpdate"+count).addClass('focused');
                $("#2mooeFormUpdate"+count).addClass('focused');
                $("#3mooeFormUpdate"+count).addClass('focused');

                $( "#mooehandler"+count).removeClass( "nopointerevent" );
                $( "#2mooehandler"+count).removeClass( "nopointerevent" );
                $( "#3mooehandler"+count).removeClass( "nopointerevent" );

                 $( "#1psFormUpdate"+count).removeClass( "focused" );
                 $( "#1coFormUpdate"+count).removeClass( "focused" );
                 $( "#1feFormUpdate"+count).removeClass( "focused" );
                 $( "#2psFormUpdate"+count).removeClass( "focused" );
                 $( "#2coFormUpdate"+count).removeClass( "focused" );
                 $( "#3psFormUpdate"+count).removeClass( "focused" );
                 $( "#3coFormUpdate"+count).removeClass( "focused" );

            }else if(selectedVal > 5029299000 && selectedVal < 5060201000){
                $("#1feUpdate"+count).removeAttr("disabled");
                $("#1feUpdate"+count).val("0");
                $("#1feFormUpdate"+count).addClass('focused');

                $( "#fehandler"+count).removeClass( "nopointerevent" );

                 $( "#1psFormUpdate"+count).removeClass( "focused" );
                 $( "#1mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#1coFormUpdate"+count).removeClass( "focused" );
                 $( "#2psFormUpdate"+count).removeClass( "focused" );
                 $( "#2coFormUpdate"+count).removeClass( "focused" );
                 $( "#2mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#3psFormUpdate"+count).removeClass( "focused" );
                 $( "#3mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#3coFormUpdate"+count).removeClass( "focused" );

            }else if(selectedVal >= 5060201000 && selectedVal <= 5060602000){
                /*alert('asd');*/
                $("#1coUpdate"+count).removeAttr("disabled");
                $("#1coUpdate"+count).val("0");
                $("#2coUpdate"+count).removeAttr("disabled");
                $("#2coUpdate"+count).val("0"); 
                $("#3coUpdate"+count).removeAttr("disabled");
                $("#3coUpdate"+count).val("0");
                $("#1coFormUpdate"+count).addClass('focused');
                $("#2coFormUpdate"+count).addClass('focused');
                $("#3coFormUpdate"+count).addClass('focused');

                $( "#cohandler"+count).removeClass( "nopointerevent" );
                $( "#2cohandler"+count).removeClass( "nopointerevent" );
                $( "#3cohandler"+count).removeClass( "nopointerevent" );

                 $( "#1psFormUpdate"+count).removeClass( "focused" );
                 $( "#1mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#1feFormUpdate"+count).removeClass( "focused" );
                 $( "#2psFormUpdate"+count).removeClass( "focused" );
                 $( "#2mooeFormUpdate"+count).removeClass( "focused" );
                 $( "#3psFormUpdate"+count).removeClass( "focused" );
                 $( "#3mooeFormUpdate"+count).removeClass( "focused" );



            }else{

            }
                
            });
    }



    //===========================================================================================================

    $(document).on('click','.removeDescrip',function(e){
            e.preventDefault();
            $(this).parent().parent().remove();
            $(this).tooltip('hide')
        });
    $(document).on('click','.removeDescripUpdate',function(e){
            e.preventDefault();
      let status = $(this).data('value');
      let existingStatus = $(this).data('status'); 
      let counting = $(this).data('count'); 
      let optionthis = $(this).parent().parent();   

                swal({
                title: "Are you sure?",
                text: "The will be found in Data Recovery",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes,   Submit it!",
                cancelButtonText: "No, I changed my mind.",
                closeOnConfirm: false,
                closeOnCancel: false
                }, function (isConfirm) 
                    {
                        if (isConfirm) 
                        {

                            $('#'+status).val('INACTIVE');
                            if(existingStatus == 1){
                                alert(status);
                                let option = optionthis.hide();
                               // alert(existingStatus);
                                $(this).parent().append(
                                    +'<option value="'+option+'">inactive</option>'
                                    );
                                $(this).tooltip('hide');   //alert('bura talaga');
                            }else{
                                $("#handlerStat"+counting).remove(); 
                            }

                            swal("SUCCESS", "success", "success");
                                   
                        } 
                        else 
                        {
                            swal("Cancelled", "Record has not been Record.", "error");
                        }
                });
        

        });
    //===========================================================================================================

    function internalValidation(){
        

             $(".numberInternal").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
            }
           });


    }

    //===============================================================================================================
    $(document).on('click','#exportExcelButton',function(){
            var dateMonth = $(this).attr('data-holder');
    var newForm = $('<form>', {        
                'action':  window.location + '/exportExcel',
                'target': '_blank',
                'method': 'POST'    
            }).append($('<input>', {        
                'name': 'excelDate',       
                'value': dateMonth,        
                'type': 'hidden'    
            })).append($('<input>', {        
                'name': 'exportExceltxt',        
                'value': dateMonth,        
                'type': 'hidden'    
            }));

            newForm.appendTo('body').submit().remove();
          });
    //===============================================================================================================

        function addCommas(string) {
        return string.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
    }
    //===============================================================================================================

    });




