$(document).ready(function(){

	$("#frmChangePass").validate({
        rules: {
            "cnewpass": {
                equalTo: '[name="newpass"]'
            }
        },
        highlight: function (input) {
            console.log(input);
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
            $(element).parents('.form-group').append(error);
        },
        submitHandler: function(form){
            swal({
		        title: "Are you sure?",
		        text: "You want to change your password!",
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Yes, delete it!",
		        cancelButtonText: "No, cancel plx!",
		        closeOnConfirm: false,
		        closeOnCancel: false
		    }, function (isConfirm) {
		        if (isConfirm) {
		            swal("Success!", "Your password has been change please check your email.", "success");
		        } else {
		            swal("Cancelled", "", "error");
		        }
		    });
        }
    });
	
	$(document).on("click","#btnResetPass",function(){
		swal({
	        title: "Are you sure?",
	        text: "You want to reset your password!",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!",
	        cancelButtonText: "No, cancel plx!",
	        closeOnConfirm: false,
	        closeOnCancel: false
	    }, function (isConfirm) {
	        if (isConfirm) {
	            swal("Success!", "Your password has been reset please check your email.", "success");
	        } else {
	            swal("Cancelled", "", "error");
	        }
	    });
	});

});