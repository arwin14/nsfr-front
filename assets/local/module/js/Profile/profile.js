$(document).ready(function(){
	
	$(document).on("click","#btnResetPass",function(){
		swal({
	        title: "Are you sure?",
	        text: "You want to reset your password!",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!",
	        cancelButtonText: "No, cancel plx!",
	        closeOnConfirm: false,
	        closeOnCancel: false
	    }, function (isConfirm) {
	        if (isConfirm) {
	            swal("Success!", "Your password has been reset please check your email.", "success");
	        } else {
	            swal("Cancelled", "", "error");
	        }
	    });
	});

});