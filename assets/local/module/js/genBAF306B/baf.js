$(document).ready(function(){


$('.modal').on('hidden.bs.modal', function (e) {
    if($('.modal').hasClass('in')) {
    $('body').addClass('modal-open');
    }    
});



//=====================my validation======================================
$('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
         });
    
    function StartPage(){
        let date = $("#FindDate").val();
        // $('#exportExcelButton').attr("data-holder",date);
        BAF(date);
    }
    StartPage();

    $( "#btnDateSearch" ).click(function() {
        BAF($("#FindDate").val());
        // $('#exportExcelButton').attr("data-holder",$("#FindDate").val());
    });



//===========================================================================================================================================
    function BAF(date){
         //$('#loaders').show();
         $.post(window.location+ '/getBaf/',{'date':date}).done(function(res){
        let data = JSON.parse(res);
        console.log(data);
             $('#tblsummary').DataTable().clear();
            $('#tblsummary').DataTable().destroy();

                $('#tblsummary').DataTable({
                    pageLength: 50,
                    paging:   false,
                    ordering: false,
                    info:     false,
                    data: data,
                    fnRowCallback : function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                            $('td', nRow).css('background-color', '#FDF2E9');
                          
                        },
                    columns: [{
                            data: 'code'
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'amount'
                        },
                        {
                            data: 'acc'
                        },
                        {
                            data: 'weight'
                        }

                    ]
                });

        });
    }






//==============================================================================





//======================================================================================================

// $(document).on('click','#exportExcelButton',function(){
//         var dateMonth = $(this).attr('data-holder');
// var newForm = $('<form>', {        
//             'action':  window.location + '/exportExcel',
//             'target': '_blank',
//             'method': 'POST'    
//         }).append($('<input>', {        
//             'name': 'excelDate',       
//             'value': dateMonth,        
//             'type': 'hidden'    
//         })).append($('<input>', {        
//             'name': 'exportExceltxt',        
//             'value': dateMonth,        
//             'type': 'hidden'    
//         }));

//         newForm.appendTo('body').submit().remove();
//       });1


//=======================================================================================================
        function addCommas(string) {
        return string.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
    }
});




