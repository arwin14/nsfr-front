

$(document).ready(function(){
$(".alp").alphanum({
    allow :'-,qwertyuiopasdfghjklzxcvbnm', // Specify characters to allow
    disallow : '1234567890'  // Specify characters to disallow
});

$(".num").alphanum({
    allow :'1234567890.', // Specify characters to allow
    disallow : 'asdfghjklqwertyuiopzxcvbnm'  // Specify characters to disallow
});




$(document).on("click", ".btnUpdateAvail, .btnUpdateRequire", function () {
    var my = $(this)
    var code = my.data('code');
    var formName = my.data('form');
    switch(formName){

        case "updateAvail":
                $('.updateAvailStable').collapse('show');
                $('.updateReqStable').collapse('hide');
                $('#addAvail').collapse('show');
                $('#addReq').collapse('hide');
        break;

        case "updateRequire":
                $('.updateReqStable').collapse('show');
                $('.updateAvailStable').collapse('hide');
                $('#addReq').collapse('show');
                $('#addAvail').collapse('hide');
        break;

    }

    $('.updateUtilitiesSAOB').collapse('show');
    $('.updateUtilitiesFAR').collapse('hide');

    $.post(window.location+ '/getUpdateAvail/',{ 'id': code }).done(function(res){
            let result = JSON.parse(res);
            response = result.ResponseResult[0];
            expendTypeCount = response.expend_type - 1;
                 $(my.data('name')).val(response.name);
                 $(my.data('codeavail')).val(response.code);
                 $(my.data('categ')).val(response.expend_type);
                 $(my.data('factor')).val(response.factor);
                 $(my.data('id')).val(code);

               combobox.selectUpdate(my.data('categ'));


    });  
      
});

$(document).on("click", "#btnsaob, #btnfar", function () {
    var my = $(this);
    var formName = my.data('form');
    switch(formName){

        case "addavail":
                $('#addAvail').collapse('show');
                $('#addReq').collapse('hide');
        break;

        case "addreq":
        
                $('#addReq').collapse('show');
                $('#addAvail').collapse('hide');
        break;
    }
    combobox.selectAdd(my.data('addcateg'));
});

function pageStat(){
    $("#btnsaob").trigger('click');
}

pageStat();



$(document).on("submit","#updateexpendReq, #updateexpendAvail",function(e){
    e.preventDefault();
    var my = $(this)
         $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {
                      var formData = new FormData($(my.data('form'))[0]);
                      var tableName = my.data('table');
                      console.log(formData);
                      var url = my.data('direct');
                      fields.updateItemAvail(formData, tableName, url)},
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });
});



$(document).on("submit","#addexpendReq, #addexpendAvail",function(e){
    e.preventDefault();
    var my = $(this)
         $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {
                      var formData = new FormData($(my.data('form'))[0]);
                      console.log(formData);
                      var url = my.data('direct');
                      fields.addItem(formData, url)},
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });
});



$(document).on("click",".deleteReq, .deleteAvail",function(e){
    //e.preventDefault();
    var my = $(this)
         $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {
                      var id = my.data('code');
                      var url = my.data('direct');
                      fields.deleteItem(id, url)},
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });
});




















    $(document).on("click", ".btnUpdateFAR", function () {
       var code = $(this).data('code');
        $('.updateUtilitiesFAR').collapse('show');
         $('.updateUtilitiesSAOB').collapse('hide');

        $.post(window.location+ '/getUpdateFAR/',{ 'id': code }).done(function(res){
             let result = JSON.parse(res);
            response = result.ResponseResult[0];


                expendTypeCount = response.expend_type - 1;
                 $("#budgetNFAR").val(response.name);
                 $("#codeFAR").val(response.code);
                 $("#codeCategFAR").val(response.expend_type);
                // alert(response.expend_type);
                 $("#theidFar").val(code);

     


    });
    });



// //     function getoptionSAOB(){
// //        $.get(window.location + '/getOption').done(function(ret) {
// //       //console.log(ret);
// //        let value = $("#codeCategSAOB").val()
// //        let result = JSON.parse(ret);
// //                  let response = result.ResponseResult;
// //                  $.each(response, function (x, y) {
// //                     expendName = response[x].title;
// //                     expendcode = response[x].id;
// //                   //  console.log(expendcode);

// //                     if(value == expendcode){
// //                     status = 'selected';
// //                     }else{
// //                     status='';
// //                     }
// //                     $('#expenTypeSaob').append(
// //                     '<option value = "'+expendcode+'"'+status+'>'+expendName+'</option>'
// //                 );
// //         });    
// //      });
// //     }
// // getoptionSAOB();

    






// $(document).on("submit","#addexpendSaob",function(e){
//      e.preventDefault();
//      addnewExpend();
//      //alert('wew');
//      //alert("wew");

//      // let BudgetName = $("#expenNameSaob").val();
//      // let BudgetCode = $("#expenCodeSaob").val();
//      // let BudgetType = $("#expenTypeSaob").val();



//      //     if((BudgetCode >= 5010101000 && BudgetCode < 5020101000) && (1 == BudgetType)){
//      //        //alert("successPS----"+BudgetCode+" = "+BudgetType);
//      //        $("#saobErrorMessage").hide();
//      //        addnewExpend();
//      //     }else if((BudgetCode >= 5020101000 && BudgetCode < 5060201000) && (2 == BudgetType)){
//      //       // alert("successMOOE----"+BudgetCode+" = "+BudgetType);
//      //       addnewExpend();
//      //       $("#saobErrorMessage").hide();
//      //    }else if((BudgetCode >= 5030100000 && BudgetCode < 5060201000) && (4 == BudgetType)){
//      //       // alert("successFE-----"+BudgetCode+" = "+BudgetType);
//      //       $("#saobErrorMessage").hide();
//      //    }else if((BudgetCode >= 5060201000 && BudgetCode < 5060602000) && (3 == BudgetType)){
//      //       // alert("successFE-----"+BudgetCode+" = "+BudgetType);
//      //       addnewExpend();
//      //       $("#saobErrorMessage").hide();
//      //    }else{
//      //        //alert(BudgetCode+" = "+BudgetType);
//      //        $("#saobErrorMessage").show();
//      //    }
//     });











//   function addnewExpend(){
//                  swal({
//             title: "Are you sure?",
//             text: "You will directly send the data in report",
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Yes,   Submit it!",
//             cancelButtonText: "No, I changed my mind.",
//             closeOnConfirm: false,
//             closeOnCancel: false
//             }, function (isConfirm) 
//                 {
//                     if (isConfirm) 
//                     {
//                          var formData = new FormData($("#addexpendSaob")[0]);
//                      //    console.log(formData);
//                           $.ajax({      
//                             url: "UserConfig/addNewExpend",
//                             type: "POST",
//                             data: formData,
//                             cache : false,
//                             contentType : false,
//                             processData : false,
//                             dataType: "json",
//                             async: false,
//                             success: function(json){
                               
//                          message = json.ResponseResult
//   //                              console.log(message);
//                                 //alert(JSON.stringify(json));
//                                 if(message  != 'Insert Successful'){
//                                     swal(message, "error", "error");
//                                 }else{
//                                     swal("Success!", "added successfully", "success");
//                                     dataformUtil();
                                
//                                 }
//                             }
//                         });
//                     } 
//                     else 
//                     {
//                         swal("Cancelled", "Record has not been Record.", "error");
//                     }
//             });
      
// }



//   function addnewExpendFar(){
//                  swal({
//             title: "Are you sure?",
//             text: "You will directly send the data in report",
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Yes,   Submit it!",
//             cancelButtonText: "No, I changed my mind.",
//             closeOnConfirm: false,
//             closeOnCancel: false
//             }, function (isConfirm) 
//                 {
//                     if (isConfirm) 
//                     {
//                          var formData = new FormData($("#addexpendFar")[0]);
//                      //    console.log(formData);
//                           $.ajax({      
//                             url: "UserConfig/addNewExpendFar",
//                             type: "POST",
//                             data: formData,
//                             cache : false,
//                             contentType : false,
//                             processData : false,
//                             dataType: "json",
//                             async: false,
//                             success: function(json){
                               
//                          message = json.ResponseResult
//   //                              console.log(message);
//                                 //alert(JSON.stringify(json));
//                                 if(message  != 'Insert Successful'){
//                                     swal(message, "error", "error");
//                                 }else{
//                                     swal("Success!", "added successfully", "success");
//                                     dataformUtilSB();
                                
//                                 }
//                             }
//                         });
//                     } 
//                     else 
//                     {
//                         swal("Cancelled", "Record has not been Record.", "error");
//                     }
//             });
      
// }




// function UpdateSaobExp(){
//                  swal({
//             title: "Are you sure?",
//             text: "You will directly send the data in report",
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Yes,   Submit it!",
//             cancelButtonText: "No, I changed my mind.",
//             closeOnConfirm: false,
//             closeOnCancel: false
//             }, function (isConfirm) 
//                 {
//                     if (isConfirm) 
//                     {
//                          var formData = new FormData($("#updateexpendSaob")[0]);
//                      //    console.log(formData);
//                           $.ajax({      
//                             url: "UserConfig/UpdateExpendSaob",
//                             type: "POST",
//                             data: formData,
//                             cache : false,
//                             contentType : false,
//                             processData : false,
//                             dataType: "json",
//                             async: false,
//                             success: function(json){
                               
//                          message = json.ResponseResult
//                                 if(message  != 'Update Successful'){
//                                     swal(message, "error", "error");
//                                 }else{
//                                     swal("Success!", "Update successfully", "success");
//                                     dataformUtil();
//                                 }
//                             }
//                         });
//                     } 
//                     else 
//                     {
//                         swal("Cancelled", "Record has not been Record.", "error");
//                     }
//             });
// }






// function UpdateFarExp(){
//                  swal({
//             title: "Are you sure?",
//             text: "You will directly send the data in report",
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Yes,   Submit it!",
//             cancelButtonText: "No, I changed my mind.",
//             closeOnConfirm: false,
//             closeOnCancel: false
//             }, function (isConfirm) 
//                 {
//                     if (isConfirm) 
//                     {
//                          var formData = new FormData($("#updateexpendFar")[0]);
//                      //    console.log(formData);
//                           $.ajax({      
//                             url: "UserConfig/UpdateExpendFar",
//                             type: "POST",
//                             data: formData,
//                             cache : false,
//                             contentType : false,
//                             processData : false,
//                             dataType: "json",
//                             async: false,
//                             success: function(json){
//                          message = json.ResponseResult
//                                 if(message  != 'Update Successful'){
//                                     swal(message, "error", "error");
//                                 }else{
//                                     swal("Success!", "Update successfully", "success");
//                                     dataformUtilSB();
//                                 }
//                             }
//                         });
//                     } 
//                     else 
//                     {
//                         swal("Cancelled", "Record has not been Record.", "error");
//                     }
//             });
// }





// function deleteConfirm(id) {
//   //  console.log(id);
//         swal({
//             title: "Are you sure?",
//             text: "You will not be able to recover this record",
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Yes, delete it!",
//             cancelButtonText: "No, I changed my mind.",
//             closeOnConfirm: false,
//             closeOnCancel: false,
//         }, function (isConfirm) {
//             if (isConfirm) {
//                 $.ajaxSetup({async: false});
//                 $.post( window.location + '/removeFarExpend', {
//                     'code': id
//                 });
//                 swal("Deleted!", "record has been deleted.", "success");
//                 dataformUtilSB();;
             
//             } else {
//                 swal("Cancelled", "Record has not been deleted.", "error");
//             }

//         });
//     }

//     $(document).on("click", ".deleteReqFar", function () {
//         //alert('wew');
//          var code = $(this).data('code');
//         deleteConfirm(code);
//        // alert(refId);
//     });



// function deleteConfirmSaob(id) {
//   //  console.log(id);
//   //alert(id);
//         swal({
//             title: "Are you sure?",
//             text: "You will not be able to recover this record",
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Yes, delete it!",
//             cancelButtonText: "No, I changed my mind.",
//             closeOnConfirm: false,
//             closeOnCancel: false,
//         }, function (isConfirm) {
//             if (isConfirm) {
//                 $.ajaxSetup({async: false});
//                 $.post( window.location + '/removeSaobExpend', {
//                     'code': id
//                 });
//                 swal("Deleted!", "record has been deleted.", "success");
//                 dataformUtil();
             
//             } else {
//                 swal("Cancelled", "Record has not been deleted.", "error");
//             }

//         });
//     }

//     $(document).on("click", ".deleteReqSaob", function () {
//         //alert('wew');
//          var code = $(this).data('code');
//         deleteConfirmSaob(code);
//        // alert(refId);
//     });











// $("#saobErrorMessage").hide();
// $("#farErrorMessage").hide();
// $("#addfar").collapse('hide');
// $(document).on("click", "#btnsaob", function () {
//          //alert('wew');
//          // $('#addsaob').fadeIn();
//          // $('#addfar').fadeOut();

//           $('#addsaob').collapse('show');
//           $('#addfar').collapse('hide');
//     });
// $(document).on("click", "#btnfar", function () {
//          $('#addfar').collapse('show');
//           $('#addsaob').collapse('hide');
//     });


// $(document).on("click", "#btnCancelFar", function () {
//          $('.updateUtilitiesFAR').collapse('hide');
//     });





//     $(document).on("click", "#btnCancel", function () {
//          $('.updateUtilitiesSAOB').collapse('hide');
//     });
//     $(document).on("click", "#btnCancelFar", function () {
//          $('.updateUtilitiesFAR').collapse('hide');
//     });









});




