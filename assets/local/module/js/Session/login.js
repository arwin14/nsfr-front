$(document).ready(function() {
	$(document).on('keypress','#username',function(event){
        if (event.which === 13) {
            $('#CHeckEmailBtn').trigger("click");
        }
    });

    $(document).on('keypress','#password',function(event){
        if (event.which === 13) {
            $('#loginBtn').trigger("click");
        }
    });

    $(document).on('click','.backitup',function(event){
        $("#CHeckEmailBtn").fadeIn();
        $(".usernamegroup").fadeIn();
		$(".passwordgroup").hide();
        $("#usernamevalidpane").html('<p class="text-center" id="putusernamehere">Please sign in</p>')
    });




	$(".passwordgroup").hide();
    

    $("#loginBtn").on('click', function(e) {
		e.preventDefault();
		var form = $("#frmLogin");
		if($('#frmLogin').valid()) {
			$(".loader").show();
			$("#loginBtn").hide();
			var data = form.serialize();
			$.ajax({
		        url: "Session/Validate",
	          	data: data,
	          	dataType: 'JSON',
	          	type: 'POST',
		        success: function (result) {
		        	console.log(result);
		            if (result.Code == 0) {
		            	location.reload();
		            }
		            else{
		            	$(".loader").hide();
						$("#loginBtn").show();
		            	ShowNotif(result.Message, "danger");
		            }
		        },
		        fail: function(){

		        },
		        error: function(err){
		            console.log(err);
		            $(".loader").hide();
					$("#loginBtn").show();
		        }
		    });
		} 
	});

    $(document).on('click','#CHeckEmailBtn',function(e){
		e.preventDefault();
		/*var emailValidator = $("#email");
		emailValidator.parsley().validate();*/
		if($('#username').valid()) {
			$(".loader").show();
			$("#CHeckEmailBtn").hide();


			var username = $('#username').val();

			/*console.log(username);*/
			$.ajax({
		        url: "Session/checkemail",
              	data: {username:username},
              	dataType: 'JSON',
              	type: 'POST',
		        success: function (result) {
		        	/*console.log(result);*/

		            if (result.StatusCode == 1) {

		            	$("#usernamevalidpane").html('<a class="backitup" href="#"><p class="text-center" id="putusernamehere">Hello! <b>' + username +'</b></p></a>');

		            	$(".loader").hide();
		            	$(".usernamegroup").hide();
		            	$(".passwordgroup").fadeIn();
		            	$('#password').focus();
		            }
		            else{
		            	$(".loader").hide();
						$("#CHeckEmailBtn").show();
		            	ShowNotif(result.ResponseResult, "danger")
		            }
		        },
		        fail: function(){

		        },
		        error: function(err){
		            console.log(err);
		            $(".loader").hide();
					$("#CHeckEmailBtn").show();
		        }
		    });
		} 
	});
});

function ShowNotif(message, typecolor) {
    /*type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    color = Math.floor((Math.random() * 6) + 1);*/

    $.notify({
        icon: "notifications",
        message: message

    }, {
        type: typecolor,
        timer: 3000,
        placement: {
            from: "top",
            align: "center"
        }
    });
}