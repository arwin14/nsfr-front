function initializefilterdates(curMonth, curYear){
	let d = new Date(),
    n = d.getMonth() + 1,
    y = d.getFullYear();
    limityear = curYear - 5;

	var monthN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var quarterN = ["First","Second","Third","Fourth"];
	var valN = ["01-01","04-01","07-01","10-01"];


    $("#datespec").empty();
    basetype = $("#basisspec").val();


    if (basetype == 0) {
    	$.each(monthN, function(i, v) {
	        let nummonth = parseInt(i) + 1;
	        let monthval = nummonth.toString();
	        let monthsNeed = (monthval.length > 1) ? nummonth : "0" + nummonth;

	        selected = "";

	        if (curMonth == i) {
	        	selected = "selected";
	        }
	        $("#datespec").append(
	            '<option value = "' + monthsNeed + '-01" '+ selected+ '>' + monthN[i] + '</option>'
	        );
	    });
    } else {
    	curquarter = (parseInt(curMonth) - 1) / 3 + 1;
    	curquarter = parseInt(curquarter);
    	

    	$.each(quarterN, function(i, v) {
    		a = curquarter - 1
    		selected = "";
    		if (a == i) {
    			selected = "selected";
    		}
    		$("#datespec").append(
	            '<option value = "' + valN[i] + '" '+ selected+ '>' + quarterN[i] + ' Quarter</option>'
	        );
    	});
    }

    for (var a = parseInt(limityear); a <= y; y--) {
    	selected = "";

    	console.log(parseInt(curYear));
        if (parseInt(curYear) == y) {
        	selected = "selected";
        }

        $("#year").append(
            '<option value = "' + y + '" '+ selected+ '>' + y + '</option>'
        );
    }
}

function buildbargraphs(tableid, labelholder, count){
	basetype = $("#basisspec").val();

	seriesholder = [];
    seriesholder.push(count);
    var data = {
      	labels: labelholder,
      	series: seriesholder
    };

    if (basetype == 0) {
    	var optionsBar = {
	    	onlyInteger: true,
	    	referenceValue: 100,
	    	seriesBarDistance: 15
    	};
    } else {
    	
    	setWidth = 0;
    	for (var i = 0; i < labelholder.length; i++) {
    		setWidth = setWidth + 38;
    	}

    	finwith = setWidth+'px';
    	console.log(finwith);
    	var optionsBar = {
	    	width: finwith,
	    	onlyInteger: true,
	    	referenceValue: 100,
	    	seriesBarDistance: 15
    	};
    }

    

    var responsiveOptions = [
      	['screen and (max-width: 640px)', {
        	seriesBarDistance: 5,
        	axisX: {
          		labelInterpolationFnc: function (value) {
            		return value[0];
          		}
        	}
      	}]
    ];

    Chartist.Bar('#'+tableid, data, optionsBar, responsiveOptions).on('draw', function(data) {
	  if(data.type === 'bar') {
	  	if ($("#basisspec").val() == 0) {
	  		data.element.attr({
		      style: 'stroke-width: 20px'
		    });
	  	} else {
	  		data.element.attr({
		      style: 'stroke-width: 15px'
		    });
	  	}
	    
	  }
	});
}


function getdashboarddata(){
	form = $("#filterform");
	data = new FormData(form[0]);
   	$.ajax({
        url: "getDashboardData",
        data: data,
        type:'POST',
        dataType: 'json',
        contentType: false,
        processData: false
    }).done(function(result){
    	$('#monthlabel').html($('#datespec option:selected').text());
        $('#yearlabel').html($('#year option:selected').text());

        
        if (result.Code == 100) {
        	nsfr = result.Data.NSFR;
        	asf = result.Data.ASF;
        	rsf = result.Data.RSF;
        	$('#appenderrordiv').fadeOut();
        } else {
            nsfr = [];
            asf = [];
            rsf = [];
            $('#appenderrordiv').fadeIn();
        }

        basetype = $("#basisspec").val();

        if (basetype == 0) {
        	//NSFR
	        label = [];
	        count = [];
	        for (var i = 0; i < nsfr.length; i++) {
	        	if (typeof nsfr[i].name === "undefined" || typeof nsfr[i].count === "undefined") {

	        	} else {
	        		namehold = nsfr[i].name.split('-');
	            	name = namehold[2];
	            	label.push(name);

	            	num = parseFloat(nsfr[i].count);
	            	count.push(num.toFixed(2));
	        	}
	        }

	        buildbargraphs('nsfrchart', label, count);

	        //ASF
	        label = [];
	        count = [];
	        for (var i = 0; i < asf.length; i++) {
	        	if (typeof asf[i].name === "undefined" || typeof asf[i].count === "undefined") {

	        	} else {
	        		namehold = asf[i].name.split('-');
	            	name = namehold[2];
	            	label.push(name);

	            	num = parseFloat(asf[i].count);
	            	count.push(num.toFixed(2));
	        	}
	        }

	        buildbargraphs('asfchart', label, count);

	        //RSF
	        label = [];
	        count = [];
	        for (var i = 0; i < rsf.length; i++) {
	        	if (typeof rsf[i].name === "undefined" || typeof rsf[i].count === "undefined") {

	        	} else {
	        		namehold = rsf[i].name.split('-');
	            	name = namehold[2];
	            	label.push(name);

	            	num = parseFloat(rsf[i].count);
	            	count.push(num.toFixed(2));
	        	}
	        }

	        buildbargraphs('rsfchart', label, count);
        } else {


        	var monthN = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        	label = [];
	        count = [];
	        for (var i = 0; i < nsfr.length; i++) {
	        	if (typeof nsfr[i].name === "undefined" || typeof nsfr[i].count === "undefined") {

	        	} else {
	        		namehold = nsfr[i].name.split('-');
	            	day = namehold[2];
	            	month = monthN[parseInt(namehold[1])-1];
	            	label.push(month+'\n'+day);

	            	num = parseFloat(nsfr[i].count);
	            	count.push(num.toFixed(2));
	        	}
	        }

	        buildbargraphs('nsfrchart', label, count);

	        //ASF
	        label = [];
	        count = [];
	        for (var i = 0; i < asf.length; i++) {
	        	if (typeof asf[i].name === "undefined" || typeof asf[i].count === "undefined") {

	        	} else {
	        		namehold = nsfr[i].name.split('-');
	            	day = namehold[2];
	            	month = monthN[parseInt(namehold[1])-1];
	            	label.push(month+'\n'+day);

	            	num = parseFloat(asf[i].count);
	            	count.push(num.toFixed(2));
	        	}
	        }

	        buildbargraphs('asfchart', label, count);

	        //RSF
	        label = [];
	        count = [];
	        for (var i = 0; i < rsf.length; i++) {
	        	if (typeof rsf[i].name === "undefined" || typeof rsf[i].count === "undefined") {

	        	} else {
	        		namehold = nsfr[i].name.split('-');
	            	day = namehold[2];
	            	month = monthN[parseInt(namehold[1])-1];
	            	label.push(month+'\n'+day);

	            	num = parseFloat(rsf[i].count);
	            	count.push(num.toFixed(2));
	        	}
	        }

	        buildbargraphs('rsfchart', label, count);
        }

        

        


    }).fail(function(){
    	swal('Error occured!', 'System is busy! Please contact your administrator.', "error");
    });
}

function testdata(){
	demo.initCharts();





	var data = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      series: [
        [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
        [412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
      ]
    };

    var options = {
        seriesBarDistance: 10,
        axisX: {
            showGrid: false
        },
        height: "245px"
    };

    var responsiveOptions = [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 5,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ];

    Chartist.Bar('#chartActivity', data, options, responsiveOptions);
    Chartist.Bar('#chartActivity1', data, options, responsiveOptions);
    Chartist.Bar('#chartActivity2', data, options, responsiveOptions);
}


$(document).ready(function() {
	$('#appenderrordiv').hide();

	let cur = new Date(),
    curn = cur.getMonth() + 1,
    cury = cur.getFullYear();

	initializefilterdates(cur.getMonth(), cur.getFullYear());
	getdashboarddata();

	$(document).on('click','#show_date_filter',function(e){
        e.preventDefault();
        
        $('#FilterContainer').fadeIn();
    });

    $(document).on('click','#closeit',function(e){
        e.preventDefault();
        $('#FilterContainer').fadeOut();
    });

    $(document).on('click','#submitfilter',function(e){
        e.preventDefault();
        getdashboarddata();
    });

    



});