function getFormData($form){
    var unindexed_array = $form.serializeArray();
    let user_id = $('#userid').val()
    var indexed_array = {};
    let returnvalue
    //let validateNum
    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

     $.ajax({

        type: "POST",
        url: "UserAccountConfig/ValidateChanges",
        data: {
            userId: user_id
        },
        dataType: "json",
        async:false,
        success: function (result) {
            returnvalue = result
    
        },

        error: function (result) {
           alert("error");
           retdata = "error";
        }
    });

console.log(JSON.stringify(returnvalue))
console.log(JSON.stringify(indexed_array))
let statmssg
statmssg = (JSON.stringify(returnvalue) == JSON.stringify(indexed_array))? "1":"0"
    return statmssg;
}


$(document).ready(function(){
//alert($('image_file').val());
    






  $(document).on("submit", "#accountInfo", function (e) {
    e.preventDefault();
     let formdata = new FormData($("#accountInfo")[0]);
    var $form = $("#accountInfo");
    var dataRender = getFormData($form);
    if(dataRender != "1"){
        $.confirm({
        title: 'Confirm!',
        content: 'Are You Sure?',
        buttons: {
        confirm: function() {
              console.log(formdata)
            $.ajax({
                                url: "UserAccountConfig/UpdateProfileAccount",
                                type: "POST",
                                data: formdata,
                                cache: false,
                                contentType: false,
                                processData: false,
                                dataType: "json",
                                async: false,
                                success: function (json) {
                                    message = json.ResponseResult
                                    if (message != 'Update Successful') {
                                        swal(message, "error", "error");
                                    } else {
                                        swal("Success!", "Update Successful", "success");
                                        $(".modal").modal("hide");
                                         monthyFunction(triggerDate); 

                                    }
                                }
                            });
                        },
                      cancel: function () {
                      $.alert('Canceled!');
                      }

                    }
                  });
    }else{
        $.alert("You Don't have any Changes");
    }
    

});


$('input[name=image_file]').change(function(e) {

 var formData = new FormData();
        formData.append('image', $('input[type=file]')[0].files[0]); 
        formData.append('id', $("#user_id").val());

   $.confirm({
                title: 'Confirm!',
                content: 'Are You Sure?',
                buttons: {
                  confirm: function() {
                  $.ajax({
                url: "UserAccountConfig/UploadProfile",
                type: "POST",
                data: formData,
                dataType: "json",
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(result){
                    if(result.data == true){
                        swal({
                            title: 'Successfully Updated!',
                            text: 'Your Profile Successfully Updated! Please Login your account Again.',
                            type: 'success',
                            confirmButtonClass: "btn btn-success btn-wd",
                            buttonsStyling: false
                        }).then(function() {
                            logout();
                        });
                    }
                    else{
                        swal("Error!","","danger");
                    }
                }
            });
                  },
                  cancel: function () {
                  $.alert('Canceled!');
                  }

                }
              });
   
  
});


function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });








});