$(document).ready(function(){
 $(document).on("click", "#btnAdd", function () {
    $("#displayModal").modal('show');

    $.ajax({
                type: "POST",
                url: window.location+"/addModal",
                dataType: "json",
                success: function (result) {
                    console.log(result.form);
                    $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-md');
                    $('#displayModal .modal-title').html('<i class="text-info"></i>');
                    $('#displayModal .modal-body').html(result.form);
                    
                      $("#imgInp").change(function() {
                      readURL(this);
                      });
                    $('#displayModal').modal('show');  

                    
                },
                error: function (result) {
                   alert("error");
                }
            });


       
});


   $('#AllotmentMssgModal').hide();
    $(document).on("submit", "#form_add_user", function (e) {
      e.preventDefault();
        $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {

                      var formData = new FormData($("#form_add_user")[0]);
                      console.log(formData);
                      var url = "addUser";
                      fields.addnew(formData, url)
                      tblUsers();

                },
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });
    });


    $(document).on("click", "#btnupdate", function () {
       // alert($(this).data('id'));

        $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {

                      var formData = new FormData($("#form_update_user")[0]);
                      console.log(formData);
                      var url = "updateUser";
                      fields.updateUser(formData, url)
                    tblUsers()
                   // $.alert('Confirmed!');
                },
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });
    });





function tblUsers() {
            //alert("me");
        $.post(window.location + '/getUser/').done(function(res) {

            let data = JSON.parse(res);
            console.log(data);
            $('#tblUser').DataTable().clear();
            $('#tblUser').DataTable().destroy();

                $('#tblUser').DataTable({
                  "headerCallback": function( thead, data, start, end, display ) {
                    $(thead).find('th').css('background-color', 'rgb(244,149,66)');
                    $(thead).find('th').css('color', 'white');
                    },
                    data: data,
                    columns: [{
                            data: 'id'
                        },
                        {
                            data: 'username'
                        },
                        {
                            data: 'userlevel'
                        },
                        {
                            data: 'fname'
                        },
                         {
                            data: 'lname'
                        },
                         {
                            data: 'email'
                        },
                         {
                            data: 'status'
                        },
                         {
                            data: 'btn'
                        }

                    ]
                 });

        });
    }

 tblUsers();



$(document).on("click", ".editbtn", function () {
    //$("#displayModal").modal('show');
    let id = $(this).data( "id" )

     $.ajax({
        type: "POST",
        url: "UserConfig/UpdateModal",
        data: {
            id: id
        },
        dataType: "json",
        async:false,
        success: function (result) {
          console.log(result.form);
                    $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-md');
                    $('#displayModal .modal-title').html('<i class="text-info">Update User</i>');
                    $('#displayModal .modal-body').html(result.form);
                    $('#displayModal').modal('show');  

        },
        error: function (result) {
           alert("error");
           retdata = "error";
        }
    });




       
});


$(document).on("click", ".deletebtn", function () {
    //$("#displayModal").modal('show');
    let baseid = $(this).data( "id" )
     $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {
                     $.ajax({
                        url: "UserConfig/deleteUser",
                        data: {
                            idbase : baseid
                        },
                        type: "POST",
                        dataType: "json",
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Deactivate Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Deleted!", "Deacitivated successfully", "success");
                                $(".modal").modal("hide");
                                 $('#displayModal').modal('hide')
                                 tblUsers()

                            }
                        }
                    });
                },
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });


       
});








});
function readURL(input) {
                      console.log(input)
                    if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                    $('.img-preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                    }
    }









