//    var tblusers;
// var table;
//     function tbluserlevelTBL() {
//             alert("date");
//         $.post(window.location + '/getuserlevel/').done(function(res) {
//             let data = JSON.parse(res);
//             console.log(data);
//             $('#userlevelTbl').DataTable().clear();
//             $('#userlevelTbl').DataTable().destroy();
//                 var table =  $('#userlevelTbl').DataTable({
//                     data: data,
//                     columns: [
//                         {
//                             data: 'id'
//                         },
//                         {
//                             data: 'name'
//                         },
//                         {
//                             data: 'description'
//                         },
//                         {
//                             data: 'status',
//                         }
//                     ]
//                 });
//             });
//         }
//         tbluserlevelTBL();
function InitData(){
  tbluserlevelTBL();
  formstart()

}
function tbluserlevelTBL() {
    $.post(window.location + '/getuserlevel/').done(function(res) {

        let data = JSON.parse(res);
        console.log(data);
        $('#userlevelTbl').DataTable().clear();
        $('#userlevelTbl').DataTable().destroy();

        var table = $('#userlevelTbl').DataTable({
            "headerCallback": function(thead, data, start, end, display) {
                $(thead).find('th').css('background-color', 'rgb(244,149,66)');
                $(thead).find('th').css('color', 'white');
            },
            data: data,
            columns: [{
                    data: 'btn'
                },
                {
                    data: 'id'
                },
                {
                    data: 'name'
                },
                {
                    data: 'description'
                },
                {
                    data: 'status',

                }

            ]
        });

    });
}

tbluserlevelTBL();


function formstart() {
    $.post(window.location + '/bootformStart').done(function(res) {
        let result = JSON.parse(res);
        $('#formcontent').html(result.form);
            $('#validateError').hide();

        $('#addroletbl').DataTable({
            "headerCallback": function(thead, data, start, end, display) {
                $(thead).find('th').css('background-color', 'rgb(244,149,66)');
                $(thead).find('th').css('color', 'white');
            },
            "bPaginate": false,
            "scrollY": '50vh',
            "scrollCollapse": true,
            'columnDefs': [{
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }]
        });
    });
}

function validatedata(data){
     var checkedBoxes = $(data).length
     stat = (checkedBoxes > 0)? "1":"0"
     
     return stat;


}




$(document).ready(function() {
    formstart();



    $(document).on("click", ".remove", function () {
    let baseid = $(this).data( "refid" )
     $.confirm({
            title: 'Confirm!',
            content: 'Are You Sure?',
            buttons: {
                confirm: function () {
                     $.ajax({
                        url: "UserLevelConfig/deleteUser",
                        data: {
                            idbase : baseid
                        },
                        type: "POST",
                        dataType: "json",
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Deactivate Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Deleted!", "Deacitivated successfully", "success");
                                tbluserlevelTBL()
                                
                            }
                        }
                    });
                },
                cancel: function () {
                    $.alert('Canceled!');
                }
               
            }
        });      
    });


    $(document).on("submit", "#adduserlevelrecordform", function(e) {
        e.preventDefault();
        var validate = validatedata('#addroletbl :input[type="checkbox"]:checked');
        if(validate == 1){
            $('#validateError').fadeOut()
            let my = $(this);
            $.confirm({
                title: '<label class="text-warning">Confirm</label>',
                content: 'Are you sure you want to save the changes?',
                type: 'orange',
                buttons: {
                    confirm: function() {
                        var formData = new FormData($(my.data('form'))[0]);
                        var FunctionName = my.data('function');
                        console.log(formData);
                        var url = my.data('direct');
                        fields.AddUserLevel(formData, FunctionName, url)
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                          $(".ckdata").prop("checked", false);
                    }

                }
            });
      }else{
           $('#validateError').fadeIn()
      }
    });


    $(document).on("submit","#updateuserlevelrecordform", function(e){
       e.preventDefault();
        var validate = validatedata('#updateroletbl :input[type="checkbox"]:checked');
        if(validate == 1){
            $('#updatevalidateError').fadeOut()
            let my = $(this);
            $.confirm({
                title: '<label class="text-warning">Confirm</label>',
                content: 'Are you sure you want to save the changes?',
                type: 'orange',
                buttons: {
                    confirm: function() {
                        var formData = new FormData($(my.data('form'))[0]);
                        var FunctionName = my.data('function');
                        console.log(formData);
                        var url = my.data('direct');
                        fields.updateUserLevel(formData, FunctionName, url)
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                          $(".ckdata").prop("checked", false);
                    }

                }
            });
      }else{
           $('#updatevalidateError').fadeIn()
      }
    })
 $(document).on('click', '#checkboxall, #checkboxallUpdate', function() {
      let me = $(this);
      let checkboxes = me.data('key')

        if (!$(this).prop('checked')) {
            $(checkboxes).prop("checked", false);
        } else {
            $(checkboxes).prop("checked", true);
        }
    });






    $(document).on("click", ".editbtn", function() {
        var me = $(this);
        var urlKey = me.data('direct');
        var id = me.data('refid');

        $.ajax({
            url: urlKey,
            data: {
                idbase: id
            },
            type: "POST",
            dataType: "json",
            success: function(result) {
                // console.log(result.form);
                $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                $('#displayModal .modal-title').html('<i class="text-info">Update Data</i>');
                $('#displayModal .modal-body').html(result.form);
                //UpdateForm(refid, type);
                $('#displayModal').modal('show');  

                  $('#updateroletbl').DataTable({
                  scrollY:        '50vh',
                  paging:         false
                  });
            },
            error: function(result) {
                alert("error");
            }
        });
    });




});