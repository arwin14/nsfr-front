$(document).ready(function() {


$('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
         });
    
    function StartPage(){
        let date = $("#FindDate").val();
       // $('#exportExcelButton').attr("data-holder",date);
        tblNSFR(date);
    }
    StartPage();

    $( "#btnDateSearch" ).click(function() {
        tblNSFR($("#FindDate").val());
        //$('#exportExcelButton').attr("data-holder",$("#FindDate").val());
    });

    //========================================================================================================
    function tblNSFR(date) {

        $("#loaders").show();
        //console.log(window.location+ '/getMonthlyData/');
        $.post(window.location + '/getDailyData/', {
            'date': date
        }).done(function(res) {

            console.log(res);
            let data = JSON.parse(res);
            $('#tlbmonthlyData').DataTable().clear();
            $('#tlbmonthlyData').DataTable().destroy();

                $('#tlbmonthlyData').DataTable({
                    data: data,
                    columns: [{
                            data: 'code'
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'amount'
                        }
                        // },
                        // {
                        //     data: 'btn'
                        // }

                    ],
                    "aaSorting": []

                });

        });
    }


    //==========================================================================================================
    function start(value, count) {
        $.get(window.location + '/getOptionData').done(function(ret) {
            let result = JSON.parse(ret);
            let response = result.ResponseResult;
            $.each(response, function(x, y) {
                expendName = response[x].name;
                expendcode = response[x].code;
                //  console.log(expendcode);

                if (value == expendcode) {
                    status = 'selected';
                } else {
                    status = '';
                }
                $('#expenditureTable').find('#theCodeUpdate' + count).append(
                    '<option value = "' + expendcode + '"' + status + '>' + expendName + '</option>'
                );
            });
        });
    }


    //===========================================================================================================

    function internalValidation() {


        $(".numberInternal").on("keypress keyup blur", function(event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });


        $(".taxClass").on("keypress keyup blur", function(event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });


    }

    //===============================================================================================================
    // $(document).on('click', '#exportExcelButton', function() {
    //     alert('wew');
    //     var dateMonth = $(this).attr('data-holder');
    //     var newForm = $('<form>', {
    //         'action': window.location + '/exportExcel',
    //         'target': '_blank',
    //         'method': 'POST'
    //     }).append($('<input>', {
    //         'name': 'excelDate',
    //         'value': dateMonth,
    //         'type': 'hidden'
    //     })).append($('<input>', {
    //         'name': 'exportExceltxt',
    //         'value': dateMonth,
    //         'type': 'hidden'
    //     }));

    //     newForm.appendTo('body').submit().remove();
    // });
    //===============================================================================================================

    function addCommas(string) {
        return string.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        })
    }
    //===============================================================================================================

});