$(document).ready(function () {

	var pathname = window.location.pathname; 
	var url = window.location.href; 
	urlSplit = url.split('/');
	urlSplit2 = urlSplit[4].split('/');
	urlSplit3 = urlSplit2[0].split('?');
	urlSplit4 = urlSplit3[1].split('=');
	id = urlSplit4[1];

	var baseurl = "http://localhost/UserManagement/";
    $( "#formChangeForgot" ).validate({
		rules: {
			newPass: "required",
			reTypePass: {
			  equalTo: "#newPass"
			}
		},
		submitHandler: function(form) {
			var formData = new FormData($("#formChangeForgot")[0]);
			$.ajax({
                url: baseurl + "forgotChangePassword/forgotChangePassword/submitChangePassword",
                type: "POST",
                data: {
                	id: id,
                	newPass: $("#newPass").val()
                },
                dataType: "json",
                success: function(json){
                	console.log(json.data.ResponseResult);
                    if(json.data.ResponseResult == "Forgot Password Success|Please check your email. Thankyou"){
                    	swal({
				            title: "Success!",
				            text: "Forgot Password Success | Please check your email. Thankyou!",
				            type: "success"
				        }, function() {
				            window.location = baseurl;
				        });
                    }
                    else{
                    	alert("error");
                    }
                }
            });
		}
	});

});


