$(document).ready(function(){
$('.modal').on('hidden.bs.modal', function (e) {
    if($('.modal').hasClass('in')) {
    $('body').addClass('modal-open');
    }    
});



//=====================my validation======================================

         $(".number").on("keypress keyup blur",function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
        }
        });


        $("#property_number").alphanum({
        allow :'1234567890-qwertyuiopasdfghjklzxcvbnm', // Specify characters to allow
        disallow : ''  // Specify characters to disallow
        });

        $("#btnupdate").hide();
        $("#disabler").click(function(){
            $(".handler").toggleClass("nopointerevent");
            $("#disabler").fadeOut("fast");
            $("#btnupdate").fadeIn("fast");
        });

//========================= my date Generator  ============================= 

            $('#MonthYear').bootstrapMaterialDatePicker({
                format: 'MM/DD/YYYY',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });
            $('#searchDate').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });
            $('#modalDate').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });


//===================================================================================================================================
            $(document).on("click","#submitDate",function(){
                    let thedate = $('#searchDate').val();
                    $("#exportExcelButton").attr({
                		"data-holder" : thedate
                	});
                    getNCA(thedate);      
                    getAllot(thedate);
                    monthyFunction(thedate);

            });
//===================================================================================================================================

//===================================================================================================================================
function pageStart(){
        $('#submitDate').trigger('click');
        }
pageStart();


//======================================================================================================
//alert('weq');
function getNCA(date){
    $("#formloaderNCA").show();

       theDate = date;
    $.post(window.location+ '/getNCA/',{'date':theDate}).done(function(res){
      // console.log(window.location+ '/getNCA/',{'date':theDate});
        $("#formloaderNCA").hide();
        let result = JSON.parse(res);
        //console.log(result);
        $('#NCAtbl').DataTable().clear();
        $('#NCAtbl').DataTable().destroy();
        let Response = result.ResponseResult;
        if(Response == "NO DATA FOUND"){
                 $("#formloaderNCA").hide();

                 $('#NCAtbl').DataTable({
                "scrollY": 300,
                "scrollX": true,
                dom: 'Bfrtip',
                fixedHeader: {
                    header: true,
                    footer: false
                },
                responsive: true,
                buttons: []
            });
        }else{
        // $.each(Response, function (i, v) {
        $.each(Response.sort(function(x, y) {
            return y.id - x.id;
         }), function (i, y) {
             child = Response[i].ncaContentList; 
             $("#UpdateNCADate").val(Response[i].dateCreated);

             $('#NCAtbl').find('tbody').append(
                            '<tr>'+ 
                                '<td align="center">' +
                                '<button type="button" class="restoreNCAReq bg-teal btn-block btn-sm waves-effect" data-refid="'+Response[i].refid+'">Restore</button>'+
                            '</td>' +
                            '<td>'+addCommas(child[0].Jan)+'</td>'+
                            '<td>'+addCommas(child[0].Feb)+'</td>'+
                            '<td>'+addCommas(child[0].Mar)+'</td>'+
                            '<td>'+addCommas(child[0].Apr)+'</td>'+
                            '<td>'+addCommas(child[0].May)+'</td>'+
                            '<td>'+addCommas(child[0].Jun)+'</td>'+
                            '<td>'+addCommas(child[0].Jul)+'</td>'+
                            '<td>'+addCommas(child[0].Aug)+'</td>'+
                            '<td>'+addCommas(child[0].Sep)+'</td>'+
                            '<td>'+addCommas(child[0].Oct)+'</td>'+
                            '<td>'+addCommas(child[0].Nov)+'</td>'+
                            '<td>'+addCommas(child[0].Dec)+'</td>'+
                            '<td>'+addCommas(child[0].NCATotal)+'</td>'+
                            '</tr>'
                            );
        });
    $('#NCAtbl').DataTable({
                
                responsive: true,
                buttons: [

                ]
            });
}
    });
}

//======================================================================================================
function getAllot(date) {
        $('#exportExcelButton').hide();

        $("#formloaderAllot").show();
        $('#ALLOTMENTHandler').hide();
        $.post(window.location + '/getAllot/', {
            'date': date
        }).done(function (res) {

            let result = JSON.parse(res);
           // console.log(result.ResponseResult[0].refid);
            $('#tblallotment').DataTable().clear();
            $('#tblallotment').DataTable().destroy();
            //console.log(result.ResponseResult);
            let resResult = result.ResponseResult;

            if(resResult == "NO DATA FOUND"){
            
                 $("#formloaderAllot").hide();
                $('#ALLOTMENTHandler').show();
                 $('#tblallotment').DataTable({
                "scrollY": 300,
                "scrollX": true,
                dom: 'Bfrtip',
                fixedHeader: {
                    header: true,
                    footer: false
                },
                responsive: true,
                buttons: [

                ]
            });
            }else{
            $.each(result.ResponseResult, function (i, v) {
                var child = result.ResponseResult[i];
                $('#tblallotment').find('tbody').append(
                    '<tr>' +
                    '<td>' +
                     '<button type="button" class="restoreAllotmentReq bg-teal btn-block btn-sm waves-effect" data-refid="'+child.refid+'">Restore</button>'+'</td>' +
                    '<td>' + child.expend_type + '</td>' +
                    '<td>' + child.fund_category + '</td>' +
                    '<td>' + child.dateCreated+ '</td>' +
                    '</tr>');

                $("#formloaderAllot").hide();
                $('#ALLOTMENTHandler').show();
            });
             $('#tblallotment').DataTable({
               
                responsive: true,
                buttons: [

                ]
            });
            }

        });


    }

//======================================================================================================
 function monthyFunction(params){ 
        $('#exportExcelButton').hide();


   
    theDate = params;

         $("#formloaderOrs").show();
         $("#ORSHandler").hide();
          $.post(window.location+ '/getORS/',{'date':theDate}).done(function(res){
        let result = JSON.parse(res);
            console.log(result);
        let resResult = result.ResponseResult;


        $('#tblOrs').DataTable().clear();
        $('#tblOrs').DataTable().destroy();

         if(resResult == "NO DATA FOUND"){
            
                 $("#formloaderOrs").hide();
                $("#ORSHandler").show();
                 $('#tblOrs').DataTable({
                "scrollY": 300,
                "scrollX": true,
                dom: 'Bfrtip',
                fixedHeader: {
                    header: true,
                    footer: false
                },
                responsive: true,
                buttons: [

                ]
            });
            }else{
                    var prevConsumer;
                    var childIndex;
                      $.each(result.ResponseResult, function (i, v) {
                   

                         var child = result.ResponseResult[i].orsContentList;
                         var OrsNum = child.length;
                         child = result.ResponseResult[i];
                         $.each(result.ResponseResult[i].orsContentList, function (a, b) {

                                    $('#tblOrs').find('tbody').append(
                                     '<tr>'+
                                            '<td align="center">' +
                                '<a type="button" class="restoreOrsReq bg-teal btn-block btn-sm waves-effect" data-refid="'+child.refid+'">Restore</button>'+
                            '</td>'+  
                                        '<td>'+addCommas(child.refid2)+'</td>'+
                                        '<td>'+addCommas(child.fundConsumer)+'</td>'+
                                        // '<td style="color:transparent;">'+addCommas(child.fundConsumer)+'</td>'+
                                        '<td style="color:transparent;"></td>'+
                                        '<td>'+addCommas(child.orsContentList[a].expendCode)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].obligations)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].totalTAX2)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].disbursements)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdAF.ps)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdAF.mooe)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdAF.co)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdAF.fe)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdAM.ps)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdAM.mooe)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdAM.co)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdOS.ps)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdOS.mooe)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].ocdOS.co)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].totalPS)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].totalCO)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].totalFE)+'</td>'+
                                        '<td>'+addCommas(child.orsContentList[a].totalMOOE)+'</td>'+
                                    '</tr>'      );
                                
                         });

                         });

                   

                    


                $('#tblOrs').DataTable({
                
                responsive: true,
                buttons: [

                ]
            });

            }
             $("#formloaderOrs").hide();
             $("#ORSHandler").show();
             $("#addForm").removeAttr('disabled');
             $("#btnDateSearch").removeAttr('disabled');
             $(".deleteReq").removeAttr('disabled');
             $(".updateReq").removeAttr('disabled');



            $('#submitDate').removeAttr('disabled');
            $('#searchDate').removeAttr('disabled');
            $('#exportExcelButton').show();
    });
}


//=====================================================================================================

 $(document).on("click", ".restoreOrsReq", function () {
       var id = $(this).data('refid');
       var url = "restoreOrs";
       restore(id, url);
    });

 $(document).on("click", ".restoreNCAReq", function () {
       var id = $(this).data('refid');
       var url = "restoreNCA";
       restore(id, url);
    });

 $(document).on("click", ".restoreAllotmentReq", function () {
       var id = $(this).data('refid');
       var url = "restoreAllot";
       restore(id, url);
    });





 function restore(id, url) {
  //  console.log(id);
        swal({
            title: "Are you sure?",
            text: "It will Appear in Active Data",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Restore it!",
            cancelButtonText: "No, I changed my mind.",
            closeOnConfirm: false,
            closeOnCancel: false,
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajaxSetup({async: false});
                $.post( window.location + '/'+url, {
                    'refId': id}).done(function(res){
                    let result = JSON.parse(res);
                    let message = result.ResponseResult;
                          swal("Deleted!", message, "success");
                          pageStart();
                });
              

            } else {
                swal("Cancelled", "Record has not been Restored.", "error");
            }

        });
    }


//======================================================================================================

$(document).on('click','#exportExcelButton',function(){
        var dateMonth = $(this).attr('data-holder');
		var newForm = $('<form>', {        
            'action':  window.location + '/exportExcel',
            'target': '_blank',
            'method': 'POST'    
        }).append($('<input>', {        
            'name': 'excelDate',       
            'value': dateMonth,        
            'type': 'hidden'    
        })).append($('<input>', {        
            'name': 'exportExceltxt',        
            'value': dateMonth,        
            'type': 'hidden'    
        }));

        newForm.appendTo('body').submit().remove();
      });1


//=======================================================================================================
    function addCommas(string) {
        return string.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
    }
});




