$(document).ready(function(){

	//var baseurl = "http://localhost/UserManagement/";
	let base_url = document.URL.split('/');
	console.log(base_url[0] + "//" + base_url[2] + "/UserManagement/UserMngmt/UserLevelConfig/get_user_data");
	$("#form_add_user").validate({
		rules: {
			 username: {required: true},
			 password: {required: true},
			 user_level: {required: true},
			 email_address: {required: true},
			 fname: {required: true},
			 mname: {required: true},
			 lname: {required: true}
		},
		messages:{
			username: { required: "Please fill out this field." },
			password: { required: "Please fill out this field." },
			user_level: { required: "Please fill out this field." },
			email_address: { required: "Please fill out this field." },
			fname: { required: "Please fill out this field." },
			mname: { required: "Please fill out this field." },
			lname: { required: "Please fill out this field." }
		},
		submitHandler: function(form) {
			var formData = new FormData($("#form_add_user")[0]);
			$.ajax({
                url: base_url[0] + "//" + base_url[2] + "/UserManagement/UserMngmt/UserLevelConfig/add_user",
                type: "POST",
                data: {
                	username: $("#username").val(),
                	password: $("#password").val(),
                	user_level: $("#user_level").val(),
                	email_address: $("#email_address").val(),
                	fname: $("#fname").val(),
                	mname: $("#mname").val(),
                	lname: $("#lname").val(),
                	gender: $("#gender").val()
                },
                dataType: "json",
                success: function(json){
                    if(json.data.ResponseResult == "Existing Email"){
                    	swal("Cancelled!", "Your Email is already Exist", "error");
                    }
                    else if(json.data.ResponseResult == "Existing Username"){
                    	swal("Cancelled!", "Your Username is already Exist", "error");
                    }
                    else{
                    	swal("Success!", "Account successfully created!", "success");
                    	$('#add_user').modal('toggle');
                    	loadUserData();
                    	clear();
                    }
                }
            });
		}
	});

	function clear(){
		$("#form_add_user")[0].reset();
	}

	function clearUpdate(){
		$("#update_user_account")[0].reset();
	}

	function loadUserData(){
		$("#get_user_data").DataTable({
			"destroy": true,
			"responsive": true,
			"bLengthChange": false,
			"ajax": {
				"url": base_url[0] + "//" + base_url[2] + "/UserManagement/UserMngmt/UserLevelConfig/get_user_data",
				"format": "json",
				"type": "GET"
			},
			"columns":[
				{
					"data": "id"
				},
				{
					"data": "username"
				},
				{
					"data": "userlevel"
				},
				{
					"data": "fname"
				},
				{
					"data": "lname"
				},
				{
					"data": "email"
				},
					{
					"data": "status"
				},
				{
					"data": function (data) {
						return `
							<center>
								<button data-id='` + data.id + `' data-userlevel='` + data.userlevel + `' data-firstname='` + data.firstname + `' id="logout_user" name="logout_user" class="btn btn-warning btn-xs waves-effect" style="padding: 6px">
									<i class="fa fa-sign-out fa-fw" style="font-size:18px;"></i>
								</button>
								<button id="update_user_account" name="update_user_account" data-toggle="modal" data-target="#updateUserAccount" 
								data-id='` + data.id + `' 
								data-username='` + data.username + `' 
								data-userlevel='` + data.userlevel + `' 
								data-email='` + data.email + `'
								data-firstname='` + data.fname + `'
								data-middlename='` + data.mname + `'
								data-lastname='` + data.lname + `'
								data-gender='` + data.gender + `'
								data-status ='` + data.status + `'
								class='btn btn-primary btn-xs waves-effect' 
								style="padding: 6px">
										<i class="fa fa-pencil fa-fw" style="font-size:18px;"></i>
								</button>
								<button data-id='` + data.id + `' data-userlevel='` + data.userlevel + `' data-firstname='` + data.firstname + `' id="delete_user" name="delete_user" class="btn btn-danger btn-xs waves-effect" style="padding: 6px">
									<i class="fa fa-trash fa-fw" style="font-size:18px;"></i>
								</button>
							</center>
	          			`;
					}
				}
			]
		});
	}

	loadUserData();

	$(document).on("click","#update_user_account",function(){
		var id = $(this).data("id");
		var username = $(this).data("username");
		var userlevel = $(this).data("userlevel");
		var email = $(this).data("email");
		var fname = $(this).data("firstname");
		var mname = $(this).data("middlename");
		var lname = $(this).data("lastname");
		var gender = $(this).data("gender");
		var status = $(this).data("status");
		$("#id").val(id);
		$("#update_user_level").val(userlevel);
		$("#update_email_address").val(email);
		$("#update_fname").val(fname);
		$("#update_mname").val(mname);
		$("#update_lname").val(lname);
		$("#update_gender").val(gender);
		$("#update_status").val(status);
		if (status == "LOCKED") {
			
			swal({
			    title: "Are you sure?",
			    text: "You will unlock this account!",
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: '#DD6B55',
			    confirmButtonText: 'Yes, I am sure!',
			    cancelButtonText: "No, cancel it!",
			    closeOnConfirm: false,
			    closeOnCancel: false
			}, 
			function (isConfirm) {
				if(isConfirm){
					$.ajax({
		                url: base_url[0] + "//" + base_url[2] + "/UserManagement/UserMngmt/UserLevelConfig/unlock_user",
		                type: "POST",
		                data: {
		                	username: username
		                },
		                dataType: "json",
		                success: function(result){
		                	console.log(result.data);
		                	if(result.data == '{"ResponseResult":"Unlocking Successful"}'){
		                		swal("Done!","Successfully Unlocked","success");
		                		loadUserData();
		                	}
		                	else{
		                		swal("Done!","This Account is already Activated!","warning");
		                	}
		                }
					});
				}
				else{
               		swal("Cancelled", "Click OK!", "error");
         		} 
			});
           

			/*function(isConfirm){

			   if (isConfirm){
			     swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");

			    } else {
			      swal("Cancelled", "Your imaginary file is safe :)", "error");
			    }
			});*/

			$("#updateUserAccount").modal('toggle');

		}


		$("#form_update_user").validate({
			rules: {
				 update_user_level: {required: true},
				 update_email_address: {required: true},
				 update_fname: {required: true},
				 update_mname: {required: true},
				 update_lname: {required: true},
				 update_gender: {required: true},
				 update_status:  {required: true}
			},
			messages:{
				update_user_level: { required: "Please fill out this field." },
				update_email_address: { required: "Please fill out this field." },
				update_fname: { required: "Please fill out this field." },
				update_mname: { required: "Please fill out this field." },
				update_lname: { required: "Please fill out this field." },
				update_gender: { required: "Please fill out this field." },
				update_status: { required: "Please fill out this field." }
			},
			submitHandler: function(form) {
				var formData = new FormData($("#form_update_user")[0]);
				$.ajax({
	                url: base_url[0] + "//" + base_url[2] + "/UserManagement/UserMngmt/UserLevelConfig/update_user",
	                type: "POST",
	                data: {
	                	id : $("#id").val(),
	                	update_user_level: $("#update_user_level").val(),
	                	update_email_address: $("#update_email_address").val(),
	                	update_fname: $("#update_fname").val(),
	                	update_mname: $("#update_mname").val(),
	                	update_lname: $("#update_lname").val(),
	                	update_gender: $("#update_gender").val(),
	                	update_status: $("#update_status").val()
	                },
	                dataType: "json",
	                success: function(json){
	                    if(json.data.ResponseResult == "Existing Email"){
	                    	swal("Cancelled!", "Your Email is already Exist", "error");
	                    }
	                    else{
	                    	swal("Success!", "Account successfully created!", "success");
	                    	$('#updateUserAccount').modal('toggle');
	                    	loadUserData();
	                    	clearUpdate();
	                    }
	                }
	            });
			}
		});
	});

	$(document).on("click","#delete_user",function(){
		id = $(this).data("id");
		swal({
			    title: "Are you sure?",
			    text: "You will unlock this account!",
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: '#DD6B55',
			    confirmButtonText: 'Yes, I am sure!',
			    cancelButtonText: "No, cancel it!",
			    closeOnConfirm: false,
			    closeOnCancel: false
			},
			function (isConfirm) {
				if(isConfirm){
					$.ajax({
			            url: base_url[0] + "//" + base_url[2] + "/UserManagement/UserMngmt/UserLevelConfig/delete_user",
			            type: "POST",
			            data: {
			            	id: id
			            },
			            dataType: "json",
			            success: function(json){
			            	if(json.data == false){
			            		swal("Cancelled!", "Error", "error");
			            	}
			            	else{
			            		swal("Success!", "Deleted Successfully!", "success");
			            		loadUserData();
			            	}
			            }
			        });
				}
				else{
               		swal("Cancelled", "Click OK!", "error");
         		} 
			}
		);
	});



	$(document).on("click","#logout_user",function(){
		id = $(this).data("id");
		swal({
			    title: "Are you sure?",
			    text: "You will destroy session of this account!",
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: '#DD6B55',
			    confirmButtonText: 'Yes, I am sure!',
			    cancelButtonText: "No, cancel it!",
			    closeOnConfirm: false,
			    closeOnCancel: false
			},
			function (isConfirm) {
				if(isConfirm){
					$.ajax({
			            url: base_url[0] + "//" + base_url[2] + "/UserManagement/UserMngmt/UserLevelConfig/logout_user",
			            type: "POST",
			            data: {
			            	id: id
			            },
			            dataType: "json",
			            success: function(json){
			            	data = json.data
			            	messages = JSON.parse(data);
			            	console.log(messages.ResponseResult);
			            	if(messages.ResponseResult == "DATA NOT EXIST"){
			            		swal("Account is not logged!", "Error", "error");
			            	}
			            	else{
			            		swal("Success!", "Destroy Session Successfully!", "success");
			            		loadUserData();
			            	}
			            }
			        });
				}
				else{
               		swal("Cancelled", "Click OK!", "error");
         		} 
			}
		);
	});


});