$(document).ready(function(){


	$("#tblroles").dataTable({
		"destroy":true,
		"dom": '<"top">rt<"bottom"ip><"clear">',
		responsive: true,
		"lengthMenu": [[10], [10]],
		"aoColumnDefs": [
			{ "bSortable": false, "aTargets": [ 4 ] }
		]
	});

	function loadusers(){
		$("#tblusers").dataTable({
			"destroy":true,
			"dom": '<"top">lfrt<"bottom"ip><"clear">',
			responsive: true,
			"lengthMenu": [[10], [10]],
			"aoColumnDefs": [
				{ "bSortable": false, "aTargets": [ 7 ] }
			],
			"ajax": "UserConfig/getUsers",
				"columns":[
					{ "data":"userid" },
					{ "data": function(data){
						return data.firstname + " " + data.middlename + " " + data.lastname;
					} },
					{ "data":"username" },
					{ "data":"userlevelname" },
					{ "data":"email" },
					{ "data":"gender" },
					{ "data":"status" },
					{ "data": function(data){
						return "<a href='#' data-userid='"+data.userid+"' data-firstname='"+data.firstname+"' data-middlename='"+data.middlename+"' data-lastname='"+data.lastname+"' data-username='"+data.username+"' data-userlevel='"+data.userlevel+"' data-email='"+data.email+"' data-gender='"+data.gender+"' data-status='"+data.status+"' class='btn btn-sm btn-primary waves-effect selectuser'>SELECT</a>"
					}}
				]
		});
	}

	function loaduserlevels(){
		$.ajax({
	        url: "UserConfig/getUserLevels", dataType: "json", success: function(jsondata){
	            for(var i = 0;i < jsondata.data.length;i++){
	                $("#userlevel").append("<option value='"+jsondata.data[i].userlevelid+"'>"+jsondata.data[i].userlevelname+"</option>");
	            }
	        }
	    });
	}
	
	function loaduserlevelroles(){
		$.ajax({
			url: "UserConfig/getuserlevelRoles", type: "POST", data:{ id:  "4"}, dataType: "json",
				success: function(jsondata){
					$("#tblroles").DataTable().clear().draw();
					var role = jsondata.data;
					for(var i =0; i < role.length;i++){
						$("#tblroles").DataTable().row.add( [
							(i+1),
							role[i].module,
							role[i].description,
							role[i].status,
							"<input type='checkbox' class='CHKROLES' name='ROLES[]' value='"+role[i].module+"'>"
						] ).draw( false );
					}
					$('input.CHKROLES').iCheck('destroy');
					$('input.CHKROLES').iCheck({
					    checkboxClass: 'icheckbox_flat-orange'
					});
					$('input.CHKROLES').iCheck("disable");
				}
		});
	}

	function loaduserroles(id){
		$.ajax({
			url: "UserConfig/getUserRoles", type: "POST", data:{ id:  id}, dataType: "json",
				success: function(jsondata){
					$("#tblroles").DataTable().clear().draw();
					var module = jsondata.data.modules;
					var usermodule = jsondata.data.usermodules;
					var activeroles = [];
					for (var i = 0; i < usermodule.length; i++) {
						activeroles.push(usermodule[i].module);
					};
					for(var i =0; i < module.length;i++){
						var checked ="";
						if($.inArray(module[i].module,activeroles)!==-1){
							checked = "checked";
						}
						$("#tblroles").DataTable().row.add( [
							(i+1),
							module[i].module,
							module[i].description,
							module[i].status,
							"<input type='checkbox' class='CHKROLES' name='ROLES[]' "+checked+" value='"+module[i].module+"'>"
						] ).draw( false );
					}
					$('input.CHKROLES').iCheck('destroy'); $('input.CHKROLES').iCheck({ checkboxClass: 'icheckbox_flat-orange' });$('input.CHKROLES').iCheck("disable");
				}
		});
	}

	loadusers();
	loaduserlevels();
	loaduserlevelroles();
	
	$(document).on("change","#userlevel",function(){
		loaduserroles($(this).val());
	});

	$(document).on("click",".selectuser",function(){
		var userid = $(this).data("userid");
		var firstname = $(this).data("firstname");
		var middlename = $(this).data("middlename");
		var lastname = $(this).data("lastname");
		var username = $(this).data("username");
		var userlevel = $(this).data("userlevel");
		var email = $(this).data("email");
		var gender = $(this).data("gender");
		var status = $(this).data("status");
		$("#agentid").val(userid).prop("readonly",true).css("pointer-events","none");
		$("#userlevel").val(userlevel);
		$("#username").val(username).prop("readonly",true).css("pointer-events","none");
		$("#fname").val(firstname);
		$("#mname").val(middlename);
		$("#lname").val(lastname);
		$("#gender").val(gender);
		$("#email").val(email);
		$("#status").val(status);
		$("#btnsubmit").addClass("displaynone");
		$("#btnupdate").removeClass("displaynone");
		if(status=="ACTIVE"){
			$("#btndeactivate").removeClass("displaynone");
			$("#btnactivate").addClass("displaynone");
		}else{
			$("#btndeactivate").addClass("displaynone");
			$("#btnactivate").removeClass("displaynone");
		}
		loaduserroles(userlevel);
	});

	$(document).on("click","#btnreset",function(){
		$("#agentid").val("").prop("readonly",false).css("pointer-events","auto");
		$("#userlevel").val($("#userlevel option:first").val());
		$("#username").val("").prop("readonly",false).css("pointer-events","auto");
		$("#fname").val("");
		$("#mname").val("");
		$("#lname").val("");
		$("#gender").val($("#gender option:first").val());
		$("#email").val("");
		$("#status").val($("#status option:first").val());
		$("#btnsubmit,#btnupdate,#btnactivate,#btndeactivate").removeClass("displaynone");
		$("#btnupdate,#btnactivate,#btndeactivate").addClass("displaynone");
	});

	// $(document).on("click","#btnsubmit",function(){
	// 	var elmForm = $("#form-user");
 //        if(elmForm){
 //            elmForm.validator('validate'); 
 //            var elmErr = $("#form-user .has-error").length;
 //            if(elmErr && elmErr > 0){
 //                return false;    
 //            }else{
 //            	var formData = new FormData( $("#frmuser")[0] );
 //                $.ajax({
 //                    url: $base_url + "UserManagement/UserConfig/submitUser",
 //                    type: "POST",
 //                    async : false,
 //                    cache : false,
 //                    contentType : false,
 //                    processData : false,
 //                    data: formData,
 //                    dataType: "json",
 //                    success: function(jsondata){
 //                        console.log(jsondata);
 //                    }
 //                });
 //            }
 //        }
	// });

	$("#frmuser").validate({
        	rules: { 
        		agentid: { required: true },
				userlevel: { required: true },
				username: { required: true },
				fname: { required: true },
				lname: { required: true },
				gender: { required: true },
				email: { required: true },
				status: { required: true }
			},
			messages:{
				agentid: { required: "Please fill out this field." },
				userlevel: { required: "Please select one of these options." },
				username: { required: "Please fill out this field." },
				fname: { required: "Please fill out this field." },
				lname: { required: "Please fill out this field." },
				gender: { required: "Please select one of these options." },
				email: { required: "Please fill out this field." },
				status: { required: "Please select one of these options." }
			},
			submitHandler: function(form) {
			          	var formData = new FormData($("#frmuser")[0]);
			            $.ajax({
				            url: "UserConfig/submitUser",
				            type: "POST",
				            data: formData,
				            cache : false,
				            contentType : false,
				            processData : false,
				            dataType: "json",
				            success: function(json){
		                        if(json != 1){
		                            swal("Cancelled!", "System is busy!", "error");
		                        }else{
		                            swal("Success!", "Account successfully created!", "success");
		                            loadusers();
		                            $(".modal").modal("hide");
		                            $("#btnreset").trigger("click")
		                        }
		                    }
				        });
			      }
        });
	
	var uservalidator = $("#frmuser").validate();

	$(document).on("click","#btnupdate",function(){
		var i = 0;
		if(!uservalidator.element("#agentid")){ i++; }
		if(!uservalidator.element("#userlevel")){ i++; }
		if(!uservalidator.element("#username")){ i++; }
		if(!uservalidator.element("#fname")){ i++; }
		if(!uservalidator.element("#lname")){ i++; }
		if(!uservalidator.element("#gender")){ i++; }
		if(!uservalidator.element("#email")){ i++; }
		if(!uservalidator.element("#status")){ i++; }
		if(i==0){
         	var formData = new FormData( $("#frmuser")[0] );
            $.ajax({
                url: "UserConfig/updateUser",
                type: "POST",
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                data: formData,
                dataType: "json",
                success: function(json){
		            if(json != 1){
		                swal("Cancelled!", "System is busy!", "error");
		            }else{
		                swal("Success!", "Account successfully updated!", "success");
		                loadusers();
		                $(".modal").modal("hide");
		                $("#btnreset").trigger("click")
		            }
		        }
            });
        }
	});

	$(document).on("click","#btnactivate",function(){
		$.ajax({ url: "UserConfig/activateUser", type:"POST", data:{ id: $("#agentid").val() }, dataType: "json",
			success: function(json){
		                        if(json != 1){
		                            swal("Cancelled!", "System is busy!", "error");
		                        }else{
		                            swal("Success!", "Account successfully activated!", "success");
		                            loadusers();
		                            $(".modal").modal("hide");
		                            $("#btnreset").trigger("click")
		                        }
		                    }
		});
	});
	

	$(document).on("click","#btndeactivate",function(){
		$.ajax({ url: "UserConfig/deactivateUser", type:"POST", data:{ id: $("#agentid").val() }, dataType: "json",
			success: function(json){
		                        if(json != 1){
		                            swal("Cancelled!", "System is busy!", "error");
		                        }else{
		                            swal("Success!", "Account successfully deactivated!", "success");
		                            loadusers();
		                            $(".modal").modal("hide");
		                            $("#btnreset").trigger("click")
		                        }
		                    }
		});
	});
});