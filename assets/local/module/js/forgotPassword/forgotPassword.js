$(document).ready(function () {
	var baseurl = "http://localhost/UserManagement/";
	$("#formForgot").validate({
		rules: {
			 email: {required: true}
		},
		messages:{
			email: { required: "Please fill out this field." }
		},
		submitHandler: function(form) {
			var formData = new FormData($("#formForgot")[0]);
			$.ajax({
				url: baseurl + "forgotPassword/forgotPassword/forgotPass",
                type: "POST",
                data: {
                	email: $("#email").val()
                },
                dataType: "json",
                success: function(json){
                	if(json.data.ResponseResult != "NO DATA FOUND"){
                		swal({
				            title: "Success!",
				            text: "Forgot Password Success, Please check your email. Thankyou!",
				            type: "success"
				        }, function() {
				            window.location = baseurl;
				        });
                	}
                	else{
                		swal("Invalid Email Address!", "No Data Found!", "warning");
                		$("#email").val('');
                	}
                }
			});
		}	
	});		
});













