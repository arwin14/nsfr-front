$(document).ready(function(){
url = 'http://180.150.134.136:48082/ACPCAccountingAPI/BUDGETService/ORS/API/';


$('.modal').on('hidden.bs.modal', function (e) {
    if($('.modal').hasClass('in')) {
    $('body').addClass('modal-open');
    }    
});



//=====================my validation======================================

         $(".number").on("keypress keyup blur",function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
        }
        });


        $("#property_number").alphanum({
        allow :'1234567890-qwertyuiopasdfghjklzxcvbnm', // Specify characters to allow
        disallow : ''  // Specify characters to disallow
        });

        $("#btnupdate").hide();
        $("#disabler").click(function(){
            $(".handler").toggleClass("nopointerevent");
            $("#disabler").fadeOut("fast");
            $("#btnupdate").fadeIn("fast");
        });

//========================= my date Generator  ============================= 

            $('#MonthYear').bootstrapMaterialDatePicker({
                format: 'MM/DD/YYYY',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });
            $('#searchDate').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });
            $('#modalDate').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD',
                clearButton: true,
                weekStart: 1,
                maxDate: new Date(),
                time: false
            });

            $("#dateMod").click(function(){
                let thedate = $('#modalDate').val();
                //console.log(thedate);
                var arr = thedate.split("-");
                var monthval = arr[1];
                var yearval = arr[0];

                let dateRaw = new Date(yearval,monthval,1);
                var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                let dd = dateneed.getDate();
                let mm = dateneed.getMonth();
                let yyyy = dateneed.getFullYear();
                let sendPostData = new Object();
                if (dd < 10) dd = '0' + dd
                if (mm < 10) mm = '0' + mm
                defDate = yyyy + '-' + mm + '-' + dd;
                defDateSearch = mm + '/' + dd + '/' + yyyy;
                // alert(yearval);

                $('#dateHead').text(defDate);
                $('#searchDate').val(defDate);
                //$('#DateCur').val(defDate);
                $("#exportExcelButton").attr({
                "data-holder" : defDate
                });

                $("#getAllotmentLast").attr({
                "data-date" : defDate
                });
                $("#getAllotmentCur").attr({
                "data-date" : defDate
                });

                $("#exportExceltxt").attr({
                "value" : defDate
                });

                BAF(defDate);

            });

//===================================================================================================================================
            $(document).on("click","#dateGen",function(){
                    let thedate = $('#searchDate').val();
                //    alert(thedate);
                $("#exportExcelButton").attr("data-holder",thedate);
                    //$('#exportExcelButton').data()
                $("#lastQuartSelect").attr({
                "data-date" : thedate
                });  
                $("#thisQuartSelect").attr({
                "data-date" : thedate
                });                   
                    BAF(thedate);
                   // optionQuarter(thedate);
                    // optionQuarterType(thedate);
                    // rightoptionQuarterType(thedate);
                    //optiondataQuarter(thedate);
            });
//===================================================================================================================================
            $("#updateAllot").click(function(){
                //$('#errorMssg').hide();
                $("#modify_date_container").collapse("hide");
                let dateRaw = new Date();
                var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                let dd = dateneed.getDate();
                let mm = dateneed.getMonth() + 1;
                let yyyy = dateneed.getFullYear();
                let sendPostData = new Object();
                if (dd < 10) dd = '0' + dd
                if (mm < 10) mm = '0' + mm
                defDate = yyyy + '-' + mm + '-' + dd;

                resetModal();
                // $('#modalDate').val(defDate);
                // $('#btnreset').trigger('click');
            });

//===================================================================================================================================
    function autoTrigger(){
         $('#dateGen').trigger('click');
    }
    autoTrigger();




//===========================================================================================================================================
    function BAF(params){
    theDate = params;
    console.log(theDate);


            $('#loaders').show();
         $.post(window.location+ '/getBaf/',{'date':theDate}).done(function(res){
         let result = JSON.parse(res);
            $('#specuBudTbl').DataTable().clear();
            $('#specuBudTbl').DataTable().destroy();

            let response = result.ResponseResult;
            let prev = '0';
            let lastQuarterFundaf = parseFloat(0);
            

                $.each(response, function (i, v) {

                let child = response[i].BAFList;
                let count = parseFloat(0);
               // console.log(response[i].ExpenditureType);
                
                window["lastQuarterFundTotalaf" + i] = parseFloat(0);
                window["lastQuarterFundTotalam" + i] = parseFloat(0);
                window["lastQuarterFundTotalos" + i] = parseFloat(0);
                window["lastQuarterFundTotaltotal" + i] = parseFloat(0);

                window["thisQuarterFundTotalaf" + i] = parseFloat(0);
                window["thisQuarterFundTotalam" + i] = parseFloat(0);
                window["thisQuarterFundTotalos" + i] = parseFloat(0);
                window["thisQuarterFundTotaltotal" + i] = parseFloat(0);

                window["allotmentFundTotalaf" + i] = parseFloat(0);
                window["allotmentFundTotalam" + i] = parseFloat(0);
                window["allotmentFundTotalos" + i] = parseFloat(0);
                window["allotmentFundTotaltotal" + i] = parseFloat(0);

                window["previousMonthFundTotalaf" + i] = parseFloat(0);
                window["previousMonthFundTotalam" + i] = parseFloat(0);
                window["previousMonthFundTotalos" + i] = parseFloat(0);
                window["previousMonthFundTotaltotal" + i] = parseFloat(0);

                window["selectedMonthFundTotalaf" + i] = parseFloat(0);
                window["selectedMonthFundTotalam" + i] = parseFloat(0);
                window["selectedMonthFundTotalos" + i] = parseFloat(0);
                window["selectedMonthFundTotaltotal" + i] = parseFloat(0);


                window["totalMonthFundTotalaf" + i] = parseFloat(0);
                window["totalMonthFundTotalam" + i] = parseFloat(0);
                window["totalMonthFundTotalos" + i] = parseFloat(0);
                window["totalMonthFundTotaltotal" + i] = parseFloat(0);

                window["balanceFundTotalaf" + i] = parseFloat(0);
                window["balanceFundTotalam" + i] = parseFloat(0);
                window["balanceFundTotalos" + i] = parseFloat(0);
                window["balanceFundTotaltotal" + i] = parseFloat(0);

                    

                    let key = i;
                    let childnum = response[i].BAFList.length;
                    $.each(child.sort(function(x, y) {
                            return x.code - y.code;
                    }), function (a, b) {


                    window["lastQuarterFundTotalaf" + i]  = window["lastQuarterFundTotalaf" + i] + child[a].lastQuarterFund.af;
                    window["lastQuarterFundTotalam" + i]  = window["lastQuarterFundTotalam" + i] + child[a].lastQuarterFund.am;
                    window["lastQuarterFundTotalos" + i]  = window["lastQuarterFundTotalos" + i] + child[a].lastQuarterFund.os;
                    window["lastQuarterFundTotaltotal" + i]  = window["lastQuarterFundTotaltotal" + i] + child[a].lastQuarterFund.total;
                  
                    window["thisQuarterFundTotalaf" + i]  = window["thisQuarterFundTotalaf" + i] + child[a].thisQuarterFund.af;
                    window["thisQuarterFundTotalam" + i]  = window["thisQuarterFundTotalam" + i] + child[a].thisQuarterFund.am;
                    window["thisQuarterFundTotalos" + i]  = window["thisQuarterFundTotalos" + i] + child[a].thisQuarterFund.os;
                    window["thisQuarterFundTotaltotal" + i]  = window["thisQuarterFundTotaltotal" + i] + child[a].thisQuarterFund.total;

                    window["allotmentFundTotalaf" + i]  = window["allotmentFundTotalaf" + i] + child[a].allotmentFund.af;
                    window["allotmentFundTotalam" + i]  = window["allotmentFundTotalam" + i] + child[a].allotmentFund.am;
                    window["allotmentFundTotalos" + i]  = window["allotmentFundTotalos" + i] + child[a].allotmentFund.os;
                    window["allotmentFundTotaltotal" + i]  = window["allotmentFundTotaltotal" + i] + child[a].allotmentFund.total;

                    window["previousMonthFundTotalaf" + i]  = window["previousMonthFundTotalaf" + i] + child[a].previousMonthFund.af;
                    window["previousMonthFundTotalam" + i]  = window["previousMonthFundTotalam" + i] + child[a].previousMonthFund.am;
                    window["previousMonthFundTotalos" + i]  = window["previousMonthFundTotalos" + i] + child[a].previousMonthFund.os;
                    window["previousMonthFundTotaltotal" + i]  = window["previousMonthFundTotaltotal" + i] + child[a].previousMonthFund.total;



                    window["selectedMonthFundTotalaf" + i]  = window["selectedMonthFundTotalaf" + i] + child[a].selectedMonthFund.af;
                    window["selectedMonthFundTotalam" + i]  = window["selectedMonthFundTotalam" + i] + child[a].selectedMonthFund.am;
                    window["selectedMonthFundTotalos" + i]  = window["selectedMonthFundTotalos" + i] + child[a].selectedMonthFund.os;
                    window["selectedMonthFundTotaltotal" + i]  = window["selectedMonthFundTotaltotal" + i] + child[a].selectedMonthFund.total;


                    window["totalMonthFundTotalaf" + i]  = window["totalMonthFundTotalaf" + i] + child[a].totalMonthFund.af;
                    window["totalMonthFundTotalam" + i]  = window["totalMonthFundTotalam" + i] + child[a].totalMonthFund.am;
                    window["totalMonthFundTotalos" + i]  = window["totalMonthFundTotalos" + i] + child[a].totalMonthFund.os;
                    window["totalMonthFundTotaltotal" + i]  = window["totalMonthFundTotaltotal" + i] + child[a].totalMonthFund.total;

                    window["balanceFundTotalaf" + i]  = window["balanceFundTotalaf" + i] + child[a].balanceFund.af;
                    window["balanceFundTotalam" + i]  = window["balanceFundTotalam" + i] + child[a].balanceFund.am;
                    window["balanceFundTotalos" + i]  = window["balanceFundTotalos" + i] + child[a].balanceFund.os;
                    window["balanceFundTotaltotal" + i]  = window["balanceFundTotaltotal" + i] + child[a].balanceFund.total;






                        count = count + 1;
                        //alert(count);
                       //console.log(key);
                      //  console.log(childnum);
                       if(a == 0 && childnum != 1){
                            $('#specuBudTbl').find('tbody').append(
                            '<tr>'+ 
                            '<td style="color:blue;">'+response[i].ExpenditureType+'</td>'+
                            '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'+
                            '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'+
                            +'</tr>'+
                            '<tr>'+ 
                            '<td>'+addCommas(child[a].expendName)+'</td>'+
                            '<td>'+addCommas(child[a].code)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.total)+'</td>'+
                            '</tr>'      
                            );
                       }else{
                            if(count == childnum || count == 1){
                            let valueHandler = parseFloat(0);

                          secondaryRowTotal = '<tr style="background:rgb(141,205,145); font-size:12pt;">'+ 
                        '<td>'+response[i].ExpenditureType+' total</td>'+
                        '<td>'+child[a].code+'</td>'+
                        '<td>'+addCommas(window["lastQuarterFundTotalaf" + i]) +'</td>'+
                        '<td>'+addCommas(window["lastQuarterFundTotalam" + i]) +'</td>'+
                        '<td>'+addCommas(window["lastQuarterFundTotalos" + i]) +'</td>'+
                        '<td>'+addCommas(window["lastQuarterFundTotaltotal" + i]) +'</td>'+
                        '<td>'+addCommas(window["thisQuarterFundTotalaf" + i])+'</td>'+
                        '<td>'+addCommas(window["thisQuarterFundTotalam" + i])+'</td>'+
                        '<td>'+addCommas(window["thisQuarterFundTotalos" + i])+'</td>'+
                        '<td>'+addCommas(window["thisQuarterFundTotaltotal" + i])+'</td>'+
                        '<td>'+ addCommas(window["allotmentFundTotalaf" + i])+'</td>'+
                        '<td>'+ addCommas(window["allotmentFundTotalam" + i])+'</td>'+
                        '<td>'+ addCommas(window["allotmentFundTotalos" + i])+'</td>'+
                        '<td>'+ addCommas(window["allotmentFundTotaltotal" + i])+'</td>'+
                        '<td>'+ addCommas(window["previousMonthFundTotalaf" + i])+'</td>'+
                        '<td>'+ addCommas(window["previousMonthFundTotalam" + i])+'</td>'+
                        '<td>'+ addCommas(window["previousMonthFundTotalos" + i])+'</td>'+
                        '<td>'+ addCommas(window["previousMonthFundTotaltotal" + i])+'</td>'+
                        '<td>'+ addCommas(window["selectedMonthFundTotalaf" + i])+'</td>'+
                        '<td>'+ addCommas(window["selectedMonthFundTotalam" + i])+'</td>'+
                        '<td>'+ addCommas(window["selectedMonthFundTotalos" + i])+'</td>'+
                        '<td>'+ addCommas(window["selectedMonthFundTotaltotal" + i])+'</td>'+
                        '<td>'+ addCommas(window["totalMonthFundTotalaf" + i])+'</td>'+
                        '<td>'+ addCommas(window["totalMonthFundTotalam" + i])+'</td>'+
                        '<td>'+ addCommas(window["totalMonthFundTotalos" + i])+'</td>'+
                        '<td>'+ addCommas(window["totalMonthFundTotaltotal" + i])+'</td>'+
                        '<td>'+ addCommas(window["balanceFundTotalaf" + i])+'</td>'+
                        '<td>'+ addCommas(window["balanceFundTotalam" + i])+'</td>'+
                        '<td>'+ addCommas(window["balanceFundTotalos" + i])+'</td>'+
                        '<td>'+ addCommas(window["balanceFundTotaltotal" + i])+'</td>'+
                        '</tr>' ;


                            $('#specuBudTbl').find('tbody').append(
                            '<tr>'+ 
                            '<td>'+addCommas(child[a].expendName)+'</td>'+
                            '<td>'+addCommas(child[a].code)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.total)+'</td>'+
                            '</tr>'+secondaryRowTotal

                             
                            );


                            }else{
                                   $('#specuBudTbl').find('tbody').append(
                            '<tr>'+ 
                            '<td>'+addCommas(child[a].expendName)+'</td>'+
                            '<td>'+addCommas(child[a].code)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].lastQuarterFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].thisQuarterFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].allotmentFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].previousMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].selectedMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].totalMonthFund.total)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.af)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.am)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.os)+'</td>'+
                            '<td>'+addCommas(child[a].balanceFund.total)+'</td>'+
                            '</tr>'      
                            );
                            
                      1

                    }
                }

                    }); 
                    prev = key;
                //alert(window["lastQuarterFundTotalaf" + i]);

                });
                 $('#loaders').hide();
                 $('#curHeadDate').text(theDate);

                $('#specuBudTbl').DataTable({
                "scrollY": 300,
                "scrollX": true,    
                dom: 'Bfrtip',
                fixedHeader: {
                header: true,
                footer: true
                },
                "paging": false,
                responsive: true,
                buttons: [

                ],
                "order": []

                });
                });


    }




//===========================================================================

    $('#searchMssgModal').hide();
    $("#formloader").hide();
    $(document).on("click", "#generate_form", function () {
        let thedate = $('#searchDate').val();
        
        let budget_classificationModal = $('#budget_classificationModal').val();
        let budget_listModal = $('#budget_listModal').val();
        // console.log(budget_classificationModal + ' ' + budget_listModal);
        if ((budget_classificationModal == '--') || (budget_listModal == '--')) {
            $('#searchMssgModal').fadeIn();

            $("#btnsubmitadd").attr("disabled", true);
            $("#btnclear").attr("disabled", true);
            $("#formGen").empty();
        } else {
            $("#btnsubmitadd").removeAttr("disabled");
            $("#btnclear").removeAttr("disabled");
            $("#formGen").empty();


            $("#formloader").show();
            $('#searchMssgModal').hide();
            //$.get(baseURL.'ORS/API/v1/getDataBAF306B/date/2018-03-10').done(function(result) {
            $.post(window.location + '/getBAFOption/',{'date':thedate}).done(function (res) {
                let result = JSON.parse(res);


                $("#formloader").hide();
                let response = result.ResponseResult;
                valindex = $('#budget_listModal').val();

                if (valindex == "PERSONNEL SERVICES") {
                    indexval = 0;
                } else if (valindex == "MAINTENANCE AND OTHER OPERATING EXPENSES") {
                    indexval = 1;
                } else if (valindex == "CAPITAL OUTLAY") {
                    indexval = 2;
                } else if (valindex == "FINANCIAL EXPENSE") {
                    indexval = 3;
                }



                child = response[indexval].BAFList;
                //     console.log(child);
                $.each(child, function (i, v) {
                    let label = child[i].expendName;
                    let code = child[i].code;
                if(budget_classificationModal == 'GAS'){
                    valdisplay = child[i].allotmentFund.af;
                }else if(budget_classificationModal == 'AMCFP'){
                    valdisplay = child[i].allotmentFund.am;
                }else{
                    valdisplay = child[i].allotmentFund.os;
                }
                    $("#formGen").append(
                        '<div class="col-md-3">' +
                        '<br>' +
                        '<div class="form-group form-float">' +
                        '<div class="focused form-line" focused>' +
                        '<input type="text" class="number form-control" id="allotmentTxt" name="allotmentTxt[]" placeholder="' + valdisplay + '">' +
                        '<input type="hidden" class="number form-control" id="expendCode" name="expendCode[]" value="' + code + '">' +
                        '<label class="form-label" style="font-size: 1.3rem">' + label.substr(0, 35) + '...</label>' +
                        '</div>' +
                        '</div>' +
                        '</div>'

                    );
                    $(".number").on("keypress keyup blur", function (event) {

                        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                            event.preventDefault();
                        }

                    });

                });
            });

            $(".number").on("keypress keyup blur", function (event) {
                //this.value = this.value.replace(/[^0-9\.]/g,'');
                $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });


            // function para sa dynamic form(date, budget_classificationModal, budget_listModal);
        }

    });



//==============================================================================

    $(document).on("click", "#addForm", function () {
        resetModal();


    });
//===============================================================================

    $(document).on("click", "#btnclear", function () {
        resetModal();


    });

//==============================================================================
    $('#AllotmentMssgModal').hide();
    $(document).on("click", "#btnsubmitadd", function () {
        $empty = $('#addrecordform').find("input").filter(function () {
            return this.value === "";
        });
        $notempty = $('#addrecordform').find("input").filter(function () {
            return this.value !== "";
        });

        //alert($notempty.length - 1+"=="+$empty.length);
        //alert($notempty.length > $empty.length);
        if ($notempty.length - 1 == $empty.length) {
            $('#AllotmentMssgModal').show();
        } else {
            $('#AllotmentMssgModal').hide();
            swal({
                title: "Are you sure?",
                text: "You will directly send the data in report",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes,   Submit it!",
                cancelButtonText: "No, I changed my mind.",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {


                    var formData = new FormData($("#addrecordform")[0]);
                    $.ajax({
                        url: "UserConfig/addAllotment",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        async: false,
                        success: function (json) {

                            message = json.ResponseResult
                            //                 console.log(message);
                            //alert(JSON.stringify(json));
                            if (message != 'Insert Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "added successfully", "success");
                                $(".modal").modal("hide");
                                // monthyFunction(defDate); 


                            }
                        }
                    });


                } else {
                    swal("Cancelled", "Record has not been Record.", "error");
                }
            });


        }


    });

//==============================================================================
    $("#getAllotmentLast").click(function(e){
         let date = $(this).attr('data-date');
         let optionone = $('#lastQuartSelect').val();
         let optiontwo = $('#lastQuartClassificationSelect').val();

            //alert(optionone+" == "+optiontwo);
            $.post(window.location + '/GetallotmentBalance/',{'date':date}).done(function(ret) {
                       let result = JSON.parse(ret);
                       let response = result.ResponseResult;
                        if(optionone == '' || optiontwo == ''){
                                $('#lastAllotmentBalance').val('PLease select Category first!');
                        }else{
                              $.each(response, function (x, y) {  
                                 child = response[x].BAFList;
                                  $.each(child, function (i, v) {
                                   if(child[i].code == optionone){
                                        //let varme = child[i].thisQuarterFund.af
                                                
                                        if(optiontwo == "GAS"){
                                                $("#lastAllotmentBalance").val(child[i].allotmentFund.af);
                                        }else if(optiontwo == "AMCFP"){
                                                 $("#lastAllotmentBalance").val(child[i].allotmentFund.am);
                                        }else if(optiontwo == "FORMULATING"){
                                                 $("#lastAllotmentBalance").val(child[i].allotmentFund.os);
                                        }
                                   }else{

                                   }
                              });
                            });
                        }
            });

});
//==============================================================================


    $("#getAllotmentCur").click(function(e){
         let date = $(this).attr('data-date');
         let optionone = $('#thisQuartSelect').val();
         let optiontwo = $('#thisQuartClassificationSelect').val();

           // alert(date);
            $.post(window.location + '/GetallotmentBalanceCur/',{'date':date}).done(function(ret) {
                       let result = JSON.parse(ret);
                       let response = result.ResponseResult;

                        if(optionone == '' || optiontwo == ''){
                                $('#curAllotmentBalance').val('PLease select Category first!');
                        }else{
                              $.each(response, function (x, y) {  
                                 child = response[x].BAFList;
                                  $.each(child, function (i, v) {
                                   if(child[i].code == optionone){
                                        //let varme = child[i].thisQuarterFund.af
                                                
                                        if(optiontwo == "GAS"){
                                                $("#curAllotmentBalance").val(child[i].allotmentFund.af);
                                        }else if(optiontwo == "AMCFP"){
                                                 $("#curAllotmentBalance").val(child[i].allotmentFund.am);
                                        }else if(optiontwo == "FORMULATING"){
                                                 $("#curAllotmentBalance").val(child[i].allotmentFund.os);
                                        }
                                   }else{

                                   }
                              });
                            });
                        }
            });

});

//==============================================================================

$( "#lastQuartClassificationSelect" ).click(function() {
    //     alert();
         let code = $(this).val();
        $("#thisQuartClassificationSelect").val(code);

    });





$('#formloader').hide();
$("#RequiredError").hide();
$("#OptionError").hide();
$("#ValueError").hide();
$(document).on("click","#calculateForm",function(e){
     let date = $("#modalDate").val();
        //alert(date);    
     let optionright = $('#lastQuartSelect').val();
     let optionleft = $('#thisQuartSelect').val();

         let optionone = $('#lastQuartSelect').val();
          let optiononeright = $('#thisQuartSelect').val();
         let optiontwo = $('#lastQuartClassificationSelect').val();
         let optiontworight = $('#thisQuartClassificationSelect').val();
         let neededValLeft
         let neededValRight = parseFloat(0);
         let realignbudget = parseFloat($("#realigncur").val());
         let lastQuartSelect = $("#lastQuartSelect").val();
         let lastQuartClassificationSelect = $("#lastQuartClassificationSelect").val();
         let realignlast = $("#realignlast").val();
         let thisQuartSelect = $("#thisQuartSelect").val();

        //     //alert(optionone+" == "+optiontwo);
        $('#formloader').show();
        $('#more_information').hide();
            $.post(window.location + '/GetallotmentBalanceCurCheck/',{'date':date}).done(function(ret) {

                       let result = JSON.parse(ret);
                        // console.log(date);
                       let response = result.ResponseResult;
                              $.each(response, function (x, y) {  
                                 child = response[x].BAFList;
                                  $.each(child, function (i, v) {
                                   if(child[i].code == optionone){
                                        //let varme = child[i].thisQuarterFund.af
                                                
                                        if(optiontwo == "GAS"){
                                                 neededValLeft = child[i].allotmentFund.af;
                                        }else if(optiontwo == "AMCFP"){
                                                 neededValLeft = child[i].allotmentFund.am;
                                        }else if(optiontwo == "FORMULATING"){
                                                 neededValLeft = child[i].allotmentFund.os;
                                        }
                                   }else{

                                   }
                              });
                            });
                             




                           // console.log(date);



                            if(optionright == optionleft || realignbudget > neededValLeft || lastQuartSelect == '' || lastQuartClassificationSelect == '' || realignlast == '' || thisQuartSelect == ''){
                                    $("#OptionError").hide();
                                    $("#RequiredError").hide();
                                    $("#ValueError").hide();
                                    if(optionright == optionleft){
                                            $("#OptionError").show();
                                    }if(realignbudget > neededValLeft){
                                             $("#ValueError").show();
                                    }if( lastQuartSelect == '' || lastQuartClassificationSelect == '' || realignlast == '' || thisQuartSelect == ''){
                                            $("#RequiredError").show();
                                    }
                                    $('#formloader').hide();
                                     $('#more_information').show();
                            }else{
                                    $("#OptionError").hide();
                                    $("#RequiredError").hide();
                                    $("#ValueError").hide();

                                        $('#commitlefthandle').addClass('focused');
                                        $('#commitrighthandle').addClass('focused');



                        $.post(window.location + '/GetallotmentBalanceCurCheckRight/',{'date':date}).done(function(retright) {
                       let resultright = JSON.parse(retright);
                       let responseright = resultright.ResponseResult;
                              $.each(responseright, function (a, b) {  
                                 childright = responseright[a].BAFList;
                                  $.each(childright, function (c, d) {
                                   if(childright[c].code == optiononeright){
                                        //let varme = child[i].thisQuarterFund.af
                            //     console.log(childright[c].thisQuarterFund.af);
                                                
                                        if(optiontworight == "GAS"){
                                                 neededValRight = childright[c].allotmentFund.af;
                                        }else if(optiontworight == "AMCFP"){
                                                 neededValRight = childright[c].allotmentFund.am;
                                        }else if(optiontworight == "FORMULATING"){
                                                 neededValRight = childright[c].allotmentFund.os;
                                        }
                                   }else{

                                   }
                              });
                            });
                            let leftTotal = parseFloat(0);
                            let rightTotal = parseFloat(0);
                            leftTotal = neededValLeft - realignlast;
                            rightTotal = neededValRight + realignbudget;
                            // console.log(neededValLeft+" - "+realignlast+" = "+leftTotal);
                        //   console.log(neededValRight);
                        //   console.log(realignbudget);
                            $('#formloader').hide();
                            $('#more_information').show();
                            $("#commitleft").val(leftTotal);
                            $("#commitright").val(rightTotal);

                            });


                             }
                        
            });

});
//======================================================================================================
$( "#btnclear" ).click(function() {
        resetModal();
});



//=======================================================================================================


function resetModal(){
                let dateRaw = new Date();
                var dateneed = new Date(dateRaw.getFullYear(), dateRaw.getMonth(),  dateRaw.getDate());
                let dd = dateneed.getDate();
                let mm = dateneed.getMonth()+1;
                let yyyy = dateneed.getFullYear();
                let sendPostData = new Object();
                if (dd < 10) dd = '0' + dd
                if (mm < 10) mm = '0' + mm
                defDate = yyyy + '-' + mm + '-' + dd;
                $("#modalDate").val(defDate);
                $("#lastQuartSelect").val("");
                $("#lastQuartClassificationSelect").val("");
                $("#lastAllotmentBalance").val(0);
                $("#realignlast").val("");
                $("#thisQuartSelect").val("");
                $("#thisQuartClassificationSelect").val("");
                $("#curAllotmentBalance").val(0);
                $("#realigncur").val("");
                $("#commitleft").val("");
                $("#commitright").val("");

                 $("#commitlefthandle").removeClass("focused");
                 $("#commitrighthandle").removeClass("focused");




}

//======================================================================================================

$(document).on('click','#exportExcelButton',function(){
        var dateMonth = $(this).attr('data-holder');
var newForm = $('<form>', {        
            'action':  window.location + '/exportExcel',
            'target': '_blank',
            'method': 'POST'    
        }).append($('<input>', {        
            'name': 'excelDate',       
            'value': dateMonth,        
            'type': 'hidden'    
        })).append($('<input>', {        
            'name': 'exportExceltxt',        
            'value': dateMonth,        
            'type': 'hidden'    
        }));

        newForm.appendTo('body').submit().remove();
      });1

//=======================================================================================================
   function resetModal() {
        $("#modify_date_container").collapse("hide");
        $("#formGen").empty();
        $("#btnsubmitadd").attr("disabled", true);
        $("#btnclear").attr("disabled", true);
        $('#searchMssg').hide();
        $('#searchMssgModal').hide();
        $('#AllotmentMssgModal').hide();
        //$('#budget_classificationModalhandle option[value="--"]').attr('selected','selected');
        $('select[name="budget_listModal"]').val('--');
        //})
    }

//=======================================================================================================
        function addCommas(string) {
        return string.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
    }
});




