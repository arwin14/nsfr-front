var fields = {
                adding : function(data, url, date){
                       return $.ajax({
                        url: "AllotmentConfig/addAllotment",
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                         async: false,
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Insert Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "Added Successfully", "success");
                                $(".modal").modal("hide");
                                 $('#displayModal').modal('hide')
                                tblAllocate(date);

                            }
                        }
                    });
                   $.alert('Confirmed!');


                },
                  AddUserLevel : function(data, fucntionname, keyurl){
                    // alert(data+" == "+fucntionname+" === "+keyurl)
                     return $.ajax({
                        url: keyurl,
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                         async: false,
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Insert Successful') {
                                swal(message, "error", "error");
                            } else {
                                 swal("Success!", "added11 successfully", "success");
                                $(".ckdata").prop("checked", false);
                                $("#checkboxall").prop("checked", false);
                               window[fucntionname]();
                            }
                        }
                    });
                },

                Updating : function(data, url){
                    //alert(data)
                   return $.ajax({
                        url: "UserConfig/updateAllotment",
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        async: false,
                        success: function (json) {

                            message = json.ResponseResult
                            //                 console.log(message);
                            //alert(JSON.stringify(json));
                            if (message != 'Update Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "Update successfully", "success");
                                 $(".modal").modal("hide");
                                 $('#displayModal').modal('hide')
                                 


                            }
                        }
                    });
                },


                updateUser : function(data, url){
                   // alert("---")
                   return $.ajax({
                        url: "UserConfig/updateUser",
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        async: false,
                        success: function (json) {

                            message = json.ResponseResult
                            //                 console.log(message);
                            //alert(JSON.stringify(json));
                            if (message != 'Update Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "Update successfully", "success");
                                 $(".modal").modal("hide");
                                 $('#displayModal').modal('hide')
                                 


                            }
                        }
                    });
                },
                updateUserLevel : function(data, fucntionname, keyurl){
                   // alert(data+" == "+fucntionname+" === "+keyurl)
                     return $.ajax({
                        url: keyurl,
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                         async: false,
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Update Successful') {
                                swal(message, "error", "error");
                            } else {
                                 swal("Success!", "added11 successfully", "success");
                                $(".ckdatamoduleupdate").prop("checked", false);
                                $("#checkboxallUpdate").prop("checked", false);
                                 $(".modal").modal("hide");
                               window[fucntionname]();
                            }
                        }
                    });
                },

                updateAttachment : function(formdata, urldata){
                   // alert(data+"---"+urldata)
                   console.log(data);
                   return $.ajax({
                        url: urldata,
                        type: "POST",
                        data: formdata,
                        dataType: "json",
                        async: false,
                        success: function (json) {

                            message = json.ResponseResult
                            if (message != 'Update Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "Update successfully", "success");
                            }
                        }
                    });
                },

                updateItemAvail : function(data, tabler, url){
                   return $.ajax({
                        url: "UserConfig/"+url,
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        async: false,
                        success: function (json) {

                            message = json.ResponseResult
                            if (message != 'Update Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "Update successfully", "success");
                               // InitiateTbl();
                                $.ajax({
                                    type: "POST",
                                    url: "UserConfig/tblRespond",
                                    dataType: "json",
                                        success: function (result) {
                                        //console.log(result.table);
                                        $('#tblContainer').html(result.table);
                                        //pageStat();
                                            $(tabler).DataTable({
                                            dom: 'Bfrtip',
                                            fixedHeader: {
                                                header: true,
                                                footer: true
                                            },
                                            responsive: true,
                                            buttons: [],
                                            "order": []
                                        });                   
                                        },
                                        error: function (result) {
                                        alert("error");
                                        }
                                 });

                            }
                        }
                    });
                },

                // UpdateUtil : function(data, fucntionname, keyurl){
                //        return $.ajax({
                //         url: keyurl,
                //         type: "POST",
                //         data: data,
                //         cache: false,
                //         contentType: false,
                //         processData: false,
                //         dataType: "json",
                //         // async: false,
                //         success: function (json) {
                //             message = json.ResponseResult
                //             console.log(message);
                //             if (message != 'Update Successful') {
                //                 swal(message, "error", "error");
                //             } else {
                //                 swal("Success!", "added successfully", "success");
                //                window[fucntionname]();
                //             }
                //         }
                //     });
                // },

                UpdateUtil : function(data, fucntionname, keyurl){
                       return $.ajax({
                        url: keyurl,
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        // async: false,
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Update Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "added successfully", "success");
                               window[fucntionname]();
                               tblcurrencyChart();
                            }
                        }
                    });
                },

                addnew : function(data, url){
                    //alert("sad")
                       return $.ajax({
                        url: "UserConfig/addUser",
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        // async: false,
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Insert Success|Please check your email. Thankyou') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", message, "success");
                                $(".modal").modal("hide");
                                 StartPage();
                                 $('#displayModal').modal('hide')

                            }
                        }
                    });


                },

                AddUtil : function(data, fucntionname, fucntionnametwo, keyurl){
                       return $.ajax({
                        url: keyurl,
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        // async: false,
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Insert Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "added successfully", "success");
                               window[fucntionname]();
                               window[fucntionnametwo]();
                                $("#btnClear").trigger('click');
                            }
                        }
                    });
                },




                  addItem : function(data, url){
                   // alert("---"+url)
                   return $.ajax({
                        url: "UserConfig/"+url,
                        type: "POST",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        async: false,
                        success: function (json) {

                            message = json.ResponseResult
                            if (message != 'Insert Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Success!", "Added successfully", "success");
                               // InitiateTbl();
                                $.ajax({
                                    type: "POST",
                                    url: "UserConfig/tblRespond",
                                    dataType: "json",
                                        success: function (result) {
                                        console.log(result.table);
                                        $('#tblContainer').html(result.table);                   
                                        },
                                        error: function (result) {
                                        alert("error");
                                        }
                                 });

                            }
                        }
                    });
                       

                },






                deleteItem : function(id, url){
                       return $.ajax({
                        url: "UserConfig/"+url,
                        data: {
                            idbase : id
                        },
                        type: "POST",
                        dataType: "json",
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Delete Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Deleted!", "Deleted successfully", "success");
                                $(".modal").modal("hide");
                                 StartPage();
                                 $('#displayModal').modal('hide')

                            }
                        }
                    });

                },
                delete : function(datalag, url){
                       return $.ajax({
                        url: "UserConfig/"+url,
                        data: {
                            idbase : datalag
                        },
                        type: "POST",
                        dataType: "json",
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Insert Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Deleted!", "Deleted successfully", "success");
                                $(".modal").modal("hide");
                                 StartPage();
                                 $('#displayModal').modal('hide')

                            }
                        }
                    });
                    },
                    deleteUtil : function(data, fucntionname, urlkey){
                        //alert(+data+' --- '+fucntionname+' ---- '+urlkey);
                       return $.ajax({
                        url: urlkey,
                        data: {
                            idbase : data
                        },
                        type: "POST",
                        dataType: "json",
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Deactivate Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Deleted!", "Deleted successfully", "success");
                                window[fucntionname]();

                            }
                        }
                    });


                },
                deletesimulation : function(data, fucntionname, urlkey, date){
                        //alert(data+' --- '+fucntionname+' ---- '+urlkey+' ---- '+date);
                       return $.ajax({
                        url: urlkey,
                        data: {
                            idbase : data
                        },
                        type: "POST",
                        dataType: "json",
                        async: false,
                        success: function (json) {
                            message = json.ResponseResult
                            console.log(message);
                            if (message != 'Deactivate Successful') {
                                swal(message, "error", "error");
                            } else {
                                swal("Deleted!", "Deleted successfully", "success");
                                window[fucntionname](date);

                            }
                        }
                    });


                }




            }
            
            