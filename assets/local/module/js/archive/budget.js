let openRows = new Array();




function getData(date) {
    datas = {}
    datas['date'] = date
    console.log(datas);
    $.ajax({
        url: 'ArchiveConfig/getArchive',
        data: datas,
        type: 'POST',
        dataType: 'json',
    }).done(function(result) {
        console.log(result);
        response = (result.Code == 100) ? result.Data : "";
        initializeData(response)
    }).fail(function() {
        swal('Error occured!', 'System is busy! Please contact your administrator.', "error");
    });
}

//function validateSub(data){

//}

function initializeData(data) {
    datawin = data.fund_date+" gg";
    console.log(data);
    $('#tblArchive').DataTable().clear();
      $('#tblArchive').DataTable().destroy();
    $('#tblArchive').DataTable({
        data: data,
        "columnDefs": [{
            "visible": false,
            "searchable": false
        }],
        columns: [{
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                data: 'NSFR'
            },
            {
                data: 'ASF'
            },
            {
                data: 'RSF'
            },
            {
                data: 'fund_date'
            },
             {
                data: 'date_created'
            }

        ]
    });
}


function closeOpenedRows(table, selectedRow) {
    $.each(openRows, function(index, openRow) {
        if ($.data(selectedRow) !== $.data(openRow)) {
            var rowToCollapse = table.row(openRow);
            rowToCollapse.child.hide();
            openRow.removeClass('shown');
            var index = $.inArray(selectedRow, openRows);
            openRows.splice(index, 1);
        }
    });
}


function format(d) {
    let ff;
    $.ajax({
        type: "POST",
        url: "ArchiveConfig/getSpecific",
        data: d,
        dataType: "json",
        async: false,
        success: function(result) {
            ff = result.form;
        },
        error: function(result) {
            alert("error");
        }

    });
    return '<div id="specContainer">' +
        ff +
        '</div>';
}

$('#tblArchive').on('click', 'td.details-control', function() {
    table = $('#tblArchive').DataTable();
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    console.log(row.data());

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('shown');
    } else {
        closeOpenedRows(table, tr);
        row.child(format(row.data())).show();
        tr.addClass('shown');
        openRows.push(tr);
    }
});



function updatedata(formData) {
 // validatesub = validateSub(formData);
  console.log(formData[5].value);
    $.confirm({
        title: '<label class="text-warning">Confirm</label>',
        content: 'Are you sure you want to Update this?',
        type: 'orange',
        buttons: {
            confirm: function() {
                $.ajax({
                    type: "POST",
                    url: "ArchiveConfig/updateArchive",
                    data: formData,
                    async: false,
                    success: function(result) {
                    },
                    error: function(result) {
                        alert("error");
                    }

                }).done(function(result) {
                  res = JSON.parse(result);
                  if(res.Code == 100){
                    swal('Success', res.Message, "success");
                    getData(formData[5].value)
                 }else{
                    swal('Error', res.Message, "error");
                  }
                }).fail(function() {
                    swal('Error occured!', 'System is busy! Please contact your administrator.', "error");
                });
            },
            cancel: function() {
                $.alert('Canceled!');
            }

        }
    });
}



$(document).ready(function() {
    d = new Date(),
        n = d.getMonth() + 1,
        y = d.getFullYear(),
        t = d.getDay();

    let date = $("#FindDate").val();
    getData(date);

    $(document).on("click", "#btnDateSearch", function() {
        date = $("#FindDate").val();
        getData(date);
    })

    $(document).on("click", "#updatebtn", function(e) {
        e.preventDefault();
       var formData = $("#reasonForm").serializeArray();
        updatedata(formData);
    })

     function dateGen() {
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    }
    dateGen();

})

