$(document).ready(function(){

	 $('.input-text').keypress(function(e){
  		var inputValue = event.which;
  		// allow letters and whitespaces only.
  		if(!(inputValue >= 65 && inputValue <= 90) && !(inputValue >=97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 8) ) { 
  			event.preventDefault(); 
   		}
	});

   // $('.isInputFactor').keypress(function(e){
   //    // allow one dot and numbers
   //    var arr = "0123456789.";
   //    var code;
   //    if (window.event){
   //      code = e.keyCode;
   //    }else{
   //      code = e.which;
   //    }
   //    var char = keychar = String.fromCharCode(code);
   //    if (arr.indexOf(char) == -1){
   //      return false;
   //    }else if (char == "."){
   //      if ($(this).val().indexOf(".") > -1){
   //      return false;
   //      }
   //    }
   //    });

   $(".isInputFactor").autoNumeric('init');


 	$('.input-integer').keypress(function(event){
  		var inputValue = event.which;
  		// allow numbers only.
  		if(!(inputValue >= 48 && inputValue <= 57) && (inputValue != 8) && (inputValue !=0 )) { 
  			event.preventDefault(); 
   		}
 	});

  $('.input-alphanum').keypress(function(e){
      // var inputValue = event.which;
      // // allow numbers only.
      var regex = new RegExp("^[A-Za-z0-9 ]*$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          return true;
      }

      e.preventDefault();
      return false;
  });

  $('.input-mobileno').keypress(function(e){
      // var inputValue = event.which;
      // // allow numbers only.
      var regex = new RegExp("^[0-9 ]*$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          return true;
      }

      e.preventDefault();
      return false;
  });

	$(document).on('keypress','.input-integer',function(event){
     
      var inputValue = event.which;  
      if(!(inputValue >= 48 && inputValue <= 57) && inputValue!=8 && inputValue!=0) { 
        event.preventDefault(); 
      }
  });



  $('.input-integer').bind("cut copy paste",function(e) {
      e.preventDefault();
  });

  $('.input-text').bind("cut copy paste",function(e) {
      e.preventDefault();
  });  

  $('.input-text-name').keyup(function(evt){
    var txt = $(this).val();

    $(this).val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
  });

  $('.input-alphanum').keypress(function(event){
      var inputValue = event.which;
      // allow letters and whitespaces only.
      if(!(inputValue >= 65 && inputValue <= 90) && !(inputValue >=97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && !(inputValue >= 48 && inputValue <= 57) && !(inputValue == 46)) { 
        event.preventDefault(); 
      }
  });
  
  $('.form-group').on('keypress','.input-integer',function(event){
      var inputValue = event.which;
      // allow numbers only.
      if(!(inputValue >= 48 && inputValue <= 57)) { 
        event.preventDefault(); 
      }
  });

  // $('.form-group').on('keypress','.input-text',function(event){
  //     var inputValue = event.which;
  //     // allow letters and whitespaces only.
  //     if(!(inputValue >= 65 && inputValue <= 90) && !(inputValue >=97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) { 
  //       event.preventDefault(); 
  //     }
  // });

  $('.form-group').on('cut copy paste','.input-integer',function(e){
      e.preventDefault();
  });

  $('.form-group').on('cut copy paste','.input-text',function(e){
      e.preventDefault();
  }); 

  $('.form-group').on('cut copy paste','.input-alphanum',function(e){
      e.preventDefault();
  }); 

  $('.form-group').on('cut copy paste','.input-text-name',function(e){
      e.preventDefault();
  });  

  $('.form-group').on('keyup','.input-text-name',function(event){
    var txt = $(this).val();

    $(this).val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
  });

  $('.form-group').on('keypress','.input-alphanum',function(event){
      var inputValue = event.which;
      // allow letters and whitespaces only.
      if(!(inputValue >= 65 && inputValue <= 90) && !(inputValue >=97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && !(inputValue >= 48 && inputValue <= 57) && !(inputValue == 46)) { 
        event.preventDefault(); 
      }
  });

  $(document).on('click','#BTNMODAL,#btnapproveddialog',function(e){
    $('.modal').modal('hide');
  });

});

 function checkEmail(data){
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(data);
    
     
  }
