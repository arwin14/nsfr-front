$(document).ready(function(){
	setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);

	$(document).on('mousedown', '#captcha, #PHOTO_DISPLAY, #SIG_DISPLAY, .HeaderImage', function (){
        return false;
    });

    $(document).on('focus', ':input', function(){
        $(this).attr('autocomplete', 'off');
    });

    $(document).on('keydown', '.REMARKS', function(event){
         if (this.value.length === 0 && event.which === 32) {
            event.preventDefault();
        }
    });
});