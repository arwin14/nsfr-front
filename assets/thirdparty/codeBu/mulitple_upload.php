<?php

public function multipleupload(){
	$this->load->library('upload');
	$error = 0;
	$file_status['error'] = 0;
	$file_status['status'] = "";
	if(isset($_FILES)){
		$files = $_FILES;
		$perFileName = [];
		$perFileNameCount = 0;
		foreach ($files as $requirementsname => $value) {
			$array_filename = array("REQUIREMENTS_NAME" => $requirementsname);
			array_push($perFileName, $array_filename);
			$perFileNameCount += 1;
		}
		if($perFileNameCount > 0 ){
			for ($a=0; $a < $perFileNameCount; $a++) {
				$REQUIREMENTS_NAME = $perFileName[$a]['REQUIREMENTS_NAME'];

				for ($i = 0; $i < count($_FILES[''.$REQUIREMENTS_NAME.'']['name']); $i++):
				    $_FILES['uploadFile']['name'] = $_FILES[''.$REQUIREMENTS_NAME.'']['name'][$i];
				    $_FILES['uploadFile']['type'] = $_FILES[''.$REQUIREMENTS_NAME.'']['type'][$i];
				    $_FILES['uploadFile']['tmp_name'] = $_FILES[''.$REQUIREMENTS_NAME.'']['tmp_name'][$i];
				    $_FILES['uploadFile']['error'] = $_FILES[''.$REQUIREMENTS_NAME.'']['error'][$i];
				    $_FILES['uploadFile']['size'] = $_FILES[''.$REQUIREMENTS_NAME.'']['size'][$i];
				    
				    $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/NWRB/uploads';
				    $config['allowed_types'] = 'pdf';
			        $config['overwrite'] = TRUE;
				    
				    $this->upload->initialize($config);

				    if ($this->upload->do_upload('uploadFile')):
				        $error += 0;
				        $file_status['error'] = 0;
				    else:
				        $error += 1;
				        $file_status['error'] = 1;
				        $file_status['status'] = array('error' => $this->upload->display_errors());
				    endif;
				endfor;		
			}
			echo json_encode($file_status); 
		}
	}
}

?>