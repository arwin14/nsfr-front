var $displayTable = $('#displayTable');
var $pendingTable = $('#pendingTable');
var BASE_URL = $('.BASE_URL').val();

function displayTableOperateFormatter(value, row, index) {        
    return [
        '<a rel="tooltip" title="View" class="btn btn-fill btn-round btn-info btn-icon table-action view" href="javascript:void(0)">',
            '<i class="fa fa-eye"></i>',
        '</a>',
        '&nbsp;',
        '<a rel="tooltip" title="Approval Request" class="btn btn-fill btn-round btn-success btn-icon table-action approvalRequest" href="javascript:void(0)">',
            '<i class="fa fa-check-square-o"></i>',
        '</a>',
        ].join('');
}

$(document).ready(function(){     
    $('.li_side').removeClass('active');
    $('.cwp_side').addClass('active');
    $('.app_collapse').addClass('in');

    var displayTableResult = $('#displayTableResult');
    var pendingTableResult = $('#pendingTableResult');

    $(document).on('focus', ':input', function(){
        $(this).attr('autocomplete', 'off');
    });


    $('.card-wizard').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function(tab, navigation, index) {
        },

        onInit : function(tab, navigation, index){
            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            var $wizard = navigation.closest('.card-wizard');
            refreshAnimation($wizard, index);
            $('.moving-tab').css('transition','transform 0s');
       },

        onTabClick : function(tab, navigation, index){
        },

        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;

            var $wizard = navigation.closest('.card-wizard');

            refreshAnimation($wizard, index);
        }
    });

    $(window).resize(function(){
        $('.card-wizard').each(function(){
            $wizard = $(this);
            index = $wizard.bootstrapWizard('currentIndex');
            refreshAnimation($wizard, index);

            $('.moving-tab').css('transition','transform 0s');
        });
    });

    function refreshAnimation($wizard, index){
        $total = $wizard.find('.nav li').length;
        $li_width = 100/$total;

        total_steps = $wizard.find('.nav li').length;
        move_distance = $wizard.width() / total_steps;
        index_temp = index;
        vertical_level = 0;

        mobile_device = $(document).width() < 600 && $total > 3;

        if(mobile_device){
            move_distance = $wizard.width() / 2;
            index_temp = index % 2;
            $li_width = 50;
        }

        $wizard.find('.nav li').css('width',$li_width + '%');

        step_width = move_distance;
        move_distance = move_distance * index_temp;

        $current = index + 1;

        if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
            move_distance -= 8;
        } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
            move_distance += 8;
        }

        if(mobile_device){
            vertical_level = parseInt(index / 2);
            vertical_level = vertical_level * 38;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
            'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
            'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
    }

    $displayTable.on('all.bs.table', function (e, name, args) {
    })
    .on('load-success.bs.table', function (e, data) {
        displayTableResult.show();
        displayTableResult.html('<b>Message</b> - Table in being loaded.');
        displayTableResult.removeClass('alert-warning').addClass('alert-success');
        displayTableResult.fadeOut(3000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('load-error.bs.table', function (e, data) {
        displayTableResult.show();
        displayTableResult.html('<b>Message</b> - Table in being loaded Error.');
        displayTableResult.removeClass('alert-success').addClass('alert-warning');
        displayTableResult.fadeOut(3000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('search.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('column-switch.bs.table', function (e) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('toggle.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    });

    $pendingTable.on('all.bs.table', function (e, name, args) {
    })
    .on('load-success.bs.table', function (e, data) {
        pendingTableResult.show();
        pendingTableResult.html('<b>Message</b> - Table in being loaded.');
        pendingTableResult.removeClass('alert-warning').addClass('alert-success');
        pendingTableResult.fadeOut(3000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('load-error.bs.table', function (e, data) {
        pendingTableResult.show();
        pendingTableResult.html('<b>Message</b> - Table in being loaded Error.');
        pendingTableResult.removeClass('alert-success').addClass('alert-warning');
        pendingTableResult.fadeOut(3000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('search.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('column-switch.bs.table', function (e) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('toggle.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    });

    window.displayTableOperateEvents = {
        'click .view': function (e, value, row, index) {
            APPLICATION_NUMBER = JSON.stringify(row['APPLICATION_NUMBER']);
            APPLICATION_NUMBER = APPLICATION_NUMBER.replace(/"/g, "");
            viewapplication(APPLICATION_NUMBER);
        },
        'click .approvalRequest': function (e, value, row, index) {
            APPLICATION_NUMBER = JSON.stringify(row['APPLICATION_NUMBER']);
            APPLICATION_NUMBER = APPLICATION_NUMBER.replace(/"/g, "");
            approvalRequest(APPLICATION_NUMBER);
        }
    };

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    } 
    if(mm<10) {
        mm = '0'+mm
    } 
    var DATE_TODAY = yyyy + '-' + mm + '-' + dd;
    $('.DISPLAY_TABLE_DATE').html(formatDate(DATE_TODAY));
    
    var GLOBAL_PAGE_FORM = "";
    var GLOBAL_PAGE_TO = "";

    $displayTable.bootstrapTable({
        url: BASE_URL+"cwp/get_list",
        formatLoadingMessage: function () {
            return '<div class="preloader"><div class="spinner-layer pl-light-blue"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p>';
        },
        method: 'post',
        cache: false,
        processData: false,
        Date: DATE_TODAY,
        toolbar: ".displayTableToolbar",
        clickToSelect: true,
        showRefresh: true,
        showToggle: true,
        showColumns: true,
        pagination: true,
        searchAlign: 'left',
        search: true,
        searchText: "",
        pageSize: 10,
        clickToSelect: false,
        pageList: [10,25,50,100],
        searchTimeOut: 0,
        sidePagination: 'server', // client or server
        minimumCountColumns: 1,
        sortOrder: 'desc',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        queryParamsType: 'limit', // undefined
        responseHandler: function (res) {
            return res;
        },
        detailFormatter: function (index, row) {
            return '';
        },
        formatShowingRows: function(pageFrom, pageTo, totalRows){
            //do nothing here, we don't want to show the text "showing x of y from..."
        },
        formatRecordsPerPage: function(pageNumber){
            return pageNumber + " rows visible";
        },
        icons: {
            refresh: 'fa fa-refresh',
            toggle: 'fa fa-th-list',
            columns: 'fa fa-columns',
            detailOpen: 'fa fa-plus-circle',
            detailClose: 'fa fa-minus-circle'
        },
    });

    $(window).resize(function () {
        $displayTable.bootstrapTable('resetView');
    });

    $pendingTable.bootstrapTable({
        url: BASE_URL+"cwp/get_list",
        formatLoadingMessage: function () {
            return '<div class="preloader"><div class="spinner-layer pl-light-blue"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p>';
        },
        method: 'post',
        cache: false,
        processData: false,
        toolbar: ".pendingTableToolbar",
        clickToSelect: true,
        showRefresh: true,
        showToggle: true,
        showColumns: true,
        pagination: true,
        searchAlign: 'left',
        search: true,
        searchText: "",
        pageSize: 10,
        clickToSelect: false,
        pageList: [10,25,50,100],
        searchTimeOut: 0,
        sidePagination: 'server', // client or server
        minimumCountColumns: 1,
        sortOrder: 'desc',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        queryParamsType: 'limit', // undefined
        responseHandler: function (res) {
            return res;
        },
        detailFormatter: function (index, row) {
            return '';
        },
        formatShowingRows: function(pageFrom, pageTo, totalRows){
            //do nothing here, we don't want to show the text "showing x of y from..."
        },
        formatRecordsPerPage: function(pageNumber){
            return pageNumber + " rows visible";
        },
        icons: {
            refresh: 'fa fa-refresh',
            toggle: 'fa fa-th-list',
            columns: 'fa fa-columns',
            detailOpen: 'fa fa-plus-circle',
            detailClose: 'fa fa-minus-circle'
        },
    });

    $(window).resize(function () {
        $pendingTable.bootstrapTable('resetView');
    });

    function viewapplication(APPLICATION_NUMBER){
        if(APPLICATION_NUMBER != ""){
            $.ajax({
                url: BASE_URL+"applicationlist/view_applciation",
                type: "POST",data: { APPLICATION_NUMBER: APPLICATION_NUMBER, KEY : 'view', },cache : false,
            }).done(function(data){
                result = JSON.parse(data);
                setTimeout(function(){
                    $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                    $('#displayModal .modal-title').html('<i class="text-info">'+APPLICATION_NUMBER+'</i>');
                    $('#displayModal .modal-body').html(result.form);
                    $('#displayModal').modal('show');  
                    $('#displayModal').on('shown.bs.modal', function() {
                        $('[rel="tooltip"]').tooltip();  
                    });
                }, 100);
            });
        }else{
            $.notify({
                icon: "pe-7s-bell",
                message: "<b>Select application from table list."

            },{
                type: 'danger',
                timer: 3000,
                delay: 2500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });                
        }
    }

    $('.DISPLAY_TABLE_SEARCH_DATE').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove',
            maxDate: today,
        }
    }).on("dp.show", function(){
        $(this).data('DateTimePicker').maxDate(today);
    }).on("dp.hide", function(){ //hide or change
        var SEARCH_DATE = new Date($('.DISPLAY_TABLE_SEARCH_DATE').val());

        var dd = SEARCH_DATE.getDate();
        var mm = SEARCH_DATE.getMonth()+1; //January is 0!
        var yyyy = SEARCH_DATE.getFullYear();
        if(dd<10) {
            dd = '0'+dd
        } 
        if(mm<10) {
            mm = '0'+mm
        } 
        NEW_DATE = yyyy + '-' + mm + '-' + dd;
        
        if(NEW_DATE != DATE_TODAY){
            if(NEW_DATE != "NaN-NaN-NaN"){
                $displayTable.bootstrapTable('refreshOptions', {
                    Date: NEW_DATE,
                    pageSize: 10,
                    pageNumber: 1,
                    searchText: ' ', //important! [space] value in searchText to reset search saved value
                    sortOrder: 'N/A',
                    sortName: 'N/A',
                });
                $('.DISPLAY_TABLE_DATE').html(formatDate(NEW_DATE));
                DATE_TODAY = NEW_DATE;
            }
        }
    });

    function approvalRequest(APPLICATION_NUMBER){
        if(APPLICATION_NUMBER != ""){
            $.ajax({
                url: BASE_URL+"cwp/approvalRequest",
                type: "POST",data: { APPLICATION_NUMBER: APPLICATION_NUMBER,},
                cache: false,
            }).done(function(data){
                result = JSON.parse(data);
                setTimeout(function(){
                    $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                    $('#displayModal .modal-title').html('<i class="text-info">'+APPLICATION_NUMBER+'</i>');
                    $('#displayModal .modal-body').html(result.form);
                    $('#displayModal').modal('show');

                    var $FORM_VALIDATOR = $("#APPROVAL_REQUEST_FORM").validate({
                        rules:{
                            REMARKS: { required: true },
                        },
                        messages:{
                            REMARKS: { required: "Required fields." },
                        }
                    });
                }, 100);                
            });
        }else{
            $.notify({
             icon: "pe-7s-bell",
             message: "<b>Select application from table list."

            },{
                type: 'danger',
                timer: 3000,
                delay: 2500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });                
        }
    }

    $(document).on('click', '.DEFERRED_BUTTON', function(){
        var valid = $("#APPROVAL_REQUEST_FORM").valid();

        if(valid){
            var form = $('#APPROVAL_REQUEST_FORM')[0];
            var formData = new FormData(form);
            formData.append('APP_NUM',APPLICATION_NUMBER);
            formData.append('STATE',"ALTER");   

            var BUTTON = "DEFERRED_BUTTON";
            submit_request(formData, BUTTON);
        }
    });

    $(document).on('click', '.APPROVE_BUTTON', function(){
        var valid = $("#APPROVAL_REQUEST_FORM").valid();

        if(valid){
            var form = $('#APPROVAL_REQUEST_FORM')[0];
            var formData = new FormData(form);
            formData.append('APP_NUM',APPLICATION_NUMBER);
            formData.append('STATE',"APPROVE");   

            var BUTTON = "APPROVE_BUTTON";
            submit_request(formData, BUTTON);
        }
    });

    $(document).on('click', '.REJECT_BUTTON', function(){
        var valid = $("#APPROVAL_REQUEST_FORM").valid();

        if(valid){
            var form = $('#APPROVAL_REQUEST_FORM')[0];
            var formData = new FormData(form);
            formData.append('APP_NUM',APPLICATION_NUMBER);
            formData.append('STATE',"REJECT");   

            var BUTTON = "REJECT_BUTTON";
            submit_request(formData, BUTTON);
        }
    });

    function submit_request(formData, BUTTON){
        $('.'+BUTTON).attr('disabled', true);
        $.ajax({
            url: BASE_URL+"cwp/submit_request",
            data:  formData,
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() { $(".page-loader-wrapper").fadeIn(); },
            complete: function() { $(".page-loader-wrapper").fadeOut(); }
        }).done(function(data){
           json = JSON.parse(data);
            if(json.Code == 0) { 
                setTimeout(function(){ 
                    $('#displayModal').modal('hide');

                    $('#messageModal .modal-dialog').attr('class', 'modal-dialog modal-sm');
                    $('#messageModal .modal-title').html('Message');
                    $('#messageModal .modal-body').html(json.Message);
                    $('#messageModal .modal-footer').html();
                    $('#messageModal').modal('show');  
                    $('#messageModal').on('shown.bs.modal', function() {
                        $('.'+BUTTON).removeAttr('disabled', true);
                        $displayTable.bootstrapTable('refresh');
                        $pendingTable.bootstrapTable('refresh');
                    });
                }, 100);
            } else {
                setTimeout(function(){
                    $('#messageModal .modal-dialog').attr('class', 'modal-dialog modal-sm');
                    $('#messageModal .modal-title').html('Message');
                    $('#messageModal .modal-body').html(json.Message);
                    $('#messageModal .modal-footer').html();
                    $('#messageModal').modal('show');
                    $('#messageModal').on('hide.bs.modal', function() {
                        $('.'+BUTTON).removeAttr('disabled', true);

                        setTimeout(function(){ 
                            $("body").addClass("modal-open");
                        }, 500);
                    });
                }, 100);
            }
        });
    }

    function formatDate(date){
        split = date.split("-");
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return months[parseInt(split[1] - 1)] + " " + split[2] + ", "+ split[0];
    }
});