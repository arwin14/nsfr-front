var BASE_URL = $('.BASE_URL').val();

$(document).ready(function(){
	var global_email = '';
	var global_ip = '';
	// var global_password = '';

	lbd.checkFullPageBackgroundImage();

    setTimeout(function(){
        $('.card').removeClass('card-hidden');
    }, 700);

    $(".EMAIL").keypress(function(event){
        if (event.which == 13) {
            $('#verify_btn').trigger("click");
        }
    });

    $('.EMAIL').focus();

    $(".PASSWORD").keypress(function(event){
        if (event.which == 13) {
            $('#login_btn').trigger("click");
        }
    });

	var findIP = new Promise(r=>{
		var w=window,
		a=new (w.RTCPeerConnection||w.mozRTCPeerConnection||w.webkitRTCPeerConnection)({iceServers:[]}),
		b=()=>{};		
		a.createDataChannel("");
		a.createOffer(c=>a.setLocalDescription(c,b,b),b);
		a.onicecandidate=c=>{
			try{
				c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r)
			} catch(e){
				
			}
		}
	});
	
	findIP.then(function(ip) {
		global_ip = ip;
	}, function(err) {
		console.log(err);
	});

    $(document).on('click', '#verify_btn', function(event){
    	event.preventDefault();
        var EMAIL = $('#EMAIL').val();

        if(EMAIL == ''){
	        $.notify({
	        	icon: "pe-7s-bell",
	        	message: "Email is required."

	        },{
	            type: 'danger',
	            timer: 1000,
	            delay: 1500,
	            placement: {
	                from: 'top',
	                align: 'center'
	            }
	        });
        } else {
        	$.ajax({
		    	url: BASE_URL+"session/checkemail",
		    	type: "post",
		    	data: {
		    		EMAIL : EMAIL
		    	}
		    }).done(function(data){
		    	json = JSON.parse(data);
		    	
		    	if(json.Code == 1) {
		    		global_email = '';
			        $.notify({
			        	icon: "pe-7s-bell",
			        	message: json.Message

			        },{
			            type: 'danger',
			            timer: 1000,
			            delay: 1500,
			            placement: {
			                from: 'top',
			                align: 'center'
			            }
			        });
		    	} else {
		    		global_email = EMAIL;
		    		$('.email_disp').hide();
		    		$('.email_btn').hide();
		    		$('.email_label').hide();

		    		$('.password_disp').show();
		    		$('.password_btn').show();
		    		$('.password_label').show();

		    		$('#PASSWORD').focus();
		    	}		    		
		    });
        }
    });

    $(document).on('click', '#back_btn', function(event){
    	event.preventDefault();
    	$('.password_disp').hide();
		$('.password_btn').hide();

    	$('.email_disp').show();
		$('.email_btn').show();

		$('#EMAIL').focus();	
    	
    });

    $(document).on('click', '#login_btn', function(event){
    	event.preventDefault();
    	var PASSWORD = $('#PASSWORD').val();

        if(PASSWORD == ''){
	        $.notify({
	        	icon: "pe-7s-bell",
	        	message: "Password is required."

	        },{
	            type: 'danger',
	            timer: 1000,
	            delay: 1500,
	            placement: {
	                from: 'top',
	                align: 'center'
	            }
	        });
        } else {
        	$.ajax({
		    	url: BASE_URL+"session/login",
		    	type: "post",
		    	data: {
		    		EMAIL : global_email,
		    		IP : global_ip,
		    		PASSWORD : PASSWORD
		    	}
		    }).done(function(data){
		    	json = JSON.parse(data);

		    	if(json.Code == 1) {
			        $.notify({
			        	icon: "pe-7s-bell",
			        	message: json.Message

			        },{
			            type: 'danger',
			            timer: 1000,
			            delay: 1500,
			            placement: {
			                from: 'top',
			                align: 'center'
			            }
			        });
		    	} else {
		    		location.reload();
		    	}	

		    });
        }
    });

    $('.show_me').on('click', function() {
    	var key = $(".show_me").data("key");

    	if(key == '0'){
			$(".show_me").data("key", "1");
          	$('.PASSWORD').attr('type', 'text');
          	$(".show_me").attr("data-original-title", "hide");
    	}else{
			$(".show_me").data("key", "0");
        	$('.PASSWORD').attr('type', 'PASSWORD');
          	$(".show_me").attr("data-original-title", "show");
    	}
    	$(".show_me").tooltip("hide");

    });
});