var $table = $('#displaytable');

function operateFormatter(value, row, index) {        
    return [
        '<a rel="tooltip" title="View Details" class="btn btn-fill btn-round btn-info btn-icon table-action view" href="javascript:void(0)">',
            '<i class="fa fa-eye"></i>',
        '</a>',
        '&nbsp;',
        '<a rel="tooltip" title="Agent Form" class="btn btn-fill btn-round btn-warning btn-icon table-action agent_form" href="javascript:void(0)">',
            '<i class="fa fa-shield"></i>',
        '</a>',
        ].join('');
}

$(document).ready(function(){     
    $('.li_side').removeClass('active');
    $('.secureforms_side').addClass('active');
    $('.app_collapse').addClass('in');

    var $result = $('#eventsResult');

    $('#displaytable').on('all.bs.table', function (e, name, args) {
    })
    .on('load-success.bs.table', function (e, data) {
        $result.show();
        $result.html('<b>Message</b> - Table in being loaded.');
        $result.removeClass('alert-warning').addClass('alert-success');
        $result.fadeOut(6000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('load-error.bs.table', function (e, data) {
        $result.show();
        $result.html('<b>Message</b> - Table in being loaded Error.');
        $result.removeClass('alert-success').addClass('alert-warning');
        $result.fadeOut(6000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('search.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('column-switch.bs.table', function (e) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('toggle.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    });

    window.operateEvents = {
        'click .view': function (e, value, row, index) {
            waterpermitid = JSON.stringify(row['waterpermitid']);
            waterpermitid = waterpermitid.replace(/"/g, "");
            viewapplication(waterpermitid);
        },
        'click .agent_form': function (e, value, row, index) {
            waterpermitid = JSON.stringify(row['waterpermitid']);
            waterpermitid = waterpermitid.replace(/"/g, "");
            agent_form(waterpermitid);
        }
    };

    $table.bootstrapTable({
        url: window.location.href+"/get_list",
        formatLoadingMessage: function () {
            return '<div class="preloader"><div class="spinner-layer pl-yellow"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p>';
        },
        cache: false,
        contentType: false,
        processData: false,
        toolbar: ".toolbar",
        clickToSelect: true,
        showRefresh: true,
        search: true,
        showToggle: true,
        showColumns: true,
        pagination: true,
        searchAlign: 'left',
        pageSize: 8,
        clickToSelect: false,
        pageList: [8,10,25,50,100],
        searchTimeOut: 0,
        formatShowingRows: function(pageFrom, pageTo, totalRows){
            //do nothing here, we don't want to show the text "showing x of y from..."
        },
        formatRecordsPerPage: function(pageNumber){
            return pageNumber + " rows visible";
        },
        icons: {
            refresh: 'fa fa-refresh',
            toggle: 'fa fa-th-list',
            columns: 'fa fa-columns',
            detailOpen: 'fa fa-plus-circle',
            detailClose: 'fa fa-minus-circle'
        },
    });

    $(window).resize(function () {
        $table.bootstrapTable('resetView');
    });

    function viewapplication(waterpermitid){
        if(waterpermitid != ""){
            $.ajax({
                url: window.location.origin+"/NWRB/applicationlist/view_applciation",
                type: "POST",data: { waterpermitid: waterpermitid, KEY : 'view', },cache : false,
            }).done(function(data){
                result = JSON.parse(data);
                $('#myModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                $('#myModal .modal-title').html('<i class="text-warning">'+waterpermitid+'</i>');
                $('#myModal .modal-body').html(result.form);
                $('#myModal .modal-footer').html(
                    '<button type="button" class="btn btn-fill btn-md" data-dismiss="modal">Close</button>'
                );
                $('#myModal').modal('show'); 
            });
        }else{
            $.notify({
                icon: "pe-7s-bell",
                message: "<b>Select application from table list."

            },{
                type: 'danger',
                timer: 3000,
                delay: 2500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });                
        }
    }

    function agent_form(waterpermitid){
        if(waterpermitid != ""){
            $.ajax({
                url: window.location.href+"/agent_form",
                type: "POST",data: { waterpermitid: waterpermitid,},cache : false,
            }).done(function(data){
                result = JSON.parse(data);
                $('#myModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                $('#myModal .modal-title').html('<i class="text-warning">'+waterpermitid+'</i>');
                $('#myModal .modal-body').html(result.form);
                $('#myModal .modal-footer').html('');
                $('#myModal').modal('show');

                $('.AGENT_PREPARED_DATE').datetimepicker({
                    format: 'MM/DD/YYYY',
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-chevron-up",
                        down: "fa fa-chevron-down",
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-screenshot',
                        clear: 'fa fa-trash',
                        close: 'fa fa-remove'
                    }
                }).on("dp.show", function(){
                }).on("dp.change", function(){ //hide or change 
                });  

                $("#AGENT_FORM").validate({
                    rules:{
                        LATITUDE: { required: true },
                        LONGITUDE: { required: true },
                        AGENT_NAME: { required: true },
                        AGENT_PREPARED_DATE: { required: true },
                    },
                    messages:{
                        LATITUDE: { required: "Required fields." },
                        LONGITUDE: { required: "Required fields." },
                        AGENT_NAME: { required: "Required fields." },
                        AGENT_PREPARED_DATE: { required: "Required fields." },
                    },
                    submitHandler: function(form) {
                        $('.SUBMIT_BUTTON').attr('disabled', true);
                        var form = $('#AGENT_FORM')[0];
                        var formData = new FormData(form);
                        $.ajax({
                            url: window.location.href+"/submit_application",
                            data:  formData,
                            type: 'POST',
                            contentType: false,
                            processData: false,
                            beforeSend: function() { $(".page-loader-wrapper").fadeIn(); },
                            complete: function() { $(".page-loader-wrapper").fadeOut(); }
                        }).done(function(data){
                           json = JSON.parse(data);
                           if(json.Code == 0) { 
                                setTimeout(function(){ 
                                    $.notify({
                                        icon: "pe-7s-bell",
                                        message: "<b>"+json.Message+""

                                    },{
                                        type: 'success',
                                        timer: 3000,
                                        delay: 2500,
                                        placement: {
                                            from: 'top',
                                            align: 'center'
                                        }
                                    });  
                                    $('.SUBMIT_BUTTON').removeAttr('disabled', true);
                                    $('.RESET_BUTTON').click();
                                    $('#myModal').modal('hide');
                                    displaytable();
                                }, 100);
                            } else {
                                setTimeout(function(){ 
                                    $('.SUBMIT_BUTTON').removeAttr('disabled', true);
                                     $.notify({
                                        icon: "pe-7s-bell",
                                        message: "<b>"+json.Message+""

                                    },{
                                        type: 'danger',
                                        timer: 3000,
                                        delay: 2500,
                                        placement: {
                                            from: 'top',
                                            align: 'center'
                                        }
                                    }); 
                                }, 100);
                            }
                        });
                    }
                });
            });
        }else{
            $.notify({
             icon: "pe-7s-bell",
             message: "<b>Select application from table list."

            },{
                type: 'danger',
                timer: 3000,
                delay: 2500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });                
        }
    }

    function displaytable(){
        $.ajax({
            url: window.location.href+"/get_list",
            type: "POST",cache : false,contentType : false,processData : false,
            beforeSend: function() { 
                '<div class="preloader">'+
                    '<div class="spinner-layer pl-yellow">'+
                        '<div class="circle-clipper left">'+
                            '<div class="circle"></div>'+
                        '</div>'+
                        '<div class="circle-clipper right">'+
                            '<div class="circle"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<p>Please wait...</p>'
            },
        }).done(function(data){
            result = JSON.parse(data); 

            $table.bootstrapTable('removeAll');
            if(result.length > 0){
                var data1 = []; 
                for (var i = 0; i < result.length; i++) {
                    var data = {
                        "waterpermitid": result[i].waterpermitid,
                        "firstname": result[i].firstname,
                        "middlename": result[i].middlename,
                        "surname": result[i].surname,
                        "suffix": result[i].suffix,
                        "citizenship": result[i].citizenship,
                        "emailaddress": result[i].emailaddress,
                        "watersourcename": result[i].watersourcename
                    }
                    data1.push(data);
                }
                $table.bootstrapTable('load', data1);
                $('[rel="tooltip"]').tooltip();
            }else{
                $table.bootstrapTable('removeAll');
            }
        });
    }

    $(document).on('click', '.RESET_BUTTON', function(){
        $('#AGENT_FORM')[0].reset();
    });
});