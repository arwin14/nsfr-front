function initMap() {
    var LATITUDE = $('.LATITUDE').val();
    var LONGITUDE = $('.LONGITUDE').val();

    LATITUDE = parseFloat(LATITUDE);
    LONGITUDE = parseFloat(LONGITUDE);
    var APPLICATION_LATLNG = {lat: LATITUDE, lng: LONGITUDE};

    var map = new google.maps.Map(document.getElementById('mymap'), {
        center: APPLICATION_LATLNG,
        zoom: 15,
        mapTypeId: 'roadmap'
    });

    var marker = new google.maps.Marker({
      position: APPLICATION_LATLNG,
      map: map,
      title: ''
    });

}
initMap();