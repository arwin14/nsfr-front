var $displaytable = $('#displaytable');
var BASE_URL = $('.BASE_URL').val();

function operateFormatter(value, row, index) { 
    return [
        '<a rel="tooltip" title="View" class="btn btn-fill btn-round btn-info btn-icon table-action view" href="javascript:void(0)">',
            '<i class="fa fa-eye"></i>',
        '</a>',
        '&nbsp;',
        ].join('');
}
$(document).ready(function(){     
    $('.li_side').removeClass('active');
    $('.applicationlist_side').addClass('active');
    $('.app_collapse').addClass('in');

    var $result = $('#eventsResult');

    $displaytable.on('all.bs.table', function (e, name, args) {
    })
    .on('load-success.bs.table', function (e, data) {
        $result.show();
        $result.html('<b>Message</b> - Table in being loaded.');
        $result.removeClass('alert-warning').addClass('alert-success');
        $result.fadeOut(3000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('load-error.bs.table', function (e, data) {
        $result.show();
        $result.html('<b>Message</b> - Table in being loaded Error.');
        $result.removeClass('alert-success').addClass('alert-warning');
        $result.fadeOut(3000);
        $('[rel="tooltip"]').tooltip();
    })
    .on('search.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('column-switch.bs.table', function (e) {
        $('[rel="tooltip"]').tooltip();
    })
    .on('toggle.bs.table', function (e, data) {
        $('[rel="tooltip"]').tooltip();
    });

    window.operateEvents = {
        'click .view': function (e, value, row, index) {
            APPLICATION_NUMBER = JSON.stringify(row['APPLICATION_NUMBER']);
            APPLICATION_NUMBER = APPLICATION_NUMBER.replace(/"/g, "");
            viewapplication(APPLICATION_NUMBER);
        }
    };

    $displaytable.bootstrapTable({
        url: BASE_URL+"applicationlist/get_list",
        formatLoadingMessage: function () {
            return '<div class="preloader"><div class="spinner-layer pl-light-blue"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p>';
        },
        method: 'post',
        cache: false,
        processData: false,
        toolbar: ".toolbar",
        clickToSelect: true,
        showRefresh: true,
        showToggle: true,
        showColumns: true,
        pagination: true,
        searchAlign: 'left',
        search: true,
        pageSize: 10,
        clickToSelect: false,
        pageList: [10,25,50,100],
        searchTimeOut: 0,
        sidePagination: 'server', // client or server
        minimumCountColumns: 1,
        sortOrder: 'desc',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        queryParamsType: 'limit', // undefined
        responseHandler: function (res) {
            return res;
        },
        detailFormatter: function (index, row) {
            return '';
        },
        formatShowingRows: function(pageFrom, pageTo, totalRows){
            //do nothing here, we don't want to show the text "showing x of y from..."
        },
        formatRecordsPerPage: function(pageNumber){
            return pageNumber + " rows visible";
        },
        icons: {
            refresh: 'fa fa-refresh',
            toggle: 'fa fa-th-list',
            columns: 'fa fa-columns',
            detailOpen: 'fa fa-plus-circle',
            detailClose: 'fa fa-minus-circle'
        },
    });

    $(window).resize(function () {
        $displaytable.bootstrapTable('resetView');
    });

    function viewapplication(APPLICATION_NUMBER){
        if(APPLICATION_NUMBER != ""){
            $.ajax({
                url: BASE_URL+"applicationlist/view_applciation",
                type: "POST",data: { APPLICATION_NUMBER: APPLICATION_NUMBER, KEY : 'view', },cache : false,
            }).done(function(data){
                result = JSON.parse(data);
                setTimeout(function(){              
                    $('#displayModal .modal-dialog').attr('class', 'modal-dialog modal-lg');
                    $('#displayModal .modal-title').html('<i class="text-info">'+APPLICATION_NUMBER+'</i>');
                    $('#displayModal .modal-body').html(result.form);
                    $('#displayModal').modal('show'); 
                    $('#displayModal').on('shown.bs.modal', function() {
                        $('[rel="tooltip"]').tooltip();  
                    });
                }, 100);
            });
        }else{
            $.notify({
             icon: "pe-7s-bell",
             message: "<b>Select application from table list."

            },{
                type: 'danger',
                timer: 3000,
                delay: 2500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });                
        }
    }
});