var BASE_URL = $('.BASE_URL').val();

$(document).ready(function(){         
    $(".myclass").attr('disabled','disabled');

    $(document).on('focus', ':input', function(){
        $(this).attr('autocomplete', 'off');
    });

    $('.card-wizard').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function(tab, navigation, index) {
        },

        onInit : function(tab, navigation, index){
            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            var $wizard = navigation.closest('.card-wizard');
            refreshAnimation($wizard, index);
            $('.moving-tab').css('transition','transform 0s');
       },

        onTabClick : function(tab, navigation, index){
        },

        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;

            var $wizard = navigation.closest('.card-wizard');

            refreshAnimation($wizard, index);
        }
    });

    $(window).resize(function(){
        $('.card-wizard').each(function(){
            $wizard = $(this);
            index = $wizard.bootstrapWizard('currentIndex');
            refreshAnimation($wizard, index);

            $('.moving-tab').css('transition','transform 0s');
        });
    });

    function refreshAnimation($wizard, index){
        $total = $wizard.find('.nav li').length;
        $li_width = 100/$total;

        total_steps = $wizard.find('.nav li').length;
        move_distance = $wizard.width() / total_steps;
        index_temp = index;
        vertical_level = 0;

        mobile_device = $(document).width() < 600 && $total > 3;

        if(mobile_device){
            move_distance = $wizard.width() / 2;
            index_temp = index % 2;
            $li_width = 50;
        }

        $wizard.find('.nav li').css('width',$li_width + '%');

        step_width = move_distance;
        move_distance = move_distance * index_temp;

        $current = index + 1;

        if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
            move_distance -= 8;
        } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
            move_distance += 8;
        }

        if(mobile_device){
            vertical_level = parseInt(index / 2);
            vertical_level = vertical_level * 38;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
            'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
            'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
    }
});