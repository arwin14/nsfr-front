var BASE_URL = $('.BASE_URL').val();

$(document).ready(function(){
    $('.li_side').removeClass('active');
    $('.application_side').addClass('active');
    $('.app_collapse').addClass('in');

    $(document).on('keydown', '.LATITUDE , .LONGITUDE', function (e) {
       var key = e.keyCode;

        if($(this).val().indexOf('.') !== -1 && key == 190){
            e.preventDefault();
        }

        if(key == 190){
            // period
            return true;
        }else if(key == 9){
            // tab
            return true;
        }else if(key == 189){
            // dash
            return true;
        }else if(key == 13){
            // enter
            return true;
        }else if((e.shiftKey &&  key >= 48) || (e.shiftKey &&  key >= 105)){
            // shift + keys
            e.preventDefault();
        }else if(key >= 96 && key <= 105){
            // numpad numbers
            return true;
        }else if(key == 37 || key == 39 || key == 116 || key == 8 || key == 46){
            // arrow left, arrow right, refresh, backspace, delete
            return true;
        }else if(key >= 48 && key <= 57){
            // numbers
            return true;
        }else{
            e.preventDefault();
        }
    });

    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });

    $(document).on('change', '.file', function(){
        var names = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            names.push($(this).get(0).files[i].name);
        }
        $(this).parent().find('.form-control').val(names);
    });

    $(document).on('click', '.upload_photo', function(){
        $(this).closest('td').find('.browse_photo').click();
    });

    $(document).on('change', '.browse_photo', function(){
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('.PHOTO_DISPLAY')
                    .attr('src', e.target.result)
                    .width(165)
                    .height(165);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('click','.open_camera',function(e){
        setTimeout(function(){ 
            $('#imageModal').modal({backdrop: 'static', keyboard: false});
            $('#imageModal .modal-dialog').attr('class','modal-dialog modal-sm');
            $('#imageModal .modal-title').html("Camera");
            $('#imageModal .modal-body').html(
                '<div id="camera" style="text-align: center;">'
                );
            $('#imageModal .modal-footer').html(
                '<button type="button" class="btn btn-success btn-sm" id="take_snapshots">Take Shots</button>'
                );
            $('#imageModal').modal('show');  
            $('#imageModal').on('shown.bs.modal', function() {
                camera_js();
                $('#camera').css('height', '');
                $('#camera').css('width', '');
            });
        }, 100);
    });

    function camera_js(){
        Webcam.set({
            width: 240,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90,
        });
        Webcam.attach('#camera');
    }

    $(document).on('click', '#take_snapshots', function(){
        Webcam.snap( function(photo_url) {
            $('#PHOTO_DISPLAY').attr('src', photo_url);
        } );
        setTimeout(function(){ 
            $('#imageModal').modal({backdrop: 'static', keyboard: true});
            $('#imageModal').modal('hide');
        }, 100);
    });

    $(document).on('click','.signature',function(e){
        setTimeout(function(){ 
            $('#signatureModal').modal({backdrop: 'static', keyboard: false});
            $('#signatureModal .modal-dialog').attr('class','modal-dialog modal-sm');
            $('#signatureModal .modal-title').html("Signature");
            $('#signatureModal .modal-body').html(
                '<div class="row">'+
                    '<div class="col-md-12">'+
                        '<div class="card">'+
                            '<div class="content text-center" id="signature-pad" style="background-color: #eaeaeb">'+
                                '<canvas style="border: 1px solid #eaeaeb;" width="230" height="230"></canvas>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                );
            $('#signatureModal .modal-footer').html(
                '<button type="button" class="btn btn-warning btn-sm" id="clear_signature">Clear</button>'+
                '<button type="button" class="btn btn-warning btn-sm" id="undo_signature">Undo</button>'+
                '<button type="button" class="btn btn-success btn-sm" id="take_signature">Take Signature</button>'
                );
            $('#signatureModal').modal('show');  
            $('#signatureModal').on('shown.bs.modal', function() {
                signature_js();
                $('#mycanvas').css('height', '230');
                $('#mycanvas').css('width', '230');
            });
        }, 100);
    });

    var global_signaturePad = "";

    function signature_js(){
        var wrapper = document.getElementById("signature-pad");
        var canvas = wrapper.querySelector("canvas");
        var savePNGButton = wrapper.querySelector("[data-action=save-png]");
        var signaturePad = new SignaturePad(canvas, {
        backgroundColor: 'rgb(255, 255, 255)'
        });

        global_signaturePad = signaturePad;

        function resizeCanvas() {
        var ratio =  Math.max(window.devicePixelRatio || 1, 1);
        // canvas.width = canvas.offsetWidth * ratio;
        // canvas.height = canvas.offsetHeight * ratio;
        canvas.width = "230";
        canvas.height = "230";
        canvas.getContext("2d").scale(ratio, ratio);
        signaturePad.clear();
        }
        window.onresize = resizeCanvas;
        resizeCanvas();
    }

    $(document).on('click', '#take_signature', function(event){
        if (global_signaturePad.isEmpty()) {
            setTimeout(function(){ 
                $('#myModal .modal-dialog').attr('class', 'modal-dialog modal-sm');
                $('#myModal .modal-title').html('Message');
                $('#myModal .modal-body').html('Please provide a signature first.');
                $('#myModal .modal-footer').html();
                $('#myModal').modal('show');
            }, 100);
        } else {
            var signature_url = global_signaturePad.toDataURL();
            $('#SIG_DISPLAY').attr('src', signature_url);
            setTimeout(function(){ 
                $('#signatureModal').modal({backdrop: 'static', keyboard: true});
                $('#signatureModal').modal('hide');  
            }, 100);
        }
    });

    $(document).on('click', '#undo_signature', function(event){
        var data = global_signaturePad.toData();
        if (data) {
            data.pop();
            global_signaturePad.fromData(data);
        }
    });

    $(document).on('click', '#clear_signature', function(event){
        global_signaturePad.clear();
    });

    $('.OFFICIAL_RECEIPT_DATE').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

    jQuery.validator.addMethod("filesize_max", function(value, element, param) {
    var isOptional = this.optional(element),
        file;

        console.log(isOptional);
        console.log(value);
        console.log(element);
        console.log(param);
    
    
        if(isOptional) {
            return isOptional;
        }
        
        if ($(element).attr("type") === "file") {
            
            if (element.files && element.files.length) {
                
                file = element.files[0];            
                return ( file.size && file.size <= param ); 
            }
        }
        return false;
    }, "File size is too large.");


    $("#APPLICATION_FORM").validate({
        errorPlacement: function (error, element) {
            // if(element[0]['type'] === "file"){
            //     $(element[0]).next('div').find('.form-control').addClass('error');
            //     // console.log();
            //     console.log(element);
            //     // $('.main-panel').scrollTop(220);

            // }
        },
        rules:{
            FIRST_NAME: { required: true },
            LAST_NAME: { required: true },
            MAILING_PROVINCE: { required: true },
            TELEPHONE_NUMBER: { required: true },
            TAX_ACCOUNT_NUMBER: { required: true },
            EMAIL_ADDRESS: { required: true },
            WATER_SOURCE_NAME: { required: true },
            DIVERSION_POINT: { required: true },
            LATITUDE: { required: true },
            LONGITUDE: { required: true },
            DIVERSION_METHOD: { required: true },
            LOW_FLOW: { required: true },
            WATER_NEEDED: { required: true },
            PURPOSE: { required: true },
            HOLDING_PERSON_FIRST_NAME: { required: true },
            HOLDING_PERSON_LAST_NAME: { required: true },
            CODE: { required: true },
            // "A[]": { required: true },
            // "B[]": { required: true },
            // "A[]": { filesize_max: "210" }
        },
        submitHandler: function(form) {
            $.post(BASE_URL+"assets/captcha/verifydata.php?"+$("#CODE").serialize(), {
            }, function(response){
                if(response == "success"){
                    $('.SUBMIT_BUTTON').attr('disabled', true);
                    var form = $('#APPLICATION_FORM')[0];
                    var formData = new FormData(form);

                    var PERSONAL_PHOTO = $('.PHOTO_DISPLAY').attr('src');
                    var SIGNATURE = $('.SIG_DISPLAY').attr('src');
                    formData.append('BASE64_PERSONAL_PHOTO',PERSONAL_PHOTO); 
                    formData.append('BASE64_SIGNATURE',SIGNATURE);

                    $.ajax({
                        url: BASE_URL+"onlineapplication/save_application",
                        data:  formData,
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        beforeSend: function() { 
                            $(".page-loader-wrapper").fadeIn(); 
                            $(".page-loader-wrapper").find('.text_loader').html('Please Wait, Saving Files...');
                        },
                        complete: function() { $(".page-loader-wrapper").fadeOut(); }
                    }).done(function(data){
                       json = JSON.parse(data);
                       if(json.error == 0) { 
                            setTimeout(function(){ 
                                $('.RESET_BUTTON').click();
                                $('#myModal .modal-dialog').attr('class', 'modal-dialog modal-sm');
                                $('#myModal .modal-title').html('Message');
                                $('#myModal .modal-body').html(json.Message);
                                $('#myModal .modal-footer').html();
                                $('#myModal').modal('show');
                                $('#myModal').on('hide.bs.modal', function() {
                                    $('.SUBMIT_BUTTON').removeAttr('disabled', true);
                                });
                            }, 100);
                        } else {
                            var Message = json.Message.replace("<p>", "");
                            var errorMessage = Message.replace("</p>", "");
                            setTimeout(function(){ 
                                $('.SUBMIT_BUTTON').removeAttr('disabled', true);
                                $('#myModal .modal-dialog').attr('class', 'modal-dialog modal-sm');
                                $('#myModal .modal-title').html('Message');
                                $('#myModal .modal-body').html(json.Message);
                                $('#myModal .modal-footer').html();
                                $('#myModal').modal('show');
                            }, 100);
                        }                       
                    });
                }else{
                    $.notify({
                     icon: "pe-7s-bell",
                     message: "<b>Something went wrong on CODE input."

                    },{
                        type: 'danger',
                        timer: 1000,
                        delay: 100,
                        placement: {
                            from: 'top',
                            align: 'center'
                        }
                    }); 
                }
            });   
        }
    });

    $(document).on('change', '.PURPOSE', function(){
        if($(this).val() != ""){
            var fees = $('option:selected', this).attr('data-fees');
            $('.PURPOSE_LEVEL').find('option').remove();
            if(fees == "0.00"){
                $.ajax({
                    url: BASE_URL+"onlineapplication/GET_SUBPERMITUSE",
                    data:  {PURPOSE_CODE: $(this).val()},
                    type: 'POST',                
                    beforeSend: function() { 
                        $('.PURPOSE_LEVEL').prepend($('<option class="myPURPOSE_LEVEL" disabled selected></option>').html('Loading Purpose Level...'));
                    },
                    complete: function() { 
                        $(".PURPOSE_LEVEL option[class='myPURPOSE_LEVEL']").remove();
                    },
                }).done(function(data){
                   json = JSON.parse(data);    
                   if(json.Code == 0){
                        for (var i = 0; i < json.Data.details.length; i++) {

                            if(json.Data.details[0].CODE == json.Data.details[i].CODE){
                                $('.PURPOSE_LEVEL').append('<option value="'+json.Data.details[i].CODE+'" selected>'+json.Data.details[i].NAME+'</option>');  
                            }else{
                                $('.PURPOSE_LEVEL').append('<option value="'+json.Data.details[i].CODE+'">'+json.Data.details[i].NAME+'</option>');
                            }
                        }
                    }      
                });
            }else{
                $('.PURPOSE_LEVEL').prepend($('<option value="'+$(this).val()+'" selected></option>').html(''));
            }

            $.ajax({
                url: BASE_URL+"onlineapplication/GET_REQUIREMENTS",
                data:  {PURPOSE_CODE: $(this).val()},
                type: 'POST',                
                beforeSend: function() { 
                    $('.DISPLAY_REQUIREMENTS').html(
                    '<div class="row REQUIREMENT_LOADER">'+
                        '<div class="col-md-12 text-center">'+
                            '<div class="preloader pl-size-xl">'+
                                '<div class="spinner-layer pl-light-blue">'+
                                    '<div class="circle-clipper left">'+
                                        '<div class="circle"></div>'+
                                    '</div>'+
                                    '<div class="circle-clipper right">'+
                                        '<div class="circle"></div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<p>Loading requirements, please wait...</p>'+
                        '</div>'+
                    '</div>'
                    );
                },
                complete: function() { 
                    $(".REQUIREMENT_LOADER").remove();
                },
            }).done(function(data){
               json = JSON.parse(data);
               if(json.Code == 0){
                    setTimeout(function(){ 
                        $('.DISPLAY_REQUIREMENTS').html(
                            '<div class="row">'+
                                '<div class="col-md-6">'+
                                    '<div class="card">'+
                                        '<div class="content">'+
                                            '<i class="fa fa-tag text-success" style="font-size: 15pt; width:100%">&nbsp;&nbsp;&nbsp;Requirements</i>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6">'+
                                    '<div class="card">'+
                                        '<div class="content">'+
                                            '<i class="fa fa-dot-circle-o text-info" style="font-size: 15pt; width:100%">&nbsp;&nbsp;&nbsp;Sub Requirements</i>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
                        );
                        for (var i = 0; i < json.Data.details.length; i++) {
                            if(json.Data.details[i].SUBREQ == ""){
                                $('.DISPLAY_REQUIREMENTS').append(
                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="control-label">'+
                                                    '<span class="btn-label" style="margin-right:5px">'+
                                                        '<i class="fa fa-tag text-success"></i>'+
                                                    '</span>'
                                                    +json.Data.details[i].REQUIREMENT_NAME+
                                                '</label>'+
                                                '<input type="file" name="'+json.Data.details[i].REQ+'[]" class="file" accept="application/pdf" multiple>'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon"><i class="fa fa-file-pdf-o"></i></span>'+
                                                    '<input type="text" class="form-control" disabled placeholder="Upload File">'+
                                                    '<span class="input-group-btn">'+
                                                        '<button class="btn btn-fill btn-success btn-wd btn-fill browse" type="button"><i class="fa fa-upload"></i> Browse</button>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div> '+
                                        '</div>'+
                                    '</div>'
                                );
                            }else{
                                $('.DISPLAY_REQUIREMENTS').append(
                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="control-label">'+
                                                    '<span class="btn-label" style="margin-right:5px">'+
                                                        '<i class="fa fa-tag text-success"></i>'+
                                                    '</span>'
                                                    +json.Data.details[i].REQUIREMENT_NAME+
                                                '</label><br>'+
                                                '<label class="control-label">'+
                                                    '<span class="btn-label" style="margin-left:30px; margin-right:5px">'+
                                                        '<i class="fa fa-dot-circle-o text-info"></i>'+
                                                    '</span>'
                                                    +json.Data.details[i].SUBREQUIREMENT_NAME+
                                                '</label>'+
                                                '<input type="file" name="'+json.Data.details[i].SUBREQ+'[]" class="file" accept="application/pdf" multiple>'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon"><i class="fa fa-file-pdf-o"></i></span>'+
                                                    '<input type="text" class="form-control" disabled placeholder="Upload File">'+
                                                    '<span class="input-group-btn">'+
                                                        '<button class="btn btn-fill btn-success btn-wd btn-fill browse" type="button"><i class="fa fa-upload"></i> Browse</button>'+
                                                    '</span>'+
                                                '</div>'+
                                            '</div> '+
                                        '</div>'+
                                    '</div>'
                                );
                            }
                        }
                    }, 100);
                } else {
                    setTimeout(function(){ 
                         $.notify({
                            icon: "pe-7s-bell",
                            message: "<b>"+json.Message+""

                        },{
                            type: 'danger',
                            timer: 3000,
                            delay: 2500,
                            placement: {
                                from: 'top',
                                align: 'center'
                            }
                        }); 
                    }, 100);
                }
            });
        }else{
            setTimeout(function(){ 
                $('.PURPOSE_LEVEL').find('option').remove();
                $('.DISPLAY_REQUIREMENTS').html('');
                
                $.notify({
                    icon: "pe-7s-bell",
                    message: "<b>Please select purpose."

                },{
                    type: 'danger',
                    timer: 1000,
                    delay: 100,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                }); 
            }, 100);
        }
    });

    $(document).on('click', '.RESET_BUTTON', function(){
        $('#APPLICATION_FORM')[0].reset();
        $('#PHOTO_DISPLAY').attr('src', BASE_URL+'assets/img/faces/personal_image.png');
        $('#SIG_DISPLAY').attr('src', BASE_URL+'assets/img/faces/signature_image.png');
        $('.PURPOSE_LEVEL').find('option').remove();
        $('.DISPLAY_REQUIREMENTS').html('');
        $('.main-panel').scrollTop(0);
        $('.NEW_CODE').click();
    });

    $(document).on('click', '.NEW_CODE', function(){
        new_captcharandomdata();
    });

    function new_captcharandomdata(){
        $("#captcha").attr("src",window.location.origin+"/NWRB/assets/captcha/captcharandomdata.php?rnd=" + Math.random());
    }
});